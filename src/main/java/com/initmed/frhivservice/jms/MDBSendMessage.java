/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.jms;

import com.initmed.frhivservice.bdmodel.converter.FRNode;
import com.initmed.frhivservice.bdmodel.facades.FrmessagesyncFacade;
import com.initmed.frhivservice.controller.FRHIClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.EJB;
import jakarta.ejb.MessageDriven;
import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.MessageListener;

/**
 *
 * @author sd199
 */
@MessageDriven(
        mappedName = "jms/RestSendQueue",
        name = "MDBSendMessage")
public class MDBSendMessage implements MessageListener {

    @EJB
    private FRHIClient fRHIClient;

    @Override
    public void onMessage(Message message) {
        try {
            FRNode r = message.getBody(FRNode.class);
            fRHIClient.sendMessage(r);
        } catch (JMSException ex) {
            Logger.getLogger(MDBSendMessage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
