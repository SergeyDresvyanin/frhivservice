/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.jms;


import com.initmed.frhivservice.controller.CallBackConverter;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.EJB;
import jakarta.ejb.MessageDriven;
import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.jms.MessageListener;

/**
 *
 * @author sd199
 */
@MessageDriven(
        mappedName = "jms/RestCallbackQueue",
        name = "MDBResponseSave")
public class MDBResponseSave implements MessageListener {

    @EJB
    private CallBackConverter frmessagesyncFacade;

    @Override
    public void onMessage(Message message) {
        try {
            ResponseTask r = message.getBody(ResponseTask.class);
            frmessagesyncFacade.parseMessage(r.getMoOid(), r.getMsgId(), r.getResponse());
        } catch (JMSException ex) {
            Logger.getLogger(MDBResponseSave.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(MDBResponseSave.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
