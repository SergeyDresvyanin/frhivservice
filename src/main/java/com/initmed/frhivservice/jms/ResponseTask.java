/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.jms;

import java.io.Serializable;

/**
 *
 * @author sd199
 */
public class ResponseTask implements Serializable {

    private String msgId;
    private String moOid;
    private String response;

    public ResponseTask(String msgId, String moOid, String response) {
        this.msgId = msgId;
        this.moOid = moOid;
        this.response = response;
    }

    public String getMoOid() {
        return moOid;
    }

    public void setMoOid(String moOid) {
        this.moOid = moOid;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

}
