//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createChemoStdExamKey complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createChemoStdExamKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chemoKey" type="{}tubChemoKey"/>
 *         &lt;element name="chemoExam" type="{}nrTubChemoStdExam"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createChemoStdExamKey", propOrder = {
    "chemoKey",
    "chemoExam"
})
public class CreateChemoStdExamKey {

    @XmlElement(required = true)
    protected TubChemoKey chemoKey;
    @XmlElement(required = true)
    protected NrTubChemoStdExam chemoExam;

    /**
     * Gets the value of the chemoKey property.
     * 
     * @return
     *     possible object is
     *     {@link TubChemoKey }
     *     
     */
    public TubChemoKey getChemoKey() {
        return chemoKey;
    }

    /**
     * Sets the value of the chemoKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link TubChemoKey }
     *     
     */
    public void setChemoKey(TubChemoKey value) {
        this.chemoKey = value;
    }

    /**
     * Gets the value of the chemoExam property.
     * 
     * @return
     *     possible object is
     *     {@link NrTubChemoStdExam }
     *     
     */
    public NrTubChemoStdExam getChemoExam() {
        return chemoExam;
    }

    /**
     * Sets the value of the chemoExam property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrTubChemoStdExam }
     *     
     */
    public void setChemoExam(NrTubChemoStdExam value) {
        this.chemoExam = value;
    }

}
