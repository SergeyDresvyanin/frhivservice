//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nrVznBalanceSpec complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nrVznBalanceSpec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="mnnId" type="{}refNsiVznMnn"/>
 *         &lt;element name="drugFormId" type="{}refNsiVznDrugForm"/>
 *         &lt;element name="dosageId" type="{}refNsiVznDosage"/>
 *         &lt;element name="expenseMonth" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="measurebalanceCount" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="inventory" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="balanceSerials">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="balanceSerial" type="{}nrVznBalanceSerial" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nrVznBalanceSpec", propOrder = {

})
public class NrVznBalanceSpec {

    @XmlElement(required = true)
    protected RefNsiVznMnn mnnId;
    @XmlElement(required = true)
    protected RefNsiVznDrugForm drugFormId;
    @XmlElement(required = true)
    protected RefNsiVznDosage dosageId;
    protected int expenseMonth;
    protected Double measurebalanceCount;
    protected Integer inventory;
    @XmlElement(required = true)
    protected NrVznBalanceSpec.BalanceSerials balanceSerials;

    /**
     * Gets the value of the mnnId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiVznMnn }
     *     
     */
    public RefNsiVznMnn getMnnId() {
        return mnnId;
    }

    /**
     * Sets the value of the mnnId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiVznMnn }
     *     
     */
    public void setMnnId(RefNsiVznMnn value) {
        this.mnnId = value;
    }

    /**
     * Gets the value of the drugFormId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiVznDrugForm }
     *     
     */
    public RefNsiVznDrugForm getDrugFormId() {
        return drugFormId;
    }

    /**
     * Sets the value of the drugFormId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiVznDrugForm }
     *     
     */
    public void setDrugFormId(RefNsiVznDrugForm value) {
        this.drugFormId = value;
    }

    /**
     * Gets the value of the dosageId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiVznDosage }
     *     
     */
    public RefNsiVznDosage getDosageId() {
        return dosageId;
    }

    /**
     * Sets the value of the dosageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiVznDosage }
     *     
     */
    public void setDosageId(RefNsiVznDosage value) {
        this.dosageId = value;
    }

    /**
     * Gets the value of the expenseMonth property.
     * 
     */
    public int getExpenseMonth() {
        return expenseMonth;
    }

    /**
     * Sets the value of the expenseMonth property.
     * 
     */
    public void setExpenseMonth(int value) {
        this.expenseMonth = value;
    }

    /**
     * Gets the value of the measurebalanceCount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMeasurebalanceCount() {
        return measurebalanceCount;
    }

    /**
     * Sets the value of the measurebalanceCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMeasurebalanceCount(Double value) {
        this.measurebalanceCount = value;
    }

    /**
     * Gets the value of the inventory property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInventory() {
        return inventory;
    }

    /**
     * Sets the value of the inventory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInventory(Integer value) {
        this.inventory = value;
    }

    /**
     * Gets the value of the balanceSerials property.
     * 
     * @return
     *     possible object is
     *     {@link NrVznBalanceSpec.BalanceSerials }
     *     
     */
    public NrVznBalanceSpec.BalanceSerials getBalanceSerials() {
        return balanceSerials;
    }

    /**
     * Sets the value of the balanceSerials property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrVznBalanceSpec.BalanceSerials }
     *     
     */
    public void setBalanceSerials(NrVznBalanceSpec.BalanceSerials value) {
        this.balanceSerials = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="balanceSerial" type="{}nrVznBalanceSerial" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "balanceSerial"
    })
    public static class BalanceSerials {

        @XmlElement(required = true)
        protected List<NrVznBalanceSerial> balanceSerial;

        /**
         * Gets the value of the balanceSerial property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the balanceSerial property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBalanceSerial().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link NrVznBalanceSerial }
         * 
         * 
         */
        public List<NrVznBalanceSerial> getBalanceSerial() {
            if (balanceSerial == null) {
                balanceSerial = new ArrayList<NrVznBalanceSerial>();
            }
            return this.balanceSerial;
        }

    }

}
