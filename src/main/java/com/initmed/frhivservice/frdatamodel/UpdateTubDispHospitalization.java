//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateTubDispHospitalization complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateTubDispHospitalization">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}tubDispHospKey"/>
 *         &lt;element name="hospitalization" type="{}nrTubDispHospitalization"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateTubDispHospitalization", propOrder = {
    "tubDispHospKey",
    "hospitalization"
})
public class UpdateTubDispHospitalization {

    @XmlElement(required = true)
    protected TubDispHospKey tubDispHospKey;
    @XmlElement(required = true)
    protected NrTubDispHospitalization hospitalization;

    /**
     * Gets the value of the tubDispHospKey property.
     * 
     * @return
     *     possible object is
     *     {@link TubDispHospKey }
     *     
     */
    public TubDispHospKey getTubDispHospKey() {
        return tubDispHospKey;
    }

    /**
     * Sets the value of the tubDispHospKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link TubDispHospKey }
     *     
     */
    public void setTubDispHospKey(TubDispHospKey value) {
        this.tubDispHospKey = value;
    }

    /**
     * Gets the value of the hospitalization property.
     * 
     * @return
     *     possible object is
     *     {@link NrTubDispHospitalization }
     *     
     */
    public NrTubDispHospitalization getHospitalization() {
        return hospitalization;
    }

    /**
     * Sets the value of the hospitalization property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrTubDispHospitalization }
     *     
     */
    public void setHospitalization(NrTubDispHospitalization value) {
        this.hospitalization = value;
    }

}
