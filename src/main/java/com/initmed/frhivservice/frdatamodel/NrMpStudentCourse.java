//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for nrMpStudentCourse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nrMpStudentCourse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="courseId" type="{}refNsiMpStudentCourse"/>
 *         &lt;element name="departId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="academicDegreeId" type="{}refNsiMpAcademicDegree" minOccurs="0"/>
 *         &lt;element name="scienceBranchId" type="{}refNsiMpScienceBranch" minOccurs="0"/>
 *         &lt;element name="diplomaSpecialityId" type="{}refNsiMpDiplomaSpeciality" minOccurs="0"/>
 *         &lt;element name="scientificSpeciality" type="{}refNsiMpScientificSpeciality" minOccurs="0"/>
 *         &lt;element name="mpSpeciality" type="{}refNsiMpSpec" minOccurs="0"/>
 *         &lt;element name="educationResult" type="{}refNsiMpStudentEducationResult" minOccurs="0"/>
 *         &lt;element name="protocolNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="protocolDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="qualificationId" type="{}refNsiMpQualification" minOccurs="0"/>
 *         &lt;element name="expelReason" type="{}refNsiMpStudentExpelReason" minOccurs="0"/>
 *         &lt;element name="expelDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="institutionId" type="{}nrPmuEoAddition" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nrMpStudentCourse", propOrder = {

})
public class NrMpStudentCourse {

    @XmlElement(required = true)
    protected RefNsiMpStudentCourse courseId;
    protected long departId;
    protected RefNsiMpAcademicDegree academicDegreeId;
    protected RefNsiMpScienceBranch scienceBranchId;
    protected RefNsiMpDiplomaSpeciality diplomaSpecialityId;
    protected RefNsiMpScientificSpeciality scientificSpeciality;
    protected RefNsiMpSpec mpSpeciality;
    protected RefNsiMpStudentEducationResult educationResult;
    protected String protocolNumber;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar protocolDate;
    protected RefNsiMpQualification qualificationId;
    protected RefNsiMpStudentExpelReason expelReason;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expelDate;
    protected NrPmuEoAddition institutionId;

    /**
     * Gets the value of the courseId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiMpStudentCourse }
     *     
     */
    public RefNsiMpStudentCourse getCourseId() {
        return courseId;
    }

    /**
     * Sets the value of the courseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiMpStudentCourse }
     *     
     */
    public void setCourseId(RefNsiMpStudentCourse value) {
        this.courseId = value;
    }

    /**
     * Gets the value of the departId property.
     * 
     */
    public long getDepartId() {
        return departId;
    }

    /**
     * Sets the value of the departId property.
     * 
     */
    public void setDepartId(long value) {
        this.departId = value;
    }

    /**
     * Gets the value of the academicDegreeId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiMpAcademicDegree }
     *     
     */
    public RefNsiMpAcademicDegree getAcademicDegreeId() {
        return academicDegreeId;
    }

    /**
     * Sets the value of the academicDegreeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiMpAcademicDegree }
     *     
     */
    public void setAcademicDegreeId(RefNsiMpAcademicDegree value) {
        this.academicDegreeId = value;
    }

    /**
     * Gets the value of the scienceBranchId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiMpScienceBranch }
     *     
     */
    public RefNsiMpScienceBranch getScienceBranchId() {
        return scienceBranchId;
    }

    /**
     * Sets the value of the scienceBranchId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiMpScienceBranch }
     *     
     */
    public void setScienceBranchId(RefNsiMpScienceBranch value) {
        this.scienceBranchId = value;
    }

    /**
     * Gets the value of the diplomaSpecialityId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiMpDiplomaSpeciality }
     *     
     */
    public RefNsiMpDiplomaSpeciality getDiplomaSpecialityId() {
        return diplomaSpecialityId;
    }

    /**
     * Sets the value of the diplomaSpecialityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiMpDiplomaSpeciality }
     *     
     */
    public void setDiplomaSpecialityId(RefNsiMpDiplomaSpeciality value) {
        this.diplomaSpecialityId = value;
    }

    /**
     * Gets the value of the scientificSpeciality property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiMpScientificSpeciality }
     *     
     */
    public RefNsiMpScientificSpeciality getScientificSpeciality() {
        return scientificSpeciality;
    }

    /**
     * Sets the value of the scientificSpeciality property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiMpScientificSpeciality }
     *     
     */
    public void setScientificSpeciality(RefNsiMpScientificSpeciality value) {
        this.scientificSpeciality = value;
    }

    /**
     * Gets the value of the mpSpeciality property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiMpSpec }
     *     
     */
    public RefNsiMpSpec getMpSpeciality() {
        return mpSpeciality;
    }

    /**
     * Sets the value of the mpSpeciality property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiMpSpec }
     *     
     */
    public void setMpSpeciality(RefNsiMpSpec value) {
        this.mpSpeciality = value;
    }

    /**
     * Gets the value of the educationResult property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiMpStudentEducationResult }
     *     
     */
    public RefNsiMpStudentEducationResult getEducationResult() {
        return educationResult;
    }

    /**
     * Sets the value of the educationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiMpStudentEducationResult }
     *     
     */
    public void setEducationResult(RefNsiMpStudentEducationResult value) {
        this.educationResult = value;
    }

    /**
     * Gets the value of the protocolNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocolNumber() {
        return protocolNumber;
    }

    /**
     * Sets the value of the protocolNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocolNumber(String value) {
        this.protocolNumber = value;
    }

    /**
     * Gets the value of the protocolDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProtocolDate() {
        return protocolDate;
    }

    /**
     * Sets the value of the protocolDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProtocolDate(XMLGregorianCalendar value) {
        this.protocolDate = value;
    }

    /**
     * Gets the value of the qualificationId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiMpQualification }
     *     
     */
    public RefNsiMpQualification getQualificationId() {
        return qualificationId;
    }

    /**
     * Sets the value of the qualificationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiMpQualification }
     *     
     */
    public void setQualificationId(RefNsiMpQualification value) {
        this.qualificationId = value;
    }

    /**
     * Gets the value of the expelReason property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiMpStudentExpelReason }
     *     
     */
    public RefNsiMpStudentExpelReason getExpelReason() {
        return expelReason;
    }

    /**
     * Sets the value of the expelReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiMpStudentExpelReason }
     *     
     */
    public void setExpelReason(RefNsiMpStudentExpelReason value) {
        this.expelReason = value;
    }

    /**
     * Gets the value of the expelDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpelDate() {
        return expelDate;
    }

    /**
     * Sets the value of the expelDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpelDate(XMLGregorianCalendar value) {
        this.expelDate = value;
    }

    /**
     * Gets the value of the institutionId property.
     * 
     * @return
     *     possible object is
     *     {@link NrPmuEoAddition }
     *     
     */
    public NrPmuEoAddition getInstitutionId() {
        return institutionId;
    }

    /**
     * Sets the value of the institutionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrPmuEoAddition }
     *     
     */
    public void setInstitutionId(NrPmuEoAddition value) {
        this.institutionId = value;
    }

}
