//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for nrHivRegistryAddition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nrHivRegistryAddition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="registryNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="epidCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="policSerial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="policNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="imcCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="newborn" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="approvedDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="tub" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="tubDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="tubMkb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="autopsy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="tubDetectPlaceId" type="{}refNsiDetectPlace" minOccurs="0"/>
 *         &lt;element name="tubDetectMethodId" type="{}refNsiHivTubDetectMethod" minOccurs="0"/>
 *         &lt;element name="tubBactDetectMethodId" type="{}refNsiTubDetectMethod" minOccurs="0"/>
 *         &lt;element name="tubMultidrugResistenceId" type="{}refNsiTubMultidrugResistence" minOccurs="0"/>
 *         &lt;element name="deathReasonId" type="{}refNsiHivDeathReason" minOccurs="0"/>
 *         &lt;element name="excludeReasonId" type="{}refNsiHivExcludeReason" minOccurs="0"/>
 *         &lt;element name="tubBactExcretionMethodId" type="{}refNsiHivTubBactExcretionMethod" minOccurs="0"/>
 *         &lt;element name="deptId" type="{}refTubDept" minOccurs="0"/>
 *         &lt;element name="tubContactTypeId" type="{}refNsiHivTubContactType" minOccurs="0"/>
 *         &lt;element name="tubDispGroupId" type="{}refNsiTubDispGroup" minOccurs="0"/>
 *         &lt;element name="policTypeId" type="{}refNsiPolicType" minOccurs="0"/>
 *         &lt;element name="tubDetectCurcId" type="{}refNsiHivTubDetectCurc" minOccurs="0"/>
 *         &lt;element name="beginningMoId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deseases">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="desease" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="revealedPathologies" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="revealedPathology" type="{}refNsiHivMkb" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="contingents">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="contingent" type="{}refNsiHivContingent" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="exams" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="exam" type="{}nrHivRegistryExam" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="newbornData" type="{}nrHivRegistryAdditionNewborn" minOccurs="0"/>
 *         &lt;element name="disabilityGroupId" type="{}refNsiDisabilityGroup" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nrHivRegistryAddition", propOrder = {

})
public class NrHivRegistryAddition {

    protected String registryNumber;
    protected String epidCode;
    protected String policSerial;
    protected String policNumber;
    protected String imcCode;
    protected Boolean newborn;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar approvedDate;
    protected Boolean tub;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar tubDate;
    protected String tubMkb;
    protected Boolean autopsy;
    protected RefNsiDetectPlace tubDetectPlaceId;
    protected RefNsiHivTubDetectMethod tubDetectMethodId;
    protected RefNsiTubDetectMethod tubBactDetectMethodId;
    protected RefNsiTubMultidrugResistence tubMultidrugResistenceId;
    protected RefNsiHivDeathReason deathReasonId;
    protected RefNsiHivExcludeReason excludeReasonId;
    protected RefNsiHivTubBactExcretionMethod tubBactExcretionMethodId;
    protected RefTubDept deptId;
    protected RefNsiHivTubContactType tubContactTypeId;
    protected RefNsiTubDispGroup tubDispGroupId;
    protected RefNsiPolicType policTypeId;
    protected RefNsiHivTubDetectCurc tubDetectCurcId;
    protected String beginningMoId;
    @XmlElement(required = true)
    protected NrHivRegistryAddition.Deseases deseases;
    protected NrHivRegistryAddition.RevealedPathologies revealedPathologies;
    @XmlElement(required = true)
    protected NrHivRegistryAddition.Contingents contingents;
    protected NrHivRegistryAddition.Exams exams;
    protected NrHivRegistryAdditionNewborn newbornData;
    protected RefNsiDisabilityGroup disabilityGroupId;

    /**
     * Gets the value of the registryNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistryNumber() {
        return registryNumber;
    }

    /**
     * Sets the value of the registryNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistryNumber(String value) {
        this.registryNumber = value;
    }

    /**
     * Gets the value of the epidCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEpidCode() {
        return epidCode;
    }

    /**
     * Sets the value of the epidCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEpidCode(String value) {
        this.epidCode = value;
    }

    /**
     * Gets the value of the policSerial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicSerial() {
        return policSerial;
    }

    /**
     * Sets the value of the policSerial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicSerial(String value) {
        this.policSerial = value;
    }

    /**
     * Gets the value of the policNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicNumber() {
        return policNumber;
    }

    /**
     * Sets the value of the policNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicNumber(String value) {
        this.policNumber = value;
    }

    /**
     * Gets the value of the imcCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImcCode() {
        return imcCode;
    }

    /**
     * Sets the value of the imcCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImcCode(String value) {
        this.imcCode = value;
    }

    /**
     * Gets the value of the newborn property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNewborn() {
        return newborn;
    }

    /**
     * Sets the value of the newborn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNewborn(Boolean value) {
        this.newborn = value;
    }

    /**
     * Gets the value of the approvedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getApprovedDate() {
        return approvedDate;
    }

    /**
     * Sets the value of the approvedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setApprovedDate(XMLGregorianCalendar value) {
        this.approvedDate = value;
    }

    /**
     * Gets the value of the tub property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTub() {
        return tub;
    }

    /**
     * Sets the value of the tub property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTub(Boolean value) {
        this.tub = value;
    }

    /**
     * Gets the value of the tubDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTubDate() {
        return tubDate;
    }

    /**
     * Sets the value of the tubDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTubDate(XMLGregorianCalendar value) {
        this.tubDate = value;
    }

    /**
     * Gets the value of the tubMkb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTubMkb() {
        return tubMkb;
    }

    /**
     * Sets the value of the tubMkb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTubMkb(String value) {
        this.tubMkb = value;
    }

    /**
     * Gets the value of the autopsy property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutopsy() {
        return autopsy;
    }

    /**
     * Sets the value of the autopsy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutopsy(Boolean value) {
        this.autopsy = value;
    }

    /**
     * Gets the value of the tubDetectPlaceId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiDetectPlace }
     *     
     */
    public RefNsiDetectPlace getTubDetectPlaceId() {
        return tubDetectPlaceId;
    }

    /**
     * Sets the value of the tubDetectPlaceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiDetectPlace }
     *     
     */
    public void setTubDetectPlaceId(RefNsiDetectPlace value) {
        this.tubDetectPlaceId = value;
    }

    /**
     * Gets the value of the tubDetectMethodId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiHivTubDetectMethod }
     *     
     */
    public RefNsiHivTubDetectMethod getTubDetectMethodId() {
        return tubDetectMethodId;
    }

    /**
     * Sets the value of the tubDetectMethodId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiHivTubDetectMethod }
     *     
     */
    public void setTubDetectMethodId(RefNsiHivTubDetectMethod value) {
        this.tubDetectMethodId = value;
    }

    /**
     * Gets the value of the tubBactDetectMethodId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiTubDetectMethod }
     *     
     */
    public RefNsiTubDetectMethod getTubBactDetectMethodId() {
        return tubBactDetectMethodId;
    }

    /**
     * Sets the value of the tubBactDetectMethodId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiTubDetectMethod }
     *     
     */
    public void setTubBactDetectMethodId(RefNsiTubDetectMethod value) {
        this.tubBactDetectMethodId = value;
    }

    /**
     * Gets the value of the tubMultidrugResistenceId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiTubMultidrugResistence }
     *     
     */
    public RefNsiTubMultidrugResistence getTubMultidrugResistenceId() {
        return tubMultidrugResistenceId;
    }

    /**
     * Sets the value of the tubMultidrugResistenceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiTubMultidrugResistence }
     *     
     */
    public void setTubMultidrugResistenceId(RefNsiTubMultidrugResistence value) {
        this.tubMultidrugResistenceId = value;
    }

    /**
     * Gets the value of the deathReasonId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiHivDeathReason }
     *     
     */
    public RefNsiHivDeathReason getDeathReasonId() {
        return deathReasonId;
    }

    /**
     * Sets the value of the deathReasonId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiHivDeathReason }
     *     
     */
    public void setDeathReasonId(RefNsiHivDeathReason value) {
        this.deathReasonId = value;
    }

    /**
     * Gets the value of the excludeReasonId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiHivExcludeReason }
     *     
     */
    public RefNsiHivExcludeReason getExcludeReasonId() {
        return excludeReasonId;
    }

    /**
     * Sets the value of the excludeReasonId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiHivExcludeReason }
     *     
     */
    public void setExcludeReasonId(RefNsiHivExcludeReason value) {
        this.excludeReasonId = value;
    }

    /**
     * Gets the value of the tubBactExcretionMethodId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiHivTubBactExcretionMethod }
     *     
     */
    public RefNsiHivTubBactExcretionMethod getTubBactExcretionMethodId() {
        return tubBactExcretionMethodId;
    }

    /**
     * Sets the value of the tubBactExcretionMethodId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiHivTubBactExcretionMethod }
     *     
     */
    public void setTubBactExcretionMethodId(RefNsiHivTubBactExcretionMethod value) {
        this.tubBactExcretionMethodId = value;
    }

    /**
     * Gets the value of the deptId property.
     * 
     * @return
     *     possible object is
     *     {@link RefTubDept }
     *     
     */
    public RefTubDept getDeptId() {
        return deptId;
    }

    /**
     * Sets the value of the deptId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefTubDept }
     *     
     */
    public void setDeptId(RefTubDept value) {
        this.deptId = value;
    }

    /**
     * Gets the value of the tubContactTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiHivTubContactType }
     *     
     */
    public RefNsiHivTubContactType getTubContactTypeId() {
        return tubContactTypeId;
    }

    /**
     * Sets the value of the tubContactTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiHivTubContactType }
     *     
     */
    public void setTubContactTypeId(RefNsiHivTubContactType value) {
        this.tubContactTypeId = value;
    }

    /**
     * Gets the value of the tubDispGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiTubDispGroup }
     *     
     */
    public RefNsiTubDispGroup getTubDispGroupId() {
        return tubDispGroupId;
    }

    /**
     * Sets the value of the tubDispGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiTubDispGroup }
     *     
     */
    public void setTubDispGroupId(RefNsiTubDispGroup value) {
        this.tubDispGroupId = value;
    }

    /**
     * Gets the value of the policTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiPolicType }
     *     
     */
    public RefNsiPolicType getPolicTypeId() {
        return policTypeId;
    }

    /**
     * Sets the value of the policTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiPolicType }
     *     
     */
    public void setPolicTypeId(RefNsiPolicType value) {
        this.policTypeId = value;
    }

    /**
     * Gets the value of the tubDetectCurcId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiHivTubDetectCurc }
     *     
     */
    public RefNsiHivTubDetectCurc getTubDetectCurcId() {
        return tubDetectCurcId;
    }

    /**
     * Sets the value of the tubDetectCurcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiHivTubDetectCurc }
     *     
     */
    public void setTubDetectCurcId(RefNsiHivTubDetectCurc value) {
        this.tubDetectCurcId = value;
    }

    /**
     * Gets the value of the beginningMoId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeginningMoId() {
        return beginningMoId;
    }

    /**
     * Sets the value of the beginningMoId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeginningMoId(String value) {
        this.beginningMoId = value;
    }

    /**
     * Gets the value of the deseases property.
     * 
     * @return
     *     possible object is
     *     {@link NrHivRegistryAddition.Deseases }
     *     
     */
    public NrHivRegistryAddition.Deseases getDeseases() {
        return deseases;
    }

    /**
     * Sets the value of the deseases property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrHivRegistryAddition.Deseases }
     *     
     */
    public void setDeseases(NrHivRegistryAddition.Deseases value) {
        this.deseases = value;
    }

    /**
     * Gets the value of the revealedPathologies property.
     * 
     * @return
     *     possible object is
     *     {@link NrHivRegistryAddition.RevealedPathologies }
     *     
     */
    public NrHivRegistryAddition.RevealedPathologies getRevealedPathologies() {
        return revealedPathologies;
    }

    /**
     * Sets the value of the revealedPathologies property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrHivRegistryAddition.RevealedPathologies }
     *     
     */
    public void setRevealedPathologies(NrHivRegistryAddition.RevealedPathologies value) {
        this.revealedPathologies = value;
    }

    /**
     * Gets the value of the contingents property.
     * 
     * @return
     *     possible object is
     *     {@link NrHivRegistryAddition.Contingents }
     *     
     */
    public NrHivRegistryAddition.Contingents getContingents() {
        return contingents;
    }

    /**
     * Sets the value of the contingents property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrHivRegistryAddition.Contingents }
     *     
     */
    public void setContingents(NrHivRegistryAddition.Contingents value) {
        this.contingents = value;
    }

    /**
     * Gets the value of the exams property.
     * 
     * @return
     *     possible object is
     *     {@link NrHivRegistryAddition.Exams }
     *     
     */
    public NrHivRegistryAddition.Exams getExams() {
        return exams;
    }

    /**
     * Sets the value of the exams property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrHivRegistryAddition.Exams }
     *     
     */
    public void setExams(NrHivRegistryAddition.Exams value) {
        this.exams = value;
    }

    /**
     * Gets the value of the newbornData property.
     * 
     * @return
     *     possible object is
     *     {@link NrHivRegistryAdditionNewborn }
     *     
     */
    public NrHivRegistryAdditionNewborn getNewbornData() {
        return newbornData;
    }

    /**
     * Sets the value of the newbornData property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrHivRegistryAdditionNewborn }
     *     
     */
    public void setNewbornData(NrHivRegistryAdditionNewborn value) {
        this.newbornData = value;
    }

    /**
     * Gets the value of the disabilityGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiDisabilityGroup }
     *     
     */
    public RefNsiDisabilityGroup getDisabilityGroupId() {
        return disabilityGroupId;
    }

    /**
     * Sets the value of the disabilityGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiDisabilityGroup }
     *     
     */
    public void setDisabilityGroupId(RefNsiDisabilityGroup value) {
        this.disabilityGroupId = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="contingent" type="{}refNsiHivContingent" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contingent"
    })
    public static class Contingents {

        @XmlElement(required = true)
        protected List<RefNsiHivContingent> contingent;

        /**
         * Gets the value of the contingent property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the contingent property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContingent().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RefNsiHivContingent }
         * 
         * 
         */
        public List<RefNsiHivContingent> getContingent() {
            if (contingent == null) {
                contingent = new ArrayList<RefNsiHivContingent>();
            }
            return this.contingent;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="desease" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "desease"
    })
    public static class Deseases {

        @XmlElement(required = true)
        protected List<String> desease;

        /**
         * Gets the value of the desease property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the desease property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDesease().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getDesease() {
            if (desease == null) {
                desease = new ArrayList<String>();
            }
            return this.desease;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="exam" type="{}nrHivRegistryExam" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "exam"
    })
    public static class Exams {

        protected List<NrHivRegistryExam> exam;

        /**
         * Gets the value of the exam property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the exam property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getExam().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link NrHivRegistryExam }
         * 
         * 
         */
        public List<NrHivRegistryExam> getExam() {
            if (exam == null) {
                exam = new ArrayList<NrHivRegistryExam>();
            }
            return this.exam;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="revealedPathology" type="{}refNsiHivMkb" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "revealedPathology"
    })
    public static class RevealedPathologies {

        protected List<RefNsiHivMkb> revealedPathology;

        /**
         * Gets the value of the revealedPathology property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the revealedPathology property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRevealedPathology().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RefNsiHivMkb }
         * 
         * 
         */
        public List<RefNsiHivMkb> getRevealedPathology() {
            if (revealedPathology == null) {
                revealedPathology = new ArrayList<RefNsiHivMkb>();
            }
            return this.revealedPathology;
        }

    }

}
