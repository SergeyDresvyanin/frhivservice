//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateHivAddress complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateHivAddress">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="key" type="{}hivAddressKey"/>
 *         &lt;element ref="{}hivAddress"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateHivAddress", propOrder = {
    "key",
    "hivAddress"
})
public class UpdateHivAddress {

    @XmlElement(required = true)
    protected HivAddressKey key;
    @XmlElement(required = true)
    protected NrHivPatientAddress hivAddress;

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link HivAddressKey }
     *     
     */
    public HivAddressKey getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link HivAddressKey }
     *     
     */
    public void setKey(HivAddressKey value) {
        this.key = value;
    }

    /**
     * Gets the value of the hivAddress property.
     * 
     * @return
     *     possible object is
     *     {@link NrHivPatientAddress }
     *     
     */
    public NrHivPatientAddress getHivAddress() {
        return hivAddress;
    }

    /**
     * Sets the value of the hivAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrHivPatientAddress }
     *     
     */
    public void setHivAddress(NrHivPatientAddress value) {
        this.hivAddress = value;
    }

}
