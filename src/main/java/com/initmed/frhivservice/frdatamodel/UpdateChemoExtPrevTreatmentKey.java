//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateChemoExtPrevTreatmentKey complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateChemoExtPrevTreatmentKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="prevTreatmentKey" type="{}chemoExtPrevTreatmentKey"/>
 *         &lt;element name="prevTreatment" type="{}nrTubChemoExtPrevTreatment"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateChemoExtPrevTreatmentKey", propOrder = {
    "prevTreatmentKey",
    "prevTreatment"
})
public class UpdateChemoExtPrevTreatmentKey {

    @XmlElement(required = true)
    protected ChemoExtPrevTreatmentKey prevTreatmentKey;
    @XmlElement(required = true)
    protected NrTubChemoExtPrevTreatment prevTreatment;

    /**
     * Gets the value of the prevTreatmentKey property.
     * 
     * @return
     *     possible object is
     *     {@link ChemoExtPrevTreatmentKey }
     *     
     */
    public ChemoExtPrevTreatmentKey getPrevTreatmentKey() {
        return prevTreatmentKey;
    }

    /**
     * Sets the value of the prevTreatmentKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChemoExtPrevTreatmentKey }
     *     
     */
    public void setPrevTreatmentKey(ChemoExtPrevTreatmentKey value) {
        this.prevTreatmentKey = value;
    }

    /**
     * Gets the value of the prevTreatment property.
     * 
     * @return
     *     possible object is
     *     {@link NrTubChemoExtPrevTreatment }
     *     
     */
    public NrTubChemoExtPrevTreatment getPrevTreatment() {
        return prevTreatment;
    }

    /**
     * Sets the value of the prevTreatment property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrTubChemoExtPrevTreatment }
     *     
     */
    public void setPrevTreatment(NrTubChemoExtPrevTreatment value) {
        this.prevTreatment = value;
    }

}
