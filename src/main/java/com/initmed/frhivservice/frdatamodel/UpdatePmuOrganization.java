//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updatePmuOrganization complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updatePmuOrganization">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{}mo"/>
 *         &lt;element ref="{}ho"/>
 *         &lt;element ref="{}po"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updatePmuOrganization", propOrder = {
    "mo",
    "ho",
    "po"
})
public class UpdatePmuOrganization {

    protected NrPmuMo mo;
    protected NrPmuHo ho;
    protected NrPmuPo po;

    /**
     * Gets the value of the mo property.
     * 
     * @return
     *     possible object is
     *     {@link NrPmuMo }
     *     
     */
    public NrPmuMo getMo() {
        return mo;
    }

    /**
     * Sets the value of the mo property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrPmuMo }
     *     
     */
    public void setMo(NrPmuMo value) {
        this.mo = value;
    }

    /**
     * Gets the value of the ho property.
     * 
     * @return
     *     possible object is
     *     {@link NrPmuHo }
     *     
     */
    public NrPmuHo getHo() {
        return ho;
    }

    /**
     * Sets the value of the ho property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrPmuHo }
     *     
     */
    public void setHo(NrPmuHo value) {
        this.ho = value;
    }

    /**
     * Gets the value of the po property.
     * 
     * @return
     *     possible object is
     *     {@link NrPmuPo }
     *     
     */
    public NrPmuPo getPo() {
        return po;
    }

    /**
     * Sets the value of the po property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrPmuPo }
     *     
     */
    public void setPo(NrPmuPo value) {
        this.po = value;
    }

}
