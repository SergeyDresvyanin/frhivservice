//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createPersonEducationCommon complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createPersonEducationCommon">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="key" type="{}personKey"/>
 *         &lt;element ref="{}educationCommon"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createPersonEducationCommon", propOrder = {

})
@XmlSeeAlso({
    UpdatePersonEducationCommon.class
})
public class CreatePersonEducationCommon {

    @XmlElement(required = true)
    protected PersonKey key;
    @XmlElement(required = true)
    protected NrMpPersonEducationCommon educationCommon;

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link PersonKey }
     *     
     */
    public PersonKey getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonKey }
     *     
     */
    public void setKey(PersonKey value) {
        this.key = value;
    }

    /**
     * Gets the value of the educationCommon property.
     * 
     * @return
     *     possible object is
     *     {@link NrMpPersonEducationCommon }
     *     
     */
    public NrMpPersonEducationCommon getEducationCommon() {
        return educationCommon;
    }

    /**
     * Sets the value of the educationCommon property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrMpPersonEducationCommon }
     *     
     */
    public void setEducationCommon(NrMpPersonEducationCommon value) {
        this.educationCommon = value;
    }

}
