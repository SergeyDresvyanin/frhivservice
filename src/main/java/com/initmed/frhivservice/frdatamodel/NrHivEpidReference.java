//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for nrHivEpidReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nrHivEpidReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="moId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="personId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pretestDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="posttestDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="infectionPathId" type="{}refNsiHivInfectionPath"/>
 *         &lt;element name="detectCurcId" type="{}refNsiHivDetectCurc" minOccurs="0"/>
 *         &lt;element name="infectionPeriodFrom" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="infectionPeriodTo" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="notifyDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="conclustionDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="contacts" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="contact" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nrHivEpidReference", propOrder = {

})
public class NrHivEpidReference {

    @XmlElement(required = true)
    protected String moId;
    @XmlElement(required = true)
    protected String personId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar pretestDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar posttestDate;
    @XmlElement(required = true)
    protected RefNsiHivInfectionPath infectionPathId;
    protected RefNsiHivDetectCurc detectCurcId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar infectionPeriodFrom;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar infectionPeriodTo;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar notifyDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar conclustionDate;
    protected NrHivEpidReference.Contacts contacts;

    /**
     * Gets the value of the moId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoId() {
        return moId;
    }

    /**
     * Sets the value of the moId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoId(String value) {
        this.moId = value;
    }

    /**
     * Gets the value of the personId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonId() {
        return personId;
    }

    /**
     * Sets the value of the personId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonId(String value) {
        this.personId = value;
    }

    /**
     * Gets the value of the pretestDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPretestDate() {
        return pretestDate;
    }

    /**
     * Sets the value of the pretestDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPretestDate(XMLGregorianCalendar value) {
        this.pretestDate = value;
    }

    /**
     * Gets the value of the posttestDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPosttestDate() {
        return posttestDate;
    }

    /**
     * Sets the value of the posttestDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPosttestDate(XMLGregorianCalendar value) {
        this.posttestDate = value;
    }

    /**
     * Gets the value of the infectionPathId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiHivInfectionPath }
     *     
     */
    public RefNsiHivInfectionPath getInfectionPathId() {
        return infectionPathId;
    }

    /**
     * Sets the value of the infectionPathId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiHivInfectionPath }
     *     
     */
    public void setInfectionPathId(RefNsiHivInfectionPath value) {
        this.infectionPathId = value;
    }

    /**
     * Gets the value of the detectCurcId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiHivDetectCurc }
     *     
     */
    public RefNsiHivDetectCurc getDetectCurcId() {
        return detectCurcId;
    }

    /**
     * Sets the value of the detectCurcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiHivDetectCurc }
     *     
     */
    public void setDetectCurcId(RefNsiHivDetectCurc value) {
        this.detectCurcId = value;
    }

    /**
     * Gets the value of the infectionPeriodFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInfectionPeriodFrom() {
        return infectionPeriodFrom;
    }

    /**
     * Sets the value of the infectionPeriodFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInfectionPeriodFrom(XMLGregorianCalendar value) {
        this.infectionPeriodFrom = value;
    }

    /**
     * Gets the value of the infectionPeriodTo property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInfectionPeriodTo() {
        return infectionPeriodTo;
    }

    /**
     * Sets the value of the infectionPeriodTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInfectionPeriodTo(XMLGregorianCalendar value) {
        this.infectionPeriodTo = value;
    }

    /**
     * Gets the value of the notifyDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNotifyDate() {
        return notifyDate;
    }

    /**
     * Sets the value of the notifyDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNotifyDate(XMLGregorianCalendar value) {
        this.notifyDate = value;
    }

    /**
     * Gets the value of the conclustionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getConclustionDate() {
        return conclustionDate;
    }

    /**
     * Sets the value of the conclustionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setConclustionDate(XMLGregorianCalendar value) {
        this.conclustionDate = value;
    }

    /**
     * Gets the value of the contacts property.
     * 
     * @return
     *     possible object is
     *     {@link NrHivEpidReference.Contacts }
     *     
     */
    public NrHivEpidReference.Contacts getContacts() {
        return contacts;
    }

    /**
     * Sets the value of the contacts property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrHivEpidReference.Contacts }
     *     
     */
    public void setContacts(NrHivEpidReference.Contacts value) {
        this.contacts = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="contact" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contact"
    })
    public static class Contacts {

        protected List<String> contact;

        /**
         * Gets the value of the contact property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the contact property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContact().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getContact() {
            if (contact == null) {
                contact = new ArrayList<String>();
            }
            return this.contact;
        }

    }

}
