//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updatePersonCard complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updatePersonCard">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element ref="{}cardKey"/>
 *         &lt;element ref="{}card"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updatePersonCard", propOrder = {

})
public class UpdatePersonCard {

    @XmlElement(required = true)
    protected PersonCardKey cardKey;
    @XmlElement(required = true)
    protected NrMpPersonCard card;

    /**
     * Gets the value of the cardKey property.
     * 
     * @return
     *     possible object is
     *     {@link PersonCardKey }
     *     
     */
    public PersonCardKey getCardKey() {
        return cardKey;
    }

    /**
     * Sets the value of the cardKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonCardKey }
     *     
     */
    public void setCardKey(PersonCardKey value) {
        this.cardKey = value;
    }

    /**
     * Gets the value of the card property.
     * 
     * @return
     *     possible object is
     *     {@link NrMpPersonCard }
     *     
     */
    public NrMpPersonCard getCard() {
        return card;
    }

    /**
     * Sets the value of the card property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrMpPersonCard }
     *     
     */
    public void setCard(NrMpPersonCard value) {
        this.card = value;
    }

}
