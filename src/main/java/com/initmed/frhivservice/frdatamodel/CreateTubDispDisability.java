//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createTubDispDisability complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createTubDispDisability">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}tubDispcardKey"/>
 *         &lt;element name="disability" type="{}nrTubDispDisability"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createTubDispDisability", propOrder = {
    "tubDispcardKey",
    "disability"
})
public class CreateTubDispDisability {

    @XmlElement(required = true)
    protected TubDispcardKey tubDispcardKey;
    @XmlElement(required = true)
    protected NrTubDispDisability disability;

    /**
     * Gets the value of the tubDispcardKey property.
     * 
     * @return
     *     possible object is
     *     {@link TubDispcardKey }
     *     
     */
    public TubDispcardKey getTubDispcardKey() {
        return tubDispcardKey;
    }

    /**
     * Sets the value of the tubDispcardKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link TubDispcardKey }
     *     
     */
    public void setTubDispcardKey(TubDispcardKey value) {
        this.tubDispcardKey = value;
    }

    /**
     * Gets the value of the disability property.
     * 
     * @return
     *     possible object is
     *     {@link NrTubDispDisability }
     *     
     */
    public NrTubDispDisability getDisability() {
        return disability;
    }

    /**
     * Sets the value of the disability property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrTubDispDisability }
     *     
     */
    public void setDisability(NrTubDispDisability value) {
        this.disability = value;
    }

}
