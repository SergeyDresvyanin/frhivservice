//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for nrHivDispcardCheckup complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nrHivDispcardCheckup">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="checkupId" type="{}refNsiHivCheckup"/>
 *         &lt;element name="checkupDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="result" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="desease" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nrHivDispcardCheckup", propOrder = {

})
public class NrHivDispcardCheckup {

    @XmlElement(required = true)
    protected RefNsiHivCheckup checkupId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar checkupDate;
    protected short result;
    protected String desease;

    /**
     * Gets the value of the checkupId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiHivCheckup }
     *     
     */
    public RefNsiHivCheckup getCheckupId() {
        return checkupId;
    }

    /**
     * Sets the value of the checkupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiHivCheckup }
     *     
     */
    public void setCheckupId(RefNsiHivCheckup value) {
        this.checkupId = value;
    }

    /**
     * Gets the value of the checkupDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCheckupDate() {
        return checkupDate;
    }

    /**
     * Sets the value of the checkupDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCheckupDate(XMLGregorianCalendar value) {
        this.checkupDate = value;
    }

    /**
     * Gets the value of the result property.
     * 
     */
    public short getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     */
    public void setResult(short value) {
        this.result = value;
    }

    /**
     * Gets the value of the desease property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesease() {
        return desease;
    }

    /**
     * Sets the value of the desease property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesease(String value) {
        this.desease = value;
    }

}
