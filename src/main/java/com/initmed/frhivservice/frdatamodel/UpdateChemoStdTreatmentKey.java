//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateChemoStdTreatmentKey complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateChemoStdTreatmentKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="treatmentKey" type="{}chemoStdTreatmentKey"/>
 *         &lt;element name="treatment" type="{}nrTubChemoStdTreatment"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateChemoStdTreatmentKey", propOrder = {
    "treatmentKey",
    "treatment"
})
public class UpdateChemoStdTreatmentKey {

    @XmlElement(required = true)
    protected ChemoStdTreatmentKey treatmentKey;
    @XmlElement(required = true)
    protected NrTubChemoStdTreatment treatment;

    /**
     * Gets the value of the treatmentKey property.
     * 
     * @return
     *     possible object is
     *     {@link ChemoStdTreatmentKey }
     *     
     */
    public ChemoStdTreatmentKey getTreatmentKey() {
        return treatmentKey;
    }

    /**
     * Sets the value of the treatmentKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChemoStdTreatmentKey }
     *     
     */
    public void setTreatmentKey(ChemoStdTreatmentKey value) {
        this.treatmentKey = value;
    }

    /**
     * Gets the value of the treatment property.
     * 
     * @return
     *     possible object is
     *     {@link NrTubChemoStdTreatment }
     *     
     */
    public NrTubChemoStdTreatment getTreatment() {
        return treatment;
    }

    /**
     * Sets the value of the treatment property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrTubChemoStdTreatment }
     *     
     */
    public void setTreatment(NrTubChemoStdTreatment value) {
        this.treatment = value;
    }

}
