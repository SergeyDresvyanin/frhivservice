//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nrRzPatientAddress complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nrRzPatientAddress">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{}address"/>
 *         &lt;element name="addressTypeId" type="{}refNsiVznAddressType" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nrRzPatientAddress", propOrder = {

})
public class NrRzPatientAddress {

    protected String phone;
    @XmlElement(required = true)
    protected NrAddress address;
    protected RefNsiVznAddressType addressTypeId;

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link NrAddress }
     *     
     */
    public NrAddress getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrAddress }
     *     
     */
    public void setAddress(NrAddress value) {
        this.address = value;
    }

    /**
     * Gets the value of the addressTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link RefNsiVznAddressType }
     *     
     */
    public RefNsiVznAddressType getAddressTypeId() {
        return addressTypeId;
    }

    /**
     * Sets the value of the addressTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefNsiVznAddressType }
     *     
     */
    public void setAddressTypeId(RefNsiVznAddressType value) {
        this.addressTypeId = value;
    }

}
