//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createHivExam complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createHivExam">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="registryNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="exam" type="{}nrHivRegistryExam"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createHivExam", propOrder = {
    "registryNumber",
    "exam"
})
public class CreateHivExam {

    @XmlElement(required = true)
    protected String registryNumber;
    @XmlElement(required = true)
    protected NrHivRegistryExam exam;

    /**
     * Gets the value of the registryNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistryNumber() {
        return registryNumber;
    }

    /**
     * Sets the value of the registryNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistryNumber(String value) {
        this.registryNumber = value;
    }

    /**
     * Gets the value of the exam property.
     * 
     * @return
     *     possible object is
     *     {@link NrHivRegistryExam }
     *     
     */
    public NrHivRegistryExam getExam() {
        return exam;
    }

    /**
     * Sets the value of the exam property.
     * 
     * @param value
     *     allowed object is
     *     {@link NrHivRegistryExam }
     *     
     */
    public void setExam(NrHivRegistryExam value) {
        this.exam = value;
    }

}
