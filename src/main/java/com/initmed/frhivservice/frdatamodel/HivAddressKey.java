//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//
package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for hivAddressKey complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="hivAddressKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="registryNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="addressTypeId" type="{}refNsiHivAddressType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hivAddressKey", propOrder = {
    "registryNumber",
    "addressTypeId"
})
public class HivAddressKey {

    @XmlElement(required = true)
    protected String registryNumber;
    @XmlElement(required = true)
    protected RefNsiHivAddressType addressTypeId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar beginDate;

    /**
     * Gets the value of the registryNumber property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getRegistryNumber() {
        return registryNumber;
    }

    /**
     * Sets the value of the registryNumber property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setRegistryNumber(String value) {
        this.registryNumber = value;
    }

    /**
     * Gets the value of the addressTypeId property.
     *
     * @return possible object is {@link RefNsiHivAddressType }
     *
     */
    public RefNsiHivAddressType getAddressTypeId() {
        return addressTypeId;
    }

    /**
     * Sets the value of the addressTypeId property.
     *
     * @param value allowed object is {@link RefNsiHivAddressType }
     *
     */
    public void setAddressTypeId(RefNsiHivAddressType value) {
        this.addressTypeId = value;
    }

    public XMLGregorianCalendar getBeginDate() {
        return beginDate;
    }

    /**
     * Sets the value of the beginDate property.
     *
     * @param value allowed object is {@link XMLGregorianCalendar }
     *
     */
    public void setBeginDate(XMLGregorianCalendar value) {
        this.beginDate = value;
    }

}
