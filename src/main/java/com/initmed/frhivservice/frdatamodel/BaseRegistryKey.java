//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.02 at 09:56:49 AM MSK 
//


package com.initmed.frhivservice.frdatamodel;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for baseRegistryKey complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="baseRegistryKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="snils" type="{}identifiedKey"/>
 *         &lt;element name="unidentified" type="{}unIdentifiedKey"/>
 *         &lt;element ref="{}registryNumber"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "baseRegistryKey", propOrder = {
    "snils",
    "unidentified",
    "registryNumber"
})
public class BaseRegistryKey {

    protected String snils;
    protected UnIdentifiedKey unidentified;
    protected Object registryNumber;

    /**
     * Gets the value of the snils property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSnils() {
        return snils;
    }

    /**
     * Sets the value of the snils property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSnils(String value) {
        this.snils = value;
    }

    /**
     * Gets the value of the unidentified property.
     * 
     * @return
     *     possible object is
     *     {@link UnIdentifiedKey }
     *     
     */
    public UnIdentifiedKey getUnidentified() {
        return unidentified;
    }

    /**
     * Sets the value of the unidentified property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnIdentifiedKey }
     *     
     */
    public void setUnidentified(UnIdentifiedKey value) {
        this.unidentified = value;
    }

    /**
     * Gets the value of the registryNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getRegistryNumber() {
        return registryNumber;
    }

    /**
     * Sets the value of the registryNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setRegistryNumber(Object value) {
        this.registryNumber = value;
    }

}
