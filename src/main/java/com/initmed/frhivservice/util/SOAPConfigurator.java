package com.initmed.frhivservice.util;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

import jakarta.xml.ws.Binding;
import jakarta.xml.ws.BindingProvider;
import jakarta.xml.ws.handler.Handler;
import jakarta.xml.ws.handler.soap.SOAPMessageContext;
import java.util.List;
import jakarta.xml.ws.handler.soap.SOAPHandler;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.cxf.jaxws.JaxWsClientProxy;

/**
 * Надстройщик сервиса обмена SOAP-сообщениями
 */
public class SOAPConfigurator {

    private static final java.lang.String JAXWS_HOSTNAME_VERIFIER
            = "com.sun.xml.internal.ws.transport.https.client.hostname.verifier";
    private static final java.lang.String JAXWS_SSL_SOCKET_FACTORY
            = "com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory";

    /**
     * Настраивает Apache CXF, чтобы в заголовках сообщений вычислялся
     * Content-Length
     *
     * @param port объект порта точки доступа в СМЭВ
     */
    public static void сontentLengthActivate(final Object port) {
        HTTPClientPolicy hTTPClientPolicy = new HTTPClientPolicy();
        hTTPClientPolicy.setAllowChunking(false);
        Client client = JaxWsClientProxy.getClient(port);
        HTTPConduit http = (HTTPConduit) client.getConduit();
        http.setClient(hTTPClientPolicy);
    }

    /**
     * Подключает SOAPHandler
     *
     * @param port объект порта точки доступа в СМЭВ
     * @param soapHandler SOAPHandler
     * @param isMTOMEnabled признак активации/деактивации MTOM
     */
    public static void bindingSOAPHandler(final Object port, final SOAPHandler<SOAPMessageContext> soapHandler, final boolean isMTOMEnabled) {

        BindingProvider provider = (BindingProvider) port;
        Binding binding = provider.getBinding();
//        ((SOAPBinding) binding).setMTOMEnabled(isMTOMEnabled);

        List<Handler> handlerChain = binding.getHandlerChain();
        handlerChain.add(soapHandler);

        binding.setHandlerChain(handlerChain);

        if (port instanceof BindingProvider) {
            BindingProvider bp = (BindingProvider) provider;
            Map requestContext = bp.getRequestContext();
            requestContext.put(JAXWS_SSL_SOCKET_FACTORY, getTrustingSSLSocketFactory());
            requestContext.put(JAXWS_HOSTNAME_VERIFIER,
                    new NaiveHostnameVerifier());
        }
    }

    /**
     * Настраивает url - точки доступа к серверу СМЭВ
     *
     * @param port
     * @param endpointURL
     */
    public static void setEndpointURL(final Object port, final String endpointURL) {
        BindingProvider bp = (BindingProvider) port;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);
    }


    private static class NaiveHostnameVerifier implements HostnameVerifier {

        @Override
        public boolean verify(String hostName,
                SSLSession session) {
            return true;
        }
    }

    private static interface SSLSocketFactoryHolder {

        public static final SSLSocketFactory INSTANCE = createSSLSocketFactory();
    }

    public static SSLSocketFactory getTrustingSSLSocketFactory() {
        return SSLSocketFactoryHolder.INSTANCE;
    }

    private static SSLSocketFactory createSSLSocketFactory() {
        TrustManager[] trustManagers = new TrustManager[]{
            new NaiveTrustManager()
        };
        SSLContext sslContext;
        try {
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(new KeyManager[0], trustManagers,
                    new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (GeneralSecurityException e) {
            return null;
        }
    }

    private static class NaiveTrustManager implements
            X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] certs,
                String authType) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] certs,
                String authType) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }
}
