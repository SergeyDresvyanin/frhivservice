/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.util;

import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.CMSVersion;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.CertificateChoices;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.CertificateSet;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.ContentInfo;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.DigestAlgorithmIdentifier;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.DigestAlgorithmIdentifiers;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.EncapsulatedContentInfo;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.IssuerAndSerialNumber;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.SignatureAlgorithmIdentifier;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.SignatureValue;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.SignedData;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.SignerIdentifier;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.SignerInfo;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.SignerInfos;
import ru.CryptoPro.JCP.ASN.PKIX1Explicit88.CertificateSerialNumber;
import ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Name;
import ru.CryptoPro.JCP.params.OID;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Base64;
import com.objsys.asn1j.runtime.Asn1BerDecodeBuffer;
import com.objsys.asn1j.runtime.Asn1BerEncodeBuffer;
import com.objsys.asn1j.runtime.Asn1Null;
import com.objsys.asn1j.runtime.Asn1ObjectIdentifier;
import java.util.Vector;
import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCSP.JCSP;

/**
 *
 * @author sd199
 */
public class FileSigner {

    private PrivateKey privateKey;
    private Certificate cer;

    public FileSigner(String name, char[] password) throws Exception {
        System.out.println("==CertStores on this machine start==");
        Vector stores = ru.CryptoPro.JCSP.KeyStore.KeyStoreConfig.getInstance().getNames();
        stores.forEach(x -> System.out.println(x.toString()));
        System.out.println("==CertStores on this machine end==");
        this.privateKey = loadKey(name, password);
        this.cer = loadCertificate(name);
        if (this.cer == null) {
            throw new RuntimeException("cerstore not found with name" + name);
        }
        if (this.privateKey == null) {
            throw new RuntimeException("PK not found with name" + name);
        }
    }

    public static PrivateKey loadKey(String name, char[] password)
            throws Exception {
        final KeyStore certStore = KeyStore.getInstance(JCSP.REG_STORE_NAME,
                JCSP.PROVIDER_NAME);
        certStore.load(null, null);
        return (PrivateKey) certStore.getKey(name, password);
    }

    public static Certificate loadCertificate(String name)
            throws Exception {
        final KeyStore hdImageStore = KeyStore.getInstance(JCSP.REG_STORE_NAME,
                JCSP.PROVIDER_NAME);
        hdImageStore.load(null, null);
        return hdImageStore.getCertificate(name);
    }

    public String signDocumnetWithGost3410(byte[] data) throws Exception {
        final Signature signature = Signature.getInstance(JCP.GOST_SIGN_2012_256_NAME, JCSP.PROVIDER_NAME);
        signature.initSign(privateKey);
        signature.update(data);

        byte[] sign = signature.sign();

//        Signature sgn = Signature.getInstance(JCP.GOST_SIGN_2012_256_NAME, JCP.PROVIDER_NAME);
//        sgn.initVerify(cer);
//        sgn.update(data);
//        final boolean checkResult = sgn.verify(sign);
//        if (checkResult) {
//            System.out.println("vaid sign");
//        } // if
//        else {
//            throw new Exception("Invalid signature.");
//        } // else
//        final byte[] sign = createCMSEx(data, signature.sign(), cer, "1.2.643.7.1.1.2.2", JCP.GOST_EL_KEY_OID);
//        CMSVerifyEx(sign, cer, data, "1.2.643.7.1.1.2.2", JCP.GOST_SIGN_2012_256_NAME, JCP.PROVIDER_NAME);
        return Base64.getEncoder().encodeToString(sign);
    }

    public static byte[] createCMSEx(byte[] buffer, byte[] sign,
            Certificate cert, String digestOid,
            String signOid) throws Exception {

        final ContentInfo all = new ContentInfo();
        all.contentType = new Asn1ObjectIdentifier(
                new OID("1.2.840.113549.1.7.2").value);

        final SignedData cms = new SignedData();
        all.content = cms;
        cms.version = new CMSVersion(1);

        // digest
        cms.digestAlgorithms = new DigestAlgorithmIdentifiers(1);
        final DigestAlgorithmIdentifier a = new DigestAlgorithmIdentifier(
                new OID(digestOid).value);

        a.parameters = new Asn1Null();
        cms.digestAlgorithms.elements[0] = a;

        cms.encapContentInfo = new EncapsulatedContentInfo(
                new Asn1ObjectIdentifier(
                        new OID("1.2.840.113549.1.7.1").value), null);

        // certificate
        cms.certificates = new CertificateSet(1);
        final ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Certificate certificate
                = new ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Certificate();
        final Asn1BerDecodeBuffer decodeBuffer
                = new Asn1BerDecodeBuffer(cert.getEncoded());
        certificate.decode(decodeBuffer);

        cms.certificates.elements = new CertificateChoices[1];
        cms.certificates.elements[0] = new CertificateChoices();
        cms.certificates.elements[0].set_certificate(certificate);

        // signer info
        cms.signerInfos = new SignerInfos(1);
        cms.signerInfos.elements[0] = new SignerInfo();
        cms.signerInfos.elements[0].version = new CMSVersion(1);
        cms.signerInfos.elements[0].sid = new SignerIdentifier();

        final byte[] encodedName = ((X509Certificate) cert)
                .getIssuerX500Principal().getEncoded();
        final Asn1BerDecodeBuffer nameBuf = new Asn1BerDecodeBuffer(encodedName);
        final Name name = new Name();
        name.decode(nameBuf);

        final CertificateSerialNumber num = new CertificateSerialNumber(
                ((X509Certificate) cert).getSerialNumber());
        cms.signerInfos.elements[0].sid.set_issuerAndSerialNumber(
                new IssuerAndSerialNumber(name, num));
        cms.signerInfos.elements[0].digestAlgorithm
                = new DigestAlgorithmIdentifier(new OID(digestOid).value);
        cms.signerInfos.elements[0].digestAlgorithm.parameters = new Asn1Null();
        cms.signerInfos.elements[0].signatureAlgorithm
                = new SignatureAlgorithmIdentifier(new OID(signOid).value);
        cms.signerInfos.elements[0].signatureAlgorithm.parameters = new Asn1Null();
        cms.signerInfos.elements[0].signature = new SignatureValue(sign);
        // encode
        final Asn1BerEncodeBuffer asnBuf = new Asn1BerEncodeBuffer();
        all.encode(asnBuf, true);
        return asnBuf.getMsgCopy();
    }

    public void CMSVerifyEx(byte[] buffer, Certificate cert,
            byte[] data, String digestOidValue, String signAlg,
            String providerName) throws Exception {
        int i;
        final Asn1BerDecodeBuffer asnBuf = new Asn1BerDecodeBuffer(buffer);
        final ContentInfo all = new ContentInfo();
        all.decode(asnBuf);

        if (!new OID("1.2.840.113549.1.7.2").eq(all.contentType.value)) {
            throw new Exception("Not supported");
        } // if

        final SignedData cms = (SignedData) all.content;
        if (cms.version.value != 1) {
            throw new Exception("Incorrect version");
        } // if

        if (!new OID("1.2.840.113549.1.7.1").eq(
                cms.encapContentInfo.eContentType.value)) {
            throw new Exception("Nested not supported");
        } // if

        byte[] text = null;
        if (data != null) {
            text = data;
        } // if
        else if (cms.encapContentInfo.eContent != null) {
            text = cms.encapContentInfo.eContent.value;
        } // else

        if (text == null) {
            throw new Exception("No content");
        } // if

        OID digestOid = null;
        DigestAlgorithmIdentifier a = new DigestAlgorithmIdentifier(
                new OID(digestOidValue).value);

        for (i = 0; i < cms.digestAlgorithms.elements.length; i++) {
            if (cms.digestAlgorithms.elements[i].algorithm.equals(a.algorithm)) {
                digestOid = new OID(cms.digestAlgorithms.elements[i].algorithm.value);
                break;
            } // if
        } // for

        if (digestOid == null) {
            throw new Exception("Unknown digest");
        } // if

        int pos = -1;

        if (cms.certificates != null) {
            for (i = 0; i < cms.certificates.elements.length; i++) {
                final Asn1BerEncodeBuffer encBuf = new Asn1BerEncodeBuffer();
                cms.certificates.elements[i].encode(encBuf);
                final byte[] in = encBuf.getMsgCopy();
                if (Arrays.equals(in, cert.getEncoded())) {
                    System.out.println("Certificate: " + ((X509Certificate) cert).getSubjectDN());
                    pos = i;
                    break;
                } // if

            } // for

            if (pos == -1) {
                throw new Exception("Not signed on certificate.");
            } // if

        } else if (cert == null) {
            throw new Exception("No certificate found.");
        } // else
        else {
            // Если задан {@link #cert}, то пробуем проверить
            // первую же подпись на нем.
            pos = 0;
        } // else

        final SignerInfo info = cms.signerInfos.elements[pos];
        if (info.version.value != 1) {
            throw new Exception("Incorrect version");
        } // if
        if (!digestOid.equals(new OID(info.digestAlgorithm.algorithm.value))) {
            throw new Exception("Not signed on certificate.");
        } // if
        final byte[] sign = info.signature.value;
        // check
        final Signature signature = Signature.getInstance(signAlg, providerName);
        signature.initVerify(cert);
        signature.update(text);
        final boolean checkResult = signature.verify(sign);
        if (checkResult) {
            System.out.println("vaid sign");
        } // if
        else {
            throw new Exception("Invalid signature.");
        } // else

    }

}
