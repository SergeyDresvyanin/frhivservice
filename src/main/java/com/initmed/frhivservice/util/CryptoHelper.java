package com.initmed.frhivservice.util;

import com.objsys.asn1j.runtime.Asn1BerDecodeBuffer;
import com.objsys.asn1j.runtime.Asn1BerEncodeBuffer;
import com.objsys.asn1j.runtime.Asn1Null;
import com.objsys.asn1j.runtime.Asn1ObjectIdentifier;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

import java.util.Arrays;
import java.util.Base64;
import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCSP.JCSP;

public class CryptoHelper {

    private PrivateKey privateKey;
    private Certificate cer;
    private FileSigner fileSigner;

    public CryptoHelper(String pkName, String password) throws Exception {
        try {
            this.fileSigner = new FileSigner(pkName, password.toCharArray());
            this.privateKey = loadKey(pkName, password.toCharArray());
            this.cer = loadCertificate(pkName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Формирует хэш данных по ГОСТ 34.11 и кодирует его в base64
     *
     * @param data входные данные
     * @return хэш в base64
     */
    public String getBase64Digest(String data) throws NoSuchAlgorithmException {
        // создание объекта хеширования данных
        final MessageDigest digest = MessageDigest.getInstance(JCP.GOST_DIGEST_2012_256_NAME);
        digest.update(data.getBytes());
        // вычисление значения хеша
        return Base64.getEncoder().encodeToString(digest.digest());
    }

    public static PrivateKey loadKey(String name, char[] password)
            throws Exception {
        final KeyStore certStore = KeyStore.getInstance(JCSP.REG_STORE_NAME,
                JCSP.PROVIDER_NAME);
        certStore.load(null, null);
        return (PrivateKey) certStore.getKey(name, password);
    }

    public static Certificate loadCertificate(String name)
            throws Exception {
        final KeyStore hdImageStore = KeyStore.getInstance(JCSP.REG_STORE_NAME,
                JCSP.PROVIDER_NAME);
        hdImageStore.load(null, null);
        return hdImageStore.getCertificate(name);
    }

//    /**
//     * Подписывает данные ЭЦП по ГОСТ 34.10 и кодирует ее в base64
//     *
//     * @param data входные данные
//     * @param key закрытый ключ
//     * @return подпись в base64
//     * @throws GeneralSecurityException
//     */
    public String getSignature(String data) throws Exception {
        return fileSigner.signDocumnetWithGost3410(data.getBytes());
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public String getCerBase64() throws CertificateEncodingException {
//        return "MIIIkTCCCD6gAwIBAgIRAUnr0AAuq6W9QHSMJvsmp5YwCgYIKoUDBwEBAwIwggFbMSAwHgYJKoZIhvcNAQkBFhFpbmZvQGNyeXB0b3Byby5ydTEYMBYGBSqFA2QBEg0xMDM3NzAwMDg1NDQ0MRowGAYIKoUDA4EDAQESDDAwNzcxNzEwNzk5MTELMAkGA1UEBhMCUlUxGDAWBgNVBAgMDzc3INCc0L7RgdC60LLQsDEVMBMGA1UEBwwM0JzQvtGB0LrQstCwMS8wLQYDVQQJDCbRg9C7LiDQodGD0YnRkdCy0YHQutC40Lkg0LLQsNC7INC0LiAxODElMCMGA1UECgwc0J7QntCeICLQmtCg0JjQn9Ci0J4t0J/QoNCeIjFrMGkGA1UEAwxi0KLQtdGB0YLQvtCy0YvQuSDQv9C+0LTRh9C40L3QtdC90L3Ri9C5INCj0KYg0J7QntCeICLQmtCg0JjQn9Ci0J4t0J/QoNCeIiDQk9Ce0KHQoiAyMDEyICjQo9CmIDIuMCkwHhcNMTkxMjI1MTIzMDM5WhcNMjAwMzI1MTI0MDM5WjCCATwxGDAWBgUqhQNkBRINMTIzMTIzMTIzMTIzMTEbMBkGCSqGSIb3DQEJARYMcy5zcGJAaGl2LnJ1MRowGAYIKoUDA4EDAQESDDEyMzEyMzEyMzEyMzEWMBQGBSqFA2QDEgsxMjMxMjMxMjMxMjEYMBYGBSqFA2QBEg0xMjM0NTY3ODkwMTIzMRUwEwYDVQQMDAzRgtC10YHRgtC10YAxDzANBgNVBAsMBtCh0J/QkTERMA8GA1UECgwI0KHQn9CY0JQxDzANBgNVBAkMBtCh0J/QkTEPMA0GA1UEBwwG0KHQn9CRMQ8wDQYDVQQIDAbQodCf0JExCzAJBgNVBAYTAlJVMQ8wDQYDVQQqDAbQodCf0JExDzANBgNVBAQMBtCh0J/QkTEYMBYGA1UEAwwP0KHQn9CY0JQg0KHQn9CRMGYwHwYIKoUDBwEBAQEwEwYHKoUDAgIkAAYIKoUDBwEBAgIDQwAEQJ/l39xZWYS1OP71qJCFEBtejOhtgzaiTYm4AT5AL/dUwKxTrjVDUjhjfAoLMiD9JDqpy9kE6IevWHH/ynH3GVqjggTvMIIE6zAOBgNVHQ8BAf8EBAMCBPAwHwYJKwYBBAGCNxUHBBIwEAYIKoUDAgIuAAgCAQECAQAwHQYDVR0OBBYEFNYgRYRrFruMstwFSgL2Kb1O+2iIMCYGA1UdJQQfMB0GCCsGAQUFBwMEBggrBgEFBQcDAgYHKoUDAgIiBjAyBgkrBgEEAYI3FQoEJTAjMAoGCCsGAQUFBwMEMAoGCCsGAQUFBwMCMAkGByqFAwICIgYwgacGCCsGAQUFBwEBBIGaMIGXMDgGCCsGAQUFBzABhixodHRwOi8vdGVzdGNhMjAxMi5jcnlwdG9wcm8ucnUvb2NzcC9vY3NwLnNyZjBbBggrBgEFBQcwAoZPaHR0cDovL3Rlc3RjYTIwMTIuY3J5cHRvcHJvLnJ1L2FpYS9mZmU0Njg2MDkyYzhlYzgxMTMxOWJiOTYzNWUzNTg0MWYxODEyZDliLmNydDAdBgNVHSAEFjAUMAgGBiqFA2RxAjAIBgYqhQNkcQEwKwYDVR0QBCQwIoAPMjAxOTEyMjUxMjMwMzlagQ8yMDIwMDMyNTEyMzAzOVowggEaBgUqhQNkcASCAQ8wggELDDTQodCa0JfQmCAi0JrRgNC40L/RgtC+0J/RgNC+IENTUCIgKNCy0LXRgNGB0LjRjyA0LjApDDHQn9CQ0JogItCa0YDQuNC/0YLQvtCf0YDQviDQo9CmIiDQstC10YDRgdC40LggMi4wDE/QodC10YDRgtC40YTQuNC60LDRgiDRgdC+0L7RgtCy0LXRgtGB0YLQstC40Y8g4oSWINCh0KQvMTI0LTMzODAg0L7RgiAxMS4wNS4yMDE4DE/QodC10YDRgtC40YTQuNC60LDRgiDRgdC+0L7RgtCy0LXRgtGB0YLQstC40Y8g4oSWINCh0KQvMTI4LTM1OTIg0L7RgiAxNy4xMC4yMDE4MCwGBSqFA2RvBCMMIdCh0JrQl9CYICLQmtGA0LjQv9GC0L7Qn9GA0L4gQ1NQIjBgBgNVHR8EWTBXMFWgU6BRhk9odHRwOi8vdGVzdGNhMjAxMi5jcnlwdG9wcm8ucnUvY2RwL2ZmZTQ2ODYwOTJjOGVjODExMzE5YmI5NjM1ZTM1ODQxZjE4MTJkOWIuY3JsMIIBlwYDVR0jBIIBjjCCAYqAFP/kaGCSyOyBExm7ljXjWEHxgS2boYIBXaSCAVkwggFVMSAwHgYJKoZIhvcNAQkBFhFpbmZvQGNyeXB0b3Byby5ydTEYMBYGBSqFA2QBEg0xMDM3NzAwMDg1NDQ0MRowGAYIKoUDA4EDAQESDDAwNzcxNzEwNzk5MTELMAkGA1UEBhMCUlUxGDAWBgNVBAgMDzc3INCc0L7RgdC60LLQsDEVMBMGA1UEBwwM0JzQvtGB0LrQstCwMS8wLQYDVQQJDCbRg9C7LiDQodGD0YnRkdCy0YHQutC40Lkg0LLQsNC7INC0LiAxODElMCMGA1UECgwc0J7QntCeICLQmtCg0JjQn9Ci0J4t0J/QoNCeIjFlMGMGA1UEAwxc0KLQtdGB0YLQvtCy0YvQuSDQs9C+0LvQvtCy0L3QvtC5INCj0KYg0J7QntCeICLQmtCg0JjQn9Ci0J4t0J/QoNCeIiDQk9Ce0KHQoiAyMDEyICjQo9CmIDIuMCmCEQHROcYA5alVtUqPfchVCm9mMAoGCCqFAwcBAQMCA0EA8SvWapduB+D4jFUqL0SAlCzB2dJa5+uLYUFa2JRH0tEV5zxKBjip6qg0YPrBbKNi6EKtNGqtJmj15pudxNZAoQ==";
        return Base64.getEncoder().encodeToString(cer.getEncoded());
    }

}
