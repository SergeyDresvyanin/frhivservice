/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "HIVDISPEXAM")
@XmlRootElement

public class Hivdispexam implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "MISID")
    private Integer misid;
    @Column(name = "EXAMDATE")
    @Temporal(TemporalType.DATE)
    private Date examdate;
    @Column(name = "SAMPLEDATE")
    @Temporal(TemporalType.DATE)
    private Date sampledate;
    @Column(name = "REFSAMPLEID")
    private Integer refsampleid;
    @Size(max = 50)
    @Column(name = "MOID")
    private String moid;
    @Size(max = 100)
    @Column(name = "TESTSYSTEMNAME")
    private String testsystemname;
    @Size(max = 100)
    @Column(name = "TESTSYSTEMSERIAL")
    private String testsystemserial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INFEDREGISTER")
    private short infedregister;
    @Column(name = "REFEXAMID")
    private short refexamid;
    @JoinColumn(name = "REFPATIENT", referencedColumnName = "ID")
    @ManyToOne
    private Patient refpatient;
    @Column(name = "QUANTITATIVERESULT")
    private Integer quantitativeresult;
    @Column(name = "QUALITATIVERESULT")
    private Short qualitativeresult;
    @Column(name = "EXAMMARK")
    private Integer examMark;

    public Hivdispexam() {
    }

    public Hivdispexam(Integer id) {
        this.id = id;
    }

    public Hivdispexam(Integer id, short infedregister) {
        this.id = id;
        this.infedregister = infedregister;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMisid() {
        return misid;
    }

    public void setMisid(Integer misid) {
        this.misid = misid;
    }

    public Date getExamdate() {
        return examdate;
    }

    public void setExamdate(Date examdate) {
        this.examdate = examdate;
    }

    public Date getSampledate() {
        return sampledate;
    }

    public void setSampledate(Date sampledate) {
        this.sampledate = sampledate;
    }

    public Integer getRefsampleid() {
        return refsampleid;
    }

    public void setRefsampleid(Integer refsampleid) {
        this.refsampleid = refsampleid;
    }

    public String getMoid() {
        return moid;
    }

    public void setMoid(String moid) {
        this.moid = moid;
    }

    public String getTestsystemname() {
        return testsystemname;
    }

    public void setTestsystemname(String testsystemname) {
        this.testsystemname = testsystemname;
    }

    public String getTestsystemserial() {
        return testsystemserial;
    }

    public void setTestsystemserial(String testsystemserial) {
        this.testsystemserial = testsystemserial;
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    public Patient getRefpatient() {
        return refpatient;
    }

    public void setRefpatient(Patient refpatient) {
        this.refpatient = refpatient;
    }

    public Integer getQuantitativeresult() {
        return quantitativeresult;
    }

    public void setQuantitativeresult(Integer quantitativeresult) {
        this.quantitativeresult = quantitativeresult;
    }

    public Short getQualitativeresult() {
        return qualitativeresult;
    }

    public void setQualitativeresult(Short qualitativeresult) {
        this.qualitativeresult = qualitativeresult;
    }

    public short getRefexamid() {
        return refexamid;
    }

    public void setRefexamid(short refexamid) {
        this.refexamid = refexamid;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public Integer getExamMark() {
        return examMark;
    }

    public void setExamMark(Integer examMark) {
        this.examMark = examMark;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivdispexam)) {
            return false;
        }
        Hivdispexam other = (Hivdispexam) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hivdispexam[ id=" + id + " ]";
    }

}
