/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.initmed.frhivservice.entity;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergeydresvyanin
 */
@Entity
@Table(name = "HivepidContacts")
@XmlRootElement
public class HivepidContacts {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @JoinColumn(name = "refHivepid", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Hivepid refHivepid;
    @Column(name = "UNRZ")
    private String unrz;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Hivepid getRefHivepid() {
        return refHivepid;
    }

    public void setRefHivepid(Hivepid refHivepid) {
        this.refHivepid = refHivepid;
    }

    public String getUnrz() {
        return unrz;
    }

    public void setUnrz(String unrz) {
        this.unrz = unrz;
    }
}
