/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import jakarta.persistence.Basic;
import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "SCHEDULEPROCESS")
@Cacheable(false)
@XmlRootElement
public class Scheduleprocess implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "NAME")
    private String name;
    @Column(name = "TRANSFER")
    private short transfer;

    public Scheduleprocess() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getTransfer() {
        return transfer;
    }

    public void setTransfer(short transfer) {
        this.transfer = transfer;
    }

}
