/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Set;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "EXCLUDEREASON")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Excludereason.findAll", query = "SELECT e FROM Excludereason e"),
    @NamedQuery(name = "Excludereason.findById", query = "SELECT e FROM Excludereason e WHERE e.id = :id"),
    @NamedQuery(name = "Excludereason.findByName", query = "SELECT e FROM Excludereason e WHERE e.name = :name")})
public class Excludereason implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 100)
    @Column(name = "NAME")
    private String name;
    @OneToMany(mappedBy = "refexcludereasonid")
    private Set<Hiv> hivSet;

    public Excludereason() {
    }

    public Excludereason(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Set<Hiv> getHivSet() {
        return hivSet;
    }

    public void setHivSet(Set<Hiv> hivSet) {
        this.hivSet = hivSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Excludereason)) {
            return false;
        }
        Excludereason other = (Excludereason) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Excludereason[ id=" + id + " ]";
    }
    
}
