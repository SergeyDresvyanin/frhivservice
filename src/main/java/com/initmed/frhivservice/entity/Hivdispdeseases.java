/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "HIVDISPDESEASES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivdispdeseases.findAll", query = "SELECT h FROM Hivdispdeseases h"),
    @NamedQuery(name = "Hivdispdeseases.findById", query = "SELECT h FROM Hivdispdeseases h WHERE h.id = :id"),
    @NamedQuery(name = "Hivdispdeseases.findByPersonid", query = "SELECT h FROM Hivdispdeseases h WHERE h.personid = :personid"),
    @NamedQuery(name = "Hivdispdeseases.findByDeseasedate", query = "SELECT h FROM Hivdispdeseases h WHERE h.deseasedate = :deseasedate"),
    @NamedQuery(name = "Hivdispdeseases.findByDesease", query = "SELECT h FROM Hivdispdeseases h WHERE h.desease = :desease")})
public class Hivdispdeseases implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "PERSONID")
    private String personid;
    @Column(name = "DESEASEDATE")
    @Temporal(TemporalType.DATE)
    private Date deseasedate;
    @Size(max = 5)
    @Column(name = "DESEASE")
    private String desease;
    @Column(name = "INFEDREGISTER")
    private int infedregister;
    @JoinColumn(name = "REFHIVDISP", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Hivdisp refhivdisp;

    public Hivdispdeseases() {
    }

    public Hivdispdeseases(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDeseasedate() {
        return deseasedate;
    }

    public void setDeseasedate(Date deseasedate) {
        this.deseasedate = deseasedate;
    }

    public String getDesease() {
        return desease;
    }

    public String getPersonid() {
        return personid;
    }

    public void setPersonid(String personid) {
        this.personid = personid;
    }

    
    public void setDesease(String desease) {
        this.desease = desease;
    }

    public Hivdisp getRefhivdisp() {
        return refhivdisp;
    }

    public void setRefhivdisp(Hivdisp refhivdisp) {
        this.refhivdisp = refhivdisp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivdispdeseases)) {
            return false;
        }
        Hivdispdeseases other = (Hivdispdeseases) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hivdispdeseases[ id=" + id + " ]";
    }

    public int getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(int infedregister) {
        this.infedregister = infedregister;
    }

}
