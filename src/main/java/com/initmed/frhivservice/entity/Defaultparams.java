/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "DEFAULTPARAMS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Defaultparams.findAll", query = "SELECT d FROM Defaultparams d"),
    @NamedQuery(name = "Defaultparams.findById", query = "SELECT d FROM Defaultparams d WHERE d.id = :id"),
    @NamedQuery(name = "Defaultparams.findByName", query = "SELECT d FROM Defaultparams d WHERE d.name = :name"),
    @NamedQuery(name = "Defaultparams.findByDescr", query = "SELECT d FROM Defaultparams d WHERE d.descr = :descr"),
    @NamedQuery(name = "Defaultparams.findByVal", query = "SELECT d FROM Defaultparams d WHERE d.val = :val")})
public class Defaultparams implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "NAME")
    private String name;
    @Size(max = 100)
    @Column(name = "DESCR")
    private String descr;
    @Size(max = 100)
    @Column(name = "VAL")
    private String val;

    public Defaultparams() {
    }

    public Defaultparams(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Defaultparams)) {
            return false;
        }
        Defaultparams other = (Defaultparams) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Defaultparams[ id=" + id + " ]";
    }
    
}
