/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "CONTINGENTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contingents.findAll", query = "SELECT c FROM Contingents c"),
    @NamedQuery(name = "Contingents.findById", query = "SELECT c FROM Contingents c WHERE c.id = :id"),
    @NamedQuery(name = "Contingents.findByName", query = "SELECT c FROM Contingents c WHERE c.name = :name")})
public class Contingents implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 100)
    @Column(name = "NAME")
    private String name;
    @JoinColumn(name = "REFHIV", referencedColumnName = "ID")
    @ManyToOne
    private Hiv refhiv;

    public Contingents() {
    }

    public Contingents(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Hiv getRefhiv() {
        return refhiv;
    }

    public void setRefhiv(Hiv refhiv) {
        this.refhiv = refhiv;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contingents)) {
            return false;
        }
        Contingents other = (Contingents) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Contingents[ id=" + id + " ]";
    }
    
}
