/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "PATHOLOGIES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pathologies.findAll", query = "SELECT p FROM Pathologies p"),
    @NamedQuery(name = "Pathologies.findById", query = "SELECT p FROM Pathologies p WHERE p.id = :id"),
    @NamedQuery(name = "Pathologies.findByName", query = "SELECT p FROM Pathologies p WHERE p.name = :name"),
    @NamedQuery(name = "Pathologies.findByMkbcode", query = "SELECT p FROM Pathologies p WHERE p.mkbcode = :mkbcode")})
public class Pathologies implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 100)
    @Column(name = "NAME")
    private String name;
    @Size(max = 5)
    @Column(name = "MKBCODE")
    private String mkbcode;
    @JoinColumn(name = "REFHIV", referencedColumnName = "ID")
    @ManyToOne
    private Hiv refhiv;

    public Pathologies() {
    }

    public Pathologies(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMkbcode() {
        return mkbcode;
    }

    public void setMkbcode(String mkbcode) {
        this.mkbcode = mkbcode;
    }

    public Hiv getRefhiv() {
        return refhiv;
    }

    public void setRefhiv(Hiv refhiv) {
        this.refhiv = refhiv;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pathologies)) {
            return false;
        }
        Pathologies other = (Pathologies) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Pathologies[ id=" + id + " ]";
    }
    
}
