/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "HIVDISP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivdisp.findAll", query = "SELECT h FROM Hivdisp h"),
    @NamedQuery(name = "Hivdisp.findById", query = "SELECT h FROM Hivdisp h WHERE h.id = :id"),
    @NamedQuery(name = "Hivdisp.findByCardnumber", query = "SELECT h FROM Hivdisp h WHERE h.cardnumber = :cardnumber"),
    @NamedQuery(name = "Hivdisp.findByIndate", query = "SELECT h FROM Hivdisp h WHERE h.indate = :indate"),
    @NamedQuery(name = "Hivdisp.findByMoid", query = "SELECT h FROM Hivdisp h WHERE h.moid = :moid"),
    @NamedQuery(name = "Hivdisp.findByPersonid", query = "SELECT h FROM Hivdisp h WHERE h.personid = :personid"),
    @NamedQuery(name = "Hivdisp.findByOutdate", query = "SELECT h FROM Hivdisp h WHERE h.outdate = :outdate"),
    @NamedQuery(name = "Hivdisp.findByOutreasonid", query = "SELECT h FROM Hivdisp h WHERE h.outreasonid = :outreasonid"),
    @NamedQuery(name = "Hivdisp.findByDeathdate", query = "SELECT h FROM Hivdisp h WHERE h.deathdate = :deathdate"),
    @NamedQuery(name = "Hivdisp.findByDeathdesease", query = "SELECT h FROM Hivdisp h WHERE h.deathdesease = :deathdesease"),
    @NamedQuery(name = "Hivdisp.findByDeathreason", query = "SELECT h FROM Hivdisp h WHERE h.deathreason = :deathreason"),
    @NamedQuery(name = "Hivdisp.findByRefpatient", query = "SELECT h FROM Hivdisp h WHERE h.refpatient = :refpatient")})
public class Hivdisp implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 20)
    @Column(name = "CARDNUMBER")
    private String cardnumber;
    @Column(name = "INDATE")
    @Temporal(TemporalType.DATE)
    private Date indate;
    @Size(max = 50)
    @Column(name = "MOID")
    private String moid;
    @Column(name = "PERSONID")
    private String personid;
    @Column(name = "OUTDATE")
    @Temporal(TemporalType.DATE)
    private Date outdate;
    @Size(max = 25)
    @Column(name = "OUTREASONID")
    private String outreasonid;
    @Column(name = "DEATHDATE")
    @Temporal(TemporalType.DATE)
    private Date deathdate;
    @Size(max = 25)
    @Column(name = "DEATHDESEASE")
    private String deathdesease;
    @Size(max = 25)
    @Column(name = "DEATHREASON")
    private String deathreason;
    @Column(name = "INFEDREGISTER")
    private short infedregister;

    @JoinColumn(name = "REFPATIENT", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Patient refpatient;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refhivdisp")
    private Set<Hivdispdeseases> hivdispdeseasesSet;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hivdisp")
    private Set<Hivdispconc> hivdispconcs;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hivdisp")
    private Set<Hivdispchekup> hivdispchekups;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hivdisp")
    private Set<Hivdispstage> hivdispstages;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hivdisp")
    private Set<Hivdisphosp> hivdisphosps;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hivdisp")
    private Set<Hivdispchemo> hivdispchemos;

    public Hivdisp() {
    }

    public Hivdisp(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public Date getIndate() {
        return indate;
    }

    public void setIndate(Date indate) {
        this.indate = indate;
    }

    public String getMoid() {
        return moid;
    }

    public void setMoid(String moid) {
        this.moid = moid;
    }

    public String getPersonid() {
        return personid;
    }

    public void setPersonid(String personid) {
        this.personid = personid;
    }

    public Date getOutdate() {
        return outdate;
    }

    public void setOutdate(Date outdate) {
        this.outdate = outdate;
    }

    public String getOutreasonid() {
        return outreasonid;
    }

    public void setOutreasonid(String outreasonid) {
        this.outreasonid = outreasonid;
    }

    public Date getDeathdate() {
        return deathdate;
    }

    public void setDeathdate(Date deathdate) {
        this.deathdate = deathdate;
    }

    public String getDeathdesease() {
        return deathdesease;
    }

    public void setDeathdesease(String deathdesease) {
        this.deathdesease = deathdesease;
    }

    public String getDeathreason() {
        return deathreason;
    }

    public void setDeathreason(String deathreason) {
        this.deathreason = deathreason;
    }

    public Patient getRefpatient() {
        return refpatient;
    }

    public void setRefpatient(Patient refpatient) {
        this.refpatient = refpatient;
    }

    @XmlTransient
    public Set<Hivdispdeseases> getHivdispdeseasesSet() {
        return hivdispdeseasesSet;
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    public void setHivdispdeseasesSet(Set<Hivdispdeseases> hivdispdeseasesSet) {
        this.hivdispdeseasesSet = hivdispdeseasesSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivdisp)) {
            return false;
        }
        Hivdisp other = (Hivdisp) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public Set<Hivdispconc> getHivdispconcs() {
        return hivdispconcs;
    }

    public void setHivdispconcs(Set<Hivdispconc> hivdispconcs) {
        this.hivdispconcs = hivdispconcs;
    }

    public Set<Hivdispchekup> getHivdispchekups() {
        return hivdispchekups;
    }

    public void setHivdispchekups(Set<Hivdispchekup> hivdispchekups) {
        this.hivdispchekups = hivdispchekups;
    }

    public Set<Hivdispstage> getHivdispstages() {
        return hivdispstages;
    }

    public void setHivdispstages(Set<Hivdispstage> hivdispstages) {
        this.hivdispstages = hivdispstages;
    }

    public Set<Hivdisphosp> getHivdisphosps() {
        return hivdisphosps;
    }

    public void setHivdisphosps(Set<Hivdisphosp> hivdisphosps) {
        this.hivdisphosps = hivdisphosps;
    }

    public Set<Hivdispchemo> getHivdispchemos() {
        return hivdispchemos;
    }

    public void setHivdispchemos(Set<Hivdispchemo> hivdispchemos) {
        this.hivdispchemos = hivdispchemos;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hivdisp[ id=" + id + " ]";
    }

}
