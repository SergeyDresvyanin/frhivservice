/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "HIVRECIPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivrecipe.findAll", query = "SELECT h FROM Hivrecipe h"),
    @NamedQuery(name = "Hivrecipe.findById", query = "SELECT h FROM Hivrecipe h WHERE h.id = :id"),
    @NamedQuery(name = "Hivrecipe.findByIssuedate", query = "SELECT h FROM Hivrecipe h WHERE h.issuedate = :issuedate"),
    @NamedQuery(name = "Hivrecipe.findByRecipeserial", query = "SELECT h FROM Hivrecipe h WHERE h.recipeserial = :recipeserial"),
    @NamedQuery(name = "Hivrecipe.findByRecipenumber", query = "SELECT h FROM Hivrecipe h WHERE h.recipenumber = :recipenumber"),
    @NamedQuery(name = "Hivrecipe.findByDosecount", query = "SELECT h FROM Hivrecipe h WHERE h.dosecount = :dosecount"),
    @NamedQuery(name = "Hivrecipe.findByDailycount", query = "SELECT h FROM Hivrecipe h WHERE h.dailycount = :dailycount"),
    @NamedQuery(name = "Hivrecipe.findByDaycount", query = "SELECT h FROM Hivrecipe h WHERE h.daycount = :daycount"),
    @NamedQuery(name = "Hivrecipe.findByDeliverydate", query = "SELECT h FROM Hivrecipe h WHERE h.deliverydate = :deliverydate"),
    @NamedQuery(name = "Hivrecipe.findByDoseinpack", query = "SELECT h FROM Hivrecipe h WHERE h.doseinpack = :doseinpack"),
    @NamedQuery(name = "Hivrecipe.findByPackcount", query = "SELECT h FROM Hivrecipe h WHERE h.packcount = :packcount"),
    @NamedQuery(name = "Hivrecipe.findByBegindate", query = "SELECT h FROM Hivrecipe h WHERE h.begindate = :begindate"),
    @NamedQuery(name = "Hivrecipe.findByEnddate", query = "SELECT h FROM Hivrecipe h WHERE h.enddate = :enddate"),
    @NamedQuery(name = "Hivrecipe.findByFactenddate", query = "SELECT h FROM Hivrecipe h WHERE h.factenddate = :factenddate"),
    @NamedQuery(name = "Hivrecipe.findByPersonid", query = "SELECT h FROM Hivrecipe h WHERE h.personid = :personid"),
    @NamedQuery(name = "Hivrecipe.findByMoid", query = "SELECT h FROM Hivrecipe h WHERE h.moid = :moid"),
    @NamedQuery(name = "Hivrecipe.findByPharmacyid", query = "SELECT h FROM Hivrecipe h WHERE h.pharmacyid = :pharmacyid"),
    @NamedQuery(name = "Hivrecipe.findByDrugdosageid", query = "SELECT h FROM Hivrecipe h WHERE h.drugdosageid = :drugdosageid"),
    @NamedQuery(name = "Hivrecipe.findByDosageid", query = "SELECT h FROM Hivrecipe h WHERE h.dosageid = :dosageid"),
    @NamedQuery(name = "Hivrecipe.findByDrugid", query = "SELECT h FROM Hivrecipe h WHERE h.drugid = :drugid"),
    @NamedQuery(name = "Hivrecipe.findByDrugformid", query = "SELECT h FROM Hivrecipe h WHERE h.drugformid = :drugformid"),
    @NamedQuery(name = "Hivrecipe.findByDeseaseid", query = "SELECT h FROM Hivrecipe h WHERE h.deseaseid = :deseaseid"),
    @NamedQuery(name = "Hivrecipe.findByMnnid", query = "SELECT h FROM Hivrecipe h WHERE h.mnnid = :mnnid"),
    @NamedQuery(name = "Hivrecipe.findByCancelreasonid", query = "SELECT h FROM Hivrecipe h WHERE h.cancelreasonid = :cancelreasonid"),
    @NamedQuery(name = "Hivrecipe.findByNote", query = "SELECT h FROM Hivrecipe h WHERE h.note = :note"),
    @NamedQuery(name = "Hivrecipe.findByIstheraphyresistence", query = "SELECT h FROM Hivrecipe h WHERE h.istheraphyresistence = :istheraphyresistence")})
public class Hivrecipe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ISSUEDATE")
    @Temporal(TemporalType.DATE)
    private Date issuedate;
    @Column(name = "RECIPESERIAL")
    private String recipeserial;
    @Column(name = "RECIPENUMBER")
    private String recipenumber;
    @Column(name = "DOSECOUNT")
    private Integer dosecount;
    @Column(name = "DAILYCOUNT")
    private Integer dailycount;
    @Column(name = "DAYCOUNT")
    private Integer daycount;
    @Column(name = "DELIVERYDATE")
    @Temporal(TemporalType.DATE)
    private Date deliverydate;
    @Column(name = "DOSEINPACK")
    private Integer doseinpack;
    @Column(name = "PACKCOUNT")
    private Integer packcount;
    @Column(name = "BEGINDATE")
    @Temporal(TemporalType.DATE)
    private Date begindate;
    @Column(name = "ENDDATE")
    @Temporal(TemporalType.DATE)
    private Date enddate;
    @Column(name = "FACTENDDATE")
    @Temporal(TemporalType.DATE)
    private Date factenddate;
    @Column(name = "PERSONID")
    private String personid;
    @Column(name = "MOID")
    private String moid;
    @Column(name = "PHARMACYID")
    private String pharmacyid;
    @Column(name = "DRUGDOSAGEID")
    private Integer drugdosageid;
    @Column(name = "DOSAGEID")
    private Integer dosageid;
    @Column(name = "DRUGID")
    private Integer drugid;
    @Column(name = "DRUGFORMID")
    private Integer drugformid;
    @Column(name = "DESEASEID")
    private Integer deseaseid;
    @Column(name = "MNNID")
    private Integer mnnid;
    @Column(name = "CANCELREASONID")
    private Integer cancelreasonid;
    @Column(name = "NOTE")
    private String note;
    @Column(name = "ISTHERAPHYRESISTENCE")
    private Integer istheraphyresistence;
    @Column(name = "INFEDREGISTER")
    private short infedregister;

    @JoinColumn(name = "REFPATIENT", referencedColumnName = "ID")
    @ManyToOne
    private Patient refpatient;

    public Hivrecipe() {
    }

    public Hivrecipe(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getIssuedate() {
        return issuedate;
    }

    public void setIssuedate(Date issuedate) {
        this.issuedate = issuedate;
    }

    public String getRecipeserial() {
        return recipeserial;
    }

    public void setRecipeserial(String recipeserial) {
        this.recipeserial = recipeserial;
    }

    public String getRecipenumber() {
        return recipenumber;
    }

    public void setRecipenumber(String recipenumber) {
        this.recipenumber = recipenumber;
    }

    public Integer getDosecount() {
        return dosecount;
    }

    public void setDosecount(Integer dosecount) {
        this.dosecount = dosecount;
    }

    public Integer getDailycount() {
        return dailycount;
    }

    public void setDailycount(Integer dailycount) {
        this.dailycount = dailycount;
    }

    public Integer getDaycount() {
        return daycount;
    }

    public void setDaycount(Integer daycount) {
        this.daycount = daycount;
    }

    public Date getDeliverydate() {
        return deliverydate;
    }

    public void setDeliverydate(Date deliverydate) {
        this.deliverydate = deliverydate;
    }

    public Integer getDoseinpack() {
        return doseinpack;
    }

    public void setDoseinpack(Integer doseinpack) {
        this.doseinpack = doseinpack;
    }

    public Integer getPackcount() {
        return packcount;
    }

    public void setPackcount(Integer packcount) {
        this.packcount = packcount;
    }

    public Date getBegindate() {
        return begindate;
    }

    public void setBegindate(Date begindate) {
        this.begindate = begindate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public Date getFactenddate() {
        return factenddate;
    }

    public void setFactenddate(Date factenddate) {
        this.factenddate = factenddate;
    }

    public String getPersonid() {
        return personid;
    }

    public void setPersonid(String personid) {
        this.personid = personid;
    }

    public String getMoid() {
        return moid;
    }

    public void setMoid(String moid) {
        this.moid = moid;
    }

    public String getPharmacyid() {
        return pharmacyid;
    }

    public void setPharmacyid(String pharmacyid) {
        this.pharmacyid = pharmacyid;
    }

    public Integer getDrugdosageid() {
        return drugdosageid;
    }

    public void setDrugdosageid(Integer drugdosageid) {
        this.drugdosageid = drugdosageid;
    }

    public Integer getDosageid() {
        return dosageid;
    }

    public void setDosageid(Integer dosageid) {
        this.dosageid = dosageid;
    }

    public Integer getDrugid() {
        return drugid;
    }

    public void setDrugid(Integer drugid) {
        this.drugid = drugid;
    }

    public Integer getDrugformid() {
        return drugformid;
    }

    public void setDrugformid(Integer drugformid) {
        this.drugformid = drugformid;
    }

    public Integer getDeseaseid() {
        return deseaseid;
    }

    public void setDeseaseid(Integer deseaseid) {
        this.deseaseid = deseaseid;
    }

    public Integer getMnnid() {
        return mnnid;
    }

    public void setMnnid(Integer mnnid) {
        this.mnnid = mnnid;
    }

    public Integer getCancelreasonid() {
        return cancelreasonid;
    }

    public void setCancelreasonid(Integer cancelreasonid) {
        this.cancelreasonid = cancelreasonid;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getIstheraphyresistence() {
        return istheraphyresistence;
    }

    public void setIstheraphyresistence(Integer istheraphyresistence) {
        this.istheraphyresistence = istheraphyresistence;
    }

    public Patient getRefpatient() {
        return refpatient;
    }

    public void setRefpatient(Patient refpatient) {
        this.refpatient = refpatient;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivrecipe)) {
            return false;
        }
        Hivrecipe other = (Hivrecipe) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hivrecipe[ id=" + id + " ]";
    }

}
