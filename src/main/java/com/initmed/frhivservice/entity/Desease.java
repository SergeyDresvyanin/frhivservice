/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "DESEASE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Desease.findAll", query = "SELECT d FROM Desease d"),
    @NamedQuery(name = "Desease.findById", query = "SELECT d FROM Desease d WHERE d.id = :id"),
    @NamedQuery(name = "Desease.findByName", query = "SELECT d FROM Desease d WHERE d.name = :name"),
    @NamedQuery(name = "Desease.findByMkbcode", query = "SELECT d FROM Desease d WHERE d.mkbcode = :mkbcode")})
public class Desease implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 100)
    @Column(name = "NAME")
    private String name;
    @Column(name = "MKBCODE")
    private Integer mkbcode;
    @JoinColumn(name = "REFHIV", referencedColumnName = "ID")
    @ManyToOne
    private Hiv refhiv;

    public Desease() {
    }

    public Desease(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMkbcode() {
        return mkbcode;
    }

    public void setMkbcode(Integer mkbcode) {
        this.mkbcode = mkbcode;
    }

    public Hiv getRefhiv() {
        return refhiv;
    }

    public void setRefhiv(Hiv refhiv) {
        this.refhiv = refhiv;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Desease)) {
            return false;
        }
        Desease other = (Desease) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Desease[ id=" + id + " ]";
    }
    
}
