/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "HIV")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hiv.findAll", query = "SELECT h FROM Hiv h"),
    @NamedQuery(name = "Hiv.findById", query = "SELECT h FROM Hiv h WHERE h.id = :id"),
    @NamedQuery(name = "Hiv.findByEpidcode", query = "SELECT h FROM Hiv h WHERE h.epidcode = :epidcode"),
    @NamedQuery(name = "Hiv.findByMoid", query = "SELECT h FROM Hiv h WHERE h.moid = :moid"),
    @NamedQuery(name = "Hiv.findByPolictypeid", query = "SELECT h FROM Hiv h WHERE h.polictypeid = :polictypeid"),
    @NamedQuery(name = "Hiv.findByPolicserial", query = "SELECT h FROM Hiv h WHERE h.policserial = :policserial"),
    @NamedQuery(name = "Hiv.findByPolicnumber", query = "SELECT h FROM Hiv h WHERE h.policnumber = :policnumber"),
    @NamedQuery(name = "Hiv.findByImccode", query = "SELECT h FROM Hiv h WHERE h.imccode = :imccode"),
    @NamedQuery(name = "Hiv.findByCategoryid", query = "SELECT h FROM Hiv h WHERE h.categoryid = :categoryid"),
    @NamedQuery(name = "Hiv.findByNewborn", query = "SELECT h FROM Hiv h WHERE h.newborn = :newborn"),
    @NamedQuery(name = "Hiv.findByApproveddate", query = "SELECT h FROM Hiv h WHERE h.approveddate = :approveddate"),
    @NamedQuery(name = "Hiv.findByIncludedate", query = "SELECT h FROM Hiv h WHERE h.includedate = :includedate"),
    @NamedQuery(name = "Hiv.findByExcludedate", query = "SELECT h FROM Hiv h WHERE h.excludedate = :excludedate"),
    @NamedQuery(name = "Hiv.findByAutopsy", query = "SELECT h FROM Hiv h WHERE h.autopsy = :autopsy"),
    @NamedQuery(name = "Hiv.findByHivdate", query = "SELECT h FROM Hiv h WHERE h.hivdate = :hivdate"),
    @NamedQuery(name = "Hiv.findByBeginningmoid", query = "SELECT h FROM Hiv h WHERE h.beginningmoid = :beginningmoid"),
    @NamedQuery(name = "Hiv.findByInfedregister", query = "SELECT h FROM Hiv h WHERE h.infedregister = :infedregister"),
    @NamedQuery(name = "Hiv.findByMisid", query = "SELECT h FROM Hiv h WHERE h.misid = :misid")})
public class Hiv implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 100)
    @Column(name = "EPIDCODE")
    private String epidcode;
    @Size(max = 50)
    @Column(name = "MOID")
    private String moid;
    @Column(name = "POLICTYPEID")
    private Integer polictypeid;
    @Size(max = 6)
    @Column(name = "POLICSERIAL")
    private String policserial;
    @Size(max = 16)
    @Column(name = "POLICNUMBER")
    private String policnumber;
    @Column(name = "IMCCODE")
    private Integer imccode;
    @Column(name = "CATEGORYID")
    private Integer categoryid;
    @Column(name = "NEWBORN")
    private Integer newborn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APPROVEDDATE")
    @Temporal(TemporalType.DATE)
    private Date approveddate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INCLUDEDATE")
    @Temporal(TemporalType.DATE)
    private Date includedate;
    @Column(name = "EXCLUDEDATE")
    @Temporal(TemporalType.DATE)
    private Date excludedate;
    @Column(name = "AUTOPSY")
    private Integer autopsy;
    @Column(name = "HIVDATE")
    @Temporal(TemporalType.DATE)
    private Date hivdate;
    @Size(max = 50)
    @Column(name = "BEGINNINGMOID")
    private String beginningmoid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INFEDREGISTER")
    private short infedregister;
    @Column(name = "MISID")
    private Integer misid;
    @OneToMany(mappedBy = "refhiv")
    private Set<Pathologies> pathologiesSet;
    @OneToMany(mappedBy = "refhiv")
    private Set<Revealedpathologies> revealedpathologiesSet;
    @OneToMany(mappedBy = "refhiv")
    private Set<Contingents> contingentsSet;
    @JoinColumn(name = "REFDEATHREASON", referencedColumnName = "ID")
    @ManyToOne
    private Deathreason refdeathreason;
    @JoinColumn(name = "REFEXCLUDEREASONID", referencedColumnName = "ID")
    @ManyToOne
    private Excludereason refexcludereasonid;
    @JoinColumn(name = "REFINFECTIONPATHID", referencedColumnName = "ID")
    @ManyToOne
    private Infectionpath refinfectionpathid;
    @JoinColumn(name = "REFPATIENT", referencedColumnName = "ID")
    @ManyToOne
    private Patient refpatient;
    @OneToMany(mappedBy = "refhiv")
    private Set<Desease> deseaseSet;

    public Hiv() {
    }

    public Hiv(Integer id) {
        this.id = id;
    }

    public Hiv(Integer id, Date approveddate, Date includedate, short infedregister) {
        this.id = id;
        this.approveddate = approveddate;
        this.includedate = includedate;
        this.infedregister = infedregister;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEpidcode() {
        return epidcode;
    }

    public void setEpidcode(String epidcode) {
        this.epidcode = epidcode;
    }

    public String getMoid() {
        return moid;
    }

    public void setMoid(String moid) {
        this.moid = moid;
    }

    public Integer getPolictypeid() {
        return polictypeid;
    }

    public void setPolictypeid(Integer polictypeid) {
        this.polictypeid = polictypeid;
    }

    public String getPolicserial() {
        return policserial;
    }

    public void setPolicserial(String policserial) {
        this.policserial = policserial;
    }

    public String getPolicnumber() {
        return policnumber;
    }

    public void setPolicnumber(String policnumber) {
        this.policnumber = policnumber;
    }

    public Integer getImccode() {
        return imccode;
    }

    public void setImccode(Integer imccode) {
        this.imccode = imccode;
    }

    public Integer getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public Integer getNewborn() {
        return newborn;
    }

    public void setNewborn(Integer newborn) {
        this.newborn = newborn;
    }

    public Date getApproveddate() {
        return approveddate;
    }

    public void setApproveddate(Date approveddate) {
        this.approveddate = approveddate;
    }

    public Date getIncludedate() {
        return includedate;
    }

    public void setIncludedate(Date includedate) {
        this.includedate = includedate;
    }

    public Date getExcludedate() {
        return excludedate;
    }

    public void setExcludedate(Date excludedate) {
        this.excludedate = excludedate;
    }

    public Integer getAutopsy() {
        return autopsy;
    }

    public void setAutopsy(Integer autopsy) {
        this.autopsy = autopsy;
    }

    public Date getHivdate() {
        return hivdate;
    }

    public void setHivdate(Date hivdate) {
        this.hivdate = hivdate;
    }

    public String getBeginningmoid() {
        return beginningmoid;
    }

    public void setBeginningmoid(String beginningmoid) {
        this.beginningmoid = beginningmoid;
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    public Integer getMisid() {
        return misid;
    }

    public void setMisid(Integer misid) {
        this.misid = misid;
    }

    @XmlTransient
    public Set<Pathologies> getPathologiesSet() {
        return pathologiesSet;
    }

    public void setPathologiesSet(Set<Pathologies> pathologiesSet) {
        this.pathologiesSet = pathologiesSet;
    }

    @XmlTransient
    public Set<Revealedpathologies> getRevealedpathologiesSet() {
        return revealedpathologiesSet;
    }

    public void setRevealedpathologiesSet(Set<Revealedpathologies> revealedpathologiesSet) {
        this.revealedpathologiesSet = revealedpathologiesSet;
    }

    @XmlTransient
    public Set<Contingents> getContingentsSet() {
        return contingentsSet;
    }

    public void setContingentsSet(Set<Contingents> contingentsSet) {
        this.contingentsSet = contingentsSet;
    }

    public Deathreason getRefdeathreason() {
        return refdeathreason;
    }

    public void setRefdeathreason(Deathreason refdeathreason) {
        this.refdeathreason = refdeathreason;
    }

    public Excludereason getRefexcludereasonid() {
        return refexcludereasonid;
    }

    public void setRefexcludereasonid(Excludereason refexcludereasonid) {
        this.refexcludereasonid = refexcludereasonid;
    }

    public Infectionpath getRefinfectionpathid() {
        return refinfectionpathid;
    }

    public void setRefinfectionpathid(Infectionpath refinfectionpathid) {
        this.refinfectionpathid = refinfectionpathid;
    }

    public Patient getRefpatient() {
        return refpatient;
    }

    public void setRefpatient(Patient refpatient) {
        this.refpatient = refpatient;
    }

    @XmlTransient
    public Set<Desease> getDeseaseSet() {
        return deseaseSet;
    }

    public void setDeseaseSet(Set<Desease> deseaseSet) {
        this.deseaseSet = deseaseSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hiv)) {
            return false;
        }
        Hiv other = (Hiv) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hiv[ id=" + id + " ]";
    }
    
}
