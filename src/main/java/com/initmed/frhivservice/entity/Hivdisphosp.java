/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIVDISPHOSP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivdisphosp.findAll", query = "SELECT h FROM Hivdisphosp h")})
public class Hivdisphosp implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "MOOID")
    private String mooid;
    @Column(name = "BEGINDATE")
    @Temporal(TemporalType.DATE)
    private Date begindate;
    @Column(name = "INFEDREGISTER")
    private short infedregister;
    @Column(name = "ENDDATE")
    @Temporal(TemporalType.DATE)
    private Date enddate;
    @Size(max = 20)
    @Column(name = "FINALMKB")
    private String finalmkb;
    @JoinColumn(name = "REFHIVDISP", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Hivdisp hivdisp;

    public Hivdisphosp() {
    }

    public Hivdisphosp(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMooid() {
        return mooid;
    }

    public void setMooid(String mooid) {
        this.mooid = mooid;
    }

    public Date getBegindate() {
        return begindate;
    }

    public void setBegindate(Date begindate) {
        this.begindate = begindate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    public String getFinalmkb() {
        return finalmkb;
    }

    public void setFinalmkb(String finalmkb) {
        this.finalmkb = finalmkb;
    }

    public Hivdisp getHivdisp() {
        return hivdisp;
    }

    public void setHivdisp(Hivdisp hivdisp) {
        this.hivdisp = hivdisp;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivdisphosp)) {
            return false;
        }
        Hivdisphosp other = (Hivdisphosp) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hivdisphosp[ id=" + id + " ]";
    }

}
