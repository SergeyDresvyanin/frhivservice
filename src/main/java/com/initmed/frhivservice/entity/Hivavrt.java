/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "HIVAVRT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivavrt.findAll", query = "SELECT h FROM Hivavrt h")})
public class Hivavrt implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "BEGINDATE")
    @Temporal(TemporalType.DATE)
    private Date begindate;
    @Column(name = "ENDDATE")
    @Temporal(TemporalType.DATE)
    private Date enddate;
    @Size(max = 50)
    @Column(name = "BEGINPERSONID")
    private String beginpersonid;
    @Size(max = 50)
    @Column(name = "ENDPERSONID")
    private String endpersonid;
    @Column(name = "ENDREASONID")
    private Integer endreasonid;
    @Size(max = 512)
    @Column(name = "ENDREASONDETAIL")
    private String endreasondetail;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISFIRSTTREATMENT")
    private short isfirsttreatment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISNEEDCHANGETREATMENT")
    private short isneedchangetreatment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INFEDREGISTER")
    private short infedregister;
    @JoinColumn(name = "REFPATIENT", referencedColumnName = "ID")
    @ManyToOne
    private Patient patient;
    @OneToMany(mappedBy = "refhivart")
    private Set<Hivavrtdrugs> hivavrtdrugsSet;

    public Hivavrt() {
    }

    public Hivavrt(Integer id) {
        this.id = id;
    }

    public Hivavrt(Integer id, short isfirsttreatment, short isneedchangetreatment, short infedregister) {
        this.id = id;
        this.isfirsttreatment = isfirsttreatment;
        this.isneedchangetreatment = isneedchangetreatment;
        this.infedregister = infedregister;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getBegindate() {
        return begindate;
    }

    public void setBegindate(Date begindate) {
        this.begindate = begindate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getBeginpersonid() {
        return beginpersonid;
    }

    public void setBeginpersonid(String beginpersonid) {
        this.beginpersonid = beginpersonid;
    }

    public String getEndpersonid() {
        return endpersonid;
    }

    public void setEndpersonid(String endpersonid) {
        this.endpersonid = endpersonid;
    }

    public Integer getEndreasonid() {
        return endreasonid;
    }

    public void setEndreasonid(Integer endreasonid) {
        this.endreasonid = endreasonid;
    }

    public String getEndreasondetail() {
        return endreasondetail;
    }

    public void setEndreasondetail(String endreasondetail) {
        this.endreasondetail = endreasondetail;
    }

    public short getIsfirsttreatment() {
        return isfirsttreatment;
    }

    public void setIsfirsttreatment(short isfirsttreatment) {
        this.isfirsttreatment = isfirsttreatment;
    }

    public short getIsneedchangetreatment() {
        return isneedchangetreatment;
    }

    public void setIsneedchangetreatment(short isneedchangetreatment) {
        this.isneedchangetreatment = isneedchangetreatment;
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    @XmlTransient
    public Set<Hivavrtdrugs> getHivavrtdrugsSet() {
        return hivavrtdrugsSet;
    }

    public void setHivavrtdrugsSet(Set<Hivavrtdrugs> hivavrtdrugsSet) {
        this.hivavrtdrugsSet = hivavrtdrugsSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivavrt)) {
            return false;
        }
        Hivavrt other = (Hivavrt) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hivavrt[ id=" + id + " ]";
    }
}
