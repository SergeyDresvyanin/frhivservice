/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Set;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "EXAMMARKS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Exammarks.findAll", query = "SELECT e FROM Exammarks e"),
    @NamedQuery(name = "Exammarks.findById", query = "SELECT e FROM Exammarks e WHERE e.id = :id"),
    @NamedQuery(name = "Exammarks.findByRefexammarks", query = "SELECT e FROM Exammarks e WHERE e.refexammarks = :refexammarks"),
    @NamedQuery(name = "Exammarks.findByName", query = "SELECT e FROM Exammarks e WHERE e.name = :name"),
    @NamedQuery(name = "Exammarks.findByMinage", query = "SELECT e FROM Exammarks e WHERE e.minage = :minage"),
    @NamedQuery(name = "Exammarks.findByMaxage", query = "SELECT e FROM Exammarks e WHERE e.maxage = :maxage"),
    @NamedQuery(name = "Exammarks.findByMinval", query = "SELECT e FROM Exammarks e WHERE e.minval = :minval"),
    @NamedQuery(name = "Exammarks.findByMaxval", query = "SELECT e FROM Exammarks e WHERE e.maxval = :maxval")})
public class Exammarks implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "REFEXAMMARKS")
    private Integer refexammarks;
    @Size(max = 50)
    @Column(name = "NAME")
    private String name;
    @Column(name = "MINAGE")
    private Integer minage;
    @Column(name = "MAXAGE")
    private Integer maxage;
    @Column(name = "MINVAL")
    private Integer minval;
    @Column(name = "MAXVAL")
    private Integer maxval;

    public Exammarks() {
    }

    public Exammarks(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRefexammarks() {
        return refexammarks;
    }

    public void setRefexammarks(Integer refexammarks) {
        this.refexammarks = refexammarks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMinage() {
        return minage;
    }

    public void setMinage(Integer minage) {
        this.minage = minage;
    }

    public Integer getMaxage() {
        return maxage;
    }

    public void setMaxage(Integer maxage) {
        this.maxage = maxage;
    }

    public Integer getMinval() {
        return minval;
    }

    public void setMinval(Integer minval) {
        this.minval = minval;
    }

    public Integer getMaxval() {
        return maxval;
    }

    public void setMaxval(Integer maxval) {
        this.maxval = maxval;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Exammarks)) {
            return false;
        }
        Exammarks other = (Exammarks) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Exammarks[ id=" + id + " ]";
    }

}
