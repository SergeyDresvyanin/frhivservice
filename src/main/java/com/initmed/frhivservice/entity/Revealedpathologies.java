/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "REVEALEDPATHOLOGIES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Revealedpathologies.findAll", query = "SELECT r FROM Revealedpathologies r"),
    @NamedQuery(name = "Revealedpathologies.findById", query = "SELECT r FROM Revealedpathologies r WHERE r.id = :id"),
    @NamedQuery(name = "Revealedpathologies.findByMkb", query = "SELECT r FROM Revealedpathologies r WHERE r.mkb = :mkb"),
    @NamedQuery(name = "Revealedpathologies.findByMkbid", query = "SELECT r FROM Revealedpathologies r WHERE r.mkbid = :mkbid")})
public class Revealedpathologies implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 5)
    @Column(name = "MKB")
    private String mkb;
    @Column(name = "MKBID")
    private Integer mkbid;
    @JoinColumn(name = "REFHIV", referencedColumnName = "ID")
    @ManyToOne
    private Hiv refhiv;

    public Revealedpathologies() {
    }

    public Revealedpathologies(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMkb() {
        return mkb;
    }

    public void setMkb(String mkb) {
        this.mkb = mkb;
    }

    public Integer getMkbid() {
        return mkbid;
    }

    public void setMkbid(Integer mkbid) {
        this.mkbid = mkbid;
    }

    public Hiv getRefhiv() {
        return refhiv;
    }

    public void setRefhiv(Hiv refhiv) {
        this.refhiv = refhiv;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Revealedpathologies)) {
            return false;
        }
        Revealedpathologies other = (Revealedpathologies) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Revealedpathologies[ id=" + id + " ]";
    }
    
}
