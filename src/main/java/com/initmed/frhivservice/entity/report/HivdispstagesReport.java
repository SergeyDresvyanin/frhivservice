/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity.report;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Temporal;

/**
 *
 * @author sd199
 */
@Entity
public class HivdispstagesReport implements Serializable {

    @Id
    private Integer id;
    @Column(name = "SNILS")
    private String snils;
    @Column(name = "SPECNUM")
    private String specnum;
    @Column(name = "MESSAGEDETAIL")
    private String messageDetail;
    @Column(name = "PERSONID")
    private String docsnils;
    @Column(name = "name")
    private String stage;
    @Column(name = "stagedate")
    @Temporal(jakarta.persistence.TemporalType.DATE)
    private Date stagedate;

    public HivdispstagesReport() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public String getSpecnum() {
        return specnum;
    }

    public void setSpecnum(String specnum) {
        this.specnum = specnum;
    }

    public String getMessageDetail() {
        return messageDetail;
    }

    public void setMessageDetail(String messageDetail) {
        this.messageDetail = messageDetail;
    }

    public Date getStagedate() {
        return stagedate;
    }

    public void setStagedate(Date stagedate) {
        this.stagedate = stagedate;
    }

    public String getDocsnils() {
        return docsnils;
    }

    public void setDocsnils(String docsnils) {
        this.docsnils = docsnils;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

}
