/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity.report;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

/**
 *
 * @author sd199
 */
@Entity
public class HivReport implements Serializable {

    @Id
    private Integer id;
    @Column(name = "SNILS")
    private String snils;
    @Column(name = "SPECNUM")
    private String specnum;

    public HivReport() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public String getSpecnum() {
        return specnum;
    }

    public void setSpecnum(String specnum) {
        this.specnum = specnum;
    }

}
