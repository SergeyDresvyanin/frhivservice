/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity.report;

import java.io.Serializable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

/**
 *
 * @author sergeydresvyanin
 */
@Entity
public class PatientErorReport implements Serializable {

    @Id
    private Integer id;
    @Column(name = "SNILS")
    private String snils;
    @Column(name = "SPECNUM")
    private String specnum;
    @Column(name = "registrynumber")
    private String registrynumber;
    @Column(name = "msgp")
    private String msgp;
    @Column(name = "msgh")
    private String msgh;
    @Column(name = "msgd")
    private String msgd;

    public PatientErorReport() {
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public String getSpecnum() {
        return specnum;
    }

    public void setSpecnum(String specnum) {
        this.specnum = specnum;
    }

    public String getRegistrynumber() {
        return registrynumber;
    }

    public void setRegistrynumber(String registrynumber) {
        this.registrynumber = registrynumber;
    }

    public String getMsgp() {
        return msgp;
    }

    public void setMsgp(String msgp) {
        this.msgp = msgp;
    }

    public String getMsgh() {
        return msgh;
    }

    public void setMsgh(String msgh) {
        this.msgh = msgh;
    }

    public String getMsgd() {
        return msgd;
    }

    public void setMsgd(String msgd) {
        this.msgd = msgd;
    }
    
    

}
