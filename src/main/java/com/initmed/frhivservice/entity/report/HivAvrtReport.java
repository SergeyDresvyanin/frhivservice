/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity.report;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

/**
 *
 * @author sd199
 */
@Entity
public class HivAvrtReport implements Serializable {

    @Id
    private Integer id;
    @Column(name = "SNILS")
    private String snils;
    @Column(name = "SPECNUM")
    private String specnum;
    @Column(name = "MESSAGEDETAIL")
    private String messageDetail;
    @Column(name = "beginpersonid")
    private String personId;
    @Column(name = "DATE")
    private Date visitDate;
    @Column(name = "TRADENAME")
    private String tradeName;
    @Column(name = "FORM")
    private String form;
    @Column(name = "DOSE")
    private String dose;
    
    public HivAvrtReport() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public String getSpecnum() {
        return specnum;
    }

    public void setSpecnum(String specnum) {
        this.specnum = specnum;
    }

    public String getMessageDetail() {
        return messageDetail;
    }

    public void setMessageDetail(String messageDetail) {
        this.messageDetail = messageDetail;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    

}
