/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import com.initmed.frhivservice.frdatamodel.HivDispcardVisits;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "PATIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Patient.findAll", query = "SELECT p FROM Patient p"),
    @NamedQuery(name = "Patient.findById", query = "SELECT p FROM Patient p WHERE p.id = :id"),
    @NamedQuery(name = "Patient.findByFirstname", query = "SELECT p FROM Patient p WHERE p.firstname = :firstname"),
    @NamedQuery(name = "Patient.findByLastname", query = "SELECT p FROM Patient p WHERE p.lastname = :lastname"),
    @NamedQuery(name = "Patient.findByPatronymic", query = "SELECT p FROM Patient p WHERE p.patronymic = :patronymic"),
    @NamedQuery(name = "Patient.findByBirthdate", query = "SELECT p FROM Patient p WHERE p.birthdate = :birthdate"),
    @NamedQuery(name = "Patient.findByGender", query = "SELECT p FROM Patient p WHERE p.gender = :gender"),
    @NamedQuery(name = "Patient.findByBirthlastname", query = "SELECT p FROM Patient p WHERE p.birthlastname = :birthlastname"),
    @NamedQuery(name = "Patient.findByBirthplace", query = "SELECT p FROM Patient p WHERE p.birthplace = :birthplace"),
    @NamedQuery(name = "Patient.findByWorkplace", query = "SELECT p FROM Patient p WHERE p.workplace = :workplace"),
    @NamedQuery(name = "Patient.findByRegistrynumber", query = "SELECT p FROM Patient p WHERE p.registrynumber = :registrynumber"),
    @NamedQuery(name = "Patient.findByCreatedate", query = "SELECT p FROM Patient p WHERE p.createdate = :createdate"),
    @NamedQuery(name = "Patient.findByInfedregister", query = "SELECT p FROM Patient p WHERE p.infedregister = :infedregister"),
    @NamedQuery(name = "Patient.findByMisid", query = "SELECT p FROM Patient p WHERE p.misid = :misid"),
    @NamedQuery(name = "Patient.findBySnils", query = "SELECT p FROM Patient p WHERE p.snils = :snils")})
public class Patient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(min = 1, max = 100)
    @Column(name = "FIRSTNAME")
    private String firstname;
    @Size(min = 1, max = 100)
    @Column(name = "LASTNAME")
    private String lastname;
    @Size(min = 1, max = 100)
    @Column(name = "PATRONYMIC")
    private String patronymic;
    @Column(name = "BIRTHDATE")
    @Temporal(TemporalType.DATE)
    private Date birthdate;
    @Column(name = "GENDER")
    private Short gender;
    @Size(max = 100)
    @Column(name = "BIRTHLASTNAME")
    private String birthlastname;
    @Size(max = 255)
    @Column(name = "BIRTHPLACE")
    private String birthplace;
    @Size(max = 100)
    @Column(name = "WORKPLACE")
    private String workplace;
    @Column(name = "REGISTRYNUMBER")
    private String registrynumber;
    @Column(name = "CREATEDATE")
    @Temporal(TemporalType.DATE)
    private Date createdate;
    @Column(name = "INFEDREGISTER")
    private short infedregister;
    @Column(name = "MISID")
    private Integer misid;
    @Column(name = "SNILS")
    private String snils;
    @Column(name = "phone")
    private String phone;
    @OneToMany(mappedBy = "refpatient")
    private Set<Hivrecipe> hivrecipes;
    @OneToMany(mappedBy = "refpatient")
    private Set<Hivaddress> hivaddressSet;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refpatient")
    private Set<Hivepid> hivepidSet;
    @OneToMany(mappedBy = "refpatient")
    private Set<Hivdispexam> hivdispexamSet;
    @OneToMany(mappedBy = "refpatient")
    private Set<Hiv> hivSet;
    @OneToMany(mappedBy = "refpatient")
    private Set<Hivdisp> hivdispsSet;
    @OneToMany(mappedBy = "refpatient")
    private Set<Relative> relativeSet;
    @OneToMany(mappedBy = "refpatient")
    private Set<Documents> documentses;

    @JoinColumn(name = "REFCITIZENSHIPID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Citizenship refcitizenshipid;
    @JoinColumn(name = "REFDECREEDGROUPID", referencedColumnName = "ID")
    @ManyToOne
    private Decreedgroup refdecreedgroupid;
    @JoinColumn(name = "REFNORESIDENTSTATUSID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Noresidentstatus refnoresidentstatusid;
    @JoinColumn(name = "REFSOCSTATUSID", referencedColumnName = "ID")
    @ManyToOne
    private Socstatus refsocstatusid;
    @OneToMany(mappedBy = "patient")
    private Set<Hivavrt> hivavrts;

    @OneToMany(mappedBy = "refpatient")
    private Set<Hivdispvisits> hivDispcardVisitses;

    public Patient() {
    }

    public Patient(Integer id) {
        this.id = id;
    }

    public Patient(Integer id, String firstname, String lastname, String patronymic, Date birthdate, short infedregister) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.patronymic = patronymic;
        this.birthdate = birthdate;
        this.infedregister = infedregister;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Short getGender() {
        return gender;
    }

    public void setGender(Short gender) {
        this.gender = gender;
    }

    public String getBirthlastname() {
        return birthlastname;
    }

    public void setBirthlastname(String birthlastname) {
        this.birthlastname = birthlastname;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public String getRegistrynumber() {
        return registrynumber;
    }

    public void setRegistrynumber(String registrynumber) {
        this.registrynumber = registrynumber;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    public Integer getMisid() {
        return misid;
    }

    public void setMisid(Integer misid) {
        this.misid = misid;
    }

    public String getSnils() {
        return snils;
    }

    public void setSnils(String snils) {
        this.snils = snils;
    }

    public Set<Hivrecipe> getHivrecipes() {
        return hivrecipes;
    }

    public void setHivrecipes(Set<Hivrecipe> hivrecipes) {
        this.hivrecipes = hivrecipes;
    }

    public Set<Hivavrt> getHivavrts() {
        return hivavrts;
    }

    public void setHivavrts(Set<Hivavrt> hivavrts) {
        this.hivavrts = hivavrts;
    }

    @XmlTransient
    public Set<Hivaddress> getHivaddressSet() {
        return hivaddressSet;
    }

    public Set<Documents> getDocumentses() {
        return documentses;
    }

    public void setDocumentses(Set<Documents> documentses) {
        this.documentses = documentses;
    }

    public void setHivaddressSet(Set<Hivaddress> hivaddressSet) {
        this.hivaddressSet = hivaddressSet;
    }

    @XmlTransient
    public Set<Hivepid> getHivepidSet() {
        return hivepidSet;
    }

    public void setHivepidSet(Set<Hivepid> hivepidSet) {
        this.hivepidSet = hivepidSet;
    }

    @XmlTransient
    public Set<Hivdispexam> getHivdispexamSet() {
        return hivdispexamSet;
    }

    public void setHivdispexamSet(Set<Hivdispexam> hivdispexamSet) {
        this.hivdispexamSet = hivdispexamSet;
    }

    @XmlTransient
    public Set<Hiv> getHivSet() {
        return hivSet;
    }

    public void setHivSet(Set<Hiv> hivSet) {
        this.hivSet = hivSet;
    }

    public Citizenship getRefcitizenshipid() {
        return refcitizenshipid;
    }

    public void setRefcitizenshipid(Citizenship refcitizenshipid) {
        this.refcitizenshipid = refcitizenshipid;
    }

    public Decreedgroup getRefdecreedgroupid() {
        return refdecreedgroupid;
    }

    public void setRefdecreedgroupid(Decreedgroup refdecreedgroupid) {
        this.refdecreedgroupid = refdecreedgroupid;
    }

    public Noresidentstatus getRefnoresidentstatusid() {
        return refnoresidentstatusid;
    }

    public void setRefnoresidentstatusid(Noresidentstatus refnoresidentstatusid) {
        this.refnoresidentstatusid = refnoresidentstatusid;
    }

    public Socstatus getRefsocstatusid() {
        return refsocstatusid;
    }

    public void setRefsocstatusid(Socstatus refsocstatusid) {
        this.refsocstatusid = refsocstatusid;
    }

    public Set<Hivdispvisits> getHivDispcardVisitses() {
        return hivDispcardVisitses;
    }

    public void setHivDispcardVisitses(Set<Hivdispvisits> hivDispcardVisitses) {
        this.hivDispcardVisitses = hivDispcardVisitses;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public Set<Relative> getRelativeSet() {
        return relativeSet;
    }

    public void setRelativeSet(Set<Relative> relativeSet) {
        this.relativeSet = relativeSet;
    }

    public Set<Hivdisp> getHivdispsSet() {
        return hivdispsSet;
    }

    public void setHivdispsSet(Set<Hivdisp> hivdispsSet) {
        this.hivdispsSet = hivdispsSet;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Patient)) {
            return false;
        }
        Patient other = (Patient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Patient[ id=" + id + " ]";
    }

}
