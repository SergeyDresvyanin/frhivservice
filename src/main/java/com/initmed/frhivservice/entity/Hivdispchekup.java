/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIVDISPCHEKUP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivdispchekup.findAll", query = "SELECT h FROM Hivdispchekup h")})
public class Hivdispchekup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "CHECKUPID")
    private Integer checkupid;
    @Column(name = "CHECKUPDATE")
    @Temporal(TemporalType.DATE)
    private Date checkupdate;
    @Column(name = "RESULT")
    private Integer result;
    @Size(max = 20)
    @Column(name = "DESEASE")
    private String desease;
    @JoinColumn(name = "REFHIVDISP", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Hivdisp hivdisp;
    @Column(name = "INFEDREGISTER")
    private short infedregister;

    public Hivdispchekup() {
    }

    public Hivdispchekup(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCheckupid() {
        return checkupid;
    }

    public void setCheckupid(Integer checkupid) {
        this.checkupid = checkupid;
    }

    public Date getCheckupdate() {
        return checkupdate;
    }

    public void setCheckupdate(Date checkupdate) {
        this.checkupdate = checkupdate;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getDesease() {
        return desease;
    }

    public void setDesease(String desease) {
        this.desease = desease;
    }

    public Hivdisp getHivdisp() {
        return hivdisp;
    }

    public void setHivdisp(Hivdisp hivdisp) {
        this.hivdisp = hivdisp;
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivdispchekup)) {
            return false;
        }
        Hivdispchekup other = (Hivdispchekup) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hivdispchekup[ id=" + id + " ]";
    }

}
