/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "HIVEPID")
@XmlRootElement
public class Hivepid implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "INFEDREGISTER")
    private short infedregister;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "PERSONID")
    private String personid;
    @Column(name = "PRETESTDATE")
    @Temporal(TemporalType.DATE)
    private Date pretestdate;
    @Column(name = "POSTTESTDATE")
    @Temporal(TemporalType.DATE)
    private Date posttestdate;
    @Column(name = "DETECTCURCID")
    private Integer detectcurcid;
    @Column(name = "INFECTIONPERIODFROM")
    @Temporal(TemporalType.DATE)
    private Date infectionperiodfrom;
    @Column(name = "INFECTIONPERIODTO")
    @Temporal(TemporalType.DATE)
    private Date infectionperiodto;
    @Column(name = "NOTIFYDATE")
    @Temporal(TemporalType.DATE)
    private Date notifydate;
    @Column(name = "CONCLUSTIONDATE")
    @Temporal(TemporalType.DATE)
    private Date conclustiondate;
    @JoinColumn(name = "REFPATIENT", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Patient refpatient;
    @Column(name = "MOID")
    private String moId;
    @Column(name = "infpath")
    private Integer infpath;
    @Column(name = "needsupdate")
    private boolean needsupdate;
     @OneToMany(mappedBy = "refHivepid")
    private Set<HivepidContacts> hivepidContactses;

    public Hivepid() {
    }

    public Hivepid(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPersonid() {
        return personid;
    }

    public void setPersonid(String personid) {
        this.personid = personid;
    }

    public Date getPretestdate() {
        return pretestdate;
    }

    public void setPretestdate(Date pretestdate) {
        this.pretestdate = pretestdate;
    }

    public Date getPosttestdate() {
        return posttestdate;
    }

    public void setPosttestdate(Date posttestdate) {
        this.posttestdate = posttestdate;
    }

    public Integer getDetectcurcid() {
        return detectcurcid;
    }

    public void setDetectcurcid(Integer detectcurcid) {
        this.detectcurcid = detectcurcid;
    }

    public Date getInfectionperiodfrom() {
        return infectionperiodfrom;
    }

    public void setInfectionperiodfrom(Date infectionperiodfrom) {
        this.infectionperiodfrom = infectionperiodfrom;
    }

    public Date getInfectionperiodto() {
        return infectionperiodto;
    }

    public void setInfectionperiodto(Date infectionperiodto) {
        this.infectionperiodto = infectionperiodto;
    }

    public Date getNotifydate() {
        return notifydate;
    }

    public void setNotifydate(Date notifydate) {
        this.notifydate = notifydate;
    }

    public Date getConclustiondate() {
        return conclustiondate;
    }

    public void setConclustiondate(Date conclustiondate) {
        this.conclustiondate = conclustiondate;
    }

    public Patient getRefpatient() {
        return refpatient;
    }

    public void setRefpatient(Patient refpatient) {
        this.refpatient = refpatient;
    }

    public String getMoId() {
        return moId;
    }

    public void setMoId(String moId) {
        this.moId = moId;
    }

    public Integer getInfpath() {
        return infpath;
    }

    public void setInfpath(Integer infpath) {
        this.infpath = infpath;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivepid)) {
            return false;
        }
        Hivepid other = (Hivepid) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hivepid[ id=" + id + " ]";
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    public boolean isNeedsupdate() {
        return needsupdate;
    }

    public void setNeedsupdate(boolean b) {
        this.needsupdate = b;
    }

    public Set<HivepidContacts> getHivepidContactses() {
        return hivepidContactses;
    }

    public void setHivepidContactses(Set<HivepidContacts> hivepidContactses) {
        this.hivepidContactses = hivepidContactses;
    }
}
