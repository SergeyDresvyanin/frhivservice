/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "DOCUMENTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Documents.findAll", query = "SELECT d FROM Documents d"),
    @NamedQuery(name = "Documents.findById", query = "SELECT d FROM Documents d WHERE d.id = :id"),
    @NamedQuery(name = "Documents.findByRefpatient", query = "SELECT d FROM Documents d WHERE d.refpatient = :refpatient"),
    @NamedQuery(name = "Documents.findBySerial", query = "SELECT d FROM Documents d WHERE d.serial = :serial"),
    @NamedQuery(name = "Documents.findByNum", query = "SELECT d FROM Documents d WHERE d.num = :num"),
    @NamedQuery(name = "Documents.findByPassdate", query = "SELECT d FROM Documents d WHERE d.passdate = :passdate"),
    @NamedQuery(name = "Documents.findByInfedregister", query = "SELECT d FROM Documents d WHERE d.infedregister = :infedregister"),
    @NamedQuery(name = "Documents.findByPassorg", query = "SELECT d FROM Documents d WHERE d.passorg = :passorg")})
public class Documents implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "REFPATIENT", referencedColumnName = "ID")
    private Patient refpatient;
    @Size(max = 10)
    @Column(name = "SERIAL")
    private String serial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "NUM")
    private String num;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PASSDATE")
    @Temporal(TemporalType.DATE)
    private Date passdate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INFEDREGISTER")
    private short infedregister;
    @Size(max = 200)
    @Column(name = "PASSORG")
    private String passorg;

    public Documents() {
    }

    public Documents(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public Date getPassdate() {
        return passdate;
    }

    public void setPassdate(Date passdate) {
        this.passdate = passdate;
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    public String getPassorg() {
        return passorg;
    }

    public void setPassorg(String passorg) {
        this.passorg = passorg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public Patient getRefpatient() {
        return refpatient;
    }

    public void setRefpatient(Patient refpatient) {
        this.refpatient = refpatient;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Documents)) {
            return false;
        }
        Documents other = (Documents) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Documents[ id=" + id + " ]";
    }

}
