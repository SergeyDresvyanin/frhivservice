/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "HIVDISPVISITS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivdispvisits.findAll", query = "SELECT h FROM Hivdispvisits h")})
public class Hivdispvisits implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "PLANDATE")
    @Temporal(TemporalType.DATE)
    private Date plandate;
    @Column(name = "TYPEID")
    private Integer typeid;
    @Column(name = "FACTDATE")
    @Temporal(TemporalType.DATE)
    private Date factdate;
    @Size(max = 50)
    @Column(name = "PERSONID")
    private String personid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INFEDREGISTER")
    private short infedregister;
    @JoinColumn(name = "REFPATIENT", referencedColumnName = "ID")
    @ManyToOne
    private Patient refpatient;

    public Hivdispvisits() {
    }

    public Hivdispvisits(Integer id) {
        this.id = id;
    }

    public Hivdispvisits(Integer id, short infedregister) {
        this.id = id;
        this.infedregister = infedregister;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getPlandate() {
        return plandate;
    }

    public void setPlandate(Date plandate) {
        this.plandate = plandate;
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public Date getFactdate() {
        return factdate;
    }

    public void setFactdate(Date factdate) {
        this.factdate = factdate;
    }

    public String getPersonid() {
        return personid;
    }

    public void setPersonid(String personid) {
        this.personid = personid;
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    public Patient getRefpatient() {
        return refpatient;
    }

    public void setRefpatient(Patient refpatient) {
        this.refpatient = refpatient;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivdispvisits)) {
            return false;
        }
        Hivdispvisits other = (Hivdispvisits) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hivdispvisits[ id=" + id + " ]";
    }

}
