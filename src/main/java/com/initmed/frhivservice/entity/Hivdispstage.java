/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sd199
 */
@Entity
@Table(name = "HIVDISPSTAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivdispstage.findAll", query = "SELECT h FROM Hivdispstage h")})
public class Hivdispstage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "STAGEID")
    private Integer stageid;
    @Column(name = "STAGEDATE")
    @Temporal(TemporalType.DATE)
    private Date stagedate;
    @Size(max = 50)
    @Column(name = "PERSONID")
    private String personid;
    @Column(name = "INFEDREGISTER")
    private short infedregister;
    @JoinColumn(name = "REFHIVDISP", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Hivdisp hivdisp;

    public Hivdispstage() {
    }

    public Hivdispstage(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStageid() {
        return stageid;
    }

    public void setStageid(Integer stageid) {
        this.stageid = stageid;
    }

    public Date getStagedate() {
        return stagedate;
    }

    public void setStagedate(Date stagedate) {
        this.stagedate = stagedate;
    }

    public String getPersonid() {
        return personid;
    }

    public void setPersonid(String personid) {
        this.personid = personid;
    }

    public Hivdisp getHivdisp() {
        return hivdisp;
    }

    public void setHivdisp(Hivdisp hivdisp) {
        this.hivdisp = hivdisp;
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivdispstage)) {
            return false;
        }
        Hivdispstage other = (Hivdispstage) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hivdispstage[ id=" + id + " ]";
    }

}
