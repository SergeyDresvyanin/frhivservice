/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "HIVAVRTDRUGS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivavrtdrugs.findAll", query = "SELECT h FROM Hivavrtdrugs h")})
public class Hivavrtdrugs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "MNNID")
    private Integer mnnid;
    @Column(name = "DRUGFORMID")
    private Integer drugformid;
    @Column(name = "DOSAGEID")
    private Integer dosageid;
    @Column(name = "NEEDYEAR")
    private Integer needyear;
    @Column(name = "NEEDDAY")
    private Integer needday;
    @JoinColumn(name = "REFHIVART", referencedColumnName = "ID")
    @ManyToOne
    private Hivavrt refhivart;

    public Hivavrtdrugs() {
    }

    public Hivavrtdrugs(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMnnid() {
        return mnnid;
    }

    public void setMnnid(Integer mnnid) {
        this.mnnid = mnnid;
    }

    public Integer getDrugformid() {
        return drugformid;
    }

    public void setDrugformid(Integer drugformid) {
        this.drugformid = drugformid;
    }

    public Integer getDosageid() {
        return dosageid;
    }

    public void setDosageid(Integer dosageid) {
        this.dosageid = dosageid;
    }

    public Integer getNeedyear() {
        return needyear;
    }

    public void setNeedyear(Integer needyear) {
        this.needyear = needyear;
    }

    public Integer getNeedday() {
        return needday;
    }

    public void setNeedday(Integer needday) {
        this.needday = needday;
    }

    public Hivavrt getRefhivart() {
        return refhivart;
    }

    public void setRefhivart(Hivavrt refhivart) {
        this.refhivart = refhivart;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivavrtdrugs)) {
            return false;
        }
        Hivavrtdrugs other = (Hivavrtdrugs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hivavrtdrugs[ id=" + id + " ]";
    }

}
