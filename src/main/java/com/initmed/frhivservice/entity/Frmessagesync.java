/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Basic;
import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Cacheable(false)
@Table(name = "FRMESSAGESYNC", uniqueConstraints
        = @UniqueConstraint(columnNames = {"MESSAGEID"}))
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Frmessagesync.findAll", query = "SELECT f FROM Frmessagesync f"),
    @NamedQuery(name = "Frmessagesync.findById", query = "SELECT f FROM Frmessagesync f WHERE f.id = :id"),
    @NamedQuery(name = "Frmessagesync.findByMessageid", query = "SELECT f FROM Frmessagesync f WHERE f.messageid = :messageid"),
    @NamedQuery(name = "Frmessagesync.findByMethod", query = "SELECT f FROM Frmessagesync f WHERE f.method = :method"),
    @NamedQuery(name = "Frmessagesync.findByRefentity", query = "SELECT f FROM Frmessagesync f WHERE f.refentity = :refentity"),
    @NamedQuery(name = "Frmessagesync.findByIdAndMethod", query = "SELECT f FROM Frmessagesync f WHERE f.refentity = :refentity and f.method = :method "),
    @NamedQuery(name = "Frmessagesync.findByIdAndMethodEmptyMsgId", query = "SELECT f FROM Frmessagesync f WHERE f.refentity = :refentity and f.method = :method and f.messageid is null"),
    @NamedQuery(name = "Frmessagesync.findByMessagedetail", query = "SELECT f FROM Frmessagesync f WHERE f.messagedetail = :messagedetail")})
public class Frmessagesync implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 100)
    @Column(name = "MESSAGEID")
    private String messageid;
    @Column(name = "MO_OID")
    private String moOid;
    @Size(max = 50)
    @Column(name = "METHOD")
    private String method;
    @Column(name = "REFENTITY")
    private Integer refentity;
    @Size(max = 3000)
    @Column(name = "MESSAGEDETAIL")
    private String messagedetail;
    @Column(name = "DATEOPERATION")
    private Date dateoperation;
    @Column(name = "ASYNCRESPONSEDATE")
    private Date asyncresponsedate;

    @Column(name = "RESPOCERECEIVED")
    private short respoceReceived;

    public Frmessagesync() {
    }

    public Frmessagesync(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessageid() {
        return messageid;
    }

    public void setMessageid(String messageid) {
        this.messageid = messageid;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Integer getRefentity() {
        return refentity;
    }

    public void setRefentity(Integer refentity) {
        this.refentity = refentity;
    }

    public String getMessagedetail() {
        return messagedetail;
    }

    public void setMessagedetail(String messagedetail) {
        this.messagedetail = messagedetail;
    }

    public Date getDateoperation() {
        return dateoperation;
    }

    public void setDateoperation(Date dateoperation) {
        this.dateoperation = dateoperation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Frmessagesync)) {
            return false;
        }
        Frmessagesync other = (Frmessagesync) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Frmessagesync[ id=" + id + " ]";
    }

    public short getRespoceReceived() {
        return respoceReceived;
    }

    public void setRespoceReceived(short respoceReceived) {
        this.respoceReceived = respoceReceived;
    }

    public Date getAsyncresponsedate() {
        return asyncresponsedate;
    }

    public void setAsyncresponsedate(Date asyncresponsedate) {
        this.asyncresponsedate = asyncresponsedate;
    }

    public String getMoOid() {
        return moOid;
    }

    public void setMoOid(String moOid) {
        this.moOid = moOid;
    }
    
    

}
