/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.entity;

import java.io.Serializable;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergey
 */
@Entity
@Table(name = "HIVADDRESS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hivaddress.findAll", query = "SELECT h FROM Hivaddress h"),
    @NamedQuery(name = "Hivaddress.findById", query = "SELECT h FROM Hivaddress h WHERE h.id = :id"),
    @NamedQuery(name = "Hivaddress.findByAoidarea", query = "SELECT h FROM Hivaddress h WHERE h.aoidarea = :aoidarea"),
    @NamedQuery(name = "Hivaddress.findByAoidstreet", query = "SELECT h FROM Hivaddress h WHERE h.aoidstreet = :aoidstreet"),
    @NamedQuery(name = "Hivaddress.findByHoused", query = "SELECT h FROM Hivaddress h WHERE h.housed = :housed"),
    @NamedQuery(name = "Hivaddress.findByRegion", query = "SELECT h FROM Hivaddress h WHERE h.region = :region"),
    @NamedQuery(name = "Hivaddress.findByInfedregister", query = "SELECT h FROM Hivaddress h WHERE h.infedregister = :infedregister"),
    @NamedQuery(name = "Hivaddress.findByHouseid", query = "SELECT h FROM Hivaddress h WHERE h.houseid = :houseid"),
    @NamedQuery(name = "Hivaddress.findByFlat", query = "SELECT h FROM Hivaddress h WHERE h.flat = :flat")})
public class Hivaddress implements Serializable {

    @Size(max = 255)
    @Column(name = "AREANAME")
    private String areaname;
    @Size(max = 5)
    @Column(name = "PREFIXAREA")
    private String prefixarea;
    @Size(max = 225)
    @Column(name = "STREETNAME")
    private String streetname;
    @Size(max = 225)
    @Column(name = "HOUSE")
    private String house;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 36)
    @Column(name = "AOIDAREA")
    private String aoidarea;
    @Size(max = 36)
    @Column(name = "AOIDSTREET")
    private String aoidstreet;
    @Size(max = 36)
    @Column(name = "HOUSED")
    private String housed;
    @Size(max = 36)
    @Column(name = "REGION")
    private String region;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INFEDREGISTER")
    private short infedregister;
    @Size(max = 36)
    @Column(name = "HOUSEID")
    private String houseid;
    @Size(max = 36)
    @Column(name = "FLAT")
    private String flat;
    @JoinColumn(name = "REFPATIENT", referencedColumnName = "ID")
    @ManyToOne
    private Patient refpatient;

    public Hivaddress() {
    }

    public Hivaddress(Integer id) {
        this.id = id;
    }

    public Hivaddress(Integer id, short infedregister) {
        this.id = id;
        this.infedregister = infedregister;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAoidarea() {
        return aoidarea;
    }

    public void setAoidarea(String aoidarea) {
        this.aoidarea = aoidarea;
    }

    public String getAoidstreet() {
        return aoidstreet;
    }

    public void setAoidstreet(String aoidstreet) {
        this.aoidstreet = aoidstreet;
    }

    public String getHoused() {
        return housed;
    }

    public void setHoused(String housed) {
        this.housed = housed;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public short getInfedregister() {
        return infedregister;
    }

    public void setInfedregister(short infedregister) {
        this.infedregister = infedregister;
    }

    public String getHouseid() {
        return houseid;
    }

    public void setHouseid(String houseid) {
        this.houseid = houseid;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public Patient getRefpatient() {
        return refpatient;
    }

    public void setRefpatient(Patient refpatient) {
        this.refpatient = refpatient;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hivaddress)) {
            return false;
        }
        Hivaddress other = (Hivaddress) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.initmed.frhivservice.dataloaders.Hivaddress[ id=" + id + " ]";
    }

    public String getAreaname() {
        return areaname;
    }

    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }

    public String getPrefixarea() {
        return prefixarea;
    }

    public void setPrefixarea(String prefixarea) {
        this.prefixarea = prefixarea;
    }

    public String getStreetname() {
        return streetname;
    }

    public void setStreetname(String streetname) {
        this.streetname = streetname;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

}
