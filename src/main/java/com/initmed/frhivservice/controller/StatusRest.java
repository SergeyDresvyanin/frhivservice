/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import org.json.JSONException;

/**
 *
 * @author sd199
 */
@Path("/isblocked")
@Stateless
public class StatusRest {

    @EJB
    private Parameters parameters;

    @GET    
    public Response getBlocked() throws JSONException {        
        return Response.status(Response.Status.OK)                
                .entity(parameters.isBlocked())
                .build();
    }
}
