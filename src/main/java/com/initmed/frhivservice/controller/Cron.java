/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import com.initmed.frhivservice.bdmodel.facades.EmptyResultExcepton;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.EJB;
import jakarta.ejb.Schedule;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.datatype.DatatypeConfigurationException;
import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.resource.ResourceManager;
import org.apache.cxf.resource.ResourceResolver;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.BasicClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

/**
 * Планировщик заданий выполняет их по расписанию
 *
 * @author sd199
 */
@Singleton
@Startup
public class Cron {

    static {
//
//        try {
//            Bus bus = BusFactory.getDefaultBus();
//            ResourceManager extension = bus.getExtension(ResourceManager.class);
//
//            extension.addResourceResolver(new ResourceResolver() {
//                @Override
//                public <T> T resolve(String resourceName, Class<T> resourceType) {
//                    System.out.println("resourceName: " + resourceName + " - resourceType: " + resourceType);
//                    return null;
//                }
//
//                @Override
//                public InputStream getAsStream(String name) {
//                    try {
//                        if (!name.startsWith("https")) {
//                            return null;
//                        }
//
//                        SSLSocketFactory sslSocketFactory = createSSLSocketFactory();                      
//                        SchemeRegistry schemeRegistry = new SchemeRegistry();
//                        schemeRegistry.register(new Scheme("https",  sslSocketFactory, 8443));
//
//                        final HttpParams httpParams = new BasicHttpParams();
//                        DefaultHttpClient httpClient = new DefaultHttpClient(new BasicClientConnectionManager(schemeRegistry), httpParams);
//
//                        HttpGet get = new HttpGet(name);
//                        HttpResponse response = httpClient.execute(get);
//                        return response.getEntity().getContent();
//                    } catch (Exception e) {
//                        return null;
//                    }
//                }
//            });
//        } catch (Exception e) {
//        }

        //for localhost testing only
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                new javax.net.ssl.HostnameVerifier() {

            public boolean verify(String hostname,
                    javax.net.ssl.SSLSession sslSession) {
                if (hostname.equals("localhost")) {
                    return true;
                }
                return false;
            }
        });
    }

    private static class NaiveTrustManager implements
            X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] certs,
                String authType) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] certs,
                String authType) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }

    @EJB
    private FRHIVServiceCommand fRHIVServiceCommand;

    @Schedule(persistent = false, hour = "*", minute = "*", second = "*/15", info = "Every 3 seconds timer")
    public void createPatient() {
        try {
            fRHIVServiceCommand.createPatient();
        } catch (DatatypeConfigurationException ex) {
        } catch (EmptyResultExcepton ex) {
        }
    }

    @Schedule(persistent = false, hour = "*", minute = "*", second = "*/15", info = "Every 15 seconds timer")
    public void readPatient() {
        try {
            fRHIVServiceCommand.readPatient();
        } catch (DatatypeConfigurationException ex) {
        } catch (EmptyResultExcepton ex) {
        }
    }

    @Schedule(persistent = false, hour = "*", minute = "*", second = "*/20", info = "Every 3 seconds timer")
    public void createHivs() {
        try {
            fRHIVServiceCommand.createHiv();
        } catch (DatatypeConfigurationException ex) {
        } catch (EmptyResultExcepton ex) {
        }
    }

    @Schedule(persistent = false, hour = "*", minute = "*", second = "*/20", info = "Every 3 seconds timer")
    public void createAddress() {
        try {
            fRHIVServiceCommand.createAddress();
        } catch (DatatypeConfigurationException ex) {
        } catch (EmptyResultExcepton ex) {
        }
    }

    @Schedule(persistent = false, hour = "*", minute = "*", second = "*/6", info = "Every 3 seconds timer")
    public void readHivs() {
        try {
            fRHIVServiceCommand.readHiv();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        }

    }

    @Schedule(persistent = false, hour = "*", minute = "*", second = "*/6", info = "Every 3 seconds timer")
    public void readHivDisp() {
        try {
            fRHIVServiceCommand.readDispCardList();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        }
    }

    @Schedule(persistent = false, hour = "*", minute = "*", second = "*/6", info = "Every 3 seconds timer")
    public void createHivDisp() {
        try {
            fRHIVServiceCommand.createDispCardPatient();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        }
    }

    @Schedule(persistent = false, hour = "*", minute = "*", second = "*/3", info = "Every 3 seconds timer")
    public void sendDispDesease() {
        try {
            fRHIVServiceCommand.createHivDispDesease();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        }
    }

    @Schedule(persistent = false, hour = "*", minute = "*", second = "*/23", info = "Every 3 seconds timer")
    public void sendDispCheckup() {
        try {
            fRHIVServiceCommand.createHivDispCheckup();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        }
    }

    @Schedule(persistent = false, hour = "*", minute = "*", second = "*/5", info = "Every 3 seconds timer")
    public void createHivDispStage() {
        try {
            fRHIVServiceCommand.createHivDispStage();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        }
    }

    @Schedule(persistent = false, hour = "*", minute = "*", second = "*/33", info = "Every 3 seconds timer")
    public void sendDispConc() {
        try {
            fRHIVServiceCommand.createHivDispConc();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        }
    }

    @Schedule(persistent = false, hour = "*", minute = "*", second = "*/33", info = "Every 3 seconds timer")
    public void sendDispChemo() {
        try {
            fRHIVServiceCommand.createHivDispChemo();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        }
    }

    @Schedule(hour = "*", minute = "*", second = "*/2", info = "Every 1 seconds timer")
    public void sendRecipes() {
        try {
            fRHIVServiceCommand.createHivRecipe();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        } catch (Exception ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Schedule(hour = "*", minute = "*", second = "*/3", info = "Every 3 seconds timer")
    public void sendBlots() {
        try {
            fRHIVServiceCommand.createHivExam();
        } catch (EmptyResultExcepton ex) {
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Schedule(hour = "*", minute = "*", second = "*/3", info = "Every 3 seconds timer")
    public void sendHivEpid() {
        try {
            fRHIVServiceCommand.createHivEpid();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        } catch (Exception ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Schedule(hour = "*", minute = "*", second = "*/4", info = "Every 3 seconds timer")
    public void sendHivDispExams() {
        try {
            fRHIVServiceCommand.createHivDispExam();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        } catch (Exception ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Schedule(hour = "*", minute = "*", second = "*/3", info = "Every 3 seconds timer")
    public void sendVisists() {
        try {
            fRHIVServiceCommand.createHivVisits();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        } catch (Exception ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Schedule(hour = "*", minute = "*", second = "*/3", info = "Every 3 seconds timer")
    public void updateHivEpid() {
        try {
            fRHIVServiceCommand.updateHivEpid();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        } catch (Exception ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Schedule(hour = "*", minute = "*", second = "*/3", info = "Every 3 seconds timer")
    public void updateHivAvrt() {
        try {
            fRHIVServiceCommand.createAVRT();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        } catch (Exception ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Schedule(hour = "*", minute = "*", second = "*/3", info = "Every 3 seconds timer")
    public void closeHivAvrt() {
        try {
            fRHIVServiceCommand.readListAVRT();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
        } catch (Exception ex) {
            Logger.getLogger(Cron.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
