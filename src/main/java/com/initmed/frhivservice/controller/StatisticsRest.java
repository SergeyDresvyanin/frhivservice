/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import com.initmed.frhivservice.bdmodel.facades.EmptyResultExcepton;
import com.initmed.frhivservice.bdmodel.facades.FrmessagesyncFacade;
import com.initmed.frhivservice.bdmodel.facades.HivFacade;
import com.initmed.frhivservice.bdmodel.facades.HivavrtFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispchekupFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispchemoFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispconcFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispexamFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispstageFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispvisitsFacade;
import com.initmed.frhivservice.bdmodel.facades.HivepidFacade;
import com.initmed.frhivservice.bdmodel.facades.HivrecipeFacade;
import com.initmed.frhivservice.bdmodel.facades.PatientFacade;
import com.initmed.frhivservice.entity.Frmessagesync;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author sd199
 */
@Path("/stat")
@Stateless
public class StatisticsRest {

    @EJB
    private PatientFacade patientFacade;
    @EJB
    private HivdispexamFacade hivdispexamFacade;
    @EJB
    private FrmessagesyncFacade frmessagesyncFacade;
    @EJB
    private HivrecipeFacade hivrecipeFacade;
    @EJB
    private HivFacade hivFacade;
    @EJB
    private HivavrtFacade hivavrtFacade;
    @EJB
    private HivepidFacade hivepidFacade;
    @EJB
    private HivdispstageFacade hivdispstageFacade;
    @EJB
    private HivdispFacade hivDispFacade;
    @EJB
    private HivdispvisitsFacade hivdispvisitsFacade;
    @EJB
    private HivdispchekupFacade hivdispcheckupFacade;
    @EJB
    private HivdispchemoFacade hivdispchemoFacade;
    @EJB
    private HivdispconcFacade hivdispconcFacade;

    @GET
    @Produces("application/json")
    @Path("/rowlist")
    public Response getIemkRowsList() throws JSONException {
        List<Frmessagesync> list = frmessagesyncFacade.loadRowsWithErrors(500);
        JSONArray values = new JSONArray();

        for (Frmessagesync x : list) {
            JSONObject jo = new JSONObject();
            if (x.getMessageid() != null) {
                jo.put("id", x.getMessageid());
            } else {
                jo.put("id", "NOT FOUND");
            }
            jo.put("date", x.getDateoperation());
            jo.put("operation", x.getMethod());
            jo.put("error", x.getMessagedetail());
            values.put(jo);
        }
        JSONObject res = new JSONObject();
        res.put("data", values);
        return Response.status(Response.Status.OK)
                .type(MediaType.APPLICATION_JSON)
                .entity(res.toString())
                .build();
    }

    @GET
    @Path("/operationscount")
    public Response getServerStatus(@QueryParam("datefrom") String dateStartRaw, @QueryParam("dateto") String dateToRaw) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date dateStart = sdf.parse(dateStartRaw);
            Date dateFinish = sdf.parse(dateToRaw);

            JSONArray ja = new JSONArray();

            int patRead = patientFacade.loadCountPatientsForRead();
            int patCreate = patientFacade.loadCountPatientsForCreate();
            int hivRead = patientFacade.loadCountHivForRead();
            int hivCreate = patientFacade.loadCountHivForCreate();
            int hivDispRead = hivDispFacade.loadCountHidDispForRead();
            int hivDispCreate = hivDispFacade.loadCountHidDispForCreate();

            int recipes = hivrecipeFacade.loadCountRecipes(dateStart, dateFinish);
            int exams = hivdispexamFacade.loadCountDispExam(dateStart, dateFinish);
            int epid = hivepidFacade.loadCountHivEpid();
            int stage = hivdispstageFacade.loadCountHivDispStage(dateStart, dateFinish);
            int visit = hivdispvisitsFacade.loadCountHivDispVisit(dateStart, dateFinish);
            int conc = hivdispconcFacade.loadCountHivDispConc();
            int chemo = hivdispchemoFacade.loadCountHivDispChemo();
            int checkup = hivdispcheckupFacade.loadCountHivDispChekup();
            int avrt = hivavrtFacade.loadCountHivAVRT();
            JSONObject patObject = new JSONObject();
            patObject.put("total", patientFacade.count());
            patObject.put("queue", patRead);
            patObject.put("error", patientFacade.loadCountPatientsForReadError());
            patObject.put("label", "Чтение пациентов");
            ja.put(patObject);

            JSONObject patCreateObject = new JSONObject();
            patCreateObject.put("total", patientFacade.count());
            patCreateObject.put("queue", patCreate);
            patCreateObject.put("error", patientFacade.loadCountPatientsForCreateError());
            patCreateObject.put("label", "Добавление пациентов");
            ja.put(patCreateObject);

            JSONObject hivObject = new JSONObject();
            hivObject.put("total", hivFacade.count());
            hivObject.put("queue", hivRead);
            hivObject.put("error", patientFacade.loadCountHivForReadError());
            hivObject.put("label", "Чтение случаев заболевания");
            ja.put(hivObject);

            JSONObject hivCreateObject = new JSONObject();
            hivCreateObject.put("total", hivFacade.count());
            hivCreateObject.put("queue", hivCreate);
            hivCreateObject.put("error", patientFacade.loadCountHivForCreateError());
            hivCreateObject.put("label", "Создание случаев заболевания");
            ja.put(hivCreateObject);

            JSONObject hivDispReadObject = new JSONObject();
            hivDispReadObject.put("total", hivDispFacade.count());
            hivDispReadObject.put("queue", hivRead);
            hivDispReadObject.put("error", hivDispFacade.loadCountHidDispForReadError());
            hivDispReadObject.put("label", "Чтение диспансерных карт");
            ja.put(hivDispReadObject);

            JSONObject hivDispCreateObject = new JSONObject();
            hivDispCreateObject.put("total", hivFacade.count());
            hivDispCreateObject.put("queue", hivCreate);
            hivDispCreateObject.put("error", hivDispFacade.loadCountHidDispForCreateError());
            hivDispCreateObject.put("label", "Создание диспансерных карт");
            ja.put(hivDispCreateObject);

            JSONObject recipesObject = new JSONObject();
            recipesObject.put("queue", recipes);
            recipesObject.put("total", hivrecipeFacade.count(dateStart, dateFinish));
            recipesObject.put("error", hivrecipeFacade.loadCountRecipesError(dateStart, dateFinish));
            recipesObject.put("label", "Рецепты");
            ja.put(recipesObject);

            JSONObject examsObject = new JSONObject();
            examsObject.put("queue", exams);
            examsObject.put("total", hivdispexamFacade.loadCountTotalDispExam(dateStart, dateFinish));
            examsObject.put("error", hivdispexamFacade.loadCountDispExamError(dateStart, dateFinish));
            examsObject.put("label", "Исследования");
            ja.put(examsObject);

            JSONObject epidObject = new JSONObject();
            epidObject.put("queue", epid);
            epidObject.put("total", hivepidFacade.count());
            epidObject.put("error", hivepidFacade.loadCountHivEpidError());
            epidObject.put("label", "Эпид. блоки");
            ja.put(epidObject);

            JSONObject stageObject = new JSONObject();
            stageObject.put("queue", stage);
            stageObject.put("total", hivdispstageFacade.count(dateStart, dateFinish));
            stageObject.put("error", hivdispstageFacade.loadCountHivDispStageError(dateStart, dateFinish));
            stageObject.put("label", "Стадии");
            ja.put(stageObject);

            JSONObject visitObject = new JSONObject();
            visitObject.put("queue", visit);
            visitObject.put("total", hivdispvisitsFacade.count(dateStart, dateFinish));
            visitObject.put("error", hivdispvisitsFacade.loadCountHivDispVisitError(dateStart, dateFinish));
            visitObject.put("label", "Посещения");
            ja.put(visitObject);

            JSONObject concObject = new JSONObject();
            concObject.put("queue", conc);
            concObject.put("total", hivdispconcFacade.count());
            concObject.put("error", hivdispconcFacade.loadCountHivDispConcError());
            concObject.put("label", "Дополнительные диагнозы");
            ja.put(concObject);

            JSONObject chemoObject = new JSONObject();
            chemoObject.put("queue", chemo);
            chemoObject.put("total", hivdispchemoFacade.count());
            chemoObject.put("error", hivdispchemoFacade.loadCountHivDispChemoError());
            chemoObject.put("label", "Схема туб. терапии");
            ja.put(chemoObject);

            JSONObject checkupObject = new JSONObject();
            checkupObject.put("queue", checkup);
            checkupObject.put("total", hivdispcheckupFacade.count());
            checkupObject.put("error", hivdispcheckupFacade.loadCountHivDispChekupError());
            checkupObject.put("label", "Обследования (флющка, гепатиты)");
            ja.put(checkupObject);

            JSONObject avrtObject = new JSONObject();
            avrtObject.put("queue", avrt);
            avrtObject.put("total", hivavrtFacade.count());
            avrtObject.put("error", hivavrtFacade.loadCountWithError());
            avrtObject.put("label", "Терапия");
            ja.put(avrtObject);

            JSONObject totalObject = new JSONObject();
            totalObject.put("queue", patRead + patCreate + hivRead + hivCreate + recipes + exams + epid + stage + visit + conc + chemo + checkup);
            totalObject.put("total", 0);
            totalObject.put("error", 0);
            totalObject.put("label", "Всего задач в очереди");
            ja.put(totalObject);

            return Response.status(Response.Status.OK)
                    .entity(ja.toString())
                    .build();
        } catch (Exception e) {
            Logger.getLogger(StatisticsRest.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(Response.Status.SERVICE_UNAVAILABLE)
                    .build();
        }
    }

    @GET
    @Path("/speed")
    public Response getCurrentSpeed() {
        int speed = 0;
        try {
            speed = patientFacade.getCurrentSpeed();
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(StatisticsRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK)
                .entity(speed)
                .build();
    }

}
