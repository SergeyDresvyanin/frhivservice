/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import com.initmed.frhivservice.bdmodel.facades.ScheduleprocessParamsFacade;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author sd199
 */
@Path("/process")
@Stateless
public class ScheduleProcessRest {

    @EJB
    private ScheduleprocessParamsFacade scheduleprocessParamsFacade;

    @POST
    @Path("/changeprocesstate")
    public Response changeProcesState(@QueryParam("method") String methodName, @QueryParam("state") Boolean state) {
        scheduleprocessParamsFacade.udpateParam(methodName, state ? (short) 1 : (short) 0);
        return Response.accepted()
                .build();
    }

    @GET
    @Path("/states")
    public Response getStates() {
        JSONArray ja = new JSONArray();
        scheduleprocessParamsFacade.findAll().stream().map(x -> {
            try {
                JSONObject patObject = new JSONObject();
                patObject.put("name", x.getName());
                patObject.put("state", x.getTransfer());
                return patObject;
            } catch (JSONException e) {
                return null;
            }

        }).forEachOrdered(patObject -> {
            ja.put(patObject);
        });
        return Response.status(Response.Status.OK)
                .entity(ja.toString())
                .build();
    }
}
