/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import com.initmed.frhivservice.bdmodel.facades.EmptyResultExcepton;
import com.initmed.frhivservice.bdmodel.facades.HivFacade;
import com.initmed.frhivservice.bdmodel.facades.HivavrtFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispconcFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispdeseasesFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispexamFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispstageFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispvisitsFacade;
import com.initmed.frhivservice.bdmodel.facades.HivrecipeFacade;
import com.initmed.frhivservice.entity.report.HivAvrtReport;
import com.initmed.frhivservice.entity.report.HivRecipesReport;
import com.initmed.frhivservice.entity.report.HivReport;
import com.initmed.frhivservice.entity.report.HivdispconcReport;
import com.initmed.frhivservice.entity.report.HivdispdeseasesReport;
import com.initmed.frhivservice.entity.report.HivdispdexamReport;
import com.initmed.frhivservice.entity.report.HivdispstagesReport;
import com.initmed.frhivservice.entity.report.HivdispvisitsReport;
import com.initmed.frhivservice.entity.report.PatientErorReport;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author sd199
 */
@Stateless
public class ReportGenerator {

    @EJB
    private HivFacade hivFacade;
    @EJB
    private HivdispFacade hivdispFacade;
    @EJB
    private HivdispdeseasesFacade hivdispdeseasesFacade;
    @EJB
    private HivrecipeFacade hivrecipeFacade;
    @EJB
    private HivdispstageFacade hivdispstageFacade;
    @EJB
    private HivdispvisitsFacade hivdispvisitsFacade;
    @EJB
    private HivdispexamFacade hivdispexamFacade;
    @EJB
    private HivdispconcFacade hivdispconcFacade;
    @EJB
    private HivavrtFacade hivavrtFacade;

    public byte[] getHivdispdeseases(Date from, Date to) throws IOException {
        try {
            List<HivdispdeseasesReport> r = hivdispdeseasesFacade.loadHivDispDeseasesReport(from, to);
            Workbook wb = getBaseReport("Перенос диагнозов", "Диагноз", "Дата диагноза");
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            r.forEach(x -> addRow(wb, x.getSpecnum(), x.getSnils(), x.getMessageDetail() == null ? "OK" : x.getMessageDetail(), x.getMkbCode(), sdf.format(x.getDeseaseDate())));
            return convertToByte(wb);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public byte[] getHivdispVisits(Date from, Date to) throws IOException {
        try {
            List<HivdispvisitsReport> r = hivdispvisitsFacade.loadHivDispVisitReport(from, to);
            Workbook wb = getBaseReport("Перенос посещений", "Диагноз", "Дата посещения");
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            r.forEach(x -> addRow(wb, x.getSpecnum(), x.getSnils(), x.getMessageDetail() == null ? "OK" : x.getMessageDetail(), x.getPersonId(), x.getVisitDate() != null ? sdf.format(x.getVisitDate()) : null));
            return convertToByte(wb);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public byte[] getHivdispExam(Date from, Date to) throws IOException {
        try {
            List<HivdispdexamReport> r = hivdispexamFacade.loadHivDispExamReport(from, to);
            Workbook wb = getBaseReport("Перенос исследований", "Дата исследования", "Показатель");
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            r.forEach(x -> addRow(wb, x.getSpecnum(), x.getSnils(), x.getMessageDetail() == null ? "OK" : x.getMessageDetail(), sdf.format(x.getExamDate()), x.getExamname()));
            return convertToByte(wb);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public byte[] getHivRecipes(Date from, Date to) throws IOException {
        try {
            List<HivRecipesReport> r = hivrecipeFacade.loadHivRecipesReport(from, to);
            Workbook wb = getBaseReport("Перенос рецептов", "Врач", "Дата выдачи", "Торговое название", "Форма", "Дозировка", "Количество упаковок", "Количество приемов", "Количество приемов в сутки", "Номер рецепта");
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            r.forEach(x -> addRow(wb, x.getSpecnum(), x.getSnils(), x.getMessageDetail() == null ? "OK" : x.getMessageDetail(), x.getPersonId(), x.getVisitDate() != null ? sdf.format(x.getVisitDate()) : null, x.getTradeName(), x.getForm(), x.getDose(), x.getPackCount().toString(), x.getDoseCount().toString(), x.getPerDay().toString(), x.getRecipenumber()));
            return convertToByte(wb);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public byte[] getStages(Date from, Date to) throws IOException {
        try {
            List<HivdispstagesReport> r = hivdispstageFacade.loadStagesReport(from, to);
            Workbook wb = getBaseReport("Перенос рецептов", "Врач", "Стадия", "Дата");
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            r.forEach(x -> addRow(wb, x.getSpecnum(), x.getSnils(), x.getMessageDetail() == null ? "OK" : x.getMessageDetail(), x.getDocsnils(), x.getStage(), x.getStagedate() != null ? sdf.format(x.getStagedate()) : null));
            return convertToByte(wb);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public byte[] getConc(Date from, Date to) throws IOException {
        try {
            List<HivdispconcReport> r = hivdispconcFacade.loadConc(from, to);
            Workbook wb = getBaseReport("Перенос дополнительных диагнозов", "МКБ", "Дата");
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            r.forEach(x -> addRow(wb, x.getSpecnum(), x.getSnils(), x.getMessageDetail() == null ? "OK" : x.getMessageDetail(), x.getConcdesease(), x.getBegindate() != null ? sdf.format(x.getBegindate()) : null));
            return convertToByte(wb);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public byte[] getCanNotAddPatient(Date from, Date to) throws IOException {
        List<HivReport> r = hivFacade.loadPatientCanNotAdd(from, to);
        Workbook wb = getBaseReport("Пациентов неудалось найти в ФРВИЧ");
        r.forEach(x -> addRow(wb, x.getSpecnum(), x.getSnils()));
        return convertToByte(wb);
    }

    public byte[] getAvrt(Date from, Date to) throws IOException {
        try {
            List<HivAvrtReport> r = hivavrtFacade.loadAVRTReport(from, to);
            Workbook wb = getBaseReport("Перенос терапии", "Врач", "Дата", "МНН", "Форма", "Дозировка");
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            r.forEach(x -> addRow(wb, x.getSpecnum(), x.getSnils(), x.getMessageDetail() == null ? "OK" : x.getMessageDetail(), x.getPersonId(), sdf.format(x.getVisitDate()), x.getTradeName(), x.getForm(), x.getDose()));
            return convertToByte(wb);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public byte[] getPatientErorr() throws IOException {
        try {
            List<PatientErorReport> r = hivFacade.loadWrongPatient();
            Workbook wb = getBaseReport("Пациенты с оишбками поиска переноса", "Снилс", "Спец номер в БД", "Номер в ФРВИЧ", "Ошибка");
            r.forEach(x -> addRow(wb, x.getSnils(), x.getSpecnum(), x.getRegistrynumber(), x.getMsgp() != null ? x.getMsgp() : "" + x.getMsgh() != null ? x.getMsgh() : "" + x.getMsgd() != null ? x.getMsgd() : ""));;
            return convertToByte(wb);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Workbook getBaseReport(String listName, String... addParams) {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet(listName);
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Д. номер");
        header.createCell(1).setCellValue("СНИЛС");
        header.createCell(2).setCellValue("Статус");
        if (addParams != null) {
            int i = 3;
            for (String x : addParams) {
                header.createCell(i).setCellValue(x);
                i++;
            }
        }
        return workbook;
    }

    private void addRow(Workbook workbook, String... addParams) {
        if (addParams != null) {
            CellStyle reddRowStyle = workbook.createCellStyle();
            reddRowStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
            reddRowStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            int i = 0;
            int indexRow = workbook.getSheetAt(0).getLastRowNum();

            Row row = workbook.getSheetAt(0).createRow(indexRow + 1);

            boolean hasError = true;
            for (String x : addParams) {
                row.createCell(i).setCellValue(x);
                i++;
                if (x != null && x.equals("OK")) {
                    hasError = false;
                }
            }
            if (hasError) {
                for (int c = 0; c < i; c++) {
                    row.getCell(c).setCellStyle(reddRowStyle);
                }
            }
        }
    }

    private byte[] convertToByte(Workbook wb) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        wb.write(baos);
        return baos.toByteArray();
    }
}
