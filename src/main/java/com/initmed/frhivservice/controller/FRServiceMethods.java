/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import com.initmed.frhivservice.bdmodel.converter.FRNode;
import com.initmed.frhivservice.frdatamodel.BasePatientKey;
import com.initmed.frhivservice.frdatamodel.BaseRegistryKey;
import com.initmed.frhivservice.frdatamodel.CreateHivAddress;
import com.initmed.frhivservice.frdatamodel.CreateHivExam;
import com.initmed.frhivservice.frdatamodel.CreateHivRegistry;
import com.initmed.frhivservice.frdatamodel.CreatePatientDocuments;
import com.initmed.frhivservice.frdatamodel.HivAddressKey;
import com.initmed.frhivservice.frdatamodel.HivExamKey;
import com.initmed.frhivservice.frdatamodel.HivRegistryNumberKey;
import com.initmed.frhivservice.frdatamodel.NrHivRecipe;
import com.initmed.frhivservice.frdatamodel.NrPatient;
import com.initmed.frhivservice.frdatamodel.PatientDocumentKey;
import com.initmed.frhivservice.frdatamodel.UpdateHivAddress;
import com.initmed.frhivservice.frdatamodel.UpdateHivExam;
import com.initmed.frhivservice.frdatamodel.UpdateHivRegistry;
import com.initmed.frhivservice.frdatamodel.UpdatePatient;
import com.initmed.frhivservice.frdatamodel.UpdatePatientDocument;
import jakarta.ejb.Local;
import javax.xml.datatype.DatatypeConfigurationException;

/**
 *
 * @author User
 */
@Local
public interface FRServiceMethods {

    public static final String PATIENT_CREATE = "patient.create";
    public static final String PATIENT_READ = "patient.read";
    public static final String PATIENT_UPDATE = "patient.update";
    public static final String PATIENT_DOCUMENT_CREATE = "patient_document.create";
    public static final String PATIENT_DOCUMENT_LIST = "patient_document.list";
    public static final String PATIENT_DOCUMENT_READ = "patient_document.read";
    public static final String PATIENT_DOCUMENT_UPDATE = "patient_document.update";
    public static final String PATIENT_DOCUMENT_DELETE = "patient_document.delete";
    public static final String PATIENT_RELATIVE_CREATE = "patient_relative.create";
    public static final String PATIENT_RELATIVE_LIST = "patient_relative.list";
    public static final String PATIENT_RELATIVE_READ = "patient_relative.read";
    public static final String PATIENT_RELATIVE_UPDATE = "patient_relative.update";
    public static final String PATIENT_RELATIVE_DELETE = "patient_relative.delete";

    public static final String HIV_CREATE = "hiv.create";
    public static final String HIV_READ = "hiv.read";
    public static final String HIV_UPDATE = "hiv.update";
    public static final String HIV_EXAM_CREATE = "hiv_exam.create";
    public static final String HIV_EXAM_LIST = "hiv_exam.list";
    public static final String HIV_EXAM_UPDATE = "hiv_exam.update";
    public static final String HIV_EXAM_READ = "hiv_exam.read";
    public static final String HIV_ADDRESS_CREATE = "hiv_address.create";
    public static final String HIV_ADDRESS_LIST = "hiv_address.list";
    public static final String HIV_ADDRESS_READ = "hiv_address.read";
    public static final String HIV_ADDRESS_UPDATE = "hiv_address.update";
    public static final String HIV_ADDRESS_DELETE = "hiv_address.delete";
    public static final String HIV_RECIPE_CREATE = "hiv_recipe.create";
    public static final String HIV_RECIPE_LIST = "hiv_recipe.list";
    public static final String HIV_EPID_READ = "hiv_epid.read";
    public static final String HIV_DISP_LIST = "hiv_disp.list";
    public static final String HIV_DISP_READ = "hiv_disp.read";
    public static final String HIV_DISP_DESEASE_LIST = "hiv_disp_desease.list";
    public static final String HIV_DISP_DESEASE_UPDATE = "hiv_disp_desease.update";
    public static final String HIV_DISP_HOSP_LIST = "hiv_disp_hosp.list";
    public static final String HIV_DISP_HOSP_UPDATE = "hiv_disp_hosp.update";
    public static final String HIV_DISP_CHEMO_LIST = "hiv_disp_chemo.list";
    public static final String HIV_DISP_CHEMO_UPDATE = "hiv_disp_chemo.update";
    public static final String HIV_DISP_CHECKUP_LIST = "hiv_disp_checkup.list";
    public static final String HIV_DISP_CHECKUP_UPDATE = "hiv_disp_checkup.update";
    public static final String HIV_DISP_EXAM_LIST = "hiv_disp_exam.list";
    public static final String HIV_DISP_EXAM_UPDATE = "hiv_disp_exam.update";
    public static final String HIV_AVRT_LIST = "hiv_avrt.list";
    public static final String HIV_AVRT_UPDATE = "hiv_avrt.update";
    public static final String HIV_DISP_STAGE_LIST = "hiv_disp_stage.list";
    public static final String HIV_DISP_STAGE_UPDATE = "hiv_disp_stage.update";
    public static final String HIV_DISP_CONC_LIST = "hiv_disp_conc.list";
    public static final String HIV_DISP_CONC_UPDATE = "hiv_disp_conc.update";
    public static final String HIV_DISP_VISIT_LIST = "hiv_disp_visit.list";
    public static final String HIV_DISP_VISIT_UPDATE = "hiv_disp_visit.update";
    public static final String HIV_EPID_UPDATE = "hiv_epid.update";
    public static final String HIV_EPID_CREATE = "hiv_epid.create";
    public static final String HIV_DISP_СREATE = "hiv_disp.create";
    public static final String HIV_DISP_UPDATE = "hiv_disp.update";
    public static final String HIV_DISP_DESEASE_CREATE = "hiv_disp_desease.create";
    public static final String HIV_DISP_DESEASE_READ = "hiv_disp_desease.read";
    public static final String HIV_DISP_HOSP_CREATE = "hiv_disp_hosp.create";
    public static final String HIV_DISP_HOSP_READ = "hiv_disp_hosp.read";
    public static final String HIV_DISP_CHEMO_CREATE = "hiv_disp_chemo.create";
    public static final String HIV_DISP_CHEMO_READ = "hiv_disp_chemo.read";
    public static final String HIV_DISP_CHECKUP_CREATE = "hiv_disp_checkup.create";
    public static final String HIV_DISP_CHECKUP_READ = "hiv_disp_checkup.read";
    public static final String HIV_DISP_EXAM_CREATE = "hiv_disp_exam.create";
    public static final String HIV_DISP_EXAM_READ = "hiv_disp_exam.read";
    public static final String HIV_AVRT_CREATE = "hiv_avrt.create";
    public static final String HIV_AVRT_READ = "hiv_avrt.read";
    public static final String HIV_DISP_STAGE_CREATE = "hiv_disp_stage.create";
    public static final String HIV_DISP_STAGE_READ = "hiv_disp_stage.read";
    public static final String HIV_DISP_CONC_CREATE = "hiv_disp_conc.create";
    public static final String HIV_DISP_CONC_READ = "hiv_disp_conc.read";
    public static final String HIV_DISP_VISIT_CREATE = "hiv_disp_visit.create";
    public static final String HIV_DISP_VISIT_READ = "hiv_disp_visit.read";

    FRNode createPatient(NrPatient pat, int entId) throws DatatypeConfigurationException;

    FRNode updatePatient(UpdatePatient pat, int entId) throws DatatypeConfigurationException;

    FRNode readPatient(BasePatientKey pat, int entId) throws DatatypeConfigurationException;

    FRNode patientDocumentRead(PatientDocumentKey documentKey, int entId) throws DatatypeConfigurationException;

    FRNode patientDocumentUpdate(UpdatePatientDocument documents, int entId) throws DatatypeConfigurationException;

    FRNode patientDocumentDelete(PatientDocumentKey documentKey, int entId) throws DatatypeConfigurationException;

    FRNode patientDocumentCreate(CreatePatientDocuments documents, int entId) throws DatatypeConfigurationException;

    FRNode patientDocumentList(BasePatientKey documents, int entId) throws DatatypeConfigurationException;

    FRNode patientHivRead(BaseRegistryKey baseRegistryKey, int entId) throws DatatypeConfigurationException;

    FRNode patientHivCreate(CreateHivRegistry createHivRegistry, int entId) throws DatatypeConfigurationException;

    FRNode patientHivUpdate(UpdateHivRegistry createHivRegistry, int entId) throws DatatypeConfigurationException;

    FRNode hivExamList(BaseRegistryKey baseRegistryKey, int entId) throws DatatypeConfigurationException;

    FRNode hivExamRead(HivExamKey baseRegistryKey, int entId) throws DatatypeConfigurationException;

    FRNode hivExamUpdate(UpdateHivExam he, int entId) throws DatatypeConfigurationException;

    FRNode hivExamCreate(CreateHivExam he, int entId) throws DatatypeConfigurationException;

    FRNode hivAddressRead(HivAddressKey hak, int entId) throws DatatypeConfigurationException;

    FRNode hivAddressList(BaseRegistryKey baseRegistryKey, int entId) throws DatatypeConfigurationException;

    FRNode hivAddressCreate(CreateHivAddress createHivAddress, int entId) throws DatatypeConfigurationException;

    FRNode hivAddressUpdate(UpdateHivAddress createHivAddress, int entId) throws DatatypeConfigurationException;

    FRNode hivAddressDelete(HivAddressKey createHivAddress, int entId) throws DatatypeConfigurationException;

    FRNode hivRecipeCreate(NrHivRecipe hivRecipe, int entId) throws DatatypeConfigurationException;

    FRNode hivRecipeList(HivRegistryNumberKey hivRecipe, int entId) throws DatatypeConfigurationException;
}
