/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import com.initmed.frhivservice.bdmodel.facades.DefaultparamsFacade;
import jakarta.annotation.PostConstruct;
import jakarta.ejb.EJB;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;

/**
 *
 * @author sd199
 */
@Startup
@Singleton
public class Parameters {

    @EJB
    private DefaultparamsFacade defaultparamsFacade;

    private String moOid;
    private String certName;
    private String misId;

    private boolean blocked;

    @PostConstruct
    public void Init() {
        this.misId = defaultparamsFacade.getMisOId();
        this.certName = defaultparamsFacade.getSignerAlias();
        this.moOid = defaultparamsFacade.getMoId();
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public String getMoOid() {
        return moOid;
    }

    public String getCertName() {
        return certName;
    }

    public String getMisId() {
        return misId;
    }
    
    

}
