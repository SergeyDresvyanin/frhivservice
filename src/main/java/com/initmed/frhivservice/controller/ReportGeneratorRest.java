/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_AVRT_CREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_CREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_DISP_CONC_CREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_DISP_DESEASE_CREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_DISP_EXAM_CREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_DISP_STAGE_CREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_DISP_VISIT_CREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_RECIPE_CREATE;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

/**
 *
 * @author sd199
 */
@Path("/reports")
@Stateless
public class ReportGeneratorRest {

    @EJB
    private ReportGenerator reportGenerator;

    @GET
    public Response getReport(@QueryParam("datefrom") String dateStartRaw, @QueryParam("dateto") String dateToRaw, @QueryParam("method") String method) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (dateStartRaw == null || dateToRaw == null || method == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        try {
            Date dateStart = sdf.parse(dateStartRaw);
            Date dateFinish = sdf.parse(dateToRaw);
            byte[] res = null;
            switch (method) {
                case HIV_DISP_VISIT_CREATE:
                    res = reportGenerator.getHivdispVisits(dateStart, dateFinish);
                    break;
                case HIV_DISP_DESEASE_CREATE:
                    res = reportGenerator.getHivdispdeseases(dateStart, dateFinish);
                    break;
                case HIV_DISP_EXAM_CREATE:
                    res = reportGenerator.getHivdispExam(dateStart, dateFinish);
                    break;
                case HIV_RECIPE_CREATE:
                    res = reportGenerator.getHivRecipes(dateStart, dateFinish);
                    break;
                case HIV_DISP_STAGE_CREATE:
                    res = reportGenerator.getStages(dateStart, dateFinish);
                    break;
                case HIV_DISP_CONC_CREATE:
                    res = reportGenerator.getConc(dateStart, dateFinish);
                    break;
                case HIV_CREATE:
                    res = reportGenerator.getCanNotAddPatient(dateStart, dateFinish);
                    break;
                case HIV_AVRT_CREATE:
                    res = reportGenerator.getAvrt(dateStart, dateFinish);
                case "patient_erors":
                    res = reportGenerator.getPatientErorr();
                default:
                    break;
            }

            res = res != null ? Base64.getEncoder().encode(res) : null;

            return Response.ok(res, "application/excel").build();
        } catch (ParseException | IOException ex) {
            Logger.getLogger(ReportGeneratorRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
