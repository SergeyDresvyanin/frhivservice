/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import com.initmed.frhivservice.bdmodel.facades.EmptyResultExcepton;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;

/**
 *
 * @author sd199
 */
@Path("/send")
@Stateless
public class RestSendDoc {

    @EJB
    private FRHIVServiceCommand fRHIVServiceCommand;

    @GET
    public Response sendDoc(@QueryParam("method") String method, @QueryParam("id") Integer id) {
//        if (method.equals("hiv_read")) {
        try {
            fRHIVServiceCommand.readHiv(id);
        } catch (DatatypeConfigurationException | EmptyResultExcepton ex) {
            Logger.getLogger(RestSendDoc.class.getName()).log(Level.SEVERE, null, ex);
        }
//        }
        return Response.ok().build();
    }
}
