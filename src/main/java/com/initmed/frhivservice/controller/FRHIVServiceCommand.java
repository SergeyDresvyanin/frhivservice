/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import com.initmed.frhivservice.entity.Relative;
import com.initmed.frhivservice.entity.Hivdispstage;
import com.initmed.frhivservice.entity.Hivdispchemo;
import com.initmed.frhivservice.entity.Hivdispdeseases;
import com.initmed.frhivservice.entity.Hivdisphosp;
import com.initmed.frhivservice.entity.Hivdispexam;
import com.initmed.frhivservice.entity.Hivaddress;
import com.initmed.frhivservice.entity.Hivdispconc;
import com.initmed.frhivservice.entity.Hivdisp;
import com.initmed.frhivservice.entity.Hivdispchekup;
import com.initmed.frhivservice.entity.Hivdispvisits;
import com.initmed.frhivservice.entity.Documents;
import com.initmed.frhivservice.entity.Hiv;
import com.initmed.frhivservice.entity.Hivepid;
import com.initmed.frhivservice.entity.Hivrecipe;
import com.initmed.frhivservice.entity.Hivavrt;
import com.initmed.frhivservice.entity.Patient;
import com.initmed.frhivservice.bdmodel.converter.Converter;
import static com.initmed.frhivservice.bdmodel.converter.EntityUtil.toXml;
import com.initmed.frhivservice.bdmodel.converter.FRNode;
import com.initmed.frhivservice.bdmodel.facades.EmptyResultExcepton;
import com.initmed.frhivservice.bdmodel.facades.FrmessagesyncFacade;
import com.initmed.frhivservice.bdmodel.facades.HivFacade;
import com.initmed.frhivservice.bdmodel.facades.HivavrtFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispchekupFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispchemoFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispconcFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispdeseasesFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispexamFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdisphospFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispstageFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispvisitsFacade;
import com.initmed.frhivservice.bdmodel.facades.HivepidFacade;
import com.initmed.frhivservice.bdmodel.facades.HivrecipeFacade;
import com.initmed.frhivservice.bdmodel.facades.PatientFacade;
import static com.initmed.frhivservice.controller.FRServiceMethods.*;
import com.initmed.frhivservice.frdatamodel.BasePatientKey;
import com.initmed.frhivservice.frdatamodel.BaseRegistryKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcard;
import com.initmed.frhivservice.frdatamodel.CreateDispcardCheckupKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardChemoKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardConcDeseaseKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardDeseaseKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardExamKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardHospKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardStageKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardVisitKey;
import com.initmed.frhivservice.frdatamodel.CreateHivAddress;
import com.initmed.frhivservice.frdatamodel.CreateHivAvrt;
import com.initmed.frhivservice.frdatamodel.CreateHivEpid;
import com.initmed.frhivservice.frdatamodel.CreateHivExam;
import com.initmed.frhivservice.frdatamodel.CreateHivRecipe;
import com.initmed.frhivservice.frdatamodel.CreateHivRegistry;
import com.initmed.frhivservice.frdatamodel.CreatePatientRelatives;
import com.initmed.frhivservice.frdatamodel.DispcardCheckupKey;
import com.initmed.frhivservice.frdatamodel.DispcardChemoKey;
import com.initmed.frhivservice.frdatamodel.DispcardConcDeseaseKey;
import com.initmed.frhivservice.frdatamodel.DispcardDeseaseKey;
import com.initmed.frhivservice.frdatamodel.DispcardExamKey;
import com.initmed.frhivservice.frdatamodel.DispcardHospKey;
import com.initmed.frhivservice.frdatamodel.DispcardKey;
import com.initmed.frhivservice.frdatamodel.DispcardStageKey;
import com.initmed.frhivservice.frdatamodel.DispcardVisitKey;
import com.initmed.frhivservice.frdatamodel.HivAddressKey;
import com.initmed.frhivservice.frdatamodel.HivAvrtKey;
import com.initmed.frhivservice.frdatamodel.HivExamKey;
import com.initmed.frhivservice.frdatamodel.NrHivDispcardExam;
import com.initmed.frhivservice.frdatamodel.NrHivRegistryAvrt;
import com.initmed.frhivservice.frdatamodel.NrPatient;
import com.initmed.frhivservice.frdatamodel.ObjectFactory;
import com.initmed.frhivservice.frdatamodel.PatientRelativeKey;
import com.initmed.frhivservice.frdatamodel.RegistryKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcard;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardCheckupKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardChemoKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardConcDeseaseKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardDeseaseKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardExamKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardHospKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardStageKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardVisitKey;
import com.initmed.frhivservice.frdatamodel.UpdateHivAddress;
import com.initmed.frhivservice.frdatamodel.UpdateHivAvrt;
import com.initmed.frhivservice.frdatamodel.UpdateHivEpid;
import com.initmed.frhivservice.frdatamodel.UpdateHivExam;
import com.initmed.frhivservice.frdatamodel.UpdateHivRegistry;
import com.initmed.frhivservice.frdatamodel.UpdatePatient;
import com.initmed.frhivservice.frdatamodel.UpdatePatientDocument;
import com.initmed.frhivservice.frdatamodel.UpdatePatientRelative;
import jakarta.annotation.Resource;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.DeliveryMode;
import jakarta.jms.JMSContext;
import jakarta.jms.Queue;
import javax.xml.datatype.DatatypeConfigurationException;

/**
 *
 * @author User
 */
@Stateless
public class FRHIVServiceCommand {

    @EJB
    private PatientFacade patientFacade;
    @EJB
    private HivFacade hivFacade;
    @EJB
    private HivdispFacade hivdispFacade;
    @EJB
    private HivdispexamFacade hivdispexamFacade;
    @EJB
    private Converter converter;
    @EJB
    private HivrecipeFacade hivrecipeFacade;
    @EJB
    private HivdispvisitsFacade hivdispvisitsFacade;
    @EJB
    private HivavrtFacade hivavrtFacade;
    @EJB
    private ObjectFactory of;
    @EJB
    private HivepidFacade hivepidFacade;
    @EJB
    private HivdispdeseasesFacade hivdispdeseasesFacade;
    @EJB
    private HivdisphospFacade hivdisphospFacade;
    @EJB
    private HivdispchemoFacade hivdispchemoFacade;
    @EJB
    private HivdispchekupFacade hivdispchekupFacade;
    @EJB
    private HivdispstageFacade hivdispstageFacade;
    @EJB
    private HivdispconcFacade hivdispconcFacade;
    @EJB
    private FrmessagesyncFacade frmessagesyncFacade;

    @Resource
    private ConnectionFactory connectionFactory;

    @Resource(lookup = "jms/RestSendQueue")
    private Queue queue;

    public void listHivDispExam() throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdisp hd = hivdispFacade.loadUpdateExamResult().get(0);
        DispcardKey cr = converter.convertDispCardKey(hd);
        addTask(new FRNode(hd.getMoid(), HIV_DISP_EXAM_LIST, toXml(of.createDispcardKey(cr)), hd.getId()));
    }

    public void createPatient() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Patient p : patientFacade.loadPatient()) {
            NrPatient nrp = converter.convertPatient(p);
            addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_CREATE, toXml(of.createPatient(nrp)), p.getId()));
        }
    }

    public void readPatient(int pId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(pId);
        BasePatientKey bp = converter.convertBasePatientKey(p);
        addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_READ, toXml(of.createPatientKey(bp)), p.getId()));
    }

    public void readPatient() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Patient p : patientFacade.loadPatientsForRead()) {
            BasePatientKey bp = converter.convertBasePatientKey(p);
            addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_READ, toXml(of.createPatientKey(bp)), p.getId()));
        }
    }

    public void createPatientDocument() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Patient p : patientFacade.loadPatientWithoutDocs()) {
            for (Documents x : p.getDocumentses()) {
                if (x.getInfedregister() == 0) {
                    addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_DOCUMENT_CREATE, toXml(of.createCreateDocuments(converter.convertCreatePatientDocument(x))), x.getId()));
                }
            }
        }
    }

    public void updatePatientDocument(int pId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(pId);
        for (Documents x : p.getDocumentses()) {
            if (x.getInfedregister() == 1) {
                UpdatePatientDocument pd = converter.convertUpdatePatientDocument(x);
                addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_DOCUMENT_UPDATE, toXml(of.createUpdateDocument(pd)), x.getId()));
            }
        }
    }

    public void readListPatientDocument(int pId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(pId);
        BasePatientKey bp = converter.convertBasePatientKey(p);
        addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_DOCUMENT_LIST, toXml(of.createPatientKey(bp)), pId));
    }

    public void readPatientDocument(int pId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(pId);
        for (Documents x : p.getDocumentses()) {
            BasePatientKey bp = converter.convertReadPatientDocument(x);
            addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_DOCUMENT_READ, toXml(of.createDocumentKey(bp)), x.getId()));
        }
    }

    public void deletePatientDocument(int pId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(pId);
        for (Documents x : p.getDocumentses()) {
            if (x.getInfedregister() == 1) {
                BasePatientKey pd = converter.convertReadPatientDocument(x);
                addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_DOCUMENT_UPDATE, toXml(of.createDocumentKey(pd)), x.getId()));
            }
        }
    }

    public void createPatientRelative() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Patient p : patientFacade.loadPatientWithRelative()) {
            for (Relative x : p.getRelativeSet()) {
                if (x.getInfedregister() == 0) {
                    CreatePatientRelatives pd = converter.convertCreatePatientRelative(x);
                    addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_RELATIVE_CREATE, toXml(of.createCreateRelatives(pd)), x.getId()));
                }
            }
        }
    }

    public void updatePatientRelative(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(patId);
        for (Relative x : p.getRelativeSet()) {
            if (x.getInfedregister() == 1) {
                UpdatePatientRelative pd = converter.convertUpdatePatientRelative(x);
                addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_RELATIVE_UPDATE, toXml(of.createUpdateRelative(pd)), x.getId()));
            }
        }
    }

    public void readListPatientRelative(int pId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(pId);
        BasePatientKey bp = converter.convertBasePatientKey(p);
        addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_RELATIVE_LIST, toXml(of.createPatientKey(bp)), pId));
    }

    public void readPatientRelative(int pId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(pId);
        for (Relative x : p.getRelativeSet()) {
            if (x.getInfedregister() == 1) {
                PatientRelativeKey bp = converter.convertReadPatientRelative(x);
                addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_RELATIVE_READ, toXml(of.createRelativeKey(bp)), x.getId()));
            }
        }
    }

    public void deletePatientRelative(int pId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(pId);
        for (Relative x : p.getRelativeSet()) {
            if (x.getInfedregister() == 1) {
                PatientRelativeKey bp = converter.convertReadPatientRelative(x);
                addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_RELATIVE_DELETE, toXml(of.createRelativeKey(bp)), x.getId()));
            }
        }
    }

    public void updatePatient(int pId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(pId);
        UpdatePatient up = converter.convertUpdatePatient(p);
        addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), PATIENT_UPDATE, toXml(of.createUpdatePatient(up)), p.getId()));
    }

    public void createHiv(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient pat = patientFacade.find(patId);
        for (Hiv x : pat.getHivSet()) {
            CreateHivRegistry chr = converter.convertCreateHivRegistry(x);
            addTask(new FRNode(x.getMoid(), HIV_CREATE, toXml(of.createCreateHiv(chr)), x.getId()));
        }
    }

    public void createHiv() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hiv h : hivFacade.loadHivForSend()) {
            CreateHivRegistry chr = converter.convertCreateHivRegistry(h);
            addTask(new FRNode(h.getMoid(), HIV_CREATE, toXml(of.createCreateHiv(chr)), h.getId()));
        }
    }

    public void readHiv(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient pat = patientFacade.find(patId);
        if (pat != null && pat.getHivSet() != null) {
            for (Hiv x : pat.getHivSet()) {
                if (x.getInfedregister() == 0) {
                    BaseRegistryKey chr = converter.convertRegistryKey(pat);
                    addTask(new FRNode(x.getMoid(), HIV_READ, toXml(of.createRegistryKey(chr)), x.getId()));
                }
            }
        }
    }

    public void readHiv() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Patient p : patientFacade.loadPatientsForHivRead()) {
            for (Hiv x : p.getHivSet()) {
                BaseRegistryKey chr = converter.convertRegistryKey(p);
                addTask(new FRNode(x.getMoid(), HIV_READ, toXml(of.createRegistryKey(chr)), x.getId()));
            }
        }

    }

    public void updateHivPatient(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient pat = patientFacade.find(patId);
        for (Hiv x : pat.getHivSet()) {
            if (x.getInfedregister() == 1) {
                UpdateHivRegistry chr = converter.convertUpdateHivRegistry(x);
                addTask(new FRNode(x.getMoid(), HIV_UPDATE, toXml(of.createUpdateHiv(chr)), x.getId()));
            }
        }
    }

    public void createAddress() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Patient pat : patientFacade.loadPatientsForCreateAddr()) {
            for (Hivaddress x : pat.getHivaddressSet()) {
                if (x.getInfedregister() == 0) {
                    CreateHivAddress chr = converter.convertCreateHivAddress(x, pat);
                    addTask(new FRNode(pat.getHivSet().iterator().next().getMoid(), HIV_ADDRESS_CREATE, toXml(of.createCreateHivAddress(chr)), x.getId()));
                }
            }
        }
    }

    public void createHivAddress(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient pat = patientFacade.find(patId);
        for (Hivaddress x : pat.getHivaddressSet()) {
            if (x.getInfedregister() == 0) {
                CreateHivAddress chr = converter.convertCreateHivAddress(x, pat);
                addTask(new FRNode(pat.getHivSet().iterator().next().getMoid(), HIV_ADDRESS_CREATE, toXml(of.createCreateHivAddress(chr)), x.getId()));
            }
        }
    }

    public void listHivAddress(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(patId);
        RegistryKey cr = converter.convertHivRegistryKey(p);
        addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), HIV_ADDRESS_LIST, toXml(of.createRegistryKey(cr)), p.getId()));
    }

    public void readHivAddress(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(patId);
        p.getHivaddressSet().stream().filter((x) -> (x.getInfedregister() == 1)).forEachOrdered((x) -> {
            HivAddressKey hak = converter.convertHivAddressKey(x);
            addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), HIV_ADDRESS_READ, toXml(of.createHivAddressKey(hak)), x.getId()));
        });
    }

    public void updateHivAddress(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(patId);
        p.getHivaddressSet().stream().filter((x) -> (x.getInfedregister() == 1)).forEachOrdered((x) -> {
            UpdateHivAddress hak = converter.convertUpdateHivAddressKey(x);
            addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), HIV_ADDRESS_UPDATE, toXml(of.createUpdateHivAddress(hak)), x.getId()));
        });
    }

    public void deleteHivAddress(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(patId);
        p.getHivaddressSet().stream().filter((x) -> (x.getInfedregister() == 1)).forEachOrdered((x) -> {
            HivAddressKey hak = converter.convertHivAddressKey(x);
            addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), HIV_ADDRESS_DELETE, toXml(of.createHivAddressKey(hak)), x.getId()));
        });
    }

    public void readDispCardList(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient pat = patientFacade.find(patId);
        RegistryKey chr = converter.convertHivRegistryKey(pat);
        addTask(new FRNode(pat.getHivSet().iterator().next().getMoid(), HIV_DISP_LIST, toXml(of.createHivRegistryKey(chr)), patId));
    }

    public void readDispCardList() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Patient pat : patientFacade.loadPatientWithDispCard()) {
            RegistryKey chr = converter.convertHivRegistryKey(pat);
            addTask(new FRNode(pat.getHivSet().iterator().next().getMoid(), HIV_DISP_LIST, toXml(of.createHivRegistryKey(chr)), pat.getHivdispsSet().iterator().next().getId()));
        }
    }

    public void readDispCard(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient pat = patientFacade.find(patId);
        for (Hivdisp x : pat.getHivdispsSet()) {
            if (x.getInfedregister() == 1) {
                DispcardKey d = converter.convertDispCardKey(x);
                addTask(new FRNode(x.getMoid(), HIV_DISP_READ, toXml(of.createDispcardKey(d)), x.getId()));
            }
        }
    }

    public void createDispCardPatient(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient pat = patientFacade.find(patId);
        for (Hivdisp x : pat.getHivdispsSet()) {
            if (x.getInfedregister() == 0) {
                CreateDispcard d = converter.convertCreateHivDispCard(x);
                addTask(new FRNode(x.getMoid(), HIV_DISP_СREATE, toXml(of.createCreateDispcard(d)), x.getId()));
            }
        }
    }

    public void createDispCardPatient() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Patient pat : patientFacade.loadPatientWithOutDispCard()) {
            for (Hivdisp x : pat.getHivdispsSet()) {
                if (x.getInfedregister() == 0) {
                    CreateDispcard d = converter.convertCreateHivDispCard(x);
                    addTask(new FRNode(x.getMoid(), HIV_DISP_СREATE, toXml(of.createCreateDispcard(d)), x.getId()));
                }
            }
        }
    }

    public void updateDispCardPatient(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient pat = patientFacade.find(patId);
        for (Hivdisp x : pat.getHivdispsSet()) {
            if (x.getInfedregister() == 1) {
                UpdateDispcard d = converter.convertUpdateHivDispCard(x);
                addTask(new FRNode(x.getMoid(), HIV_DISP_UPDATE, toXml(of.createUpdateDispcard(d)), x.getId()));
            }
        }
    }

    public void createHivDispDesease() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivdispdeseases hr : hivdispdeseasesFacade.loadHivDispDeseases()) {
            CreateDispcardDeseaseKey cr = converter.convertCreateHivDispcardDesease(hr);
            addTask(new FRNode(hr.getRefhivdisp().getMoid(), HIV_DISP_DESEASE_CREATE, toXml(of.createCreateHivDispcardDesease(cr)), hr.getId()));
        }
    }

    public void updateHivDispDesease(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient pat = patientFacade.find(patId);
        Hivdispdeseases hd = pat.getHivdispsSet().iterator().next().getHivdispdeseasesSet().iterator().next();
        UpdateDispcardDeseaseKey cr = converter.convertUpdateHivDispcardDesease(hd);
        addTask(new FRNode(hd.getRefhivdisp().getMoid(), HIV_DISP_DESEASE_UPDATE, toXml(of.createUpdateHivDispcardDesease(cr)), hd.getId()));
    }

    public void readListHivDispDesease(int dCardId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdisp hdisp = hivdispFacade.find(dCardId);
        DispcardKey dk = converter.convertDispCardKey(hdisp);
        addTask(new FRNode(hdisp.getMoid(), HIV_DISP_DESEASE_LIST, toXml(of.createDispcardKey(dk)), dCardId));
    }

    public void readHivDispDesease(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient pat = patientFacade.find(patId);
        Hivdispdeseases hd = pat.getHivdispsSet().iterator().next().getHivdispdeseasesSet().iterator().next();
        DispcardDeseaseKey dispcardDeseaseKey = converter.convertReadHivDispcardDesease(hd);
        addTask(new FRNode(hd.getRefhivdisp().getMoid(), HIV_DISP_DESEASE_READ, toXml(of.createHivDispcardDeseaseKey(dispcardDeseaseKey)), hd.getId()));
    }

    public void createHivRecipe() throws DatatypeConfigurationException, Exception {
        for (Hivrecipe hr : hivrecipeFacade.loadHivRecipe()) {
            if (hr != null) {
                CreateHivRecipe cr = converter.convertCreateHivRecipe(hr);
                addTask(new FRNode(hr.getMoid(), HIV_RECIPE_CREATE, toXml(of.createCreateHivRecipe(cr)), hr.getId()));
            }
        }
    }

    public void createHivRecipe(int recipeId) throws DatatypeConfigurationException, EmptyResultExcepton, Exception {
        Hivrecipe hr = hivrecipeFacade.find(recipeId);
        CreateHivRecipe cr = converter.convertCreateHivRecipe(hr);
        addTask(new FRNode(hr.getMoid(), HIV_RECIPE_CREATE, toXml(of.createCreateHivRecipe(cr)), hr.getId()));
    }

    public void listHivRecipe(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(patId);
        addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), HIV_RECIPE_LIST, toXml(converter.convertHivRegistryNumber(p)), p.getId()));
    }

    public void createHivDispExam() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivdispexam h : hivdispexamFacade.loadDispExam()) {
            CreateDispcardExamKey che = converter.convertCreateHivDispExam(h);
            addTask(new FRNode(h.getMoid(), HIV_DISP_EXAM_CREATE, toXml(of.createCreateHivDispcardExam(che)), h.getId()));
        }
    }

    public void listHivDispExam(int hivDispId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdisp hd = hivdispFacade.find(hivDispId);
        DispcardKey cr = converter.convertDispCardKey(hd);
        addTask(new FRNode(hd.getMoid(), HIV_DISP_EXAM_LIST, toXml(of.createDispcardKey(cr)), hd.getId()));
    }

    public void readHivDispExam(int hivDispExamId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispexam hd = hivdispexamFacade.find(hivDispExamId);
        DispcardExamKey cr = converter.convertReadHivDispExamKey(hd);
        addTask(new FRNode(hd.getMoid(), HIV_DISP_EXAM_READ, toXml(of.createHivDispcardExamKey(cr)), hd.getId()));
    }

    public void updateHivDispExam(int hivDispExamId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispexam hd = hivdispexamFacade.find(hivDispExamId);
        UpdateDispcardExamKey cr = converter.convertUpdateDispcardExamKey(hd);
        addTask(new FRNode(hd.getMoid(), HIV_DISP_EXAM_UPDATE, toXml(of.createUpdateHivDispcardExam(cr)), hd.getId()));
    }

    public void convertUpdateDispcardExamKey(Patient p, NrHivDispcardExam ex) throws DatatypeConfigurationException, EmptyResultExcepton {
        UpdateDispcardExamKey cr = converter.convertUpdateDispcardExamKey(p, ex);
        addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), HIV_DISP_EXAM_UPDATE, toXml(of.createUpdateHivDispcardExam(cr)), p.getId()));
    }

    public void createHivExam() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivdispexam h : hivdispexamFacade.loadHivExam()) {
            CreateHivExam che = converter.convertHivExam(h);
            addTask(new FRNode(h.getMoid(), HIV_EXAM_CREATE, toXml(of.createCreateHivExam(che)), h.getId()));
        }
    }

    public void listHivExam(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(patId);
        RegistryKey cr = converter.convertHivRegistryKey(p);
        addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), HIV_EXAM_LIST, toXml(of.createRegistryKey(cr)), p.getId()));
    }

    public void readHivExam(int hivDispExamId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispexam hd = hivdispexamFacade.find(hivDispExamId);
        HivExamKey cr = converter.convertReadHivExamKey(hd);
        addTask(new FRNode(hd.getMoid(), HIV_EXAM_READ, toXml(of.createHivExamKey(cr)), hd.getId()));
    }

    public void updateHivExam(int hivDispExamId) throws DatatypeConfigurationException, EmptyResultExcepton, EmptyResultExcepton {
        Hivdispexam hd = hivdispexamFacade.find(hivDispExamId);
        UpdateHivExam cr = converter.convertUpdateHivExamKey(hd);
        addTask(new FRNode(hd.getMoid(), HIV_EXAM_UPDATE, toXml(of.createUpdateHivExam(cr)), hd.getId()));
    }

    public void createHivEpid() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivepid h : hivepidFacade.loadHivEpid()) {
            CreateHivEpid chi = converter.convertCreateEpidData(h);
            addTask(new FRNode(h.getMoId(), HIV_EPID_CREATE, toXml(of.createCreateHivEpid(chi)), h.getId()));
        }
    }

    public void updateHivEpid() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivepid h : hivepidFacade.loadHivEpidForUpdate()) {
            UpdateHivEpid chi = converter.convertUpdateEpidData(h);
            addTask(new FRNode(h.getMoId(), HIV_EPID_UPDATE, toXml(of.createUpdateHivEpid(chi)), h.getId()));
        }
    }

    public void updateHivEpid(int hivEpidId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivepid hivEpid = hivepidFacade.find(hivEpidId);
        UpdateHivEpid cr = converter.convertUpdateEpidData(hivEpid);
        addTask(new FRNode(hivEpid.getMoId(), HIV_EPID_UPDATE, toXml(of.createUpdateHivEpid(cr)), hivEpid.getId()));
    }

    public void readHivEpid(int patId) throws DatatypeConfigurationException, EmptyResultExcepton, EmptyResultExcepton {
        Patient p = patientFacade.find(patId);
        RegistryKey rk = converter.convertHivRegistryKey(p);
        addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), HIV_EPID_READ, toXml(of.createHivRegistryKey(rk)), patId));
    }

    public void createHivVisits() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivdispvisits visit : hivdispvisitsFacade.loadHivDispVisit()) {
            CreateDispcardVisitKey createVisit = converter.converCreateDispcardVisit(visit);
            addTask(new FRNode(visit.getRefpatient().getHivSet().iterator().next().getMoid(), HIV_DISP_VISIT_CREATE, toXml(of.createCreateHivDispcardVisit(createVisit)), visit.getId()));
        }
    }

    public void listHivVisits(int dCardId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdisp hd = hivdispFacade.find(dCardId);
        DispcardKey rk = converter.convertDispCardKey(hd);
        addTask(new FRNode(hd.getMoid(), HIV_DISP_VISIT_LIST, toXml(of.createDispcardKey(rk)), dCardId));
    }

    public void readHivVisits(int visitId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispvisits h = hivdispvisitsFacade.find(visitId);
        DispcardVisitKey rk = converter.readDispCardVisit(h);
        addTask(new FRNode(h.getRefpatient().getHivSet().iterator().next().getMoid(), HIV_DISP_VISIT_READ, toXml(of.createHivDispcardVisitKey(rk)), visitId));
    }

    public void updateHivVisits(int visitId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispvisits h = hivdispvisitsFacade.find(visitId);
        UpdateDispcardVisitKey rk = converter.converUpdateDispcardVisit(h);
        addTask(new FRNode(h.getRefpatient().getHivSet().iterator().next().getMoid(), HIV_DISP_VISIT_UPDATE, toXml(of.createUpdateHivDispcardVisit(rk)), visitId));
    }

    public void createAVRT() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivavrt h : hivavrtFacade.loadHivAVRT()) {
            CreateHivAvrt che = converter.convertHivAVRT(h);
            addTask(new FRNode(h.getPatient().getHivSet().iterator().next().getMoid(), HIV_AVRT_CREATE, toXml(of.createCreateHivAvrt(che)), h.getId()));
        }
    }

    public void updateAVRT(int avrtId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivavrt h = hivavrtFacade.find(avrtId);
        UpdateHivAvrt che = converter.convertUpdateHivAVRT(h);
        addTask(new FRNode(h.getPatient().getHivSet().iterator().next().getMoid(), HIV_AVRT_UPDATE, toXml(of.createUpdateHivAvrt(che)), h.getId()));
    }

    public void updateAVRT(NrHivRegistryAvrt ha, int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(patId);
        Hivavrt h = hivavrtFacade.loadLastHivAVRT(patId);
        UpdateHivAvrt che = converter.convertUpdateHivAVRT(ha, h);
        addTask(new FRNode(h.getPatient().getHivSet().iterator().next().getMoid(),HIV_AVRT_UPDATE, toXml(of.createUpdateHivAvrt(che)), h.getId()));
    }

    public void updateAVRT() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivavrt h : hivavrtFacade.loadForUpdateClose()) {
            UpdateHivAvrt che = converter.convertUpdateHivAVRT(h);
            addTask(new FRNode(h.getPatient().getHivSet().iterator().next().getMoid(), HIV_AVRT_UPDATE, toXml(of.createUpdateHivAvrt(che)), h.getId()));
        }
    }

    public void readListAVRT(int patId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Patient p = patientFacade.find(patId);
        RegistryKey rk = converter.convertHivRegistryKey(p);
        addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), HIV_AVRT_LIST, toXml(of.createHivRegistryKey(rk)), patId));
    }

    public void readListAVRT() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Patient p : hivavrtFacade.loadForListClose()) {
            RegistryKey rk = converter.convertHivRegistryKey(p);
            addTask(new FRNode(p.getHivSet().iterator().next().getMoid(), HIV_AVRT_LIST, toXml(of.createHivRegistryKey(rk)), p.getId()));
        }
    }

    public void readAVRT(int avrtId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivavrt ha = hivavrtFacade.find(avrtId);
        HivAvrtKey rk = converter.convertReadAVRT(ha);
        addTask(new FRNode(ha.getPatient().getHivSet().iterator().next().getMoid(), HIV_AVRT_READ, toXml(of.createHivAvrtKey(rk)), ha.getId()));
    }

    public void createHivDispHosp() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivdisphosp hh : hivdisphospFacade.loadHivDispHosp()) {
            CreateDispcardHospKey che = converter.convertCreateHivDipsHosp(hh);
            addTask(new FRNode(hh.getMooid(), HIV_DISP_HOSP_CREATE, toXml(of.createCreateHivDispcardHosp(che)), hh.getId()));
        }
    }

    public void updateHivDispHosp(int hospId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdisphosp hh = hivdisphospFacade.find(hospId);
        UpdateDispcardHospKey che = converter.convertUpdateHivDipsHosp(hh);
        addTask(new FRNode(hh.getMooid(), HIV_DISP_HOSP_UPDATE, toXml(of.createUpdateHivDispcardHosp(che)), hh.getId()));
    }

    public void readHivDispHosp(int hospId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdisphosp hh = hivdisphospFacade.find(hospId);
        DispcardHospKey che = converter.convertDispcardHospKey(hh);
        addTask(new FRNode(hh.getMooid(), HIV_DISP_HOSP_READ, toXml(of.createHivDispcardHospKey(che)), hh.getId()));
    }

    public void listHivDispHosp(int dCardId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdisp hd = hivdispFacade.find(dCardId);
        DispcardKey dk = converter.convertDispCardKey(hd);
        addTask(new FRNode(hd.getMoid(), HIV_DISP_HOSP_LIST, toXml(of.createDispcardKey(dk)), hd.getId()));
    }

    public void createHivDispChemo() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivdispchemo hh : hivdispchemoFacade.loadHivDispChemo()) {
            CreateDispcardChemoKey che = converter.convertCreateHivDispChemo(hh);
            addTask(new FRNode(hh.getHivdisp().getMoid(), HIV_DISP_CHEMO_CREATE, toXml(of.createCreateHivDispcardChemo(che)), hh.getId()));
        }
    }

    public void updateHivDispChemo(int chemoId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispchemo hh = hivdispchemoFacade.find(chemoId);
        UpdateDispcardChemoKey che = converter.convertUpdateHivDispChemo(hh);
        addTask(new FRNode(hh.getHivdisp().getMoid(), HIV_DISP_CHEMO_UPDATE, toXml(of.createUpdateHivDispcardChemo(che)), hh.getId()));
    }

    public void readHivDispChemo(int chemoId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispchemo hh = hivdispchemoFacade.find(chemoId);
        DispcardChemoKey che = converter.convertDispcardChemoKey(hh);
        addTask(new FRNode(hh.getHivdisp().getMoid(), HIV_DISP_CHEMO_READ, toXml(of.createHivDispcardChemoKey(che)), hh.getId()));
    }

    public void listHivDispChemo(int dCardId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdisp hd = hivdispFacade.find(dCardId);
        DispcardKey dk = converter.convertDispCardKey(hd);
        addTask(new FRNode(hd.getMoid(), HIV_DISP_CHEMO_LIST, toXml(of.createDispcardKey(dk)), dCardId));
    }

    public void createHivDispCheckup() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivdispchekup hh : hivdispchekupFacade.loadHivDispChekup()) {
            CreateDispcardCheckupKey che = converter.convertCreateHivDispCheckup(hh);
            addTask(new FRNode(hh.getHivdisp().getMoid(), HIV_DISP_CHECKUP_CREATE, toXml(of.createCreateHivDispcardCheckup(che)), hh.getId()));
        }
    }

    public void updateHivDispCheckup(int checkUpId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispchekup hh = hivdispchekupFacade.find(checkUpId);
        UpdateDispcardCheckupKey che = converter.convertUpdateHivDispCheckup(hh);
        addTask(new FRNode(hh.getHivdisp().getMoid(), HIV_DISP_CHECKUP_UPDATE, toXml(of.createUpdateHivDispcardCheckup(che)), hh.getId()));
    }

    public void readHivDispCheckup(int checkUpId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispchekup hh = hivdispchekupFacade.find(checkUpId);
        DispcardCheckupKey che = converter.convertDispcardCheckupKey(hh);
        addTask(new FRNode(hh.getHivdisp().getMoid(), HIV_DISP_CHECKUP_READ, toXml(of.createHivDispcardCheckupKey(che)), hh.getId()));
    }

    public void listHivDispCheckup(int dCardId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdisp hd = hivdispFacade.find(dCardId);
        DispcardKey dk = converter.convertDispCardKey(hd);
        addTask(new FRNode(hd.getMoid(), HIV_DISP_CHECKUP_LIST, toXml(of.createDispcardKey(dk)), hd.getId()));
    }

    public void createHivDispStage() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivdispstage hh : hivdispstageFacade.loadHivDispStage()) {
            CreateDispcardStageKey che = converter.convertCreateHivDispStage(hh);
            addTask(new FRNode(hh.getHivdisp().getMoid(), HIV_DISP_STAGE_CREATE, toXml(of.createCreateHivDispcardStage(che)), hh.getId()));
        }
    }

    public void updateHivDispStage(int stageId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispstage hh = hivdispstageFacade.find(stageId);
        UpdateDispcardStageKey che = converter.convertUpdateHivDispStage(hh);
        addTask(new FRNode(hh.getHivdisp().getMoid(), HIV_DISP_STAGE_UPDATE, toXml(of.createUpdateHivDispcardStage(che)), hh.getId()));
    }

    public void readHivDispStage(int stageId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispstage hh = hivdispstageFacade.find(stageId);
        DispcardStageKey che = converter.convertDispcardStageKey(hh);
        addTask(new FRNode(hh.getHivdisp().getMoid(), HIV_DISP_STAGE_READ, toXml(of.createHivDispcardStageKey(che)), hh.getId()));
    }

    public void listHivDispStage(int hivDispId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdisp hd = hivdispFacade.find(hivDispId);
        DispcardKey dk = converter.convertDispCardKey(hd);
        addTask(new FRNode(hd.getMoid(), HIV_DISP_STAGE_LIST, toXml(of.createDispcardKey(dk)), hivDispId));
    }

    public void createHivDispConc() throws DatatypeConfigurationException, EmptyResultExcepton {
        for (Hivdispconc hh : hivdispconcFacade.loadHivDispConc()) {
            CreateDispcardConcDeseaseKey che = converter.convertCreateHivDispConc(hh);
            addTask(new FRNode(hh.getHivdisp().getMoid(), HIV_DISP_CONC_CREATE, toXml(of.createCreateHivDispcardConcDesease(che)), hh.getId()));
        }
    }

    public void updateHivDispConc(int concId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispconc hh = hivdispconcFacade.find(concId);
        UpdateDispcardConcDeseaseKey che = converter.convertUpdateHivDispConc(hh);
        addTask(new FRNode(hh.getHivdisp().getMoid(), HIV_DISP_CONC_UPDATE, toXml(of.createUpdateHivDispcardConcDesease(che)), hh.getId()));
    }

    public void readHivDispConc(int concId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdispconc hh = hivdispconcFacade.find(concId);
        DispcardConcDeseaseKey che = converter.convertDispcardConcKey(hh);
        addTask(new FRNode(hh.getHivdisp().getMoid(), HIV_DISP_CONC_READ, toXml(of.createHivDispcardConcDeseaseKey(che)), hh.getId()));
    }

    public void listHivDispConc(int dCardId) throws DatatypeConfigurationException, EmptyResultExcepton {
        Hivdisp hd = hivdispFacade.find(dCardId);
        DispcardKey dk = converter.convertDispCardKey(hd);
        addTask(new FRNode(hd.getMoid(), HIV_DISP_CONC_LIST, toXml(of.createDispcardKey(dk)), hd.getId()));
    }

    private void addTask(FRNode frn) {
        try (JMSContext jmsContext = connectionFactory.createContext()) {
            jmsContext.createProducer().setDeliveryMode(DeliveryMode.PERSISTENT).setDeliveryDelay(300)
                    .setTimeToLive(60 * 60 * 1000).send(queue, frn);
        }
    }

}
