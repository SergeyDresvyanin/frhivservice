/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import com.initmed.frhivservice.jms.ResponseTask;
import java.util.Base64;
import jakarta.annotation.Resource;
import jakarta.jms.ConnectionFactory;
import jakarta.jms.DeliveryMode;
import jakarta.jms.JMSContext;
import jakarta.jms.Queue;
import jakarta.jws.HandlerChain;
import jakarta.jws.WebService;
import static java.nio.charset.StandardCharsets.UTF_8;
import org.apache.cxf.annotations.SchemaValidation;

/**
 *
 * @author sd199
 */
@SchemaValidation
@WebService(serviceName = "callback", portName = "CallbackPort", endpointInterface = "ru.rt.eu.nr.service.mis.callback.emu.Callback", targetNamespace = "http://emu.callback.mis.service.nr.eu.rt.ru/", wsdlLocation = "WEB-INF/wsdl/reciverService.wsdl")
@HandlerChain(file = "handler-chain.xml")
public class callback {

    @Resource
    private ConnectionFactory connectionFactory;
    @Resource(lookup = "jms/RestCallbackQueue")
    private Queue queue;
//    @EJB
//    private CallBackConverter callBackConverter;

    //Обязательное условие ФРВИЧ
    private static final int OK = 0;

    public int sendResponse(String id, String oid, String response) {
        try (JMSContext jmsContext = connectionFactory.createContext()) {
            jmsContext.createProducer().setDeliveryMode(DeliveryMode.PERSISTENT).setDeliveryDelay(300)
                    .setTimeToLive(24 * 60 * 60 * 1000).send(queue, new ResponseTask(id, oid, new String(Base64.getDecoder().decode(response), UTF_8)));
        }
        return OK;
    }

}
