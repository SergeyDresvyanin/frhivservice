/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import static com.initmed.frhivservice.bdmodel.converter.EntityUtil.clearError;
import static com.initmed.frhivservice.bdmodel.converter.EntityUtil.parseAVRT;
import static com.initmed.frhivservice.bdmodel.converter.EntityUtil.parseHIVDISPLIST;
import com.initmed.frhivservice.bdmodel.facades.DocumentsFacade;
import com.initmed.frhivservice.bdmodel.facades.EmptyResultExcepton;
import com.initmed.frhivservice.bdmodel.facades.FrmessagesyncFacade;
import com.initmed.frhivservice.bdmodel.facades.HivFacade;
import com.initmed.frhivservice.bdmodel.facades.HivaddressFacade;
import com.initmed.frhivservice.bdmodel.facades.HivavrtFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispchekupFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispchemoFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispconcFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispdeseasesFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispexamFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdisphospFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispstageFacade;
import com.initmed.frhivservice.bdmodel.facades.HivdispvisitsFacade;
import com.initmed.frhivservice.bdmodel.facades.HivepidFacade;
import com.initmed.frhivservice.bdmodel.facades.HivrecipeFacade;
import com.initmed.frhivservice.bdmodel.facades.PatientFacade;
import com.initmed.frhivservice.bdmodel.facades.RelativeFacade;
import com.initmed.frhivservice.bdmodel.facades.ScheduleprocessParamsFacade;
import static com.initmed.frhivservice.bdmodel.facades.ScheduleprocessParamsFacade.CREATE_PATIENT;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_ADDRESS_CREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_CREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_DISP_EXAM_CREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_DISP_LIST;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_DISP_СREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_READ;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_RECIPE_CREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.PATIENT_CREATE;
import static com.initmed.frhivservice.controller.FRServiceMethods.HIV_EXAM_CREATE;
import com.initmed.frhivservice.entity.Documents;
import com.initmed.frhivservice.entity.Frmessagesync;
import com.initmed.frhivservice.entity.Hiv;
import com.initmed.frhivservice.entity.Hivaddress;
import com.initmed.frhivservice.entity.Hivavrt;
import com.initmed.frhivservice.entity.Hivdisp;
import com.initmed.frhivservice.entity.Hivdispchekup;
import com.initmed.frhivservice.entity.Hivdispchemo;
import com.initmed.frhivservice.entity.Hivdispconc;
import com.initmed.frhivservice.entity.Hivdispdeseases;
import com.initmed.frhivservice.entity.Hivdispexam;
import com.initmed.frhivservice.entity.Hivdisphosp;
import com.initmed.frhivservice.entity.Hivdispstage;
import com.initmed.frhivservice.entity.Hivdispvisits;
import com.initmed.frhivservice.entity.Hivepid;
import com.initmed.frhivservice.entity.Hivrecipe;
import com.initmed.frhivservice.entity.Patient;
import com.initmed.frhivservice.entity.Relative;
import com.initmed.frhivservice.frdatamodel.HivDispcards;
import com.initmed.frhivservice.frdatamodel.NrHivDispcard;
import com.initmed.frhivservice.frdatamodel.NrHivRegistryAvrt;
import com.initmed.frhivservice.frdatamodel.NrHivRegistryAvrts;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.Asynchronous;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

/**
 *
 * @author sd199
 */
@Stateless
public class CallBackConverter {

    @EJB
    private volatile FRHIVServiceCommand fRHIVServiceCommand;
    @EJB
    private volatile FrmessagesyncFacade frmessagesyncFacade;
    @EJB
    private volatile PatientFacade patientFacade;
    @EJB
    private volatile HivFacade hivFacade;
    @EJB
    private volatile HivdispFacade hivdispFacade;
    @EJB
    private volatile HivaddressFacade hivaddressFacade;
    @EJB
    private volatile HivrecipeFacade hivrecipeFacade;
    @EJB
    private volatile HivdispexamFacade hivdispexamFacade;
    @EJB
    private volatile HivdispvisitsFacade hivdispvisitsFacade;
    @EJB
    private volatile HivavrtFacade hivavrtFacade;
    @EJB
    private volatile DocumentsFacade documentsFacade;
    @EJB
    private volatile HivepidFacade hivepidFacade;
    @EJB
    private volatile HivdispdeseasesFacade hivdispdeseasesFacade;
    @EJB
    private volatile HivdispchekupFacade hivdispchekupFacade;
    @EJB
    private volatile HivdispchemoFacade hivdispchemoFacade;
    @EJB
    private volatile HivdispstageFacade hivdispstageFacade;
    @EJB
    private volatile HivdispconcFacade hivdispconcFacade;
    @EJB
    private volatile HivdisphospFacade hivdisphospFacade;
    @EJB
    private volatile RelativeFacade relativeFacade;
    @EJB
    private ScheduleprocessParamsFacade scheduleprocessParamsFacade;

    public CallBackConverter() {
    }

    @Asynchronous
    public void parseMessage(String oid, String msgId, String msg) throws InterruptedException {
        Frmessagesync frs = frmessagesyncFacade.getMessage(msgId);
        if (frs != null) {
            frs.setMoOid(oid);
            frs.setRespoceReceived((short) 1);
            frmessagesyncFacade.edit(frs);

            //Парсим ошибки            
            if (msg != null && msg.contains("error")) {
                msg = clearError(msg);
                if (frs.getRefentity() != null && msg.equals("VALIDATION_FAILED Сведения об исследовании указанного типа на заданную дату уже существуют в КДН")) {
                    Hivdispexam he = hivdispexamFacade.find(frs.getRefentity());
                    he.setInfedregister((short) 1);
                    hivdispexamFacade.edit(he);
                } else if (frs.getRefentity() != null && msg.equals("VALIDATION_FAILED Сведения о посещении заданного типа на заданную дату с указанным врачом уже существует в КДН")) {
                    Hivdispvisits hv = hivdispvisitsFacade.find(frs.getRefentity());
                    hv.setInfedregister((short) 1);
                    hivdispvisitsFacade.edit(hv);
                } else if (frs.getRefentity() != null && msg.equals("VALIDATION_FAILED Сведения о стадии заболевания на заданную дату уже существует в КДН")) {
                    Hivdispstage hs = hivdispstageFacade.find(frs.getRefentity());
                    hs.setInfedregister((short) 1);
                    hivdispstageFacade.edit(hs);
                } else if (frs.getRefentity() != null && msg.equals("VALIDATION_FAILED Эпидемиологические сведения уже существуют")) {
                    Hivepid e = hivepidFacade.find(frs.getRefentity());
                    e.setInfedregister((short) 1);
                    e.setNeedsupdate(true);
                    hivepidFacade.edit(e);
                } else if (frs.getRefentity() != null && msg.equals("VALIDATION_FAILED Диагноз на заданную дату уже существует в КДН")) {
                    Hivdispdeseases h = hivdispdeseasesFacade.find(frs.getRefentity());
                    h.setInfedregister((short) 1);
                    hivdispdeseasesFacade.edit(h);
                } else if (frs.getRefentity() != null && msg.equals("NOT_FOUND Не найдена регистровая запись")) {
                    frmessagesyncFacade.saveErrorMessage(frs, msg);
                    if (scheduleprocessParamsFacade.loadParameter(CREATE_PATIENT)) {
                        try {
                            fRHIVServiceCommand.createHiv(hivFacade.find(frs.getRefentity()).getRefpatient().getId());
                        } catch (DatatypeConfigurationException ex) {
                            Logger.getLogger(CallBackConverter.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (EmptyResultExcepton ex) {
                            Logger.getLogger(CallBackConverter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                } else {
                    frmessagesyncFacade.saveErrorMessage(frs, msg);
                    //if pat exists
                    if (frs.getMethod() != null) {
                        if (msg.contains("Пациент с ключом")) {
                            patientFacade.setPatInFr(frs.getRefentity());
                        }
                        if (frs.getMethod().equals(HIV_DISP_LIST)) {
                            frs.setMessagedetail("У пациента отсуствует д карта");
                            frmessagesyncFacade.edit(frs);
                            try {
                                fRHIVServiceCommand.createDispCardPatient(frs.getRefentity());
                            } catch (DatatypeConfigurationException ex) {
                                Logger.getLogger(CallBackConverter.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (EmptyResultExcepton ex) {
                                Logger.getLogger(CallBackConverter.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    } else if (frs.getRefentity() != null
                            && msg.equals("VALIDATION_FAILED Не может быть больше одной открытой схемы лечения")) {
                        try {
                            Hivavrt ha = hivavrtFacade.find(frs.getRefentity());
                            fRHIVServiceCommand.readListAVRT(ha.getPatient().getId());
                        } catch (Exception ex) {
                            Logger.getLogger(CallBackConverter.class.getName()).log(Level.SEVERE, null, ex);
                        } 
                    }
                }
            } //парсим успешные
            else if (frs.getMethod() != null) {
                if (frs.getMethod().equals(PATIENT_CREATE)) {
                    patientFacade.setPatInFr(frs.getRefentity());
                } else if (frs.getMethod().equals(HIV_CREATE) || frs.getMethod().equals(HIV_READ)) {
                    String unrz = msg.substring(msg.indexOf("<registryNumber>") + 16, msg.indexOf("</registryNumber>"));
                    //save unrz
                    Hiv h = hivFacade.find(frs.getRefentity());
                    Patient p = h.getRefpatient();
                    p.setRegistrynumber(unrz);
                    p.setInfedregister((short) 1);
                    patientFacade.edit(p);
                    h.setInfedregister((short) 1);
                    hivFacade.edit(h);

                } else if (frs.getMethod().equals(HIV_ADDRESS_CREATE)) {
                    Hivaddress ha = hivaddressFacade.find(frs.getRefentity());
                    ha.setInfedregister((short) 1);
                    hivaddressFacade.edit(ha);
                } else if (frs.getMethod().equals(HIV_DISP_СREATE)) {
                    String cardNo = msg.substring(msg.indexOf("<cardNumber>") + 12, msg.indexOf("</cardNumber>"));
                    Hivdisp hd = hivdispFacade.find(frs.getRefentity());
                    hd.setCardnumber(cardNo);
                    hd.setInfedregister((short) 1);
                    hivdispFacade.edit(hd);
                } else if (frs.getMethod().equals(HIV_RECIPE_CREATE)) {
                    Hivrecipe hr = hivrecipeFacade.find(frs.getRefentity());
                    hr.setInfedregister((short) 1);
                    hivrecipeFacade.edit(hr);
                } else if (frs.getMethod().equals(HIV_DISP_EXAM_CREATE) || frs.getMethod().equals(HIV_EXAM_CREATE)) {
                    Hivdispexam he = hivdispexamFacade.find(frs.getRefentity());
                    he.setInfedregister((short) 1);
                    hivdispexamFacade.edit(he);
                } else if (frs.getMethod().equals(FRServiceMethods.HIV_DISP_VISIT_CREATE)) {
                    Hivdispvisits hv = hivdispvisitsFacade.find(frs.getRefentity());
                    hv.setInfedregister((short) 1);
                    hivdispvisitsFacade.edit(hv);
                } else if (frs.getMethod().equals(FRServiceMethods.PATIENT_DOCUMENT_CREATE)) {
                    Documents d;
                    try {
                        d = documentsFacade.loadDocuments(patientFacade.find(frs.getRefentity()));
                        d.setInfedregister((short) 1);
                        documentsFacade.edit(d);
                    } catch (EmptyResultExcepton ex) {
                        Logger.getLogger(CallBackConverter.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (frs.getMethod().equals(HIV_DISP_LIST)) {
                    try {
                        HivDispcards dispcardList = parseHIVDISPLIST(msg);
                        NrHivDispcard dispCard = dispcardList.getDispcard().stream().filter(dc -> dc.getOutDate() == null).findAny().orElse(null);
                        if (dispCard != null) {
                            Hivdisp hd = hivdispFacade.find(frs.getRefentity());
                            hd.setCardnumber(dispCard.getCardNumber());
                            hd.setInfedregister((short) 1);
                            hivdispFacade.edit(hd);
                        } else {
                            frs.setMessagedetail("У пациента отсуствует д карта");
                            frmessagesyncFacade.edit(frs);
//                            Hivdisp hdd = hivdispFacade.find(frs.getRefentity());
                        }
                    } catch (JAXBException ex) {
                        frs.setMessagedetail(ex.getMessage());
                        Logger.getLogger(CallBackConverter.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    frmessagesyncFacade.edit(frs);
                } else if (frs.getMethod().equals(FRServiceMethods.HIV_EPID_CREATE)) {
                    Hivepid e = hivepidFacade.find(frs.getRefentity());
                    e.setInfedregister((short) 1);
                    hivepidFacade.edit(e);
                } else if (frs.getMethod().equals(FRServiceMethods.HIV_EPID_UPDATE)) {
                    Hivepid e = hivepidFacade.find(frs.getRefentity());
                    e.setNeedsupdate(false);
                    hivepidFacade.edit(e);
                } else if (frs.getMethod().equals(FRServiceMethods.HIV_DISP_DESEASE_CREATE)) {
                    Hivdispdeseases h = hivdispdeseasesFacade.find(frs.getRefentity());
                    h.setInfedregister((short) 1);
                    hivdispdeseasesFacade.edit(h);
                } else if (frs.getMethod().equals(FRServiceMethods.HIV_AVRT_CREATE)) {
                    Hivavrt ha = hivavrtFacade.find(frs.getRefentity());
                    ha.setInfedregister((short) 1);
                    hivavrtFacade.edit(ha);
                } else if (frs.getMethod().equals(FRServiceMethods.HIV_AVRT_LIST)) {
                    try {
                        NrHivRegistryAvrts avrtList = parseAVRT(msg);
                        Optional<NrHivRegistryAvrt> ha = avrtList.getHivAvrt().stream().filter(x -> x.getEndDate() == null).findFirst();
                        if (ha.isPresent()) {
                            try {
                                fRHIVServiceCommand.updateAVRT(ha.get(), frs.getRefentity());
//                            hivavrtFacade.saveAvrt(ha.get(), frs.getRefentity());
                            } catch (DatatypeConfigurationException ex) {
                                Logger.getLogger(CallBackConverter.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    } catch (JAXBException | EmptyResultExcepton ex) {
                        Logger.getLogger(CallBackConverter.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (frs.getMethod().equals(FRServiceMethods.HIV_AVRT_UPDATE)) {
                    Hivavrt ha = hivavrtFacade.find(frs.getRefentity());
                    ha.setInfedregister((short) 1);
                    hivavrtFacade.edit(ha);
                    frmessagesyncFacade.deleteByIdAndMethod(frs.getRefentity(), FRServiceMethods.HIV_AVRT_CREATE);
                } else if (frs.getMethod().equals(FRServiceMethods.HIV_DISP_CHECKUP_CREATE)) {
                    Hivdispchekup hdc = hivdispchekupFacade.find(frs.getRefentity());
                    hdc.setInfedregister((short) 1);
                    hivdispchekupFacade.edit(hdc);
                } else if (frs.getMethod().equals(FRServiceMethods.HIV_DISP_HOSP_CREATE)) {
                    Hivdisphosp hh = hivdisphospFacade.find(frs.getRefentity());
                    hh.setInfedregister((short) 1);
                    hivdisphospFacade.edit(hh);
                } else if (frs.getMethod().equals(FRServiceMethods.HIV_DISP_CHEMO_CREATE)) {
                    Hivdispchemo hc = hivdispchemoFacade.find(frs.getRefentity());
                    hc.setInfedregister((short) 1);
                    hivdispchemoFacade.edit(hc);
                } else if (frs.getMethod().equals(FRServiceMethods.HIV_DISP_CONC_CREATE)) {
                    Hivdispconc hc = hivdispconcFacade.find(frs.getRefentity());
                    hc.setInfedregister((short) 1);
                    hivdispconcFacade.edit(hc);
                } else if (frs.getMethod().equals(FRServiceMethods.HIV_DISP_STAGE_CREATE)) {
                    Hivdispstage hs = hivdispstageFacade.find(frs.getRefentity());
                    hs.setInfedregister((short) 1);
                    hivdispstageFacade.edit(hs);
                } else if (frs.getMethod().equals(FRServiceMethods.PATIENT_RELATIVE_CREATE)) {
                    Relative r = relativeFacade.find(frs.getRefentity());
                    r.setInfedregister((short) 1);
                    relativeFacade.edit(r);
                }
            }
        } else {
            if (msg.contains("error")) {
                frmessagesyncFacade.createErrorMessage(oid, msgId, null, 0, clearError(msg));
            } else {
                frmessagesyncFacade.createMessage(oid, msgId, null, 0, (short) 1);
            }

        }
    }

}
