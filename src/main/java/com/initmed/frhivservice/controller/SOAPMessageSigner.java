package com.initmed.frhivservice.controller;

import com.initmed.frhivservice.util.CryptoHelper;
import com.initmed.frhivservice.util.XmlHelper;

import jakarta.xml.soap.*;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.util.logging.Level;
import jakarta.annotation.PostConstruct;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.xml.ws.BindingProvider;
import jakarta.xml.ws.handler.MessageContext;
import jakarta.xml.ws.handler.soap.SOAPHandler;
import jakarta.xml.ws.handler.soap.SOAPMessageContext;
import java.security.Security;
import java.util.prefs.Preferences;
import javax.xml.namespace.QName;
import org.apache.commons.lang3.CharEncoding;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.utils.XMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.CryptoPro.Crypto.CryptoProvider;
import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCSP.JCSP;
import ru.CryptoPro.reprov.RevCheck;
import ru.CryptoPro.ssl.Provider;

/**
 * Класс-обработчик для подписи SOAP-сообщений, отправляемых на сервисы ЕГИСЗ
 * ИПС
 */
@Stateless
public class SOAPMessageSigner implements SOAPHandler<SOAPMessageContext> {

//    
//    @EJB
//    private 
    private CryptoHelper cryptoHelper;
    private String informationSystemOid;

    //СПБ ПРОД
//    private String informationSystemOid = "828dfdaa-77b4-b20a-02b6-90a65c427a18";
//    private String certname = "cert";
//    //АРХ прод
//    private String certname = "mycert";
//    private String informationSystemOid = "2e286a6f-2690-69ff-8244-e77afa0aa0bc";
//  Краснодар
//    private String certname = "ГБУЗ КЦ ПБ СПИД";
//    private String informationSystemOid = "4a5d0255-a1f8-e12c-8de6-142a7411220d";
//    МО
//    private String certname = "cert";
//    private String informationSystemOid = "ad1b161f-7dab-aabf-92df-5015c30b0fdf";
//    Kaliningrad
//    private String certname = "cert";
//    private String informationSystemOid = "c793ec08-6ce9-dc5f-1661-2edabce5f4a7";
    private static final Logger logger = LoggerFactory.getLogger(SOAPMessageSigner.class);

    private static final String WWW_W3_ORG_2000_09_XMLDSIG = "http://www.w3.org/2000/09/xmldsig#";
    private static final String WWW_W3_ORG_2001_10_XML_EXC_C14N = "http://www.w3.org/2001/10/xml-exc-c14n#";
    private static final String WWW_W3_ORG_2001_04_XMLDSIG_GOST34102001 = "http://www.w3.org/2001/04/xmldsig-more#gostr34102012-gostr34112012-256";
    private static final String WWW_W3_ORG_2001_04_XMLDSIG_GOSTR3411 = "http://www.w3.org/2001/04/xmldsig-more#gostr34112012-256";
    private static final String WWW_W3_ORG_2005_08_ADDRESSING = "http://www.w3.org/2005/08/addressing";
    private static final String OASIS_200401_WSS_WSSECURITY_SECEXT = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
    private static final String OASIS_200401_WSS_WSSECURITY_UTILITY = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
    private static final String OASIS_200401_WSS_X509_TOKEN_PROFILE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3";
    private static final String OASIS_200401_WSS_SOAP_MESSAGE_SECURITY = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary";
    private static final String ROSMINZDRAV_NAMESPACE_URI = "http://egisz.rosminzdrav.ru";
    @EJB
    private Parameters parameters;

    @PostConstruct
    public void init() {
        try {
            Security.addProvider(new JCP()); // провайдер JCP
            Security.addProvider(new JCSP()); // провайдер JCSP
            Security.addProvider(new RevCheck());// провайдер проверки сертификатов JCPRevCheck
//(revocation-провайдер)
            Security.addProvider(new CryptoProvider());// провайдер шифрования JCryptoP
            Security.addProvider(new RevCheck());
            Security.addProvider(new Provider());
            Preferences p = Preferences.userRoot().node("ru/CryptoPro/ssl");
            p.put("Enable_revocation_default", "false");
            p.flush();
            p.sync();
            Preferences p1 = Preferences.userRoot().node("ru/CryptoPro/ssl/util");
            p1.put("tls_prohibit_disabled_validation", "false");
            p1.flush();
            p1.sync();
            System.setProperty("ru.CryptoPro.reprov.enableCRLDP", "false");
            System.setProperty("com.sun.security.enableCRLDP", "false");
            System.setProperty("com.ibm.security.enableCRLDP", "false");
            System.setProperty("ocsp.enable", "false");
            System.setProperty("com.sun.net.ssl.checkRevocation", "false");
            System.setProperty("com.sun.net.ssl.checkRevocation", "false");
            System.setProperty("java.security.certpath.disableroot", "true");
            System.setProperty("jdk.internal.httpclient.disableHostnameVerification", "true");
            System.setProperty("tls_prohibit_disabled_validation", "false");
            
            this.cryptoHelper = new CryptoHelper(parameters.getCertName(), "");
            this.informationSystemOid = parameters.getMisId();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SOAPMessageSigner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public SOAPMessageSigner() {
    }

    public SOAPMessageSigner(String certName, String misOid) {
        try {
            this.cryptoHelper = new CryptoHelper(certName, "");
            this.informationSystemOid = misOid;
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SOAPMessageSigner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Set<QName> getHeaders() {
        return Collections.emptySet();
    }

    private void charsetInitialize() {
        System.setProperty("file.encoding", CharEncoding.UTF_8);
        try {
            Field charset = Charset.class.getDeclaredField("defaultCharset");
            charset.setAccessible(true);
            charset.set(null, null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Инициализация библиотеки XML Security
     */
    private void apacheXmlSecurityInitialize() {
        if (!org.apache.xml.security.Init.isInitialized()) {
            System.setProperty("org.apache.xml.security.ignoreLineBreaks", "true");
            org.apache.xml.security.Init.init();
            try {
                Field f = XMLUtils.class.getDeclaredField("ignoreLineBreaks");
                f.setAccessible(true);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        String endpointAddress = (String) context.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        System.out.println("endpoint is " + endpointAddress);

        /* инициализация библиотеки XML-security */
        apacheXmlSecurityInitialize();
        /* инициализация провайдера BouncyCastle */
//        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        /* настройка кодировки */
//        charsetInitialize();
        System.setProperty("jakarta.net.ssl.keyStorePassword", "changeit");
        System.setProperty("jakarta.net.ssl.trustStorePassword", "changeit");
        System.setProperty("jakarta.net.ssl.trustStoreType", "jks");
        System.setProperty("jakarta.net.ssl.keyStoreType", "jks");
        SOAPMessage message = context.getMessage();
        SOAPPart soapPart = message.getSOAPPart();

        try {
            SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
            SOAPHeader soapHeader = soapEnvelope.getHeader();
            SOAPBody soapBody = soapEnvelope.getBody();
//            SOAPMessageType messageType = SOAPMessageType.get(soapBody.getFirstChild().getNodeName());
//            if (messageType == SOAPMessageType.SEND_RESPONSE) {
//                SOAPElement messageIDElem = (SOAPElement) XmlHelper.selectSingleNode(soapHeader, "//*[local-name()='MessageID']");
//                context.put("MessageID", messageIDElem.getTextContent());
//            }
//            if (isOutbound != null && isOutbound) {
            soapEnvelope.setAttribute("xmlns:ds", WWW_W3_ORG_2000_09_XMLDSIG);
            soapEnvelope.setAttribute("xmlns:wsa", WWW_W3_ORG_2005_08_ADDRESSING);
            soapEnvelope.setAttribute("xmlns:wsse", OASIS_200401_WSS_WSSECURITY_SECEXT);
            soapEnvelope.setAttribute("xmlns:wsu", OASIS_200401_WSS_WSSECURITY_UTILITY);
            soapEnvelope.setAttribute("xmlns:egisz", ROSMINZDRAV_NAMESPACE_URI);
            soapBody.setAttribute("wsu:Id", "body");
//                if (messageType == SOAPMessageType.SEND_RESPONSE_RESPONSE) {
//                    String messageID = (String) context.get("MessageID");
//                    SOAPElement relatesToElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2005_08_ADDRESSING, "RelatesTo", "wsa"));
//                    relatesToElem.setTextContent(messageID);
//                }                
            /*    добавление элемента для указания идентификатора системы */
            addingClientEntityIdElement(soapHeader);
            /* добавление элемента для указания идентификатора сообщения */
//                addingMessageIdElement(soapHeader);
            /* добавление блока элементов для указания ЭП */
            addingSignatureElements(soapEnvelope, soapHeader);
            dumpSOAPMessage(message);

            // Делаем такое преобразование, чтобы не поломался в последующем хэш для Body
            resetSOAPPartContent(message);
            /* проставление в элементе DigestValue расчитанную хеш-сумму блока с бизнес-данными запроса */
            SOAPElement digestValueElem = (SOAPElement) XmlHelper.selectSingleNode(message.getSOAPHeader(), "//*[local-name()='DigestValue']");
            genericDigestValue(message.getSOAPBody(), digestValueElem);
            /* подписание ЭП-ОВ */
            signDigestValue(message.getSOAPBody());
            //show soap msg                
            dumpSOAPMessage(message);
//            }
        } catch (SOAPException e) {
            dumpSOAPMessage(message);
            return false;
        }

        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        SOAPMessage message = context.getMessage();
        dumpSOAPMessage(message);
        SOAPPart soapPart = message.getSOAPPart();

        Boolean isOutbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        String debugMessage = isOutbound != null && isOutbound ? "при отправке сообщения в" : "при получении сообщения из";

        logger.debug("Ошибка {} ЕГИСЗ ИПС: {}", debugMessage, XmlHelper.elementToString(soapPart.getDocumentElement()));

        return true;
    }

    /**
     * Добавляет блок элементов для указания ЭП
     *
     * @param soapHeader
     * @throws SOAPException
     */
    private void addingSignatureElements(final SOAPEnvelope soapEnvelope, final SOAPHeader soapHeader) throws SOAPException {
        SOAPElement securityElem = soapHeader.addChildElement(new QName(OASIS_200401_WSS_WSSECURITY_SECEXT, "Security", "wsse"));
        SOAPElement binarySecurityTokenElem = soapHeader.addChildElement(new QName(OASIS_200401_WSS_WSSECURITY_SECEXT, "BinarySecurityToken", "wsse"));

//        jakarta.xml.soap.Name timestampElementName = soapEnvelope.createName("Timestamp", "wsu", OASIS_200401_WSS_WSSECURITY_UTILITY);
//        SOAPElement timestampSOAPElement = securityElem.addChildElement(timestampElementName);
//        String created = getCurrentDateTime();
//        String expires = getCurrentDateTimePlusDelay(600L);
        /* 60 minutes delay */
        // Add Created to Timestamp
//        SOAPElement createdSOAPElement = timestampSOAPElement
//                .addChildElement("Created"/* local name */, "wsu" /* prefix */, OASIS_200401_WSS_WSSECURITY_UTILITY);
//        createdSOAPElement.addTextNode(created);
//
//        // Add Expires to Timestamp
//        SOAPElement expiresSOAPElement = timestampSOAPElement
//                .addChildElement("Expires"/* local name */, "wsu" /* prefix */, OASIS_200401_WSS_WSSECURITY_UTILITY);
//        expiresSOAPElement.addTextNode(expires);
        binarySecurityTokenElem.addAttribute(soapEnvelope.createName("EncodingType"), OASIS_200401_WSS_SOAP_MESSAGE_SECURITY);
        binarySecurityTokenElem.addAttribute(soapEnvelope.createName("ValueType"), OASIS_200401_WSS_X509_TOKEN_PROFILE);
        binarySecurityTokenElem.addAttribute(soapEnvelope.createName("Id", "wsu", OASIS_200401_WSS_WSSECURITY_UTILITY), "CertId");
        securityElem.addChildElement(binarySecurityTokenElem);
        try {
            binarySecurityTokenElem.setTextContent(cryptoHelper.getCerBase64());
        } catch (CertificateEncodingException ex) {
            java.util.logging.Logger.getLogger(SOAPMessageSigner.class.getName()).log(Level.SEVERE, null, ex);
        }

        SOAPElement signatureElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2000_09_XMLDSIG, "Signature", "ds"));

        SOAPElement signedInfoElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2000_09_XMLDSIG, "SignedInfo", "ds"));

        SOAPElement canonicalizationMethodElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2000_09_XMLDSIG, "CanonicalizationMethod", "ds"));
        canonicalizationMethodElem.setAttribute("Algorithm", WWW_W3_ORG_2001_10_XML_EXC_C14N);
        signedInfoElem.addChildElement(canonicalizationMethodElem);

        SOAPElement signatureMethodElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2000_09_XMLDSIG, "SignatureMethod", "ds"));
        signatureMethodElem.setAttribute("Algorithm", WWW_W3_ORG_2001_04_XMLDSIG_GOST34102001);
        signedInfoElem.addChildElement(signatureMethodElem);

        SOAPElement referenceElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2000_09_XMLDSIG, "Reference", "ds"));
        referenceElem.setAttribute("URI", "#body");

        SOAPElement transformsElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2000_09_XMLDSIG, "Transforms", "ds"));

        SOAPElement transformElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2000_09_XMLDSIG, "Transform", "ds"));
        transformElem.setAttribute("Algorithm", WWW_W3_ORG_2001_10_XML_EXC_C14N);
        transformsElem.addChildElement(transformElem);

        referenceElem.addChildElement(transformsElem);

        SOAPElement digestMethodElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2000_09_XMLDSIG, "DigestMethod", "ds"));
        digestMethodElem.setAttribute("Algorithm", WWW_W3_ORG_2001_04_XMLDSIG_GOSTR3411);

        SOAPElement digestValueElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2000_09_XMLDSIG, "DigestValue", "ds"));

        referenceElem.addChildElement(digestMethodElem);
        referenceElem.addChildElement(digestValueElem);

        signedInfoElem.addChildElement(referenceElem);

        signatureElem.addChildElement(signedInfoElem);

        SOAPElement signatureValueElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2000_09_XMLDSIG, "SignatureValue", "ds"));
        signatureElem.addChildElement(signatureValueElem);

        SOAPElement keyInfoElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2000_09_XMLDSIG, "KeyInfo", "ds"));

        SOAPElement securityTokenReferenceElem = soapHeader.addChildElement(new QName(OASIS_200401_WSS_WSSECURITY_SECEXT, "SecurityTokenReference", "wsse"));

        SOAPElement referElem = soapHeader.addChildElement(new QName(OASIS_200401_WSS_WSSECURITY_SECEXT, "Reference", "wsse"));
        referElem.addAttribute(soapEnvelope.createName("URI"), "#CertId");
        referElem.addAttribute(soapEnvelope.createName("ValueType"), OASIS_200401_WSS_X509_TOKEN_PROFILE);

        securityTokenReferenceElem.addChildElement(referElem);

        keyInfoElem.addChildElement(securityTokenReferenceElem);

        signatureElem.addChildElement(keyInfoElem);

        securityElem.addChildElement(signatureElem);
    }

    /**
     * Добавляет элемент для указания идентификатора системы
     *
     * @param soapHeader
     * @throws RuntimeException
     */
    private void addingClientEntityIdElement(final SOAPHeader soapHeader) {
        try {
            SOAPElement transportHeaderElement = soapHeader.addChildElement(new QName(ROSMINZDRAV_NAMESPACE_URI, "transportHeader", "egisz"));
            SOAPElement authInfoElement = soapHeader.addChildElement(new QName(ROSMINZDRAV_NAMESPACE_URI, "authInfo", "egisz"));
            SOAPElement clientEntityIdElement = soapHeader.addChildElement(new QName(ROSMINZDRAV_NAMESPACE_URI, "clientEntityId", "egisz"));
            clientEntityIdElement.setTextContent(informationSystemOid);
            authInfoElement.addChildElement(clientEntityIdElement);
            transportHeaderElement.addChildElement(authInfoElement);
        } catch (SOAPException ex) {
            throw new RuntimeException("Не удалось добавить элемент для указания идентификатора системы.", ex);
        }
    }

    /**
     * Проставляет в элемент расчитанную хеш-сумму для значения заданного узла
     *
     * @param node обрабатываемый узел
     * @param destinationElem элмент, в который проставляется значение хеш-суммы
     * @throws RuntimeException
     */
    private void genericDigestValue(final org.w3c.dom.Node node, final SOAPElement destinationElem) {
        try {
            byte[] subtreeCanonicalized = XmlHelper.getCanonicalizeSubtree(Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS, node);
            destinationElem.addTextNode(cryptoHelper.getBase64Digest(new String(subtreeCanonicalized)));
        } catch (NoSuchAlgorithmException | SOAPException ex) {
            java.util.logging.Logger.getLogger(SOAPMessageSigner.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Не удалось вычислить хеш-сумму для блока элементов.", ex);
        }
    }

    private void resetSOAPPartContent(SOAPMessage message) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            message.writeTo(baos);
        } catch (SOAPException | IOException e) {
            throw new RuntimeException(e);
        }

        ByteArrayOutputStream rawMessage = new ByteArrayOutputStream();
        try {
            message.writeTo(rawMessage);
            message.getSOAPPart().setContent(new StreamSource(new ByteArrayInputStream(rawMessage.toByteArray())));
        } catch (SOAPException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Cчитывает подпись после всех манипуляций с SignedInfo
     *
     * @param soapBody объект содержания тела SOAP-сообщения
     * @throws RuntimeException
     */
    private void signDigestValue(final SOAPBody soapBody) {
        try {
            SOAPElement signatureValueElem = (SOAPElement) XmlHelper.selectSingleNode(soapBody, "//*[local-name()='SignatureValue']");
            SOAPElement signedInfoElem = (SOAPElement) XmlHelper.selectSingleNode(soapBody, "//*[local-name()='SignedInfo']");
            byte[] subtreeCanonicalized = XmlHelper.getCanonicalizeSubtree(Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS, signedInfoElem);

            signatureValueElem.addTextNode(cryptoHelper.getSignature(new String(subtreeCanonicalized)));
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SOAPMessageSigner.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Не удалось считать подпись после всех манипуляций с SignedInfo.", ex);
        }
    }

    /**
     * Добавляет элемент для указания идентификатора сообщения
     *
     * @param soapHeader
     * @throws SOAPException
     */
    private void addingMessageIdElement(final SOAPHeader soapHeader) throws SOAPException {
        SOAPElement messageIdElem = soapHeader.addChildElement(new QName(WWW_W3_ORG_2005_08_ADDRESSING, "MessageID", "wsa"));
        messageIdElem.setTextContent(UUID.randomUUID().toString());
    }

    @Override
    public void close(MessageContext context) {
    }

    /**
     * Dump SOAP Message to console
     *
     * @param msg
     */
    private void dumpSOAPMessage(SOAPMessage msg) {
        if (msg == null) {
            System.out.println("SOAP Message is null");
            return;
        }
        System.out.println("--------------------");
        System.out.println("DUMP OF SOAP MESSAGE");
        System.out.println("--------------------");
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            msg.writeTo(baos);
            System.out.println(baos.toString(getMessageEncoding(msg)));
            // show included values
            String values = msg.getSOAPBody().getTextContent();
//            System.out.println("Included values:" + values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the message encoding (e.g. utf-8)
     *
     * @param msg
     * @return
     * @throws jakarta.xml.soap.SOAPException
     */
    private String getMessageEncoding(SOAPMessage msg) throws SOAPException {
        String encoding = "UTF-8";
        if (msg.getProperty(SOAPMessage.CHARACTER_SET_ENCODING) != null) {
            encoding = msg.getProperty(SOAPMessage.CHARACTER_SET_ENCODING).toString();
        }
        return encoding;
    }

    public String getCurrentDateTime() {
        /* e.g. 2001-10-13T09:00:00Z */
        final SimpleDateFormat FORMATTER_DATETIME_NO_MS = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        DateFormat dfETC = FORMATTER_DATETIME_NO_MS;
        dfETC.setTimeZone(TimeZone.getTimeZone("CET"));

        StringBuffer dateETC = new StringBuffer(dfETC.format(new Date()));
        dateETC.append('Z');
        return dateETC.toString();
    }

    public String getCurrentDateTimePlusDelay(long delayInSeconds) {
        /* e.g. 2001-10-13T09:00:00Z */
        final SimpleDateFormat FORMATTER_DATETIME_NO_MS = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        DateFormat dfETC = FORMATTER_DATETIME_NO_MS;
        dfETC.setTimeZone(TimeZone.getTimeZone("Europe/Moscow"));
        Date date = new Date();

        long timeInMsecs = date.getTime();
        date.setTime(timeInMsecs + delayInSeconds * 1000L);
        StringBuffer dateETC = new StringBuffer(dfETC.format(date));
        dateETC.append('Z');
        return dateETC.toString();
    }
}
