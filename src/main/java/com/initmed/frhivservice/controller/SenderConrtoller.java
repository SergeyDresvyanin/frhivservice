/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import com.initmed.frhivservice.bdmodel.facades.EmptyResultExcepton;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;

/**
 *
 * @author sergeydresvyanin
 */
@Path("/test")
@Stateless
public class SenderConrtoller {

    @EJB
    private FRHIVServiceCommand fRHIVServiceCommand;

    @GET
    @Path("/patient")
    public Response sendPatient() {
        try {
            fRHIVServiceCommand.createPatient();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK)
                .build();
    }

    @GET
    @Path("/send")
    public Response send() {
        try {
            fRHIVServiceCommand.createHivDispCheckup();
            fRHIVServiceCommand.createHivDispChemo();
            fRHIVServiceCommand.createHivDispConc();
            fRHIVServiceCommand.createHivDispDesease();
            fRHIVServiceCommand.createHivDispExam();
            fRHIVServiceCommand.createHivDispHosp();
            fRHIVServiceCommand.createHivDispStage();
            fRHIVServiceCommand.createHivEpid();
            fRHIVServiceCommand.createHivRecipe();
            fRHIVServiceCommand.createHivVisits();
            fRHIVServiceCommand.createPatientDocument();
            fRHIVServiceCommand.createPatientRelative();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK)
                .build();
    }

    @GET
    @Path("/read")
    public Response read() {
        try {
            int patId = 1;
            fRHIVServiceCommand.readDispCard(patId);
            fRHIVServiceCommand.readHiv(patId);
            fRHIVServiceCommand.readHivAddress(patId);
            fRHIVServiceCommand.readHivDispCheckup(patId);
            fRHIVServiceCommand.readHivDispChemo(patId);
            fRHIVServiceCommand.readHivDispConc(patId);
            fRHIVServiceCommand.readHivDispDesease(patId);
            fRHIVServiceCommand.readHivDispExam(patId);
            fRHIVServiceCommand.readHivDispHosp(patId);
            fRHIVServiceCommand.readHivDispStage(patId);
            fRHIVServiceCommand.readHivEpid(patId);
            fRHIVServiceCommand.readHivVisits(patId);
            fRHIVServiceCommand.readPatient(patId);
            fRHIVServiceCommand.readPatientDocument(patId);
            fRHIVServiceCommand.readPatientRelative(patId);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK)
                .build();
    }

    @GET
    @Path("/list")
    public Response list() {
        try {
            int patId = 1;
            fRHIVServiceCommand.listHivAddress(patId);
            fRHIVServiceCommand.listHivDispCheckup(patId);
            fRHIVServiceCommand.listHivDispChemo(patId);
            fRHIVServiceCommand.listHivDispConc(patId);
            fRHIVServiceCommand.listHivDispExam(patId);
            fRHIVServiceCommand.listHivDispHosp(patId);
            fRHIVServiceCommand.listHivDispStage(patId);
            fRHIVServiceCommand.listHivExam(patId);
            fRHIVServiceCommand.listHivVisits(patId);
            fRHIVServiceCommand.listHivRecipe(patId);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK)
                .build();
    }

    @GET
    @Path("/update")
    public Response update() {
        try {
            int patId = 1;
            fRHIVServiceCommand.updateDispCardPatient(patId);
            fRHIVServiceCommand.updateHivAddress(patId);
            fRHIVServiceCommand.updateHivDispCheckup(patId);
            fRHIVServiceCommand.updateHivDispChemo(patId);
            fRHIVServiceCommand.updateHivDispConc(patId);
            fRHIVServiceCommand.updateHivDispDesease(patId);
            fRHIVServiceCommand.updateHivDispExam(patId);
            fRHIVServiceCommand.updateHivDispHosp(patId);
            fRHIVServiceCommand.updateHivDispStage(patId);
            fRHIVServiceCommand.updateHivEpid(patId);
            fRHIVServiceCommand.updateHivExam(patId);
            fRHIVServiceCommand.updateHivPatient(patId);
            fRHIVServiceCommand.updateHivVisits(patId);
            fRHIVServiceCommand.updatePatient(patId);
            fRHIVServiceCommand.updatePatientDocument(patId);
            fRHIVServiceCommand.updatePatientRelative(patId);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EmptyResultExcepton ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SenderConrtoller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK)
                .build();
    }
}
