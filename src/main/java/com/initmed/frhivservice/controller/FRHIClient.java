/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.controller;

import com.initmed.frhivservice.bdmodel.converter.FRNode;
import com.initmed.frhivservice.bdmodel.facades.FrmessagesyncFacade;
import com.initmed.frhivservice.entity.Frmessagesync;
import com.initmed.frhivservice.util.JAXBHelper;
import com.initmed.frhivservice.util.SOAPConfigurator;
import jakarta.annotation.PostConstruct;
import org.apache.commons.lang3.CharEncoding;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import javax.xml.namespace.QName;
import jakarta.xml.ws.BindingProvider;
import org.apache.cxf.headers.Header;
import org.apache.cxf.jaxb.JAXBDataBinding;
import ru.CryptoPro.JCPxml.xmldsig.JCPXMLDSigInit;
import ru.CryptoPro.XAdES.util.XMLUtils;

/**
 *
 * @author sd199
 */
@Stateless
public class FRHIClient {
//  Мой тест
//    private static final String ipsEndPoint = "http://desktop-d8cnmem:9090/FrHivSrvTest/receiver";
//  Прод

    private static final String ipsEndPoint = "https://api.egisz.rosminzdrav.ru/57406573a4083";
//    private static final String ipsEndPoint = "http://10.41.19.39/57406573a4083";
//  Тест
//    private static final String ipsEndPoint = "https://ips-test.rosminzdrav.ru/57234d87b0838";
    //ARH
//    private static final String ORGANISATION_OID = "1.2.643.5.1.13.13.12.2.29.2750";
    //SPB
//    private static final String ORGANISATION_OID = "1.2.643.5.1.13.13.12.2.78.8572";
    //Krasnodar
//    private static final String ORGANISATION_OID = "1.2.643.5.1.13.13.12.2.23.1820";
    //MO
//    private static final String ORGANISATION_OID = "1.2.643.5.1.13.13.12.2.50.4628";
//    Kaliningrad
//    private static final String ORGANISATION_OID = "1.2.643.5.1.13.13.12.2.23.1820";

    private static final String WWW_W3_ORG_2005_08_ADDRESSING = "http://www.w3.org/2005/08/addressing";
    private Receiver port;

    @EJB
    private Parameters parameters;

    @EJB
    private FrmessagesyncFacade frmessagesyncFacade;

    @PostConstruct
    public void init() {
//        /* инициализация библиотеки XML-security */
        apacheXmlSecurityInitialize();

//        /* инициализация провайдера BouncyCastle */
//        Security.insertProviderAt(new org.bouncycastle.jce.provider.BouncyCastleProvider(), 1);
        /* настройка кодировки */
        charsetInitialize();
        try {
            this.port = new Receiver_Service(new URL(ipsEndPoint + "?wsdl"), new QName("http://receiver.service.nr.eu.rt.ru/", "receiver")).getReceiverPort();
            /* настройка url точки доступа к серверу ЕГИСЗ ИПС */
            SOAPConfigurator.setEndpointURL(this.port, ipsEndPoint);
            /* настройка Apache CXF, чтобы в заголовках soap-запроса вычислялся Content-Length */
            SOAPConfigurator.сontentLengthActivate(this.port);
            SOAPConfigurator.bindingSOAPHandler(this.port, new SOAPMessageSigner(parameters.getCertName(), parameters.getMisId()), false);
            List<Header> headersList = new ArrayList<>();
            JAXBDataBinding jaxbDataBinding = JAXBHelper.getJAXBDataBinding();
            headersList.add(new Header(new QName(WWW_W3_ORG_2005_08_ADDRESSING, "To", "wsa"), ipsEndPoint, jaxbDataBinding));
            headersList.add(new Header(new QName(WWW_W3_ORG_2005_08_ADDRESSING, "Action", "wsa"), "sendDocument", jaxbDataBinding));
            addHeaders(this.port, headersList);
        } catch (Exception ex) {
            Logger.getLogger(FRHIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendCustomMessage(String moId, String method, String base64msg) {
        try {
            String msgID = port.sendDocument(moId, method, base64msg);
            frmessagesyncFacade.createMessage(moId, msgID, method, -1, (short) 0);
        } catch (AuthorizationFaultException | SystemFaultException | ValidationFaultException ex) {
            Logger.getLogger(FRHIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Настройка системной кодировки
     */
    private void charsetInitialize() {
        System.setProperty("file.encoding", CharEncoding.UTF_8);
    }

    /**
     * Инициализация библиотеки XML Security
     */
    private void apacheXmlSecurityInitialize() {
//
        //init jcp
        if (!JCPXMLDSigInit.isInitialized()) {
            JCPXMLDSigInit.init();
        }
        System.setProperty("com.ibm.security.enableCRLDP", "false");
//        

        if (!org.apache.xml.security.Init.isInitialized()) {
            System.setProperty("org.apache.xml.security.ignoreLineBreaks", "true");
            org.apache.xml.security.Init.init();
            try {
                Field f = XMLUtils.class.getDeclaredField("ignoreLineBreaks");
                f.setAccessible(true);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void sendMessage(FRNode fRNode) {
        try {
            if (port != null) {
                String msgID = port.sendDocument(fRNode.getMoOid(), fRNode.getMethod(), fRNode.getVal());
                Frmessagesync fm = frmessagesyncFacade.getMessage(msgID);
                if (fm == null) {
                    frmessagesyncFacade.createMessage(fRNode.getMoOid(),  msgID, fRNode.getMethod(), fRNode.getEntityID(), (short) 0);
                } else {
                    fm.setRefentity(fRNode.getEntityID());
                    fm.setMethod(fRNode.getMethod());
                    frmessagesyncFacade.edit(fm);
                }
                frmessagesyncFacade.deleteDouble(fRNode.getMethod(), fRNode.getEntityID());
                parameters.setBlocked(false);
            } else {
                init();
            }
        } catch (Exception ex) {
            parameters.setBlocked(true);
            Logger.getLogger(FRHIClient.class.getName()).log(Level.SEVERE, null, ex);
            frmessagesyncFacade.deleteDouble(fRNode.getMethod(), fRNode.getEntityID());
        }
    }

    private void addHeaders(final Receiver port, final List<Header> headersList) {
        ((BindingProvider) port).getRequestContext().put(Header.HEADER_LIST, headersList);
    }
}
