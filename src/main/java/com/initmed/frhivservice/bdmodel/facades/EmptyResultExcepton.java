/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

/**
 *
 * @author sergey
 */
public class EmptyResultExcepton extends Exception {

    /**
     * Creates a new instance of <code>EmptyResultExcepton</code> without detail
     * message.
     */
    public EmptyResultExcepton() {
    }

    /**
     * Constructs an instance of <code>EmptyResultExcepton</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public EmptyResultExcepton(String msg) {
        super(msg);
    }
}
