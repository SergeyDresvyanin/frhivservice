/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Hivepid;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

/**
 *
 * @author sd199
 */
@Stateless
public class HivepidFacade extends AbstractFacade<Hivepid> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivepidFacade() {
        super(Hivepid.class);
    }

    public List<Hivepid> loadHivEpid() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM hivepid he\n"
                + "INNER JOIN PATIENT P ON P.ID = he.REFPATIENT\n"
                + "inner join HIVDISP hd on hd.REFPATIENT = p.ID\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1 and he.INFEDREGISTER=0\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = he.id and method='hiv_epid.create')\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_hivepid' and TRANSFER=1)", Hivepid.class);
        return q.setMaxResults(1).getResultList();
    }

    public List<Hivepid> loadHivEpidForUpdate() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM hivepid he\n"
                + "INNER JOIN PATIENT P ON P.ID = he.REFPATIENT\n"
                + "inner join HIVDISP hd on hd.REFPATIENT = p.ID\n"
                + "WHERE he.needsupdate = true\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = he.id and method='hiv_epid.update')\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_hivepid' and TRANSFER=1) limit 1", Hivepid.class);
        return q.setMaxResults(1).getResultList();
    }

    public Integer loadCountHivEpid() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT COUNT(HE.ID) FROM hivepid he\n"
                + "INNER JOIN PATIENT P ON P.ID = he.REFPATIENT\n"
                + "inner join HIVDISP hd on hd.REFPATIENT = p.ID\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1 and he.INFEDREGISTER=0\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = he.id and method='hiv_epid.create')");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountHivEpidError() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT COUNT(HE.ID) FROM hivepid he\n"
                + "INNER JOIN PATIENT P ON P.ID = he.REFPATIENT\n"
                + "inner join HIVDISP hd on hd.REFPATIENT = p.ID\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1 and he.INFEDREGISTER=0\n"
                + "and exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = he.id and method='hiv_epid.create' and fsm.MESSAGEDETAIL is not null)");
        return ((Number) q.getSingleResult()).intValue();
    }
}
