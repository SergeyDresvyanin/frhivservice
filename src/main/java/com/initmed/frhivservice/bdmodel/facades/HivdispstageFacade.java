/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Hivdispstage;
import com.initmed.frhivservice.entity.report.HivRecipesReport;
import com.initmed.frhivservice.entity.report.HivdispstagesReport;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.persistence.TemporalType;

/**
 *
 * @author sd199
 */
@Stateless
public class HivdispstageFacade extends AbstractFacade<Hivdispstage> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivdispstageFacade() {
        super(Hivdispstage.class);
    }

    public List<Hivdispstage> loadHivDispStage() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select * from Hivdispstage hh\n"
                + "inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + "inner join patient p on p.id =  hd.refpatient\n"
                + "where p.REGISTRYNUMBER is not null and hd.CARDNUMBER is not null\n"
                + "and  hh.infedregister = 0 "
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hh.id and method='hiv_disp_stage.create')\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_dispstage' and TRANSFER=1)\n"
                + "", Hivdispstage.class);
        return (List<Hivdispstage>) q.setMaxResults(150).getResultList();
    }

    public Integer loadCountHivDispStage(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select count(distinct hh.id) from Hivdispstage hh\n"
                + "inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + "inner join patient p on p.id =  hd.refpatient\n"
                + "where p.REGISTRYNUMBER is not null and hd.CARDNUMBER is not null\n"
                + "and hh.infedregister = 0 "
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hh.id and method='hiv_disp_stage.create')\n"
                + "and hh.STAGEDATE between ? and ?");
        q.setParameter(1, from, TemporalType.DATE);
        q.setParameter(2, to, TemporalType.DATE);
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountHivDispStageError(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select count(distinct hh.id) from Hivdispstage hh\n"
                + "inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + "inner join patient p on p.id =  hd.refpatient\n"
                + "where p.REGISTRYNUMBER is not null and hd.CARDNUMBER is not null\n"
                + "and exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hh.id and method='hiv_disp_stage.create' and fsm.MESSAGEDETAIL is not null) "
                + "and hh.STAGEDATE between ? and ?");
        q.setParameter(1, from, TemporalType.DATE);
        q.setParameter(2, to, TemporalType.DATE);
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer count(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select count(distinct hh.id) from Hivdispstage hh\n"
                + "inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + "inner join patient p on p.id =  hd.refpatient\n"
                + "where hh.STAGEDATE between ? and ?\n");
        q.setParameter(1, from, TemporalType.DATE);
        q.setParameter(2, to, TemporalType.DATE);
        return ((Number) q.getSingleResult()).intValue();
    }

    public List<HivdispstagesReport> loadStagesReport(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT FRM.ID, P.SNILS, H.SPECNUM, COALESCE(FRM.MESSAGEDETAIL, 'OK') MESSAGEDETAIL, HS.PERSONID,\n"
                + "ST.\"name\", HS.stagedate\n"
                + "FROM HIV H\n"
                + "INNER JOIN PATIENT P ON P.ID = H.REFPATIENT\n"
                + "INNER JOIN hivdisp HD ON HD.refpatient = H.refpatient\n"
                + "INNER JOIN hivdispstage HS ON HS.refhivdisp = HD.ID\n"
                + "INNER JOIN STAGES ST ON ST.ID = HS.stageid\n"
                + "INNER JOIN FRMESSAGESYNC FRM ON FRM.REFENTITY = HS.ID and frm.METHOD='hiv_disp_stage.create'\n"
                + "WHERE FRM.RESPOCERECEIVED=1\n"
                + "AND FRM.DATEOPERATION BETWEEN ? AND ?", HivdispstagesReport.class);
        q.setParameter(1, new Timestamp(from.getTime()), TemporalType.TIMESTAMP);
        q.setParameter(2, new Timestamp(to.getTime()), TemporalType.TIMESTAMP);

        return q.getResultList();
    }
}
