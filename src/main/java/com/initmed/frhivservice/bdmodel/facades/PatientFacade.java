/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Patient;
import java.util.Date;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.ws.rs.QueryParam;

/**
 *
 * @author User
 */
@Stateless
public class PatientFacade extends AbstractFacade<Patient> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PatientFacade() {
        super(Patient.class);
    }

    public void setPatInFr(int patId) {
        Query q = em.createNativeQuery("UPDATE PATIENT SET INFEDREGISTER=1 WHERE ID=?");
        q.setParameter(1, patId);
        q.executeUpdate();
    }

    public List<Patient> loadPatient() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM PATIENT p WHERE INFEDREGISTER = 0  and p.snils is not null  \n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = p.id and method='patient.create')\n"
                + "and p.closed = 0 \n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_patient' and TRANSFER=1)\n"
                + "and exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = p.id and method='patient.read' and fsm.respoceReceived=1)", Patient.class);
//        Query q = em.createNativeQuery("SELECT p.* FROM HIVRECIPE HR\n"
//                + "INNER JOIN PATIENT P ON P.ID = HR.REFPATIENT\n"
//                + "WHERE \n"
//                + "not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hr.id and method='hiv_recipe.create')\n"
//                + "and exists(select * from HIVDISP hd where hd.REFPATIENT = p.id)", Patient.class);
        return (List<Patient>) q.setMaxResults(1).getResultList();
    }

    public List<Patient> loadPatientsForHivRead() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT distinct p.* FROM PATIENT p\n"
                + "inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=0\n"
                + "where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = h.id and method='hiv.read') and p.closed = 0\n"
                + "and p.registrynumber is null and exists(select * from SCHEDULEPROCESS where NAME='read_hiv' and TRANSFER=1)", Patient.class);
        return (List<Patient>) q.setMaxResults(1).getResultList();
    }

    public List<Patient> loadPatientsForCreateAddr() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT distinct p.* FROM PATIENT p\n"
                + "inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=1\n"
                + "inner join hivaddress ha on ha.refpatient = p.id\n"
                + "where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = ha.id and method='hiv_address.create') and p.closed = 0\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='read_hiv' and TRANSFER=1)\n"
                + "and ha.infedregister=0", Patient.class);
        return (List<Patient>) q.setMaxResults(1).getResultList();
    }

    public List<Patient> loadPatientsForRead() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT distinct p.* FROM PATIENT p\n"
                + "inner join hiv h on p.id = h.refpatient\n"
                + "where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = p.id and method='patient.read')"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_patient' and TRANSFER=1) \n"
                + "and p.closed = 0\n"
                + "and p.infedregister =0", Patient.class);
        return (List<Patient>) q.setMaxResults(1).getResultList();
    }

    public Integer loadCountPatientsForRead() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct p.id) FROM PATIENT p \n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=0\n"
                + " where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = p.id and method='patient.read') and p.closed = 0");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountPatientsForCreate() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct p.id) FROM PATIENT p \n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=0\n"
                + " where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = p.id and method='patient.create')  and p.closed = 0");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountPatientsForReadError() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct p.id) FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=0\n"
                + " where  exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = p.id and method='patient.read' and fsm.MESSAGEDETAIL is not null)  and p.closed = 0");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountPatientsForCreateError() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct p.id) FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=0\n"
                + " where  exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = p.id and method='patient.create' and fsm.MESSAGEDETAIL is not null)  and p.closed = 0");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountHivForCreate() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct p.id) FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=0\n"
                + " where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = h.id and method='hiv.create')  and p.closed = 0");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountHivForCreateError() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct p.id) FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=0\n"
                + " where  exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = h.id and method='hiv.create' and fsm.MESSAGEDETAIL is not null)  and p.closed = 0");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountHivForRead() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct p.id) FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=0\n"
                + " where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = h.id and method='hiv.read')  and p.closed = 0");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountHivForReadError() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct p.id) FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=0\n"
                + " where  exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = h.id and method='hiv.read' and fsm.MESSAGEDETAIL is not null)  and p.closed = 0");
        return ((Number) q.getSingleResult()).intValue();
    }

    public List<Patient> loadPatientsReadDispCards() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT distinct p.* FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=1\n"
                + " inner join HIVDISP hd on hd.REFPATIENT = p.ID\n"
                + " where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hd.id and method='hiv_disp.list')\n"
                + " and p.REGISTRYNUMBER is not null\n"
                + " and hd.CARDNUMBER is null\n"
                + " and exists(select * from SCHEDULEPROCESS where NAME='read_disp' and TRANSFER=1)", Patient.class);
        return (List<Patient>) q.setMaxResults(1).getResultList();
    }

    public Integer loadCountPatientsReadDispCards() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT COUNT(distinct p.id) FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=1\n"
                + " inner join HIVDISP hd on hd.REFPATIENT = p.ID\n"
                + " where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hd.id and method='hiv_disp.list')\n"
                + " and p.REGISTRYNUMBER is not null\n"
                + " and hd.CARDNUMBER is null");
        return ((Number) q.getSingleResult()).intValue();
    }

    public List<Patient> loadPatientWithDispCard() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT distinct p.* FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=1\n"
                + " inner join HIVDISP hd on hd.REFPATIENT = p.ID\n"
                + " where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hd.id and method='hiv_disp.list')"
                + " and exists(select * from SCHEDULEPROCESS where NAME='read_disp' and TRANSFER=1)\n"
                + " and p.REGISTRYNUMBER is not null\n"
                + " and hd.CARDNUMBER is null", Patient.class);
        return (List<Patient>) q.setMaxResults(1).getResultList();
    }

    public List<Patient> loadPatientWithOutDispCard() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT distinct p.* FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=1\n"
                + " inner join HIVDISP hd on hd.REFPATIENT = p.ID\n"
                + " where exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hd.id and method='hiv_disp.list' and fsm.MESSAGEDETAIL = 'У пациента отсуствует д карта')\n"
                + " and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hd.id and method='hiv_disp.create')\n"
                + " and p.REGISTRYNUMBER is not null\n"
                + " and hd.CARDNUMBER is null\n"
                + " and exists(select * from SCHEDULEPROCESS where NAME='create_disp' and TRANSFER=1)", Patient.class);
        return (List<Patient>) q.setMaxResults(1).getResultList();
    }

    public List<Patient> loadPatientInFr() throws EmptyResultExcepton {
//        Query q = em.createNativeQuery("SELECT distinct p.* FROM PATIENT p\n"
//                + "inner join documents d on p.id = d.refpatient and d.INFEDREGISTER=1\n"
//                + "inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=1\n"
//                + "inner join relative r on p.id = r.refpatient and r.INFEDREGISTER=1\n"
//                + "inner join hivaddress addr on p.id = addr.refpatient and addr.INFEDREGISTER=1\n"
//                + "inner join hivepid he on p.id = he.refpatient and he.INFEDREGISTER=1\n"
//                + "inner join hivavrt avrt on p.id = avrt.refpatient and avrt.INFEDREGISTER=1\n"
//                //                + "inner join hivrecipe hr on p.id = hr.refpatient and hr.INFEDREGISTER=1\n"
//                //                + "inner join HIVDISPVISITS  hv on p.id = hv.refpatient and hv.INFEDREGISTER=1\n"
//                + "inner join hivdispexam hivexam on p.id = hivexam.refpatient and hivexam.INFEDREGISTER=1  and hivexam.REFEXAMID in(4,5,7,9)\n"
//                + "inner join hivdispexam dispexam on p.id = dispexam.refpatient and dispexam.INFEDREGISTER=1  and dispexam.REFEXAMID in(6,8,10)\n"
//                + "inner join hivdisp hd on p.id = hd.refpatient and hd.INFEDREGISTER=1\n"
//                + "inner join hivdispconc hc on hc.REFHIVDISP =hd.id and hc.INFEDREGISTER=1\n"
//                + "inner join hivdispstage hs on hs.REFHIVDISP =hd.id and hs.INFEDREGISTER=1\n"
//                + "inner join hivdispchemo hch on hch.REFHIVDISP =hd.id and hch.INFEDREGISTER=1\n"
//                + "inner join HIVDISPCHEKUP hcheck on hc.REFHIVDISP =hd.id and hcheck.INFEDREGISTER=1\n"
//                + "WHERE p.INFEDREGISTER = 1\n"
//                + "order by id desc", Patient.class);;;
        Query q = em.createNativeQuery("SELECT distinct p.* FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=0\n"
                + " where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = h.id and method='hiv.read')", Patient.class);
        return (List<Patient>) q.setMaxResults(10000).getResultList();
    }

    public Patient loadPatientBySnils(String snils) throws EmptyResultExcepton, EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM PATIENT WHERE SNILS=?1 AND INFEDREGISTER = 0", Patient.class);
        try {
            q.setParameter(1, snils);
            List<Patient> res = (List<Patient>) q.getResultList();
            return res.get(0);
        } catch (Exception e) {
            throw new EmptyResultExcepton();
        }
    }

    public List<Patient> loadPatientWithoutDocs() throws EmptyResultExcepton {
        Query q = em.createNativeQuery(" SELECT * FROM PATIENT P WHERE\n"
                + " EXISTS(SELECT * FROM DOCUMENTS DC WHERE DC.REFPATIENT = P.ID AND DC.INFEDREGISTER=0)\n"
                + " and INFEDREGISTER = 1", Patient.class);
        return (List<Patient>) q.setMaxResults(1).getResultList();
    }

    public List<Patient> loadPatientWithRelative() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM PATIENT P\n"
                + " inner join RELATIVE r on R.REFPATIENT = P.ID\n"
                + " and r.INFEDREGISTER = 0\n"
                + " and p.INFEDREGISTER = 1\n"
                + " and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = r.id and method='patient_relative.create')", Patient.class);
        return (List<Patient>) q.setMaxResults(1).getResultList();
    }

    public List<Patient> loadPatientWithoutAddress() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM PATIENT P\n"
                + " INNER JOIN HIVADDRESS h on h.REFPATIENT = P.ID\n"
                + " INNER JOIN FRMESSAGESYNC fsm ON fsm.REFENTITY = h.id and method='hiv_address.create'\n"
                + " and h.INFEDREGISTER = 0\n"
                + " and p.INFEDREGISTER = 1", Patient.class);
        return (List<Patient>) q.setMaxResults(1).getResultList();
    }

    public Integer loadCountPatientWithoutAddress() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(p.id) FROM PATIENT P\n"
                + " INNER JOIN HIVADDRESS h on h.REFPATIENT = P.ID\n"
                + " INNER JOIN FRMESSAGESYNC fsm ON fsm.REFENTITY = h.id and method='hiv_address.create'\n"
                + " and h.INFEDREGISTER = 0\n"
                + " and p.INFEDREGISTER = 1");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer getCurrentSpeed() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select count(*) from  frmessagesync \n"
                + "where dateoperation > CURRENT_TIMESTAMP - INTERVAL '1 minute' and MESSAGEID is not null");
        return ((Number) q.getSingleResult()).intValue();
    }

}
