/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Defaultparams;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

/**
 *
 * @author User
 */
@Stateless
public class DefaultparamsFacade extends AbstractFacade<Defaultparams> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DefaultparamsFacade() {
        super(Defaultparams.class);
    }

    public String getMoId() {
        return loadParameter("ORGANISATIONID");
    }

    public String getMisOId() {
        return loadParameter("INFORMATION-SYSTEM-OID");
    }

    public String getSignerAlias() {
        return loadParameter("SIGNER-ALIAS");
    }

    public String getDefaultRegion() {
        return loadParameter("DEFAULT_REGION");
    }

    public String getDefaultCity() {
        return loadParameter("DEFAULT_CITY");
    }

    private String loadParameter(String paramName) {
        Query q = em.createNativeQuery("SELECT * FROM DEFAULTPARAMS where NAME=?", Defaultparams.class);
        q.setParameter(1, paramName);
        return ((Defaultparams) q.getSingleResult()).getVal();
    }

}
