/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Hivdisp;
import com.initmed.frhivservice.entity.Patient;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

/**
 *
 * @author sergey
 */
@Stateless
public class HivdispFacade extends AbstractFacade<Hivdisp> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivdispFacade() {
        super(Hivdisp.class);
    }

    public Hivdisp loadCardByPatietntId(int patId) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM Hivdisp WHERE REFPATIENT = ?", Hivdisp.class);
        q.setParameter(1, patId);
        try {
            List<Hivdisp> res = (List<Hivdisp>) q.getResultList();
            return res.get(0);
        } catch (Exception e) {
            throw new EmptyResultExcepton();
        }
    }

    public Hivdisp loadCardByCardNo(String cardNo) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM HIVDISP WHERE CARDNUMBER = ?", Hivdisp.class);
        q.setParameter(1, cardNo);
        try {
            List<Hivdisp> res = (List<Hivdisp>) q.getResultList();
            return res.get(0);
        } catch (Exception e) {
            throw new EmptyResultExcepton();
        }
    }

    public List<Hivdisp> loadHivDisp() {
        Query q = em.createNativeQuery("SELECT * FROM Hivdisp WHERE INFEDREGISTER = 1", Hivdisp.class);
        return (List<Hivdisp>) q.getResultList();
    }

    public List<Hivdisp> loadExamLists() {
        Query q = em.createNativeQuery("SELECT * FROM Hivdisp hd WHERE INFEDREGISTER = 1\n"
                + "and exists(select * from HIVDISPEXAM hv where hv.REFPATIENT = hd.REFPATIENT and hv.INFEDREGISTER =1)", Hivdisp.class);
        return (List<Hivdisp>) q.getResultList();
    }

    public List<Hivdisp> loadRecipeList() {
        Query q = em.createNativeQuery("SELECT * FROM Hivdisp hd WHERE INFEDREGISTER = 1\n"
                + "and exists(select * from HIVRECIPE hr where hr.REFPATIENT = hd.REFPATIENT and hr.INFEDREGISTER =1)", Hivdisp.class);
        return (List<Hivdisp>) q.getResultList();
    }

    public List<Hivdisp> loadAVRTList() {
        Query q = em.createNativeQuery("SELECT * FROM Hivdisp hd WHERE INFEDREGISTER = 1\n"
                + "and exists(select * from HIVAVRT ha where ha.REFPATIENT = hd.REFPATIENT and ha.INFEDREGISTER =1)", Hivdisp.class);
        return (List<Hivdisp>) q.getResultList();
    }

    public List<Hivdisp> loadVisitsList() {
        Query q = em.createNativeQuery("SELECT * FROM Hivdisp hd WHERE INFEDREGISTER = 1\n"
                + "and exists(select * from HIVDISPVISITS hv where hv.REFPATIENT = hd.REFPATIENT and hv.INFEDREGISTER =1)", Hivdisp.class);
        return (List<Hivdisp>) q.getResultList();
    }

    public List<Hivdisp> loadUpdateExamResult() {
        Query q = em.createNativeQuery("SELECT * FROM Hivdisp hd \n"
                + "inner join patient p on p.id = hd.refpatient\n"
                + "WHERE p.researchlag is null\n"
                + "and hd.INFEDREGISTER = 1", Hivdisp.class);
        return (List<Hivdisp>) q.setMaxResults(1).getResultList();
    }

    public Integer loadCountHidDispForRead() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct p.id) FROM PATIENT p \n"
                + " inner join hivdisp hd on p.id = hd.refpatient and hd.INFEDREGISTER=0\n"
                + " where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = p.id and method='hiv_disp.list')  and p.closed = 0");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountHidDispForReadError() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct p.id) FROM PATIENT p\n"
                + " inner join hivdisp hd on p.id = hd.refpatient and hd.INFEDREGISTER=0\n"
                + " where  exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = p.id and method='hiv_disp.list' and fsm.MESSAGEDETAIL is not null)  and p.closed = 0");
        return ((Number) q.getSingleResult()).intValue();
    }
    
     public Integer loadCountHidDispForCreate() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct p.id) FROM PATIENT p \n"
                + " inner join hivdisp hd on p.id = hd.refpatient and hd.INFEDREGISTER=0\n"
                + " where not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = p.id and method='hiv_disp.create')  and p.closed = 0");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountHidDispForCreateError() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct p.id) FROM PATIENT p\n"
                + " inner join hivdisp hd on p.id = hd.refpatient and hd.INFEDREGISTER=0\n"
                + " where  exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = p.id and method='hiv_disp.create' and fsm.MESSAGEDETAIL is not null)  and p.closed = 0");
        return ((Number) q.getSingleResult()).intValue();
    }
}
