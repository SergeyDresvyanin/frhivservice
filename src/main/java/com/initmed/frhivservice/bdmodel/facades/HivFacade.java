/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Hiv;
import com.initmed.frhivservice.entity.report.HivReport;
import com.initmed.frhivservice.entity.report.PatientErorReport;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.persistence.TemporalType;

/**
 *
 * @author User
 */
@Stateless
public class HivFacade extends AbstractFacade<Hiv> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivFacade() {
        super(Hiv.class);
    }

    public Hiv loadHivByPatietntId(int patId) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM HIV WHERE REFPATIENT = ?", Hiv.class);
        q.setParameter(1, patId);
        try {
            List<Hiv> res = (List<Hiv>) q.getResultList();
            return res.get(0);
        } catch (Exception e) {
            throw new EmptyResultExcepton();
        }
    }

    public List<Hiv> loadHivInFr() {
        Query q = em.createNativeQuery("SELECT * FROM HIV WHERE INFEDREGISTER =1", Hiv.class);
        return (List<Hiv>) q.getResultList();
    }

    public List<Hiv> loadHivForSend() {
        Query q = em.createNativeQuery("SELECT distinct h.* FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=0\n"
                + " where exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = h.id and method='hiv.read')\n"
                + " and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = h.id and method='hiv.create')\n"
                + " and exists(select * from SCHEDULEPROCESS where NAME='create_hiv' and TRANSFER=1)\n"
                + " and P.REGISTRYNUMBER is null", Hiv.class);
        return q.setMaxResults(1).getResultList();
    }

    public List<Hiv> loadHiv() {
        Query q = em.createNativeQuery("SELECT distinct h.* FROM PATIENT p\n"
                + " inner join hiv h on p.id = h.refpatient and h.INFEDREGISTER=0\n"
                + " where exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = h.id and method='hiv.read')\n"
                + " and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = h.id and method='hiv.create')\n"
                + " and exists(select * from SCHEDULEPROCESS where NAME='create_hiv' and TRANSFER=1)\n"
                + " and P.REGISTRYNUMBER is null", Hiv.class);
        return q.setMaxResults(1).getResultList();
    }

    public List<HivReport> loadPatientCanNotAdd(Date from, Date to) {
        Query q = em.createNativeQuery("select distinct frm.id, P.SNILS, H.SPECNUM\n"
                + "from hiv h\n"
                + "INNER JOIN PATIENT P ON P.ID = H.REFPATIENT\n"
                + "inner join frmessagesync frm on frm.refentity = h.id and frm.method='hiv.create'\n"
                + "where frm.messagedetail ='VALIDATION_FAILED Невозможно создать регистровую запись, так как у пациента она уже существует'\n"
                + "AND FRM.DATEOPERATION BETWEEN ? AND ?", HivReport.class);
        q.setParameter(1, new Timestamp(from.getTime()), TemporalType.TIMESTAMP);
        q.setParameter(2, new Timestamp(to.getTime()), TemporalType.TIMESTAMP);
        return q.getResultList();
    }

    public List<PatientErorReport> loadWrongPatient() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select p.id, p.snils, h.specnum,  p.registrynumber, string_agg(frp.messagedetail, ';') msgp,  \n"
                + "string_agg(frh.messagedetail, ';') msgh,\n"
                + "string_agg(frd.messagedetail, ';') msgd\n"
                + "from patient p\n"
                + "inner join hiv h on h.refpatient = p.id\n"
                + "inner join hivdisp hd on hd.refpatient = p.id\n"
                + "left join frmessagesync frp on frp.refentity = p.id and frp.method like 'patient.%'\n"
                + "left join frmessagesync frh on frh.refentity = h.id and frh.method like 'hiv.%'\n"
                + "left join frmessagesync frd on frd.refentity = hd.id and frd.method like 'hiv_disp.%'\n"
                + "where p.registrynumber is null or hd.cardnumber is null \n"
                + "group by p.id, p.snils, h.specnum,  p.registrynumber", PatientErorReport.class);
        return q.getResultList();
    }
}
