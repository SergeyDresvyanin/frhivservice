/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Hivavrt;
import com.initmed.frhivservice.entity.Hivdispvisits;
import com.initmed.frhivservice.entity.report.HivdispvisitsReport;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.persistence.TemporalType;

/**
 *
 * @author sergey
 */
@Stateless
public class HivdispvisitsFacade extends AbstractFacade<Hivdispvisits> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivdispvisitsFacade() {
        super(Hivdispvisits.class);
    }

    public List<Hivdispvisits> loadHivDispVisit() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT hv.* FROM hivdispvisits hv\n"
                + "INNER JOIN PATIENT P ON P.ID = hv.REFPATIENT\n"
                + "INNER JOIN HIVDISP hd ON hd.REFPATIENT = p.id and P.INFEDREGISTER=1\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1 and hv.INFEDREGISTER=0\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hv.id and method='hiv_disp_visit.create')\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_visits' and TRANSFER=1)", Hivdispvisits.class);
        return (List<Hivdispvisits>) q.setMaxResults(1).getResultList();

    }

    public Integer loadCountHivDispVisit(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT COUNT(distinct hv.ID) FROM hivdispvisits hv\n"
                + "INNER JOIN PATIENT P ON P.ID = hv.REFPATIENT\n"
                + "INNER JOIN HIVDISP hd ON hd.REFPATIENT = p.id and P.INFEDREGISTER=1\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1 and hv.INFEDREGISTER=0\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hv.id and method='hiv_disp_visit.create')\n"
                + "and hv.PLANDATE between ? and ? ");
        q.setParameter(1, from, TemporalType.DATE);
        q.setParameter(2, to, TemporalType.DATE);
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountHivDispVisitError(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT COUNT(distinct hv.ID) FROM hivdispvisits hv\n"
                + "INNER JOIN PATIENT P ON P.ID = hv.REFPATIENT\n"
                + "INNER JOIN HIVDISP hd ON hd.REFPATIENT = p.id and P.INFEDREGISTER=1\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1 \n"
                + "and exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hv.id and method='hiv_disp_visit.create'  and fsm.MESSAGEDETAIL is not null)\n"
                + "and hv.PLANDATE between ? and ?");
        q.setParameter(1, from);
        q.setParameter(2, to);
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer count(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT COUNT(distinct hv.ID) FROM hivdispvisits hv\n"
                + "INNER JOIN PATIENT P ON P.ID = hv.REFPATIENT\n"
                + "INNER JOIN HIVDISP hd ON hd.REFPATIENT = p.id and P.INFEDREGISTER=1\n"
                + "WHERE hv.PLANDATE between ? and ?\n");
        q.setParameter(1, from, TemporalType.DATE);
        q.setParameter(2, to, TemporalType.DATE);
        return ((Number) q.getSingleResult()).intValue();
    }

    public List<HivdispvisitsReport> loadHivDispVisitReport(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT FRM.ID, P.SNILS, H.SPECNUM, COALESCE(FRM.MESSAGEDETAIL, 'OK') MESSAGEDETAIL, HDD.PERSONID, COALESCE(HDD.FACTDATE, HDD.PLANDATE) VISITDATE FROM HIV H\n"
                + "INNER JOIN PATIENT P ON P.ID = H.REFPATIENT\n"
                + "INNER JOIN HIVDISPVISITS HDD ON HDD.REFPATIENT = P.ID\n"
                + "INNER JOIN FRMESSAGESYNC FRM ON FRM.REFENTITY = HDD.ID and frm.METHOD='hiv_disp_visit.create'\n"
                + "WHERE FRM.RESPOCERECEIVED=1\n"
                + "AND FRM.DATEOPERATION BETWEEN ? AND ?", HivdispvisitsReport.class);
        q.setParameter(1, new Timestamp(from.getTime()), TemporalType.TIMESTAMP);
        q.setParameter(2, new Timestamp(to.getTime()), TemporalType.TIMESTAMP);
//        q.setParameter("from", from);
//        q.setParameter("to", to);
        return q.getResultList();
    }
}
