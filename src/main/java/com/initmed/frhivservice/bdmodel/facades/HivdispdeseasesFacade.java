/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Hivdispdeseases;
import com.initmed.frhivservice.entity.report.HivdispdeseasesReport;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.persistence.TemporalType;

/**
 *
 * @author sd199
 */
@Stateless
public class HivdispdeseasesFacade extends AbstractFacade<Hivdispdeseases> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivdispdeseasesFacade() {
        super(Hivdispdeseases.class);
    }

    public List<Hivdispdeseases> loadHivDispDeseases() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select hdd.ID, hdd.REFHIVDISP, hdd.DESEASEDATE DESEASEDATE,\n"
                + "hdd.DESEASE, hdd.PERSONID, hdd.INFEDREGISTER\n"
                + "from HIVDISPDESEASES hdd\n"
                + "inner join hivdisp hd on hdd.REFHIVDISP = hd.id\n"
                + "inner join patient p on p.id = hd.REFPATIENT\n"
                + "where hd.CARDNUMBER is not null\n"
                + "and p.REGISTRYNUMBER is not null\n"
                + "and hdd.INFEDREGISTER=0 and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hdd.id and method='hiv_disp_desease.create')\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_dispdesease' and TRANSFER=1)",
                Hivdispdeseases.class);
        return (List<Hivdispdeseases>) q.setMaxResults(1).getResultList();

    }

    public Integer loadCountHivDispDeseases() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select Count(distinct hdd.ID) "
                + "from HIVDISPDESEASES hdd\n"
                + "inner join hivdisp hd on hdd.REFHIVDISP = hd.id\n"
                + "inner join patient p on p.id = hd.REFPATIENT\n"
                + "where hd.CARDNUMBER is not null\n"
                + "and p.REGISTRYNUMBER is not null\n"
                + "and hdd.INFEDREGISTER=0 and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hdd.id and method='hiv_disp_desease.create') ");
        return ((Number) q.getSingleResult()).intValue();
    }

    public List<HivdispdeseasesReport> loadHivDispDeseasesReport(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT FRM.ID, P.SNILS, H.SPECNUM, FRM.MESSAGEDETAIL, HDD.DESEASE MKBCODE, HDD.DESEASEDATE FROM HIV H\n"
                + "INNER JOIN HIVDISP HD ON HD.REFPATIENT = H.ID\n"
                + "INNER JOIN PATIENT P ON P.ID = H.REFPATIENT\n"
                + "INNER JOIN HIVDISPDESEASES HDD ON HDD.REFHIVDISP = HD.ID\n"
                + "INNER JOIN FRMESSAGESYNC FRM ON FRM.REFENTITY = HDD.ID and frm.METHOD='hiv_disp_desease.create'\n"
                + "WHERE FRM.RESPOCERECEIVED=1\n"
                + "AND FRM.DATEOPERATION BETWEEN ? AND ?", HivdispdeseasesReport.class);
        q.setParameter(1, new Timestamp(from.getTime()), TemporalType.TIMESTAMP);
        q.setParameter(2, new Timestamp(to.getTime()), TemporalType.TIMESTAMP);
        return q.getResultList();
    }
}
