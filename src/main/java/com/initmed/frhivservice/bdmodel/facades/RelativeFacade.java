/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Patient;
import com.initmed.frhivservice.entity.Relative;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

/**
 *
 * @author sd199
 */
@Stateless
public class RelativeFacade extends AbstractFacade<Relative> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RelativeFacade() {
        super(Relative.class);
    }

    public Relative loadRelatives(Patient p) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM Relative where refpatient=?1", Relative.class);
        q.setParameter(1, p.getId());
        try {
            List<Relative> res = (List<Relative>) q.setMaxResults(1).getResultList();
            return res.get(0);
        } catch (Exception e) {
            throw new EmptyResultExcepton();
        }
    }

}
