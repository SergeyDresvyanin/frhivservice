/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Documents;
import com.initmed.frhivservice.entity.Patient;

import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

/**
 *
 * @author User
 */
@Stateless
public class DocumentsFacade extends AbstractFacade<Documents> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DocumentsFacade() {
        super(Documents.class);
    }

    public Documents loadDocuments(Patient p) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM DOCUMENTS where refpatient=?1", Documents.class);
        q.setParameter(1, p.getId());
        try {
            List<Documents> res = (List<Documents>) q.getResultList();
            return res.get(0);
        } catch (Exception e) {
            throw new EmptyResultExcepton();
        }
    }

    public Documents loadDocumentsBySnils(String snils) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM DOCUMENTS d "
                + " INENR JOIN PATIENT P ON P.id = d.refpatient where p.snils=? and INFEDREGISTER=0", Documents.class);
        q.setParameter(1, snils);
        try {
            List<Documents> res = (List<Documents>) q.getResultList();
            return res.get(0);
        } catch (Exception e) {
            throw new EmptyResultExcepton();
        }
    }
}
