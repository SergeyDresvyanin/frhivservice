/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Hivdispchemo;
import com.initmed.frhivservice.entity.Hivdispconc;
import com.initmed.frhivservice.entity.report.HivdispconcReport;
import com.initmed.frhivservice.entity.report.HivdispdexamReport;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.persistence.TemporalType;

/**
 *
 * @author sd199
 */
@Stateless
public class HivdispconcFacade extends AbstractFacade<Hivdispconc> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivdispconcFacade() {
        super(Hivdispconc.class);
    }

    public List<Hivdispconc> loadHivDispConc() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select * from Hivdispconc hh\n"
                + "inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + "inner join patient p on p.id =  hd.refpatient\n"
                + "where p.REGISTRYNUMBER is not null and hd.CARDNUMBER is not null\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hh.id and method='hiv_disp_conc.create')\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_dispconc' and TRANSFER=1)\n"
                + "and  hh.infedregister = 0", Hivdispconc.class);
        return (List<Hivdispconc>) q.setMaxResults(150).getResultList();
    }

    public Integer loadCountHivDispConc() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select count(distinct hh.id)  from Hivdispconc hh\n"
                + "inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + "inner join patient p on p.id =  hd.refpatient\n"
                + "where p.REGISTRYNUMBER is not null and hd.CARDNUMBER is not null\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hh.id and method='hiv_disp_conc.create')\n"
                + "and  hh.infedregister = 0"
        );
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountHivDispConcError() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select count(distinct hh.id)  from Hivdispconc hh\n"
                + "inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + "inner join patient p on p.id =  hd.refpatient\n"
                + "where p.REGISTRYNUMBER is not null and hd.CARDNUMBER is not null\n"
                + "and exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hh.id and method='hiv_disp_conc.create' and fsm.MESSAGEDETAIL is not null)\n"
        );
        return ((Number) q.getSingleResult()).intValue();
    }

    public List<HivdispconcReport> loadConc(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT FRM.ID, P.SNILS, H.SPECNUM, COALESCE(FRM.MESSAGEDETAIL, 'OK'), HC.concdesease, HC.begindate\n"
                + "FROM HIV H\n"
                + "INNER JOIN PATIENT P ON P.ID = H.REFPATIENT\n"
                + "INNER JOIN HIVDISP HD ON HD.refpatient = P.ID\n"
                + "INNER JOIN HIVDISPCONC HC ON HC.refhivdisp = HD.id\n"
                + "INNER JOIN FRMESSAGESYNC FRM ON FRM.REFENTITY = HC.ID and frm.METHOD='hiv_disp_conc.create'\n"
                + "WHERE FRM.RESPOCERECEIVED=1\n"
                + "AND FRM.DATEOPERATION BETWEEN ? AND ?", HivdispdexamReport.class);
        q.setParameter(1, new Timestamp(from.getTime()), TemporalType.TIMESTAMP);
        q.setParameter(2, new Timestamp(to.getTime()), TemporalType.TIMESTAMP);
        return q.getResultList();
    }
}
