/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Scheduleprocess;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

/**
 *
 * @author User
 */
@Stateless
public class ScheduleprocessParamsFacade extends AbstractFacade<Scheduleprocess> {

    public static final String CREATE_VISITS = "create_visits";
    public static final String CREATE_DISPEXAM = "create_dispexam";
    public static final String CREATE_HIVEPID = "create_hivepid";
    public static final String CREATE_BLOT = "create_blot";
    public static final String CREATE_RECIPES = "create_recipes";
    public static final String CREATE_DISPCHEMO = "create_dispChemo";
    public static final String CREATE_DISPCONC = "create_dispconc";
    public static final String CREATE_DISPSTAGE = "create_dispstage";
    public static final String CREATE_DISPCHECKUP = "create_dispcheckup";
    public static final String CREATE_DISPDESEASE = "create_dispdesease";
    public static final String CREATE_DISP = "create_disp";
    public static final String REA_DHIV = "read_hiv";
    public static final String READ_DISP = "read_disp";
    public static final String CREATE_PATIENT = "create_patient";
    public static final String CREATE_AVRT = "create_avrt";

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ScheduleprocessParamsFacade() {
        super(Scheduleprocess.class);
    }

    public boolean loadParameter(String paramName) {
        Query q = em.createNativeQuery("SELECT * FROM SCHEDULEPROCESS where NAME=?", Scheduleprocess.class);
        q.setParameter(1, paramName);
        boolean res = ((Scheduleprocess) q.getSingleResult()).getTransfer() == 1;
        System.out.println(" send visits is  " + res + " " + paramName);
        return res;
    }

    public void udpateParam(String param, Short state) {
        Query q = em.createNativeQuery("UPDATE SCHEDULEPROCESS SET TRANSFER = ? where NAME=?");
        q.setParameter(1, state);
        q.setParameter(2, param);
        q.executeUpdate();
    }

}
