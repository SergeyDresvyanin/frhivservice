/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import jakarta.persistence.EntityManager;
import com.initmed.frhivservice.entity.Hivdispchemo;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

/**
 *
 * @author sd199
 */
@Stateless
public class HivdispchemoFacade extends AbstractFacade<Hivdispchemo> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivdispchemoFacade() {
        super(Hivdispchemo.class);
    }

    public List<Hivdispchemo> loadHivDispChemo() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select * from hivdispchemo hh\n"
                + "inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + "inner join patient p on p.id =  hd.refpatient\n"
                + "where p.REGISTRYNUMBER is not null and hd.CARDNUMBER is not null\n"
                + "and  hh.infedregister = 0 \n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hh.id and method='hiv_disp_chemo.create')\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_dispChemo' and TRANSFER=1)\n", Hivdispchemo.class);
        return (List<Hivdispchemo>) q.setMaxResults(150).getResultList();

    }

    public Integer loadCountHivDispChemo() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select count(distinct hh.id) from hivdispchemo hh\n"
                + "inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + "inner join patient p on p.id =  hd.refpatient\n"
                + "where p.REGISTRYNUMBER is not null and hd.CARDNUMBER is not null\n"
                + "and  hh.infedregister = 0\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hh.id and method='hiv_disp_chemo.create')");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountHivDispChemoError() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select count(distinct hh.id) from hivdispchemo hh\n"
                + "inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + "inner join patient p on p.id =  hd.refpatient\n"
                + "where p.REGISTRYNUMBER is not null and hd.CARDNUMBER is not null\n"
                + "and  exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hh.id and method='hiv_disp_chemo.create'  and fsm.MESSAGEDETAIL is not null)");
        return ((Number) q.getSingleResult()).intValue();
    }
}
