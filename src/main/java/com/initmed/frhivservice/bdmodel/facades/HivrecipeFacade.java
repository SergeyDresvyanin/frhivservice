/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Hivrecipe;
import com.initmed.frhivservice.entity.report.HivRecipesReport;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.persistence.TemporalType;

/**
 *
 * @author sergey
 */
@Stateless
public class HivrecipeFacade extends AbstractFacade<Hivrecipe> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivrecipeFacade() {
        super(Hivrecipe.class);
    }

    public List<Hivrecipe> loadHivRecipe() {
        Query q = em.createNativeQuery("SELECT hr.* FROM HIVRECIPE HR\n"
                + "INNER JOIN PATIENT P ON P.ID = HR.REFPATIENT\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1 and hr.INFEDREGISTER=0\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hr.id and method='hiv_recipe.create')\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_recipes' and TRANSFER=1)\n"
                + "order by hr.issuedate desc \n"
                + "", Hivrecipe.class);
        return (List<Hivrecipe>) q.setMaxResults(1).getResultList();
    }

    public List<Hivrecipe> loadHivRecipes() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT hr.* FROM HIVRECIPE HR\n"
                + "INNER JOIN PATIENT P ON P.ID = HR.REFPATIENT\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1 and hr.INFEDREGISTER=0\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hr.id and method='hiv_recipe.create')", Hivrecipe.class);
        try {
            List<Hivrecipe> res = (List<Hivrecipe>) q.setMaxResults(10000).getResultList();
            return res;
        } catch (Exception e) {
            throw new EmptyResultExcepton();
        }
    }

    public Integer loadCountRecipes(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT COUNT(distinct hr.id) FROM HIVRECIPE HR\n"
                + "INNER JOIN PATIENT P ON P.ID = HR.REFPATIENT\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1 and hr.INFEDREGISTER=0\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hr.id and method='hiv_recipe.create')"
                + "and ISSUEDATE between ? and ?");
        q.setParameter(1, from, TemporalType.DATE);
        q.setParameter(2, to, TemporalType.DATE);
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountRecipesError(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT COUNT(distinct hr.id) FROM HIVRECIPE HR\n"
                + "INNER JOIN PATIENT P ON P.ID = HR.REFPATIENT\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1 \n"
                + "and  exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hr.id and method='hiv_recipe.create' and fsm.MESSAGEDETAIL is not null)"
                + "and ISSUEDATE between ? and ?");
        q.setParameter(1, from, TemporalType.DATE);
        q.setParameter(2, to, TemporalType.DATE);
        return ((Number) q.getSingleResult()).intValue();
    }

    public int count(Date from, Date to) {
        Query q = em.createNativeQuery("SELECT COUNT(distinct hr.id) FROM HIVRECIPE HR\n"
                + "INNER JOIN PATIENT P ON P.ID = HR.REFPATIENT\n"
                + "where ISSUEDATE between ? and ?");
        q.setParameter(1, from, TemporalType.DATE);
        q.setParameter(2, to, TemporalType.DATE);
        return ((Number) q.getSingleResult()).intValue();
    }

    public List<HivRecipesReport> loadHivRecipesReport(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT FRM.ID, P.SNILS, H.SPECNUM, COALESCE(FRM.MESSAGEDETAIL, 'OK') MESSAGEDETAIL, HR.PERSONID,\n"
                + "HR.issuedate ISSUEDATE,\n"
                + "DT.\"name\" TRADENAME,\n"
                + "DF.\"name\" FORM,\n"
                + "DD.\"name\" DOSE,\n"
                + "HR.packcount PACKCOUNT,\n"
                + "HR.dosecount DOSECOUNT,\n"
                + "HR.dailycount PERDAY,\n"
                + "HR.recipenumber recipenumber\n"
                + "FROM HIV H\n"
                + "INNER JOIN PATIENT P ON P.ID = H.REFPATIENT\n"
                + "INNER JOIN hivrecipe HR ON HR.REFPATIENT = P.ID\n"
                + "INNER JOIN drug_dir_trade DT ON DT.id = HR.drugid\n"
                + "INNER JOIN drug_dir_form DF ON DF.id = HR.drugformid\n"
                + "INNER JOIN drug_dir_dose DD ON DD.id = HR.drugdosageid\n"
                + "INNER JOIN FRMESSAGESYNC FRM ON FRM.REFENTITY = HR.ID and frm.METHOD='hiv_recipe.create'\n"
                + "WHERE FRM.RESPOCERECEIVED=1\n"
                + "AND FRM.DATEOPERATION BETWEEN ? AND ?", HivRecipesReport.class);
        q.setParameter(1, new Timestamp(from.getTime()), TemporalType.TIMESTAMP);
        q.setParameter(2, new Timestamp(to.getTime()), TemporalType.TIMESTAMP);

        return q.getResultList();
    }

}
