/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Frmessagesync;
import com.initmed.frhivservice.entity.report.PatientErorReport;
import java.util.Date;

import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

/**
 *
 * @author sd199
 */
@Stateless
public class FrmessagesyncFacade extends AbstractFacade<Frmessagesync> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FrmessagesyncFacade() {
        super(Frmessagesync.class);
    }

    public Frmessagesync getMessage(String msgId) {
        Query q = em.createNamedQuery("Frmessagesync.findByMessageid", Frmessagesync.class);
        q.setParameter("messageid", msgId);
        List<Frmessagesync> list = q.getResultList();
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public void deleteByIdAndMethod(Integer patient, String method) {
        Query q = em.createNamedQuery("DELETE FROM Frmessagesync FRM\n"
                + "WHERE FRM.REFENTITY in(SELECT ID FROM hivavrt H WHERE H.refpatient = (SELECT REFPATIENT FROM HIVAVRT H1 WHERE H1.ID = ?))\n"
                + "AND FRM.METHOD = ?");
        q.setParameter(1, patient);
        q.setParameter(2, method);
        q.executeUpdate();
    }

    public void deleteDouble(String method, Integer entityId) {
        Query q = em.createNamedQuery("Frmessagesync.findByIdAndMethodEmptyMsgId", Frmessagesync.class);
        q.setParameter("refentity", entityId);
        q.setParameter("method", method);
        List<Frmessagesync> list = q.getResultList();
        if (list != null && !list.isEmpty()) {
            remove(list.get(0));
        }
    }

    public void readDouble(String method, Integer entityId) {
        Query q = em.createNamedQuery("Frmessagesync.findByIdAndMethodEmptyMsgId", Frmessagesync.class);
        q.setParameter("refentity", entityId);
        q.setParameter("method", method);
        List<Frmessagesync> list = q.getResultList();
        if (list != null && !list.isEmpty()) {
            remove(list.get(0));
        }
    }

    public void createMessage(String moOid, String msgId, String method, int entityId, short respocereceived) {
        Query q = em.createNativeQuery("INSERT INTO frmessagesync(MESSAGEID, METHOD, REFENTITY, respocereceived, MO_OID) VALUES(?, ?, ?, ?, ?)\n"
                + "ON CONFLICT (MESSAGEID) DO UPDATE SET METHOD = EXCLUDED.METHOD, REFENTITY = EXCLUDED.REFENTITY, MESSAGEDETAIL = EXCLUDED.MESSAGEDETAIL, MO_OID = EXCLUDED.MO_OID, RESPOCERECEIVED = 1");
        q.setParameter(1, msgId);
        q.setParameter(2, method);
        q.setParameter(3, entityId);
        q.setParameter(4, respocereceived);
        q.setParameter(5, moOid);
        q.executeUpdate();

//        Frmessagesync frmessagesync = null;
//        List<Frmessagesync> list = null;
//        if (msgId != null) {
//            Query q = em.createNamedQuery("Frmessagesync.findByMessageid", Frmessagesync.class);
//            q.setParameter("messageid", msgId);
//            list = q.getResultList();
//        }
//        if (list != null && !list.isEmpty()) {
//            frmessagesync = list.get(0);
//            frmessagesync.setRefentity(entityId);
//            frmessagesync.setMethod(method);
//            edit(frmessagesync);
//        } else {
//            Query q = em.createNamedQuery("Frmessagesync.findByIdAndMethod");
//            q.setParameter("refentity", entityId);
//            q.setParameter("method", method);
//            List<Frmessagesync> fs = q.getResultList();
//            if (fs.isEmpty()) {
//                frmessagesync = new Frmessagesync();
//                frmessagesync.setMessageid(msgId);
//                frmessagesync.setDateoperation(new Date());
//                frmessagesync.setMethod(method);
//                frmessagesync.setRefentity(entityId);
//                create(frmessagesync);
//            } else {
//                Frmessagesync s = fs.get(0);
//                s.setRefentity(entityId);
//                s.setMethod(method);
//                edit(s);
//            }
//        }
    }

    public void createErrorMessage(String moOid, String msgId, String method, int entityId, String error) {
        Query q = em.createNativeQuery("INSERT INTO frmessagesync(MESSAGEID, METHOD, REFENTITY, MESSAGEDETAIL, RESPOCERECEIVED, MO_OID) VALUES(?, ?, ?, ?, ?, 0)\n"
                + "ON CONFLICT (MESSAGEID) \n"
                + "DO UPDATE SET METHOD = EXCLUDED.METHOD, "
                + "REFENTITY = EXCLUDED.REFENTITY, "
                + "MESSAGEDETAIL = EXCLUDED.MESSAGEDETAIL, "
                + "MO_OID = EXCLUDED.MO_OID, "
                + "RESPOCERECEIVED = 1");
        q.setParameter(1, msgId);
        q.setParameter(2, method);
        q.setParameter(3, entityId);
        q.setParameter(4, error);
        q.setParameter(5, moOid);
        q.executeUpdate();
    }

//    public void createMessage(String msgId, String method, int entityId) {
//        Query q = em.createNativeQuery("UPSERT frmessagesync(MESSAGEID, METHOD, REFENTITY, MESSAGEDETAIL, RESPOCERECEIVED) VALUES(?, ?, ?, ?, ?)\n"
//                + "ON CONFLICT (MESSAGEID) \n"
//                + "DO UPDATE SET METHOD = EXCLUDED.METHOD, REFENTITY = EXCLUDED.REFENTITY, MESSAGEDETAIL = EXCLUDED.MESSAGEDETAIL,  RESPOCERECEIVED = EXCLUDED.RESPOCERECEIVED");
//        q.setParameter(1, msgId);
//        q.setParameter(2, method);
//        q.setParameter(3, entityId);
//        q.executeUpdate();
//    }
    public void saveErrorMessage(Frmessagesync msg, String errror) {
        msg.setMessagedetail(errror);
        em.merge(msg);
    }

    public List<Frmessagesync> getMEssagesFromLastHour() {
        Query q = em.createNativeQuery("SELECT * FROM FRMESSAGESYNC WHERE DATEOPERATION>DATEADD(-1, HOUR, CURRENT_TIMESTAMP)", Frmessagesync.class);
        return q.getResultList();
    }

    public List<Frmessagesync> loadRowsWithErrors(int limit) {
        Query q = em.createNativeQuery("SELECT * FROM FRMESSAGESYNC WHERE messagedetail IS NOT NULL ORDER BY DATEOPERATION DESC", Frmessagesync.class);
        return q.setMaxResults(limit).getResultList();
    }

}
