/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Hivavrt;
import com.initmed.frhivservice.entity.Hivavrtdrugs;
import com.initmed.frhivservice.entity.Patient;
import com.initmed.frhivservice.entity.report.HivAvrtReport;
import com.initmed.frhivservice.frdatamodel.NrHivRegistryAvrt;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import java.sql.Timestamp;
import jakarta.persistence.TemporalType;

/**
 *
 * @author sergey
 */
@Stateless
public class HivavrtFacade extends AbstractFacade<Hivavrt> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @EJB
    private HivavrtdrugsFacade hivavrtdrugsFacade;
    @EJB
    private PatientFacade patientFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivavrtFacade() {
        super(Hivavrt.class);
    }

    public List<Hivavrt> loadHivAVRT() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT distinct ha.* FROM HIVAVRT HA\n"
                + "INNER JOIN PATIENT P ON P.ID = HA.REFPATIENT\n"
                + "INNER JOIN HIVDISP hd ON hd.REFPATIENT = p.id and HD.INFEDREGISTER=1\n"
                + "LEFT JOIN FRMESSAGESYNC fsm ON fsm.REFENTITY = ha.id and method='hiv_avrt.create'\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1 AND HA.infedregister =0\n"
                + "AND (fsm.ID IS NULL or exists(select * from FRMESSAGESYNC fsm1 \n"
                + "							  where fsm1.REFENTITY = ha.id and fsm1.method='hiv_avrt.list' \n"
                + "							  and not exists (select fsm2.REFENTITY from frmessagesync fsm2 \n"
                + "											  where fsm2.REFENTITY = ha.id \n"
                + "											  and fsm2.method='hiv_avrt.create'\n"
                + "											  group by fsm2.REFENTITY\n"
                + "											  having count(fsm2.REFENTITY)>1)))\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_avrt' and TRANSFER=1)", Hivavrt.class);
        return (List<Hivavrt>) q.setMaxResults(1).getResultList();
    }

    public Integer loadCountHivAVRT() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct ha.*) FROM HIVAVRT HA\n"
                + "INNER JOIN PATIENT P ON P.ID = HA.REFPATIENT\n"
                + "INNER JOIN HIVDISP hd ON hd.REFPATIENT = p.id and HD.INFEDREGISTER=1\n"
                + "LEFT JOIN FRMESSAGESYNC fsm ON fsm.REFENTITY = ha.id and method='hiv_avrt.create'\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1 AND HA.infedregister =0\n"
                + "AND fsm.ID IS NULL\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_avrt' and TRANSFER=1)");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Hivavrt loadLastHivAVRT(int patId) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM HIVAVRT HA\n"
                + "WHERE REFPATIENT = ? order by begindate desc", Hivavrt.class);
        try {
            q.setParameter(1, patId);
            List<Hivavrt> res = (List<Hivavrt>) q.getResultList();
            return res.get(0);
        } catch (Exception e) {
            throw new EmptyResultExcepton();
        }
    }

    public void saveAvrt(NrHivRegistryAvrt avrt, int patId) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT * FROM HIVAVRT HA\n"
                + "WHERE REFPATIENT=? and begindate =? order by begindate desc", Hivavrt.class);
        q.setParameter(1, patId);
        q.setParameter(2, avrt.getBeginDate().toGregorianCalendar().getTime());
        List<Hivavrt> res = (List<Hivavrt>) q.getResultList();
        Query lastQ = em.createNativeQuery("SELECT * FROM HIVAVRT HA\n"
                + "WHERE HA.REFPATIENT=? order by ha.begindate desc", Hivavrt.class);
        lastQ.setParameter(1, patId);
        List<Hivavrt> r = lastQ.getResultList();

        if (res.isEmpty()) {
            Hivavrt ha = new Hivavrt();
            ha.setBegindate(avrt.getBeginDate().toGregorianCalendar().getTime());
            ha.setBeginpersonid(avrt.getBeginPersonId());
            ha.setPatient(patientFacade.find(patId));
            ha.setInfedregister((short) 0);
            ha.setIsfirsttreatment(avrt.isIsFirstTreatment() ? (short) 1 : (short) (0));
            ha.setIsneedchangetreatment(avrt.isIsNeedChangeTreatment() ? (short) 1 : (short) (0));
            ha.setEndreasonid(8);
            ha.setEndreasondetail("обновление терапии");
            Set<Hivavrtdrugs> set = new HashSet<>();
            create(ha);
            avrt.getDrugs().getDrug().stream().map(d -> {
                Hivavrtdrugs hivavrtdrugs = new Hivavrtdrugs();
                hivavrtdrugs.setDosageid((int) d.getDosageId().getId());
                hivavrtdrugs.setDrugformid((int) d.getDrugFormId().getId());
                hivavrtdrugs.setMnnid((int) d.getMnnId().getId());
                hivavrtdrugs.setNeedday(d.getNeedDay().intValue());
                hivavrtdrugs.setNeedyear(d.getNeedYear().intValue());
                hivavrtdrugs.setRefhivart(ha);
                hivavrtdrugsFacade.create(hivavrtdrugs);
                return hivavrtdrugs;
            }).forEachOrdered(hivavrtdrugs -> {
                set.add(hivavrtdrugs);
            });
            ha.setHivavrtdrugsSet(set);
            ha.setEnddate(r.get(0).getBegindate());
            ha.setEndpersonid(avrt.getBeginPersonId());
            edit(ha);
        } else {
            Hivavrt ha = res.get(0);
            ha.setEnddate(r.get(0).getBegindate());
            ha.setEndpersonid(avrt.getBeginPersonId());
            edit(ha);
        }
    }

    public List<Patient> loadForListClose() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT distinct p.* FROM HIVAVRT HA\n"
                + "INNER JOIN PATIENT P ON P.ID = HA.REFPATIENT\n"
                + "INNER JOIN HIVDISP hd ON hd.REFPATIENT = p.id and HD.INFEDREGISTER=1\n"
                + "LEFT JOIN FRMESSAGESYNC fsm ON fsm.REFENTITY = ha.id and method='hiv_avrt.create'\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1\n"
                + "AND fsm.messagedetail = 'VALIDATION_FAILED Не может быть больше одной открытой схемы лечения'\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = p.id and method='hiv_avrt.list')", Patient.class);
        return (List<Patient>) q.setMaxResults(150).getResultList();
    }

    public List<Hivavrt> loadForUpdateClose() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT distinct ha.* FROM HIVAVRT HA\n"
                + "INNER JOIN PATIENT P ON P.ID = HA.REFPATIENT\n"
                + "INNER JOIN HIVDISP hd ON hd.REFPATIENT = p.id and HD.INFEDREGISTER=1\n"
                + "LEFT JOIN FRMESSAGESYNC fsm ON fsm.REFENTITY = ha.id and method='hiv_avrt.update'\n"
                + "WHERE P.REGISTRYNUMBER IS NOT NULL AND P.INFEDREGISTER =1\n"
                + "AND fsm.ID IS NULL AND HA.enddate IS NOT NULL AND HA.infedregister =0\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_avrt' and TRANSFER=1)", Hivavrt.class);
        return (List<Hivavrt>) q.setMaxResults(150).getResultList();

    }

    public Integer loadCountWithError() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT count(distinct ha.id) FROM HIVAVRT HA\n"
                + "INNER JOIN PATIENT P ON P.ID = HA.REFPATIENT\n"
                + "INNER JOIN HIVDISP hd ON hd.REFPATIENT = p.id and HD.INFEDREGISTER=1\n"
                + "INNER JOIN FRMESSAGESYNC fsm ON fsm.REFENTITY = ha.id and method='hiv_avrt.create'\n"
                + "WHERE fsm.MESSAGEDETAIL is not null");
        return ((Number) q.getSingleResult()).intValue();
    }

    public List<HivAvrtReport> loadAVRTReport(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT FRM.ID, P.SNILS, H.SPECNUM, COALESCE(FRM.MESSAGEDETAIL, 'OK') MESSAGEDETAIL, hi.beginpersonid,\n"
                + "hi.begindate DATE,\n"
                + "DT.\"name\" MNN,\n"
                + "DF.\"name\" FORM,\n"
                + "DD.\"name\" DOSE\n"
                + "FROM HIV H\n"
                + "INNER JOIN PATIENT P ON P.ID = H.REFPATIENT\n"
                + "INNER JOIN hivavrt Hi ON Hi.refpatient =  p.id\n"
                + "inner join hivavrtdrugs ha on ha.refhivart = Hi.id\n"
                + "INNER JOIN drug_dir_trade DT ON DT.id = Ha.mnnid\n"
                + "INNER JOIN drug_dir_form DF ON DF.id = Ha.drugformid\n"
                + "INNER JOIN drug_dir_dose DD ON DD.id = Ha.dosageid\n"
                + "INNER JOIN FRMESSAGESYNC FRM ON FRM.REFENTITY = Hi.ID and frm.METHOD='hiv_avrt.create'\n"
                + "WHERE FRM.RESPOCERECEIVED=1\n"
                + "AND FRM.DATEOPERATION BETWEEN ? AND ?\n"
                + "order by DATEOPERATION", HivAvrtReport.class);
        q.setParameter(1, new Timestamp(from.getTime()), TemporalType.TIMESTAMP);
        q.setParameter(2, new Timestamp(to.getTime()), TemporalType.TIMESTAMP);
        return q.getResultList();
    }

}
