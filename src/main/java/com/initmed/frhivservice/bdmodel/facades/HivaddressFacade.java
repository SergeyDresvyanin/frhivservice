/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;


import com.initmed.frhivservice.entity.Hivaddress;
import com.initmed.frhivservice.entity.Patient;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

/**
 *
 * @author User
 */
@Stateless
public class HivaddressFacade extends AbstractFacade<Hivaddress> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivaddressFacade() {
        super(Hivaddress.class);
    }

    public Hivaddress loadHivAdress(Patient p) {
        try {
            Query q = em.createNativeQuery("SELECT * FROM  HIVADDRESS where refpatient=?1", Hivaddress.class);
            q.setParameter(1, p.getId());
            List<Hivaddress> addrList = (List<Hivaddress>) q.setMaxResults(1).getResultList();
            return addrList.get(0);
        } catch (Exception e) {
            return null;
        }
    }

}
