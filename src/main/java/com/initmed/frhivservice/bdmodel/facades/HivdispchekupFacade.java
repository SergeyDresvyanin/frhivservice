/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Hivdispchekup;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

/**
 *
 * @author sd199
 */
@Stateless
public class HivdispchekupFacade extends AbstractFacade<Hivdispchekup> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivdispchekupFacade() {
        super(Hivdispchekup.class);
    }

    public List<Hivdispchekup> loadHivDispChekup() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select * from Hivdispchekup hh\n"
                + " inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + " inner join patient p on p.id =  hd.refpatient\n"
                + " where p.REGISTRYNUMBER is not null and hd.CARDNUMBER is not null\n"
                + " and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hh.id and method='hiv_disp_checkup.create')\n"
                + " and exists(select * from SCHEDULEPROCESS where NAME='create_dispcheckup' and TRANSFER=1)\n"
                + " and  hh.infedregister = 0", Hivdispchekup.class);
        return (List<Hivdispchekup>) q.setMaxResults(150).getResultList();
    }

    public Integer loadCountHivDispChekup() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select count(distinct hh.id) from Hivdispchekup hh\n"
                + "inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + "inner join patient p on p.id =  hd.refpatient\n"
                + "where p.REGISTRYNUMBER is not null and hd.CARDNUMBER is not null\n"
                + "and not exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hh.id and method='hiv_disp_checkup.create')\n"
                + "and  hh.infedregister = 0");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountHivDispChekupError() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select count(distinct hh.id) from Hivdispchekup hh\n"
                + "inner join hivdisp hd on hd.id = hh.refhivdisp\n"
                + "inner join patient p on p.id =  hd.refpatient\n"
                + "where p.REGISTRYNUMBER is not null and hd.CARDNUMBER is not null\n"
                + "and exists(select * from FRMESSAGESYNC fsm where fsm.REFENTITY = hh.id and method='hiv_disp_checkup.create' and fsm.MESSAGEDETAIL is not null)");
        return ((Number) q.getSingleResult()).intValue();
    }

}
