/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.facades;

import com.initmed.frhivservice.entity.Hivdispexam;
import com.initmed.frhivservice.entity.report.HivdispconcReport;
import com.initmed.frhivservice.entity.report.HivdispdexamReport;
import com.initmed.frhivservice.entity.report.HivdispvisitsReport;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.persistence.TemporalType;

/**
 *
 * @author User
 */
@Stateless
public class HivdispexamFacade extends AbstractFacade<Hivdispexam> {

    @PersistenceContext(unitName = "fbsource")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HivdispexamFacade() {
        super(Hivdispexam.class);
    }

    public List<Hivdispexam> loadDispExam() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select  HDE.* from HIVDISPEXAM hde\n"
                + "inner join PATIENT p on hde.REFPATIENT = p.ID\n"
                + "inner join HIVDISP hd on hd.REFPATIENT = p.ID\n"
                + "where  HDE.INFEDREGISTER=0 and hde.REFEXAMID in(6,8,10) and hd.INFEDREGISTER =1 \n"
                + "and NOT exists(select * from FRMESSAGESYNC FR where FR.REFENTITY = HDE.ID and FR.method='hiv_disp_exam.create')\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_dispexam' and TRANSFER=1)\n", Hivdispexam.class);
        return (List<Hivdispexam>) q.setMaxResults(150).getResultList();

    }

    public Integer loadCountDispExam(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select COUNT(distinct HDE.id) from HIVDISPEXAM hde\n"
                + "inner join PATIENT p on hde.REFPATIENT = p.ID\n"
                + "inner join HIVDISP hd on hd.REFPATIENT = p.ID\n"
                + "where  HDE.INFEDREGISTER=0 and hde.REFEXAMID in(6,8,10) and hd.INFEDREGISTER =1 \n"
                + "and NOT exists(select * from FRMESSAGESYNC FR where FR.REFENTITY = HDE.ID and FR.method='hiv_disp_exam.create')\n"
                + "and HDE.EXAMDATE BETWEEN ? AND ?");
        q.setParameter(1, from, TemporalType.DATE);
        q.setParameter(2, to, TemporalType.DATE);
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountDispExamError(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select COUNT(distinct HDE.id) from HIVDISPEXAM hde\n"
                + "inner join PATIENT p on hde.REFPATIENT = p.ID\n"
                + "inner join HIVDISP hd on hd.REFPATIENT = p.ID\n"
                + "where hde.REFEXAMID in(6,8,10) and hd.INFEDREGISTER =1 \n"
                + "and exists(select * from FRMESSAGESYNC FR where FR.REFENTITY = HDE.ID and FR.method='hiv_disp_exam.create' and FR.MESSAGEDETAIL is not null) and HDE.EXAMDATE BETWEEN ? AND ?");
        q.setParameter(1, from, TemporalType.DATE);
        q.setParameter(2, to, TemporalType.DATE);
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountTotalDispExam(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select COUNT(distinct HDE.id) from HIVDISPEXAM hde\n"
                + "where hde.REFEXAMID in(6,8,10) and HDE.EXAMDATE BETWEEN ? AND ?");
        q.setParameter(1, from, TemporalType.DATE);
        q.setParameter(2, to, TemporalType.DATE);
        return ((Number) q.getSingleResult()).intValue();
    }

    public List<Hivdispexam> loadHivExam() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select HDE.* from HIVDISPEXAM hde\n"
                + "inner join PATIENT p on hde.REFPATIENT = p.ID\n"
                + "where HDE.INFEDREGISTER=0 and hde.REFEXAMID in(2,4,5,7,9)\n"
                + "and NOT exists(select * from FRMESSAGESYNC FR where FR.REFENTITY = HDE.ID and FR.method='hiv_exam.create')\n"
                + "and exists(select * from SCHEDULEPROCESS where NAME='create_blot' and TRANSFER=1)\n", Hivdispexam.class);
        return (List<Hivdispexam>) q.getResultList();
    }

    public Integer loadCountHivExam() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select COUNT(distinct HDE.id)"
                + "from HIVDISPEXAM hde \n"
                + "inner join PATIENT p on hde.REFPATIENT = p.ID \n"
                + "where HDE.INFEDREGISTER=0 and hde.REFEXAMID in(2,4,5,7,9) \n"
                + "and NOT exists(select * from FRMESSAGESYNC FR where FR.REFENTITY = HDE.ID and FR.method='hiv_exam.create')");
        return ((Number) q.getSingleResult()).intValue();
    }

    public Integer loadCountTotalHivExam() throws EmptyResultExcepton {
        Query q = em.createNativeQuery("select COUNT(distinct HDE.id)"
                + "from HIVDISPEXAM hde \n"
                + "where hde.REFEXAMID in(2,4,5,7,9)");
        return ((Number) q.getSingleResult()).intValue();
    }

    public List<HivdispdexamReport> loadHivDispExamReport(Date from, Date to) throws EmptyResultExcepton {
        Query q = em.createNativeQuery("SELECT FRM.ID, P.SNILS, H.SPECNUM, COALESCE(FRM.MESSAGEDETAIL, 'OK'), HE.EXAMDATE, EM.NAME\n"
                + "FROM HIV H\n"
                + "INNER JOIN PATIENT P ON P.ID = H.REFPATIENT\n"
                + "INNER JOIN HIVDISPEXAM HE ON HE.REFPATIENT = P.ID\n"
                + "INNER JOIN EXAM EM ON EM.id = HE.REFEXAMID\n"
                + "INNER JOIN FRMESSAGESYNC FRM ON FRM.REFENTITY = HE.ID and frm.METHOD='hiv_disp_exam.create'\n"
                + "WHERE FRM.RESPOCERECEIVED=1\n"
                + "AND FRM.DATEOPERATION BETWEEN ? AND ?", HivdispdexamReport.class);
        q.setParameter(1, new Timestamp(from.getTime()), TemporalType.TIMESTAMP);
        q.setParameter(2, new Timestamp(to.getTime()), TemporalType.TIMESTAMP);
        return q.getResultList();
    }

}
