/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.converter;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 *
 * @author User
 */
public class FRNode implements Serializable {

    private String moOid;
    private String method;
    private String val;
    private int entityID;

    public FRNode(String moOid, String method, String val, int entityID) {
        this.moOid = moOid;
        this.method = method;
        this.val = Base64.getEncoder().encodeToString(val.getBytes(StandardCharsets.UTF_8));
        this.entityID = entityID;
    }

    public String getMethod() {
        return method;
    }

    public String getVal() {
        return val;
    }

    public int getEntityID() {
        return entityID;
    }

    public String getMoOid() {
        return moOid;
    }

    public void setMoOid(String moOid) {
        this.moOid = moOid;
    }

}
