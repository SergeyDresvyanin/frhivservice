/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.converter;

import com.initmed.frhivservice.frdatamodel.HivDispcards;
import com.initmed.frhivservice.frdatamodel.NrHivDispcard.Exams;
import com.initmed.frhivservice.frdatamodel.NrHivRegistryAvrts;
import com.initmed.frhivservice.frdatamodel.ObjectFactory;
import java.io.StringReader;
import java.io.StringWriter;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

/**
 *
 * @author sd199
 */
public class EntityUtil {

    public static String toXml(JAXBElement element) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance("com.initmed.frhivservice.frdatamodel");
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty("jaxb.encoding", "Unicode");
            StringWriter stringWriter = new StringWriter();
            marshaller.marshal(element, stringWriter);
            return stringWriter.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static NrHivRegistryAvrts parseAVRT(String msg) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
        NrHivRegistryAvrts res = ((JAXBElement<NrHivRegistryAvrts>) jaxbContext.createUnmarshaller().unmarshal(new StringReader(msg))).getValue();
        return res;
    }

    public static HivDispcards parseHIVDISPLIST(String msg) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
        HivDispcards res = ((JAXBElement<HivDispcards>) jaxbContext.createUnmarshaller().unmarshal(new StringReader(msg))).getValue();
        return res;
    }
    
      public static Exams parseHIVDISPEXAMLIST(String msg) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
        Exams res = ((JAXBElement<Exams>) jaxbContext.createUnmarshaller().unmarshal(new StringReader(msg))).getValue();
        return res;
    }

    public static String clearError(String msg) {
        msg = msg.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><error><code>", "").replace("</detail></error>", "").replace("</code><detail>", " ");
        if (msg.length() >= 3000) {
            msg = msg.substring(0, 2999);
        }
        return msg;
    }
}
