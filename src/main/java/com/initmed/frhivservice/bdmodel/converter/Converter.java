/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.initmed.frhivservice.bdmodel.converter;

import com.initmed.frhivservice.entity.Relative;
import com.initmed.frhivservice.entity.Hivavrtdrugs;
import com.initmed.frhivservice.entity.Hivdispstage;
import com.initmed.frhivservice.entity.Hivdispchemo;
import com.initmed.frhivservice.entity.Hivdispdeseases;
import com.initmed.frhivservice.entity.Hivdisphosp;
import com.initmed.frhivservice.entity.Hivdispexam;
import com.initmed.frhivservice.entity.Hivaddress;
import com.initmed.frhivservice.entity.Contingents;
import com.initmed.frhivservice.entity.Hivdispconc;
import com.initmed.frhivservice.entity.Hivdisp;
import com.initmed.frhivservice.entity.Hivdispchekup;
import com.initmed.frhivservice.entity.Hivdispvisits;
import com.initmed.frhivservice.entity.Desease;
import com.initmed.frhivservice.entity.Hiv;
import com.initmed.frhivservice.entity.Revealedpathologies;
import com.initmed.frhivservice.entity.Hivrecipe;
import com.initmed.frhivservice.entity.Hivepid;
import com.initmed.frhivservice.entity.Hivavrt;
import com.initmed.frhivservice.entity.Patient;
import com.initmed.frhivservice.entity.Documents;
import com.initmed.frhivservice.bdmodel.facades.DefaultparamsFacade;
import com.initmed.frhivservice.bdmodel.facades.EmptyResultExcepton;
import com.initmed.frhivservice.entity.HivepidContacts;
import com.initmed.frhivservice.frdatamodel.BasePatientKey;
import com.initmed.frhivservice.frdatamodel.BaseRegistryKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcard;
import com.initmed.frhivservice.frdatamodel.CreateDispcardCheckupKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardChemoKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardConcDeseaseKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardDeseaseKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardExamKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardHospKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardStageKey;
import com.initmed.frhivservice.frdatamodel.CreateDispcardVisitKey;
import com.initmed.frhivservice.frdatamodel.CreateHivAddress;
import com.initmed.frhivservice.frdatamodel.CreateHivAvrt;
import com.initmed.frhivservice.frdatamodel.CreateHivEpid;
import com.initmed.frhivservice.frdatamodel.CreateHivExam;
import com.initmed.frhivservice.frdatamodel.CreateHivRecipe;
import com.initmed.frhivservice.frdatamodel.CreateHivRegistry;
import com.initmed.frhivservice.frdatamodel.CreatePatientDocuments;
import com.initmed.frhivservice.frdatamodel.CreatePatientRelatives;
import com.initmed.frhivservice.frdatamodel.DispcardCheckupKey;
import com.initmed.frhivservice.frdatamodel.DispcardChemoKey;
import com.initmed.frhivservice.frdatamodel.DispcardConcDeseaseKey;
import com.initmed.frhivservice.frdatamodel.DispcardDeseaseKey;
import com.initmed.frhivservice.frdatamodel.DispcardExamKey;
import com.initmed.frhivservice.frdatamodel.DispcardHospKey;
import com.initmed.frhivservice.frdatamodel.DispcardKey;
import com.initmed.frhivservice.frdatamodel.DispcardStageKey;
import com.initmed.frhivservice.frdatamodel.DispcardVisitKey;
import com.initmed.frhivservice.frdatamodel.HivAddressKey;
import com.initmed.frhivservice.frdatamodel.HivAvrtKey;
import com.initmed.frhivservice.frdatamodel.HivExamKey;
import com.initmed.frhivservice.frdatamodel.NrAddress;
import com.initmed.frhivservice.frdatamodel.NrHivDispVisit;
import com.initmed.frhivservice.frdatamodel.NrHivDispcardCheckup;
import com.initmed.frhivservice.frdatamodel.NrHivDispcardChemoCourse;
import com.initmed.frhivservice.frdatamodel.NrHivDispcardConcDesease;
import com.initmed.frhivservice.frdatamodel.NrHivDispcardDesease;
import com.initmed.frhivservice.frdatamodel.NrHivDispcardExam;
import com.initmed.frhivservice.frdatamodel.NrHivDispcardExamResult;
import com.initmed.frhivservice.frdatamodel.NrHivDispcardHospitalization;
import com.initmed.frhivservice.frdatamodel.NrHivDispcardStage;
import com.initmed.frhivservice.frdatamodel.NrHivEpidReference;
import com.initmed.frhivservice.frdatamodel.NrHivLightDispcard;
import com.initmed.frhivservice.frdatamodel.NrHivPatientAddress;
import com.initmed.frhivservice.frdatamodel.NrHivRecipe;
import com.initmed.frhivservice.frdatamodel.NrHivRecipeReleased;
import com.initmed.frhivservice.frdatamodel.NrHivRegistryAddition;
import com.initmed.frhivservice.frdatamodel.NrHivRegistryAddition.Deseases;
import com.initmed.frhivservice.frdatamodel.NrHivRegistryAvrt;
import com.initmed.frhivservice.frdatamodel.NrHivRegistryAvrtDrug;
import com.initmed.frhivservice.frdatamodel.NrHivRegistryExam;
import com.initmed.frhivservice.frdatamodel.NrHivRegistryExamResult;
import com.initmed.frhivservice.frdatamodel.NrPatient;
import com.initmed.frhivservice.frdatamodel.NrPatientDocument;
import com.initmed.frhivservice.frdatamodel.NrPatientRelative;
import com.initmed.frhivservice.frdatamodel.ObjectFactory;
import com.initmed.frhivservice.frdatamodel.PatientDocumentKey;
import com.initmed.frhivservice.frdatamodel.PatientRelativeKey;
import com.initmed.frhivservice.frdatamodel.PatientRelatives;
import com.initmed.frhivservice.frdatamodel.RefNsiCategory;
import com.initmed.frhivservice.frdatamodel.RefNsiDocument;
import com.initmed.frhivservice.frdatamodel.RefNsiHivAddressType;
import com.initmed.frhivservice.frdatamodel.RefNsiHivAvrtEndReason;
import com.initmed.frhivservice.frdatamodel.RefNsiHivBiomaterial;
import com.initmed.frhivservice.frdatamodel.RefNsiHivCheckup;
import com.initmed.frhivservice.frdatamodel.RefNsiHivChemoCourse;
import com.initmed.frhivservice.frdatamodel.RefNsiHivContingent;
import com.initmed.frhivservice.frdatamodel.RefNsiHivDeathReason;
import com.initmed.frhivservice.frdatamodel.RefNsiHivDetectCurc;
import com.initmed.frhivservice.frdatamodel.RefNsiHivDosage;
import com.initmed.frhivservice.frdatamodel.RefNsiHivDrug;
import com.initmed.frhivservice.frdatamodel.RefNsiHivDrugForm;
import com.initmed.frhivservice.frdatamodel.RefNsiHivExam;
import com.initmed.frhivservice.frdatamodel.RefNsiHivExamMark;
import com.initmed.frhivservice.frdatamodel.RefNsiHivExcludeReason;
import com.initmed.frhivservice.frdatamodel.RefNsiHivInfectionPath;
import com.initmed.frhivservice.frdatamodel.RefNsiHivMkb;
import com.initmed.frhivservice.frdatamodel.RefNsiHivMnn;
import com.initmed.frhivservice.frdatamodel.RefNsiHivOutReason;
import com.initmed.frhivservice.frdatamodel.RefNsiHivStage;
import com.initmed.frhivservice.frdatamodel.RefNsiHivVisitType;
import com.initmed.frhivservice.frdatamodel.RefNsiOksm;
import com.initmed.frhivservice.frdatamodel.RefRegion;
import com.initmed.frhivservice.frdatamodel.RegistryKey;
import com.initmed.frhivservice.frdatamodel.UnIdentifiedKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcard;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardCheckupKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardChemoKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardConcDeseaseKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardDeseaseKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardExamKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardHospKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardStageKey;
import com.initmed.frhivservice.frdatamodel.UpdateDispcardVisitKey;
import com.initmed.frhivservice.frdatamodel.UpdateHivAddress;
import com.initmed.frhivservice.frdatamodel.UpdateHivAvrt;
import com.initmed.frhivservice.frdatamodel.UpdateHivEpid;
import com.initmed.frhivservice.frdatamodel.UpdateHivExam;
import com.initmed.frhivservice.frdatamodel.UpdateHivRegistry;
import com.initmed.frhivservice.frdatamodel.UpdatePatient;
import com.initmed.frhivservice.frdatamodel.UpdatePatientDocument;
import com.initmed.frhivservice.frdatamodel.UpdatePatientRelative;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author User
 */
@Stateless
public class Converter {

    @EJB
    private DefaultparamsFacade defaultparamsFacade;
    @EJB
    private ObjectFactory of;

    public Converter() {
    }

    public NrPatient convertPatient(Patient p) throws DatatypeConfigurationException {
        NrPatient np = of.createNrPatient();
        if (p.getSnils() != null) {
            np.setSnils(p.getSnils().replaceAll("-", "").replaceAll(" ", ""));
        }
        np.setLastName(p.getLastname());
        np.setFirstName(p.getFirstname());
        np.setPatronymic(p.getPatronymic());
        np.setGender(p.getGender());
        np.setBirthDate(createGregorianCalendar(p.getBirthdate()));
        RefNsiOksm rnok = of.createRefNsiOksm();
        rnok.setId(p.getRefcitizenshipid().getId().shortValue());
        np.setCitizenShipId(rnok);
//        np.setDocuments(loadDocuments(p));
        return np;
    }

    public CreatePatientDocuments convertCreatePatientDocument(Documents d) throws DatatypeConfigurationException, EmptyResultExcepton {
        CreatePatientDocuments pd = of.createCreatePatientDocuments();
        pd.setDocuments(loadDocuments(d));
        pd.setKey(convertBasePatientKey(d.getRefpatient()));
        return pd;
    }

    public UpdatePatientDocument convertUpdatePatientDocument(Documents doc) throws DatatypeConfigurationException, EmptyResultExcepton {
        UpdatePatientDocument upd = of.createUpdatePatientDocument();
        upd.setDocument(loadDocuments(doc).getDocument().get(0));
        //create patient document key
        PatientDocumentKey pdk = of.createPatientDocumentKey();
        pdk.setNumber(upd.getDocument().getNumber().replaceAll("\\s", ""));
        pdk.setSerial(upd.getDocument().getSerial().replaceAll("\\s", ""));
        pdk.setDocumentId(upd.getDocument().getDocumentId());
        pdk.setSnils(doc.getRefpatient().getSnils());

        BasePatientKey bk = convertBasePatientKey(doc.getRefpatient());
        if (bk.getSnils() != null) {
            pdk.setSnils(bk.getSnils());
        } else {
            pdk.setUnidentified(bk.getUnidentified());
        }
        upd.setDocumentKey(pdk);
        return upd;
    }

    public PatientDocumentKey convertReadPatientDocument(Documents doc) throws DatatypeConfigurationException, EmptyResultExcepton {
        NrPatientDocument d = loadDocuments(doc).getDocument().get(0);
        PatientDocumentKey pdk = of.createPatientDocumentKey();
        pdk.setNumber(d.getNumber());
        pdk.setSerial(d.getSerial());
        pdk.setDocumentId(d.getDocumentId());
        BasePatientKey bk = convertBasePatientKey(doc.getRefpatient());
        if (bk.getSnils() != null) {
            pdk.setSnils(bk.getSnils());
        } else {
            pdk.setUnidentified(bk.getUnidentified());
        }
        return pdk;
    }

    public CreatePatientRelatives convertCreatePatientRelative(Relative r) throws DatatypeConfigurationException, EmptyResultExcepton {
        CreatePatientRelatives pd = of.createCreatePatientRelatives();
        pd.setKey(convertBasePatientKey(r.getRefpatient()));
        pd.setRelatives(loadRelatives(r));
        return pd;
    }

    public UpdatePatientRelative convertUpdatePatientRelative(Relative r) throws DatatypeConfigurationException, EmptyResultExcepton {
        UpdatePatientRelative pd = of.createUpdatePatientRelative();
        pd.setRelative(loadRelatives(r).getRelative().get(0));
        //create patient relative key
        PatientRelativeKey prk = of.createPatientRelativeKey();
        prk.setFirstName(pd.getRelative().getFirstName());
        prk.setLastName(pd.getRelative().getLastName());
        prk.setPatronymic(pd.getRelative().getPatronymic());
        BasePatientKey bk = convertBasePatientKey(r.getRefpatient());
        if (bk.getSnils() != null) {
            prk.setSnils(bk.getSnils());
        } else {
            prk.setUnidentified(bk.getUnidentified());
        }
        pd.setRelativeKey(prk);
        return pd;
    }

    public PatientRelativeKey convertReadPatientRelative(Relative r) throws DatatypeConfigurationException, EmptyResultExcepton {
        NrPatientRelative d = loadRelatives(r).getRelative().get(0);
        PatientRelativeKey prk = of.createPatientRelativeKey();
        prk.setFirstName(d.getFirstName());
        prk.setLastName(d.getLastName());
        prk.setPatronymic(d.getPatronymic());
        BasePatientKey bk = convertBasePatientKey(r.getRefpatient());
        if (bk.getSnils() != null) {
            prk.setSnils(bk.getSnils());
        } else {
            prk.setUnidentified(bk.getUnidentified());
        }
        return prk;
    }

    public PatientRelatives loadRelatives(Relative r) throws EmptyResultExcepton {
        PatientRelatives pr = of.createPatientRelatives();
        if (r != null) {
            List<NrPatientRelative> relatives = pr.getRelative();
            NrPatientRelative nrp = of.createNrPatientRelative();
            nrp.setFirstName(r.getFirstname());
            nrp.setLastName(r.getLastname());
            nrp.setPatronymic(r.getPatronymic());
            nrp.setAddress(loadAdressForPat(r.getRefpatient().getHivaddressSet().iterator().next()));
            relatives.add(nrp);
        }
        return pr;
    }

    public CreateHivEpid convertCreateEpidData(Hivepid he) throws DatatypeConfigurationException {
        CreateHivEpid createHivEpid = of.createCreateHivEpid();
        createHivEpid.setHivRegistryKey(convertHivRegistryKey(he.getRefpatient()));
        createHivEpid.setNrHivEpidReference(convertEpidData(he));
        return createHivEpid;
    }

    public UpdateHivEpid convertUpdateEpidData(Hivepid he) throws DatatypeConfigurationException {
        UpdateHivEpid createHivEpid = of.createUpdateHivEpid();
        createHivEpid.setHivRegistryKey(convertHivRegistryKey(he.getRefpatient()));
        createHivEpid.setNrHivEpidReference(convertEpidData(he));
        return createHivEpid;
    }

    public NrHivEpidReference convertEpidData(Hivepid he) throws DatatypeConfigurationException {
        NrHivEpidReference data = of.createNrHivEpidReference();
        data.setPretestDate(createGregorianCalendar(he.getPretestdate()));
        data.setPosttestDate(createGregorianCalendar(he.getPosttestdate()));
        data.setMoId(he.getMoId());
        data.setPersonId(he.getPersonid());
        data.setInfectionPeriodFrom(createGregorianCalendar(he.getInfectionperiodfrom()));
        data.setInfectionPeriodTo(createGregorianCalendar(he.getInfectionperiodto()));
        data.setConclustionDate(createGregorianCalendar(he.getConclustiondate()));
        data.setNotifyDate(createGregorianCalendar(he.getNotifydate()));
        if (he.getHivepidContactses() != null && !he.getHivepidContactses().isEmpty()) {
            List<String> contactsUnrz = he.getHivepidContactses().stream().map(HivepidContacts::getUnrz).collect(Collectors.toList());
            NrHivEpidReference.Contacts contacts = of.createNrHivEpidReferenceContacts();
            contacts.getContact().addAll(contactsUnrz);
            data.setContacts(contacts);
        }
        RefNsiHivDetectCurc detect = of.createRefNsiHivDetectCurc();
        detect.setId(he.getDetectcurcid().shortValue());
        data.setDetectCurcId(detect);

        RefNsiHivInfectionPath infPath = of.createRefNsiHivInfectionPath();
        infPath.setId(he.getInfpath().shortValue());
        data.setInfectionPathId(infPath);

        return data;
    }

    public UpdateHivAvrt convertUpdateHivAVRT(Hivavrt dbhivavrt) throws DatatypeConfigurationException {
        UpdateHivAvrt uha = of.createUpdateHivAvrt();
        NrHivRegistryAvrt h = createAVRTData(dbhivavrt);
        uha.setHivAvrt(h);
        uha.setHivAvrtKey(readHivAvrt(dbhivavrt));
        return uha;
    }

    public UpdateHivAvrt convertUpdateHivAVRT(NrHivRegistryAvrt hivavrt, Hivavrt dbhivavrt) throws DatatypeConfigurationException {
        UpdateHivAvrt uha = of.createUpdateHivAvrt();
        uha.setHivAvrt(hivavrt);
        if (hivavrt.getEndDate() == null || hivavrt.getBeginDate().toGregorianCalendar().getTime().compareTo(dbhivavrt.getBegindate()) > 0) {
            hivavrt.setEndDate(createGregorianCalendar(dbhivavrt.getBegindate()));
        } else {
            hivavrt.setEndDate(createGregorianCalendar(dbhivavrt.getEnddate()));
        }
        hivavrt.setEndPersonId(hivavrt.getBeginPersonId());
        RefNsiHivAvrtEndReason endReasonId = of.createRefNsiHivAvrtEndReason();
        endReasonId.setId((short) 8);
        hivavrt.setEndReasonId(endReasonId);
        hivavrt.setEndReasonDetail("обновление терапии");

        HivAvrtKey hak = of.createHivAvrtKey();
        hak.setBeginDate(hivavrt.getBeginDate());
        hak.setBeginPersonId(hivavrt.getBeginPersonId());
        hak.setRegistryNumber(dbhivavrt.getPatient().getRegistrynumber());
        uha.setHivAvrtKey(hak);
        return uha;
    }

    public HivAvrtKey readHivAvrt(Hivavrt dbhivavrt) throws DatatypeConfigurationException {
        HivAvrtKey hak = of.createHivAvrtKey();
        hak.setBeginDate(createGregorianCalendar(dbhivavrt.getBegindate()));
        hak.setBeginPersonId(dbhivavrt.getBeginpersonid());
        hak.setRegistryNumber(dbhivavrt.getPatient().getRegistrynumber());
        return hak;
    }

    public CreateDispcardDeseaseKey convertCreateHivDispcardDesease(Hivdispdeseases hd) throws DatatypeConfigurationException {
        CreateDispcardDeseaseKey dk = of.createCreateDispcardDeseaseKey();
        dk.setDispcardKey(convertDispCardKey(hd.getRefhivdisp()));
        dk.setDesease(convertDispCardDeseaseData(hd));
        return dk;
    }

    public DispcardDeseaseKey convertReadHivDispcardDesease(Hivdispdeseases hd) throws DatatypeConfigurationException {
        DispcardDeseaseKey dispcardDeseaseKey = of.createDispcardDeseaseKey();
        dispcardDeseaseKey.setCardNumber(hd.getRefhivdisp().getCardnumber());
        dispcardDeseaseKey.setRegistryNumber(hd.getRefhivdisp().getRefpatient().getRegistrynumber());
        dispcardDeseaseKey.setDeseaseDate(createGregorianCalendar(hd.getDeseasedate()));
        return dispcardDeseaseKey;
    }

    public UpdateDispcardDeseaseKey convertUpdateHivDispcardDesease(Hivdispdeseases hd) throws DatatypeConfigurationException {
        UpdateDispcardDeseaseKey dk = of.createUpdateDispcardDeseaseKey();
        DispcardDeseaseKey dispcardDeseaseKey = of.createDispcardDeseaseKey();
        dispcardDeseaseKey.setCardNumber(hd.getRefhivdisp().getCardnumber());
        dispcardDeseaseKey.setRegistryNumber(hd.getRefhivdisp().getRefpatient().getRegistrynumber());
        dispcardDeseaseKey.setDeseaseDate(createGregorianCalendar(hd.getDeseasedate()));
        dk.setDispcardDeseaseKey(dispcardDeseaseKey);
        dk.setDesease(convertDispCardDeseaseData(hd));
        return dk;
    }

    public NrHivDispcardDesease convertDispCardDeseaseData(Hivdispdeseases hd) throws DatatypeConfigurationException {
        NrHivDispcardDesease nrdd = of.createNrHivDispcardDesease();
        nrdd.setDesease(hd.getDesease());
        nrdd.setDeseaseDate(createGregorianCalendar(hd.getDeseasedate()));
        nrdd.setPersonId(hd.getPersonid());
        return nrdd;
    }

    public CreateHivRecipe convertCreateHivRecipe(Hivrecipe hr) throws Exception {
        NrHivRecipe nhr = of.createNrHivRecipe();
        nhr.setIsTheraphyResistence(false);
        //createRecipe desease
        RefNsiHivMkb mkb = of.createRefNsiHivMkb();
        mkb.setId(hr.getDeseaseid());
        nhr.setDeseaseId(mkb);
        //set recipe date
        nhr.setIssueDate(createGregorianCalendar(hr.getIssuedate()));
        nhr.setMoId(hr.getMoid());
        nhr.setPersonId(hr.getPersonid());
        nhr.setRecipeSerial(hr.getRecipeserial());
        nhr.setRecipeNumber(hr.getRecipenumber());
        //drugData        
        //mnn
        RefNsiHivMnn mnn = of.createRefNsiHivMnn();
        mnn.setId(hr.getMnnid());
        nhr.setMnnId(mnn);
        //mnn dosage
        RefNsiHivDosage refNsiHivDosage = of.createRefNsiHivDosage();
        refNsiHivDosage.setId(hr.getDosageid().shortValue());
        nhr.setDosageId(refNsiHivDosage);
        //drug form
        RefNsiHivDrugForm drugForm = of.createRefNsiHivDrugForm();
        drugForm.setId(hr.getDrugformid().shortValue());
        nhr.setDrugFormId(drugForm);
        //set dose count
        nhr.setDoseCount(BigDecimal.valueOf(hr.getDosecount().longValue()));
        nhr.setDailyCount(new BigDecimal(hr.getDailycount()));
        nhr.setDayCount(hr.getDaycount().shortValue());
        //data about issue
        nhr.setDeliveryDate(createGregorianCalendar(hr.getDeliverydate()));
        nhr.setPharmacyId(hr.getPharmacyid());

        //release information
        NrHivRecipe.HivRecipesReleased releases = of.createNrHivRecipeHivRecipesReleased();
        nhr.setHivRecipesReleased(releases);
        NrHivRecipeReleased release = of.createNrHivRecipeReleased();
        releases.getHivRecipeReleased().add(release);

        //drug market name
        RefNsiHivDrug nsiHivDrug = of.createRefNsiHivDrug();
        nsiHivDrug.setId(hr.getDrugid().shortValue());
        release.setDrugId(nsiHivDrug);
        //drug market dosage
        RefNsiHivDosage refNsiHivDosageMarket = of.createRefNsiHivDosage();
        refNsiHivDosageMarket.setId(hr.getDrugdosageid().shortValue());
        release.setDrugDosageId(refNsiHivDosageMarket);
        //dose in pack        
        release.setDoseInPack(hr.getDoseinpack());
        //pack count
        release.setPackCount(hr.getPackcount().shortValue());
        //
        CreateHivRecipe createHivRecipe = of.createCreateHivRecipe();
        createHivRecipe.setHivRecipe(nhr);
        //create reg key
        createHivRecipe.setRegistryNumber(hr.getRefpatient().getRegistrynumber());
        //

        return createHivRecipe;
    }

    public NrPatient.Documents loadDocuments(Documents d) throws DatatypeConfigurationException, EmptyResultExcepton {
        NrPatient.Documents nr = of.createNrPatientDocuments();
        if (d != null) {
            List<NrPatientDocument> docs = nr.getDocument();
            NrPatientDocument npd = of.createNrPatientDocument();
            npd.setSerial(d.getSerial().replaceAll("\\s", ""));
            npd.setNumber(d.getNum().replaceAll("\\s", ""));
            npd.setPassDate(createGregorianCalendar(d.getPassdate()));
            npd.setPassOrg(d.getPassorg());
            RefNsiDocument rnd = of.createRefNsiDocument();
            rnd.setId((short) 14);
            npd.setDocumentId(rnd);
            docs.add(npd);
        }
        return nr;
    }

    public CreateHivRegistry convertCreateHivRegistry(Hiv h) throws DatatypeConfigurationException, EmptyResultExcepton {
        CreateHivRegistry hr = of.createCreateHivRegistry();
        hr.setMoId(h.getMoid());
        hr.setIncludeDate(createGregorianCalendar(h.getIncludedate()));
        hr.setKey(convertBasePatientKey(h.getRefpatient()));
        hr.setRegistry(loadHivRegistry(h));
        return hr;
    }

    public BaseRegistryKey convertRegistryKey(Patient p) throws DatatypeConfigurationException {
        BaseRegistryKey rk = of.createBaseRegistryKey();
        if (p.getSnils() == null) {
            UnIdentifiedKey uk = of.createUnIdentifiedKey();
            uk.setBirthDate(createGregorianCalendar(p.getBirthdate()));
            uk.setFirstName(p.getFirstname());
            uk.setLastName(p.getLastname());
            uk.setPatronymic(p.getPatronymic());
            rk.setUnidentified(uk);
        } else {
            rk.setSnils(p.getSnils().replaceAll("-", "").replaceAll(" ", ""));
        }
        return rk;
    }

    public RegistryKey convertHivRegistryKey(Patient p) {
        RegistryKey rk = of.createRegistryKey();
        rk.setRegistryNumber(p.getRegistrynumber());
        return rk;
    }

    public RegistryKey convertDispCard(Patient p) {
        RegistryKey rk = of.createRegistryKey();
        rk.setRegistryNumber(p.getRegistrynumber());

        return rk;
    }

    public JAXBElement<Object> convertHivRegistryNumber(Patient p) {
        return of.createRegistryNumber(p.getRegistrynumber());
    }

    public UpdateHivRegistry convertUpdateHivRegistry(Hiv h) throws DatatypeConfigurationException, EmptyResultExcepton {
        UpdateHivRegistry hr = of.createUpdateHivRegistry();
        hr.setIncludeDate(createGregorianCalendar(h.getIncludedate()));
        hr.setRegistry(loadHivRegistry(h));
        if (h.getExcludedate() != null) {
            hr.setExcludeDate(createGregorianCalendar(h.getExcludedate()));
        }
        hr.getRegistry().setRegistryNumber(h.getRefpatient().getRegistrynumber());
        return hr;
    }

    public CreateHivAddress convertCreateHivAddress(Hivaddress ha, Patient pat) throws DatatypeConfigurationException {
        CreateHivAddress hr = of.createCreateHivAddress();
        hr.setRegistryNumber(ha.getRefpatient().getRegistrynumber());
        NrHivPatientAddress nrHad = of.createNrHivPatientAddress();
        nrHad.setBeginDate(createGregorianCalendar(pat.getHivSet().iterator().next().getIncludedate()));
        nrHad.setPhone(pat.getPhone());
        //create address type
        RefNsiHivAddressType addrType = of.createRefNsiHivAddressType();
        addrType.setId((short) 1);
        nrHad.setAddressTypeId(addrType);
        //create address
        NrAddress na = of.createNrAddress();
        na.setAoidArea(ha.getAoidarea());
        na.setAreaName(ha.getAreaname());
        na.setPrefixArea(ha.getPrefixarea());
        na.setStreetName(ha.getStreetname());
        na.setHouse(ha.getStreetname());
        na.setFlat(ha.getFlat());
        //create region id
        RefRegion r = of.createRefRegion();
        r.setId(Short.parseShort(ha.getRegion()));
        na.setRegion(r);
        na.setAoidStreet(ha.getAoidstreet());
        //set address
        nrHad.setAddress(na);
        //set addr to crate object
        hr.setHivAddress(nrHad);
        return hr;
    }

    public HivAddressKey convertHivAddressKey(Hivaddress p) {
        HivAddressKey ha = of.createHivAddressKey();
        ha.setRegistryNumber(p.getRefpatient().getRegistrynumber());
        RefNsiHivAddressType addrType = of.createRefNsiHivAddressType();
        addrType.setId((short) 1);
        ha.setAddressTypeId(addrType);
        return ha;
    }

    public UpdateHivAddress convertUpdateHivAddressKey(Hivaddress haddr) {
        UpdateHivAddress ha = of.createUpdateHivAddress();
        ha.setKey(convertHivAddressKey(haddr));
        NrHivPatientAddress npa = of.createNrHivPatientAddress();
        ha.setHivAddress(npa);
        npa.setAddress(loadAdressForPat(haddr));
        RefNsiHivAddressType addrType = of.createRefNsiHivAddressType();
        addrType.setId((short) 1);
        npa.setAddressTypeId(addrType);
        return ha;
    }

    public CreateDispcard convertCreateHivDispCard(Hivdisp h) throws DatatypeConfigurationException {
        CreateDispcard cdc = of.createCreateDispcard();
        //set unrz             
        cdc.setRegistryNumberKey(h.getRefpatient().getRegistrynumber());
        //create dc
        NrHivLightDispcard dc = of.createNrHivLightDispcard();
        cdc.setDispcard(dc);
        dc.setMoId(h.getMoid());
        dc.setInDate(createGregorianCalendar(h.getIndate()));
        dc.setPersonId(h.getPersonid());
        //create desease        
        dc.setDeseases(createDesease(h));
        return cdc;
    }

    public UpdateDispcard convertUpdateHivDispCard(Hivdisp h) throws DatatypeConfigurationException {
        UpdateDispcard cdc = of.createUpdateDispcard();
        DispcardKey dk = of.createDispcardKey();
        dk.setRegistryNumber(h.getRefpatient().getRegistrynumber());
        dk.setCardNumber(h.getCardnumber());
        cdc.setDispcardKey(dk);
        //create dc
        NrHivLightDispcard dc = of.createNrHivLightDispcard();
        cdc.setDispcard(dc);
        dc.setMoId(h.getMoid());
        dc.setInDate(createGregorianCalendar(h.getIndate()));
        dc.setPersonId(h.getPersonid());
        //create desease        
        dc.setDeseases(createDesease(h));
        if (h.getOutdate() != null) {
            dc.setOutDate(createGregorianCalendar(h.getOutdate()));
            RefNsiHivOutReason r = of.createRefNsiHivOutReason();
            r.setId(Short.parseShort(h.getOutreasonid()));
            dc.setOutReasonId(r);
            if (h.getDeathdate() != null) {
                dc.setDeathDate(createGregorianCalendar(h.getDeathdate()));
                RefNsiHivDeathReason dr = of.createRefNsiHivDeathReason();
                dr.setId(Short.parseShort(h.getDeathreason()));
                dc.setDeathReasonId(dr);
                dc.setDeathDesease(h.getDeathdesease());
            }
        }
        return cdc;
    }

    public NrHivLightDispcard.Deseases createDesease(Hivdisp hd) throws DatatypeConfigurationException {
        Hivdispdeseases baseDes = hd.getHivdispdeseasesSet().iterator().next();
        NrHivLightDispcard.Deseases des = of.createNrHivLightDispcardDeseases();
        List<NrHivDispcardDesease> desList = des.getDesease();
        NrHivDispcardDesease ndcd = of.createNrHivDispcardDesease();
        ndcd.setDesease(baseDes.getDesease());
        ndcd.setPersonId(baseDes.getPersonid().toString());
        ndcd.setDeseaseDate(createGregorianCalendar(baseDes.getDeseasedate()));
        desList.add(ndcd);
        return des;
    }

    public CreateDispcardHospKey convertCreateHivDipsHosp(Hivdisphosp h) throws DatatypeConfigurationException {
        Patient p = h.getHivdisp().getRefpatient();
        CreateDispcardHospKey hospKey = of.createCreateDispcardHospKey();
        hospKey.setDispcardKey(convertDispCardKey(h.getHivdisp()));
        hospKey.setHospitalization(convertHospData(h));
        return hospKey;
    }

    public UpdateDispcardHospKey convertUpdateHivDipsHosp(Hivdisphosp h) throws DatatypeConfigurationException {
        UpdateDispcardHospKey hospKey = of.createUpdateDispcardHospKey();
        hospKey.setDispcardHospKey(convertDispcardHospKey(h));
        hospKey.setHospitalization(convertHospData(h));
        return hospKey;
    }

    public DispcardHospKey convertDispcardHospKey(Hivdisphosp h) throws DatatypeConfigurationException {
        DispcardHospKey dh = of.createDispcardHospKey();
        dh.setBeginDate(createGregorianCalendar(h.getBegindate()));
        dh.setCardNumber(h.getHivdisp().getCardnumber());
        dh.setRegistryNumber(h.getHivdisp().getRefpatient().getRegistrynumber());
        return dh;
    }

    public NrHivDispcardHospitalization convertHospData(Hivdisphosp h) throws DatatypeConfigurationException {
        NrHivDispcardHospitalization nr = of.createNrHivDispcardHospitalization();
        nr.setMoOid(h.getMooid());
        nr.setBeginDate(createGregorianCalendar(h.getBegindate()));
        nr.setEndDate(createGregorianCalendar(h.getEnddate()));
        nr.setFinalDesease(h.getFinalmkb());
        return nr;
    }

    public NrHivRegistryAddition loadHivRegistry(Hiv h) throws DatatypeConfigurationException, EmptyResultExcepton {
        NrHivRegistryAddition addition = of.createNrHivRegistryAddition();
        addition.setEpidCode(h.getEpidcode());
        addition.setNewborn(h.getNewborn() == 2);
        addition.setApprovedDate(createGregorianCalendar(h.getApproveddate()));
        // craete deseases
        Deseases dsList = of.createNrHivRegistryAdditionDeseases();
        for (Desease x : h.getDeseaseSet()) {
            dsList.getDesease().add(x.getName());
        }
        addition.setDeseases(dsList);
        // craete patalogies
        NrHivRegistryAddition.RevealedPathologies patList = of.createNrHivRegistryAdditionRevealedPathologies();
        for (Revealedpathologies x : h.getRevealedpathologiesSet()) {
            RefNsiHivMkb mkb = of.createRefNsiHivMkb();
            mkb.setId(x.getMkbid());
            patList.getRevealedPathology().add(mkb);
        }
        addition.setRevealedPathologies(patList);

        // craete contingents
        NrHivRegistryAddition.Contingents cList = of.createNrHivRegistryAdditionContingents();
        for (Contingents x : h.getContingentsSet()) {
            RefNsiHivContingent cont = of.createRefNsiHivContingent();
            cont.setId(Short.parseShort(x.getName()));
            cList.getContingent().add(cont);
        }
        addition.setContingents(cList);
// blot
        NrHivRegistryAddition.Exams nre = of.createNrHivRegistryAdditionExams();
        addition.setExams(nre);

        NrHivRegistryExam nr = of.createNrHivRegistryExam();
        nre.getExam().add(nr);

        nr.setExamDate(createGregorianCalendar(h.getApproveddate()));
        nr.setMoOid(h.getMoid());
        nr.setSampleDate(createGregorianCalendar(h.getApproveddate()));

        RefNsiHivExam typeId = of.createRefNsiHivExam();
        typeId.setId((short) 2);
        nr.setExamTypeId(typeId);

        RefNsiHivBiomaterial sampleId = of.createRefNsiHivBiomaterial();
        sampleId.setId((short) 1);
        nr.setSampleId(sampleId);

//      create result
        NrHivRegistryExam.ExamResults examResults = of.createNrHivRegistryExamExamResults();
//
        NrHivRegistryExamResult result = of.createNrHivRegistryExamResult();
        nr.setExamResults(examResults);
        examResults.getExamResult().add(result);
        result.setQualitativeResult((short) 1);
////        ex mark
//        RefNsiHivExamMark exMarkObject = of.createRefNsiHivExamMark();
//        exMarkObject.setId((short) 1);
//        result.setMarkId(exMarkObject);

        if (h.getRefexcludereasonid() != null) {
            RefNsiHivExcludeReason excReason = of.createRefNsiHivExcludeReason();
            excReason.setId(h.getRefexcludereasonid().getId().shortValue());
            addition.setExcludeReasonId(excReason);
            if (h.getRefexcludereasonid().getId() == 1) {
                RefNsiHivDeathReason dr = of.createRefNsiHivDeathReason();
                dr.setId(h.getRefdeathreason().getId().shortValue());
                addition.setDeathReasonId(dr);
            }
        }

        //category
        RefNsiCategory rNsiCat = of.createRefNsiCategory();
        rNsiCat.setId(h.getCategoryid().shortValue());
//        addition.setC(rNsiCat);

        return addition;
    }

    public CreateHivAvrt convertHivAVRT(Hivavrt h) throws DatatypeConfigurationException {
        CreateHivAvrt ch = of.createCreateHivAvrt();
//        create hiv avrt key               
        ch.setRegistryNumber(h.getPatient().getRegistrynumber());
        ch.setHivAvrt(createAVRTData(h));
        return ch;
    }

    public NrHivRegistryAvrt createAVRTData(Hivavrt h) throws DatatypeConfigurationException {
        NrHivRegistryAvrt nHivAvrt = of.createNrHivRegistryAvrt();
        nHivAvrt.setBeginDate(createGregorianCalendar(h.getBegindate()));
        nHivAvrt.setBeginPersonId(h.getBeginpersonid());
        nHivAvrt.setIsFirstTreatment(h.getIsfirsttreatment() == 1);
        nHivAvrt.setIsNeedChangeTreatment(h.getIsneedchangetreatment() == 1);
        nHivAvrt.setHepatite(false);
        NrHivRegistryAvrt.Drugs drugs = of.createNrHivRegistryAvrtDrugs();
        nHivAvrt.setDrugs(drugs);
        List<NrHivRegistryAvrtDrug> drugList = drugs.getDrug();
        Iterator<Hivavrtdrugs> drugsIterator = h.getHivavrtdrugsSet().iterator();

        if (h.getEnddate() != null) {
            nHivAvrt.setEndDate(createGregorianCalendar(h.getEnddate()));
            nHivAvrt.setEndPersonId(h.getEndpersonid());

            RefNsiHivAvrtEndReason endReason = of.createRefNsiHivAvrtEndReason();
            endReason.setId(h.getEndreasonid().shortValue());
            nHivAvrt.setEndReasonId(endReason);
            nHivAvrt.setEndReasonDetail(h.getEndreasondetail());
        }
        //      set end reasosns        
//        create drugs

        while (drugsIterator.hasNext()) {
            Hivavrtdrugs d = drugsIterator.next();
            NrHivRegistryAvrtDrug nrDrugs = of.createNrHivRegistryAvrtDrug();
//            create mnn
            RefNsiHivMnn mnn = of.createRefNsiHivMnn();
            mnn.setId(d.getMnnid());
            nrDrugs.setMnnId(mnn);
//            create form
            RefNsiHivDrugForm form = of.createRefNsiHivDrugForm();
            form.setId(d.getDrugformid());
            nrDrugs.setDrugFormId(form);
//            create dosage
            RefNsiHivDosage dosage = of.createRefNsiHivDosage();
            dosage.setId(d.getDosageid().shortValue());
            nrDrugs.setDosageId(dosage);
//            create need day
            nrDrugs.setNeedDay(BigDecimal.valueOf(d.getNeedday().longValue()));
//            create need year
            nrDrugs.setNeedYear(BigDecimal.valueOf(d.getNeedyear().longValue()));
            drugList.add(nrDrugs);
        }

        return nHivAvrt;
    }

    public HivAvrtKey convertReadAVRT(Hivavrt h) throws DatatypeConfigurationException {
        HivAvrtKey ha = of.createHivAvrtKey();
        ha.setBeginDate(createGregorianCalendar(h.getBegindate()));
        ha.setBeginPersonId(h.getBeginpersonid());
        ha.setRegistryNumber(h.getPatient().getRegistrynumber());
        return ha;
    }

    public CreateHivExam convertHivExam(Hivdispexam hd) throws DatatypeConfigurationException {
        CreateHivExam che = of.createCreateHivExam();
        che.setRegistryNumber(hd.getRefpatient().getRegistrynumber());
        che.setExam(convertNrHivRegistryExam(hd));
        return che;
    }

    public NrHivRegistryExam convertNrHivRegistryExam(Hivdispexam hd) throws DatatypeConfigurationException {
        NrHivRegistryExam nr = of.createNrHivRegistryExam();
        nr.setExamDate(createGregorianCalendar(hd.getExamdate()));
        nr.setMoOid(hd.getMoid());
        nr.setSampleDate(createGregorianCalendar(hd.getSampledate()));
        nr.setTestSystemName(hd.getTestsystemname());
        nr.setTestSystemSerial(hd.getTestsystemserial());

        RefNsiHivExam typeId = of.createRefNsiHivExam();
        typeId.setId(hd.getRefexamid());
        nr.setExamTypeId(typeId);

        RefNsiHivBiomaterial sampleId = of.createRefNsiHivBiomaterial();
        sampleId.setId(hd.getRefsampleid().shortValue());
        nr.setSampleId(sampleId);
//      create result
        NrHivRegistryExam.ExamResults examResults = of.createNrHivRegistryExamExamResults();

        NrHivRegistryExamResult result = of.createNrHivRegistryExamResult();
        nr.setExamResults(examResults);
//        ex mark
        RefNsiHivExamMark exMarkObject = of.createRefNsiHivExamMark();
        short examMark = hd.getExamMark().shortValue();
        exMarkObject.setId(examMark);
        result.setMarkId(exMarkObject);
        result.setQualitativeResult(hd.getQualitativeresult());
        result.setQuantitativeResult(hd.getQuantitativeresult());
        examResults.getExamResult().add(result);
        return nr;
    }

    public UpdateHivExam convertUpdateHivExamKey(Hivdispexam hd) throws DatatypeConfigurationException {
        UpdateHivExam uhe = of.createUpdateHivExam();
        uhe.setHivExamKey(convertReadHivExamKey(hd));
        uhe.setExam(convertNrHivRegistryExam(hd));
        return uhe;
    }

    public HivExamKey convertReadHivExamKey(Hivdispexam hd) throws DatatypeConfigurationException {
        HivExamKey he = of.createHivExamKey();
        he.setRegistryNumber(hd.getRefpatient().getRegistrynumber());

        RefNsiHivExam typeId = of.createRefNsiHivExam();
        typeId.setId(hd.getRefexamid());
        he.setExamTypeId(typeId);
        he.setExamDate(createGregorianCalendar(hd.getExamdate()));
        return he;
    }

    public DispcardExamKey convertReadHivDispExamKey(Hivdispexam hd) throws DatatypeConfigurationException {
        DispcardExamKey he = of.createDispcardExamKey();
        he.setRegistryNumber(hd.getRefpatient().getRegistrynumber());
        he.setCardNumber(hd.getRefpatient().getHivdispsSet().iterator().next().getCardnumber());
        he.setExamDate(createGregorianCalendar(hd.getExamdate()));
        he.setExamTypeId(hd.getRefexamid());
        return he;
    }

    public UpdateDispcardExamKey convertUpdateDispcardExamKey(Hivdispexam hd) throws DatatypeConfigurationException {
        UpdateDispcardExamKey uhe = of.createUpdateDispcardExamKey();
        uhe.setDispcardExamKey(convertReadHivDispExamKey(hd));
        uhe.setExam(convertCreateHivDispcardExam(hd));
        return uhe;
    }

    public UpdateDispcardExamKey convertUpdateDispcardExamKey(Patient p, NrHivDispcardExam ex) throws DatatypeConfigurationException {
        UpdateDispcardExamKey uhe = of.createUpdateDispcardExamKey();
        DispcardExamKey he = of.createDispcardExamKey();
        he.setRegistryNumber(p.getRegistrynumber());
        he.setCardNumber(p.getHivdispsSet().iterator().next().getCardnumber());
        he.setExamDate(ex.getExamDate());
        he.setExamTypeId(ex.getExamTypeId().getId());
        uhe.setDispcardExamKey(he);
        ex.getExamTypeId().setId((short) 58);
        ex.getExamResults().getExamResult().get(0).setQuantitativeResult(200);

        uhe.setExam(ex);
        return uhe;
    }

    public CreateDispcardExamKey convertCreateHivDispExam(Hivdispexam h) throws DatatypeConfigurationException {
        CreateDispcardExamKey res = of.createCreateDispcardExamKey();
        //create dispacd key
        DispcardKey dk = of.createDispcardKey();
        dk.setRegistryNumber(h.getRefpatient().getRegistrynumber());
        dk.setCardNumber(h.getRefpatient().getHivdispsSet().iterator().next().getCardnumber());
        res.setDispcardKey(dk);
        //create result       
        res.setExam(convertCreateHivDispcardExam(h));
        return res;
    }

    public NrHivDispcardExam convertCreateHivDispcardExam(Hivdispexam h) throws DatatypeConfigurationException {
        NrHivDispcardExam nhr = of.createNrHivDispcardExam();
        nhr.setMoOid(h.getMoid());
        nhr.setExamDate(createGregorianCalendar(h.getExamdate()));
        //
        RefNsiHivExam typeId = of.createRefNsiHivExam();
        typeId.setId(h.getRefexamid());
        nhr.setExamTypeId(typeId);
        //create result
        NrHivDispcardExamResult exRes = of.createNrHivDispcardExamResult();
        //create mark exam id
        RefNsiHivExamMark exMarkObject = of.createRefNsiHivExamMark();
        short examMark = h.getExamMark().shortValue();
        exMarkObject.setId(examMark);
        exRes.setMarkId(exMarkObject);
        exRes.setQualitativeResult(h.getQualitativeresult());
        exRes.setQuantitativeResult(h.getQuantitativeresult());
        NrHivDispcardExam.ExamResults hr = of.createNrHivDispcardExamExamResults();
        hr.getExamResult().add(exRes);
        nhr.setExamResults(hr);
        //end create result
        nhr.setSampleDate(createGregorianCalendar(h.getSampledate()));
        RefNsiHivBiomaterial sampleId = of.createRefNsiHivBiomaterial();
        sampleId.setId(h.getRefsampleid().shortValue());
        nhr.setSampleId(sampleId);
        nhr.setTestSystemName(h.getTestsystemname());
        nhr.setTestSystemSerial(h.getTestsystemserial());
        return nhr;
    }

    public CreateDispcardVisitKey converCreateDispcardVisit(Hivdispvisits v) throws DatatypeConfigurationException {
        CreateDispcardVisitKey dispCardVisitKey = of.createCreateDispcardVisitKey();
//        create dispcard key
        dispCardVisitKey.setDispcardKey(convertDispCardKey(v.getRefpatient().getHivdispsSet().iterator().next()));
//        create visit
        dispCardVisitKey.setVisit(convertVisit(v));
        return dispCardVisitKey;
    }

    public UpdateDispcardVisitKey converUpdateDispcardVisit(Hivdispvisits v) throws DatatypeConfigurationException {
        UpdateDispcardVisitKey uk = of.createUpdateDispcardVisitKey();
        uk.setVisit(convertVisit(v));
        uk.setDispcardVisitKey(readDispCardVisit(v));
        return uk;
    }

    public DispcardVisitKey readDispCardVisit(Hivdispvisits v) throws DatatypeConfigurationException {
        DispcardVisitKey dk = of.createDispcardVisitKey();
        dk.setCardNumber(v.getRefpatient().getHivdispsSet().iterator().next().getCardnumber());
        dk.setPersonId(v.getPersonid());
        dk.setPlanDate(createGregorianCalendar(v.getPlandate()));
        dk.setRegistryNumber(v.getRefpatient().getRegistrynumber());
        dk.setTypeId(v.getTypeid().shortValue());
        return dk;
    }

    public NrHivDispVisit convertVisit(Hivdispvisits v) throws DatatypeConfigurationException {
        NrHivDispVisit visit = of.createNrHivDispVisit();
        visit.setFactDate(createGregorianCalendar(v.getFactdate()));
        visit.setPlanDate(createGregorianCalendar(v.getPlandate()));
        visit.setPersonId(v.getPersonid());
        RefNsiHivVisitType type = of.createRefNsiHivVisitType();
        type.setId(v.getTypeid().shortValue());
        visit.setTypeId(type);
        return visit;
    }

    public CreateDispcardChemoKey convertCreateHivDispChemo(Hivdispchemo h) throws DatatypeConfigurationException {
        CreateDispcardChemoKey dck = of.createCreateDispcardChemoKey();
        dck.setDispcardKey(convertDispCardKey(h.getHivdisp()));
        dck.setChemoCourse(convetNrHivDispcardChemoCourse(h));
        return dck;
    }

    public UpdateDispcardChemoKey convertUpdateHivDispChemo(Hivdispchemo h) throws DatatypeConfigurationException {
        UpdateDispcardChemoKey dck = of.createUpdateDispcardChemoKey();
        dck.setDispcardChemoKey(convertDispcardChemoKey(h));
        dck.setChemoCourse(convetNrHivDispcardChemoCourse(h));
        return dck;
    }

    public DispcardChemoKey convertDispcardChemoKey(Hivdispchemo h) throws DatatypeConfigurationException {
        DispcardChemoKey d = of.createDispcardChemoKey();
        d.setBeginDate(createGregorianCalendar(h.getBegindate()));
        d.setCardNumber(h.getHivdisp().getCardnumber());
        d.setRegistryNumber(h.getHivdisp().getRefpatient().getRegistrynumber());
        RefNsiHivChemoCourse courseId = of.createRefNsiHivChemoCourse();
        courseId.setId(h.getChemoid().shortValue());
        d.setChemoCourseId(h.getChemoid().shortValue());
        return d;
    }

    public NrHivDispcardChemoCourse convetNrHivDispcardChemoCourse(Hivdispchemo h) throws DatatypeConfigurationException {
        NrHivDispcardChemoCourse chemoCourse = of.createNrHivDispcardChemoCourse();
        chemoCourse.setBeginDate(createGregorianCalendar(h.getBegindate()));
        chemoCourse.setEndDate(createGregorianCalendar(h.getEnddate()));
        if (h.getNote() != null && h.getNote().length() > 3) {
            chemoCourse.setNote(h.getNote());
        }
        RefNsiHivChemoCourse courseId = of.createRefNsiHivChemoCourse();
        courseId.setId(h.getChemoid().shortValue());
        chemoCourse.setChemoCourseId(courseId);
        return chemoCourse;
    }

    public CreateDispcardCheckupKey convertCreateHivDispCheckup(Hivdispchekup h) throws DatatypeConfigurationException {
        CreateDispcardCheckupKey dck = of.createCreateDispcardCheckupKey();
        dck.setDispcardKey(convertDispCardKey(h.getHivdisp()));
        dck.setCheckup(convetNrHivDispcardCheckup(h));
        return dck;
    }

    public UpdateDispcardCheckupKey convertUpdateHivDispCheckup(Hivdispchekup h) throws DatatypeConfigurationException {
        UpdateDispcardCheckupKey dck = of.createUpdateDispcardCheckupKey();
        dck.setCheckup(convetNrHivDispcardCheckup(h));
        dck.setDispcardCheckupKey(convertDispcardCheckupKey(h));
        return dck;
    }

    public NrHivDispcardCheckup convetNrHivDispcardCheckup(Hivdispchekup h) throws DatatypeConfigurationException {
        NrHivDispcardCheckup chekcUp = of.createNrHivDispcardCheckup();
        chekcUp.setCheckupDate(createGregorianCalendar(h.getCheckupdate()));

        RefNsiHivCheckup checkup = of.createRefNsiHivCheckup();
        checkup.setId(h.getCheckupid().shortValue());
        chekcUp.setCheckupId(checkup);

        chekcUp.setDesease(h.getDesease());
        chekcUp.setResult(h.getResult().shortValue());
        return chekcUp;
    }

    public DispcardCheckupKey convertDispcardCheckupKey(Hivdispchekup h) throws DatatypeConfigurationException {
        DispcardCheckupKey d = of.createDispcardCheckupKey();
        d.setCheckupDate(createGregorianCalendar(h.getCheckupdate()));
        d.setCardNumber(h.getHivdisp().getCardnumber());
        d.setRegistryNumber(h.getHivdisp().getRefpatient().getRegistrynumber());
        d.setCheckupId(h.getCheckupid().shortValue());
        return d;
    }

    public CreateDispcardStageKey convertCreateHivDispStage(Hivdispstage h) throws DatatypeConfigurationException {
        CreateDispcardStageKey dck = of.createCreateDispcardStageKey();
        dck.setDispcardKey(convertDispCardKey(h.getHivdisp()));
        dck.setStage(convetNrHivDispcardStage(h));
        return dck;
    }

    public UpdateDispcardStageKey convertUpdateHivDispStage(Hivdispstage h) throws DatatypeConfigurationException {
        UpdateDispcardStageKey dck = of.createUpdateDispcardStageKey();
        dck.setDispcardStageKey(convertDispcardStageKey(h));
        dck.setStage(convetNrHivDispcardStage(h));
        return dck;
    }

    public DispcardStageKey convertDispcardStageKey(Hivdispstage h) throws DatatypeConfigurationException {
        DispcardStageKey d = of.createDispcardStageKey();
        d.setStageDate(createGregorianCalendar(h.getStagedate()));
        d.setCardNumber(h.getHivdisp().getCardnumber());
        d.setRegistryNumber(h.getHivdisp().getRefpatient().getRegistrynumber());
        return d;
    }

    public NrHivDispcardStage convetNrHivDispcardStage(Hivdispstage h) throws DatatypeConfigurationException {
        NrHivDispcardStage stage = of.createNrHivDispcardStage();
        stage.setStageDate(createGregorianCalendar(h.getStagedate()));
        RefNsiHivStage stageId = of.createRefNsiHivStage();
        stageId.setId(h.getStageid().shortValue());
        stage.setStageId(stageId);
        stage.setPersonId(h.getPersonid());

        return stage;
    }

    public CreateDispcardConcDeseaseKey convertCreateHivDispConc(Hivdispconc h) throws DatatypeConfigurationException {
        CreateDispcardConcDeseaseKey dck = of.createCreateDispcardConcDeseaseKey();
        dck.setDispcardKey(convertDispCardKey(h.getHivdisp()));
        dck.setConcDesease(convetNrHivDispcardConcDesease(h));
        return dck;
    }

    public UpdateDispcardConcDeseaseKey convertUpdateHivDispConc(Hivdispconc h) throws DatatypeConfigurationException {
        UpdateDispcardConcDeseaseKey dck = of.createUpdateDispcardConcDeseaseKey();
        dck.setConcDesease(convetNrHivDispcardConcDesease(h));
        dck.setDispcardConcDeseaseKey(convertDispcardConcKey(h));
        return dck;
    }

    public DispcardConcDeseaseKey convertDispcardConcKey(Hivdispconc h) throws DatatypeConfigurationException {
        DispcardConcDeseaseKey d = of.createDispcardConcDeseaseKey();
        d.setCardNumber(h.getHivdisp().getCardnumber());
        d.setRegistryNumber(h.getHivdisp().getRefpatient().getRegistrynumber());
        d.setBeginDate(createGregorianCalendar(h.getBegindate()));
        d.setDesease(h.getConcdesease());
        return d;
    }

    public NrHivDispcardConcDesease convetNrHivDispcardConcDesease(Hivdispconc h) throws DatatypeConfigurationException {
        NrHivDispcardConcDesease conc = of.createNrHivDispcardConcDesease();
        conc.setBeginDate(createGregorianCalendar(h.getBegindate()));
        conc.setDesease(h.getConcdesease());
        conc.setEndDate(createGregorianCalendar(h.getEnddate()));
        return conc;
    }

    public NrAddress loadAdressForPat(Hivaddress ha) {
        NrAddress na = of.createNrAddress();
        if (ha != null) {
            na.setAoidArea(ha.getAoidarea());
            na.setAreaName(ha.getAreaname());
            na.setPrefixArea(ha.getPrefixarea());
            na.setStreetName(ha.getStreetname());
            na.setHouse(ha.getStreetname());
            na.setFlat(ha.getFlat());
            //create region id
            RefRegion r = of.createRefRegion();
            r.setId(Short.parseShort(ha.getRegion()));
            na.setRegion(r);
            na.setAoidStreet(ha.getAoidstreet());
            //set address            
        }
        return na;
    }

    public XMLGregorianCalendar createGregorianCalendar(Date date) throws DatatypeConfigurationException {
        if (date != null) {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(new SimpleDateFormat("yyyy-MM-dd").format(date));
        }
        return null;
    }

    public BasePatientKey convertBasePatientKey(Patient p) throws DatatypeConfigurationException {
        BasePatientKey bp = of.createBasePatientKey();
        if (p.getSnils() == null) {
            UnIdentifiedKey kp = of.createUnIdentifiedKey();
            kp.setFirstName(p.getFirstname());
            kp.setLastName(p.getLastname());
            kp.setPatronymic(p.getPatronymic());
            kp.setBirthDate(createGregorianCalendar(p.getBirthdate()));
            bp.setUnidentified(kp);
        } else {
            bp.setSnils(p.getSnils().replaceAll("-", "").replaceAll(" ", ""));
        }
        return bp;
    }

    public DispcardKey convertDispCardKey(Hivdisp d) {
        DispcardKey dk = of.createDispcardKey();
        dk.setRegistryNumber(d.getRefpatient().getRegistrynumber());
        dk.setCardNumber(d.getCardnumber());
        return dk;
    }

    public UpdatePatient convertUpdatePatient(Patient p) throws DatatypeConfigurationException {
        UpdatePatient up = of.createUpdatePatient();
        BasePatientKey bp = convertBasePatientKey(p);
        UnIdentifiedKey kp = of.createUnIdentifiedKey();
        kp.setFirstName(p.getFirstname());
        kp.setLastName(p.getLastname());
        kp.setPatronymic(p.getPatronymic());
        kp.setBirthDate(createGregorianCalendar(p.getBirthdate()));
        up.setKey(bp);
//        bp.setUnidentified(kp);        
        up.setPatient(convertPatient(p));
        return up;
    }

}
