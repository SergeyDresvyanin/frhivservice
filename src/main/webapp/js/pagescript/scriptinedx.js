/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    setDates();
    checkBlock();
    loadStatistics($('#datestart').val(), $('#datefinish').val());
    setSpeed();
});


function setDates() {
    var now = new Date();
    var dayFrom = ("0" + now.getDate()).slice(-2);
    var monthFrom = ("0" + (now.getMonth() + 1)).slice(-2);
    var todayFrom = now.getFullYear() + "-" + (monthFrom) + "-" + (dayFrom);
    $('#datefinish').val(todayFrom);

    var newDate = new Date(new Date().setMonth(new Date().getMonth() - 6));
    var dayTo = ("0" + newDate.getDate()).slice(-2);
    var monthTo = ("0" + (newDate.getMonth() + 1)).slice(-2);
    var todayTo = newDate.getFullYear() + "-" + (monthTo) + "-" + (dayTo);
    $('#datestart').val(todayTo);
}

function loadStatistics(start, finish) {
    if (start > finish) {
        alert("Неправильный интервал дат");
    } else {
        $.ajax({
            url: 'api/stat/operationscount?datefrom=' + start + '&dateto=' + finish,
            type: 'get',
            async: true,
            beforeSend: function () {
                loadingStart();
            },
            success: function (data) {
                loadingEnd();
                var d = JSON.parse(data);
                $('#queue').empty();
                $('#errors').empty();
                for (var i = 0; i < d.length; i++) {
                    var item = d[i];
                    $("#queue").append("<div><h4 class='small font-weight-bold'>" + item.label + "<span class='text-left float-right' id='dispexams'>" + Math.round((item.total - item.queue) / item.total * 100) + "%</span></h4><h4 class='small font-weight-bold'><span class='text-left float-right'>" + item.total + "</span>" + item.queue + "<span class='text-left float-right small font-weight-bold'>Всего:&nbsp;</span></h4><div class='progress mb-4'><div class='progress-bar bg-primary' aria-valuenow='" + Math.round((item.total - item.queue) / item.total * 100) + "' aria-valuemin='0' aria-valuemax='100' style='width: " + Math.round((item.total - item.queue) / item.total * 100) + "%;'><span class='sr-only'>" + Math.round((item.total - item.queue) / item.total * 100) + "%</span></div></div></div>");
                    $("#errors").append("<div><h4 class='small font-weight-bold'>" + item.label + "<span class='float-right'>" + Math.round(item.error / item.total * 100) + "%</span></h4><h4 class='small font-weight-bold'><span class='text-left float-right'>" + item.total + "</span>" + item.error + "<span class='text-left float-right small font-weight-bold'>Всего:&nbsp;</span></h4><div class='progress mb-4'><div class='progress-bar bg-danger' aria-valuenow='" + Math.round(item.error / item.total * 100) + "' aria-valuemin='0' aria-valuemax='100' style='width: " + Math.round(item.error / item.total * 100) + "%;'><span class='sr-only'>" + Math.round(item.error / item.total * 100) + "%</span></div></div>");
                }
            }
        });
    }
}
function setSpeed() {
    $.ajax({
        url: 'api/stat/speed',
        type: 'get',
        async: true,
        success: function (data) {
            $('#speed').text(data);
        }
    });
}


function loadingStart() {
    $("#loadingbar").modal("show");
}

function loadingEnd() {
    $("#loadingbar").modal("hide");
}

function checkBlock() {
    var x = document.getElementById("blocked");
    $.ajax({
        url: 'api/isblocked',
        type: 'get',
        async: true,
        success: function (data) {
            if (data==='true') {
                x.style.display = 'block';
            } else {
                x.style.display = "none";
            }
        }
    });

}