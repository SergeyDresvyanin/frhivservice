/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    buttonStateChangeListener('create_visits');
    buttonStateChangeListener('create_dispexam');
    buttonStateChangeListener('create_hivepid');
    buttonStateChangeListener('create_blot');
    buttonStateChangeListener('create_recipes');
    buttonStateChangeListener('create_dispChemo');
    buttonStateChangeListener('create_dispconc');
    buttonStateChangeListener('create_dispstage');
    buttonStateChangeListener('create_dispcheckup');
    buttonStateChangeListener('create_dispdesease');
    buttonStateChangeListener('create_disp');
    buttonStateChangeListener('read_hiv');
    buttonStateChangeListener('read_disp');
    checkBlock();
    loadingStart();
    updateState();
    loadingEnd();
});


function buttonStateChangeListener(name) {
    $('#' + name).change(function () {
        changeState(name, $(this).prop('checked'));
    });
}


function changeState(method, state) {
    $.ajax({
        url: 'api/process/changeprocesstate?method=' + method + '&state=' + state,
        type: 'post',
        async: true,
        beforeSend: function (xhr) {
            loadingStart();
        },
        success: function (res) {
            updateState();
            loadingEnd();
        }
    });
}


function updateState() {
    $.ajax({
        url: 'api/process/states',
        type: 'get',
        async: true,
        success: function (res) {
            JSON.parse(res).forEach(element => toggleApiSilent(element));
        }
    });
}


function toggleApiSilent(elem) {
    if (elem.state === 1) {
        $('#' + elem.name).bootstrapToggle('on', true);
    } else {
        $('#' + elem.name).bootstrapToggle('off', true);
    }
}


function loadingStart() {
    $("#loadingbar").modal("show");
}

function loadingEnd() {
    $("#loadingbar").modal("hide");
}

function checkBlock() {
    var x = document.getElementById("blocked");
    $.ajax({
        url: 'api/isblocked',
        type: 'get',
        async: true,
        success: function (data) {
            if (data === 'true') {
                x.style.display = 'block';
            } else {
                x.style.display = "none";
            }
        }
    });

}

