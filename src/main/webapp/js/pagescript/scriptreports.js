/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    setDates();
    checkBlock();

});


function setDates() {
    var now = new Date();
    now.setDate(now.getDate() + 1);
    var dayFrom = ("0" + now.getDate()).slice(-2);
    var monthFrom = ("0" + (now.getMonth() + 1)).slice(-2);
    var todayFrom = now.getFullYear() + "-" + (monthFrom) + "-" + (dayFrom);
    $('#datefinish').val(todayFrom);

    var newDate = new Date();
    newDate.setDate(newDate.getDate() - 7);
    var dayTo = ("0" + newDate.getDate()).slice(-2);
    var monthTo = ("0" + (newDate.getMonth() + 1)).slice(-2);
    var todayTo = newDate.getFullYear() + "-" + (monthTo) + "-" + (dayTo);
    $('#datestart').val(todayTo);
}


function downloadReport(method, start, finish) {
    $.post({
        url: 'api/reports?datefrom=' + start + '&dateto=' + finish + '&method=' + method,
        type: 'get',
        async: true,
        beforeSend: function () {
            loadingStart();
        },
        data: {filterName: $('#personName').val(), placeLive: $('#cityLiv').val(), datefrom: $('#datefrom').val(), dateto: $('#dateto').val(), lowcd4: $('#lowcd4').is(":checked")},
        success: function (data) {
            loadingEnd();
            var blob = new Blob([s2ab(atob(data))], {type: "image/excel"});
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(blob);
                return;
            }
            var link = document.createElement('a');
            var filename = 'report.xlsx';
            link.href = window.URL.createObjectURL(blob);
            link.download = filename;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);

        }
    });
}
;

function s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i)
        view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
}

function loadingStart() {
    $("#loadingbar").modal("show");
}

function loadingEnd() {
    $("#loadingbar").modal("hide");
}

function checkBlock() {
    var x = document.getElementById("blocked");
    $.ajax({
        url: 'api/isblocked',
        type: 'get',
        async: true,
        success: function (data) {
            if (data === 'true') {
                x.style.display = 'block';
            } else {
                x.style.display = "none";
            }
        }
    });

}