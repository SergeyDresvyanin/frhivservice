/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    loadData();
});


function loadData() {
    //Записи на странице


    //500 последних записей с iemk table
    $('#caseslist').DataTable({
        'ajax': 'api/stat/rowlist',
        'ordering': false,
        'info': false,
        'searching': false,
        'retrieve': true,
        'columns': [
            {'data': 'operation', 'defaultContent': ''},
            {'data': 'id'},
            {'data': 'error', 'defaultContent': 'OK'},
            {'data': 'date'}
        ]
    });

}
