# FRHIVService Шина Интеграции с ФРВИЧ

Обеспечивает перенос данных из базы frhivsrvice в федеральный регистр ВИЧ, обеспечивает перенос всех возомжных данных через API

# Алгоритм работы

Добавляем данные в бд запросами или через программу синхронизатор в БД frhivsrvice, даллее севрсис по расписанию синхронизирует все данные
Есть возмонжность подсчета очереди данных на отправку и формирование отчетов по выгрузке для проверки и контроля

# Алгоритм развертывания

1. Устанвливаем glassfish
2. Устанвливаем Postgresql
3. Устанвливаем КриптоПро, сертификат для достсупа к ФРВИЧ, JavaCSP в JDK
4. Устанвливаем настроки для модуля в таблицt БД, таблица defaultparams
5. Создаем в Glassfish ресурс для подключения jdbc/frhivservice
6. Создаем JMS Destination Resources, две очереди с типом Queue 
jms/RestCallbackQueue c Physical Destination Name RestCallbackQueue
jms/RestSendQueue c Physical Destination Name RestSendQueue
