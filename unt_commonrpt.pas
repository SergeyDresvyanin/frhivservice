unit unt_commonrpt;

interface

uses SysUtils, Classes, FIBQuery, FIBDataset, FIBDatabase, Windows, Graphics,
  Forms, Dialogs, Variants, ComCtrls, Math, ComObj; // , gbreportslib;

type
  TVariantArray = Array of Variant;

  TColPeriod = record
    StartDate: TDateTime;
    EndDate: TDateTime;
    Column: integer;
  end;

  /// <summary>
  /// ������������ �������� ����� �4
  /// </summary>
  /// <param name="Month">
  /// ����� - ����� ������������ ������
  /// </param>
  /// <param name="Year">
  /// ���
  /// </param>
  /// <param name="DistrictFilter">
  /// ������ �� �������
  /// </param>
  /// <param name="Trans">
  /// �������� ����������
  /// </param>
  /// <param name="Status">
  /// ������ TStatusBar ��� ������ ���������� � ������������ ������
  /// </param>
procedure PrintHIVStatisticF4(Month, Year: integer; DistrictFilter: String;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
/// <summary>
/// ������������ �������� ����� �61
/// </summary>
/// <param name="Year">
/// ���
/// </param>
/// <param name="DistrictFilter">
/// ������ �� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure PrintHIVStatisticF61(Year: integer; DistrictFilter: String;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ������������ �������� ����� �61 (����� 2017)
/// </summary>
/// <param name="Year">
/// ���
/// </param>
/// <param name="DistrictFilter">
/// ������ �� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure PrintHIVStatisticF61_new(Year: integer; DistrictFilter: String;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ������������ ������������ ������"����������� ����������� ���������� �
/// ���������� ������������� ������������� ������� "��������"
/// </summary>
/// <param name="StartDate">
/// ���� �
/// </param>
/// <param name="EndDate">
/// ���� ��
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
procedure PrintHIVDetectionAndTreatmentRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction);

/// <summary>
/// ������������ ������������ ������"����������� ����������� ���������� �
/// ���������� ������������� ������������� ������� "��������"
/// </summary>
/// <param name="StartDate">
/// ���� �
/// </param>
/// <param name="EndDate">
/// ���� ��
/// </param>
/// <param name="DistrictFilter">
/// ������ �� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
procedure PrintIfaIbRpt(StartDate, EndDate: TDateTime; DistrictFilter: String;
  Trans: TFIBTransaction);

/// <summary>
/// ������������ ������ "����� �� ������������ �������������"
/// </summary>
/// <param name="StartDate">
/// ���� �
/// </param>
/// <param name="EndDate">
/// ���� ��
/// </param>
/// <param name="DistrictFilter">
/// ������ �� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
procedure PrintLaboratoryTestIFARpt(StartDate, EndDate: TDateTime;
  DistrictFilter: String; Trans: TFIBTransaction);

// ������ �������� �� ��������� ���
procedure PrintPDNAgreement(PersonID: integer; Trans: TFIBTransaction);
// ������ �������� �� �������������
procedure PrintInfConsent(PersonID: integer; Trans: TFIBTransaction);

/// <summary>
/// ������������ ������"�������� � ������������"
/// </summary>
/// <param name="Month">
/// ����� - ����� ������������ ������
/// </param>
/// <param name="Year">
/// ���
/// </param>
/// <param name="DistrictFilter">
/// ������ �� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure PrintHIVMeasuresInfo(Month, Year: integer; DistrictFilter: String;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ������������ ������"������ ���������, ���������� ��������"
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="DrugsFilter">
/// ������ �� ���������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure PrintPatientsList(StartDate, EndDate: TDateTime; DrugsFilter: String;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ������������ ������"������� ���������� ��������"
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure FormHIVDetectionRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ������������ ������"������� ����� �� ������������"
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure FormHIVLabDirectionsRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ������������ ������"������� ����� �� ������������ �� ���"
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure FormHIVLabDirectionsYearlyRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ������������ ������"���������� ����� �� ������������"
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure FormHIVLabDirectionsDailyRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ������������ �������� ��������� ������������ ������������
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure FormHIVLabAvgCountRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ������������ ������ ��������� � CD4<350
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure FormHIVCD4LessThan350Rpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ��������� ����� "C������� �� ����������� ������������� ��� �����"
/// </summary>
/// <param name="ContractorID">
/// ������������� �������� (Persons.ID)
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ ��� ��� ����������� ���������� � ������������ ������
/// </param>
procedure PrintConsentHK(ContractorID: integer; Trans: TFIBTransaction;
  Status: TStatusBar = nil);

/// <summary>
/// ������������ ������ �� ������������ �� ����������
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure FormContractorDirectionsRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ������������ ������ "������-����������� � �����������"
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure FormHIVDirectionsListRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
/// <summary>
/// ������������ ������ �� ������������ ����� ���-��������������
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure FormHIVDispensaryRegistrationRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ������ ��������� � �������������� �������� ���������
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure FormHIVZeroViralLoadRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ����� �� ���������� �� �������
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure FormVisitsByTerritoriesRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

procedure PrintHIVPregnancy(Year: integer; DistrictFilter: String;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

procedure PrintHIVVisitsList(const Filter: String; Trans: TFIBTransaction);

/// <summary>
/// ����� �� ������� ��������
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
procedure FormPrescriptionsReport(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction);

/// <summary>
/// ����� - ��������� ��������� ���������
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="DoctorID">
/// ������������� �����
/// </param>
procedure FormAllVisitsByDateRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; DoctorID: Variant);

/// <summary>
/// ����� - ��������� ��������� �����
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="DoctorID">
/// ������������� �����
/// </param>
procedure FormDoctorsVisitsByDateRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; DoctorID: Variant);

/// <summary>
/// ����� - ������ ��������� �� ������������ ����� ���
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
procedure FormHIVDispensaryReport(const StartDate, EndDate: TDateTime; Trans: TFIBTransaction);

/// <summary>
/// ����� ���+�������� 2018
/// </summary>
/// <param name="StartDate">
/// ��������� ���� �������
/// </param>
/// <param name="EndDate">
/// �������� ���� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
procedure FormHIVandHepatitReport(const StartDate, EndDate: TDateTime; Trans: TFIBTransaction);

/// <summary>
/// ����� "����������� ����������" ��� ������ ���� ��������������� �������
/// </summary>
/// <param name="CalcDate">
/// ����, �� ������� ��������� �����
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
/// <param name="Status">
/// ������ TStatusBar ��� ������ ���������� � ������������ ������
/// </param>
procedure OperativeMonitoringRpt(CalcDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

/// <summary>
/// ����� "������ ��������� ������������" ��� ������ ���� ��������������� �������
/// </summary>
/// <param name="StartDate">
/// ���� ������ ��������� �������
/// </param>
/// <param name="EndDate">
/// ���� ����� ��������� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
procedure FormXRayListReport(const StartDate, EndDate: TDateTime; Trans: TFIBTransaction);

/// <summary>
/// ����� "������ �������" ��� ������ ���� ��������������� �������
/// </summary>
/// <param name="StartDate">
/// ���� ������ ��������� �������
/// </param>
/// <param name="EndDate">
/// ���� ����� ��������� �������
/// </param>
/// <param name="Trans">
/// �������� ����������
/// </param>
procedure FormHIVDeathRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

implementation

uses unt_dm1, gbtypes, dateutils, VVKutils, pFIBDataset;

const

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_1000 (F4) +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  F4AGE_014_SQL = ' and ((cast(hifa.TAKINGDATE-p.BIRTHDAY AS INTEGER)/365)<15)';
  F4AGE_1517_SQL =
    ' and ((cast(hifa.TAKINGDATE-p.BIRTHDAY AS INTEGER)/365) between 15 and 17)';

  F4PERSONS_SQL = 'select count(distinct p.ID) ' + 'from HIVIFAIB hifa ' +
    'inner join HIVSCREENINGS hscr on hifa.ID=hscr.REFHIVIFAIB ' +
    'inner join HIVSCREENINGCODES hscc on hscr.REFHIVSCREENINGCODES=hscc.ID ' +
    'inner join PERSONS p on hifa.REFPERSONS=p.ID ' +
    'where (hifa.TAKINGDATE between :STARTDATE and :ENDDATE)';
  F4IFAIB_SQL = 'select count(distinct hifa.ID) ' + 'from HIVIFAIB hifa ' +
    'inner join HIVSCREENINGS hscr on hifa.ID=hscr.REFHIVIFAIB ' +
    'inner join HIVSCREENINGCODES hscc on hscr.REFHIVSCREENINGCODES=hscc.ID ' +
    'inner join PERSONS p on hifa.REFPERSONS=p.ID ' +
    'where (hifa.TAKINGDATE between :STARTDATE and :ENDDATE)';

  F4SECTION1000_ROWCOUNT = 11;

  F4SECTION1000_ROWS_SQL: array [1 .. F4SECTION1000_ROWCOUNT] of PositionedSQL =
    ((Row: 6; SQL: ' and hscc.CODE=''108'''), (Row: 7;
    SQL: ' and hscc.CODE=''115'''), (Row: 9; SQL: ' and hscc.CODE=''102'''),
    (Row: 10; SQL: ' and hscc.CODE=''103'''), (Row: 11;
    SQL: ' and hscc.CODE=''104'''), (Row: 12; SQL: ' and hscc.CODE=''112'''),
    (Row: 13; SQL: ' and hscc.CODE=''113'''), (Row: 14;
    SQL: ' and hscc.CODE=''109'''), (Row: 15; SQL: ' and hscc.CODE=''118'''),
    (Row: 16; SQL: ' and hscc.CODE starting with ''120'''), (Row: 17;
    SQL: ' and hscc.CODE starting with ''20'''));

  F4SECTION1000_COLCOUNT = 7;

  F4SECTION1000_COLUMNS_SQL: array [1 .. F4SECTION1000_COLCOUNT]
    of PositionedSQL = ((Row: 4; SQL: F4PERSONS_SQL), (Row: 5;
    SQL: F4PERSONS_SQL + F4AGE_014_SQL), (Row: 6;
    SQL: F4PERSONS_SQL + F4AGE_1517_SQL), (Row: 7;
    SQL: F4IFAIB_SQL + ' and (p.ISANONYM=1)'), (Row: 8; SQL: F4IFAIB_SQL),
    (Row: 9; SQL: F4IFAIB_SQL + ' and (hifa.REFHIVANALYSISRESULTS_IFA=1)'),
    (Row: 10; SQL: F4IFAIB_SQL + ' and (hifa.REFHIVANALYSISRESULTS_IB=1)'));

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // F16 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  // ���������
  ISMEN_SQL = ' and (p.REFSEX=1)';
  ISWOMEN_SQL = ' and (p.REFSEX=2)';
  ISTOWNSPEOPLE_SQL = ' and (h.REFPLACESOFLIVING=1)';
  ISVILLAGEPEOPLE_SQL = ' and (h.REFPLACESOFLIVING=2)';
  ISPOSTMORTEM_SQL = ' and (h.ISPOSTMORTEM=1)';
  ISCONCOMITANTTREATMENT_SQL = ' and (h.ISCONCOMITANTTREATMENT=1)';
  ISAUTOPSY_SQL = ' and (h.ISAUTOPSY=1)';
  ISOTHERORG_SQL = ' and (h.ISOTHERORG=1)';
  ISUFSIN_SQL = ' and ((h.ISOTHERORG=1) or (h.ISUFSIN=1))';
  ISOTHERREGION_SQL = ' and (h.ISOTHERREGION=1)';
  ISFOREIGN_SQL = ' and (h.ISFOREIGN=1)';
  ISBOMZH_SQL = ' and (p.ISBOMZH=1)';
  ISFIRSTTIME_SQL = ' and (h.ISFIRSTTIME=1)';
  DETECTEDINPERIOD_SQL =
    ' and (h.REGISTRYDATE between :STARTDATE and :ENDDATE)';
  ISFIRSTTIMEINPERIOD_SQL = ISFIRSTTIME_SQL + DETECTEDINPERIOD_SQL;
  REGISTEREDINPERIOD_SQL =
    ' and (h.REGISTRYDATE between :STARTDATE and :ENDDATE)';
  DEADINPERIOD_SQL = ' and (p.DATEOFDEATH between :STARTDATE and :ENDDATE)';
  UNREGISTEREDINPERIOD_SQL =
    ' and (h.UNREGISTRYDATE between :STARTDATE and :ENDDATE)';
  NOTUNREGISTEREDINPERIOD_SQL =
    ' and (not(h.UNREGISTRYDATE between :STARTDATE and :ENDDATE) or (h.UNREGISTRYDATE is null))';
  ISCONCOMITANT_SQL = ' and (hd.ISCONCOMITANT=1)';
  NOTCONCOMITANT_SQL = ' and (hd.ISCONCOMITANT=0)';
  ISNEWDISEASEINPERIOD_SQL =
    ' and (hd.STARTDATE between :STARTDATE and :ENDDATE)';

  // �������
  AGE_07_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365)<8)';
  AGE_814_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365) between 8 and 14)';
  AGE_1517_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365) between 15 and 17)';
  AGE_18_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365)>17)';

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_1000 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SECTION1000_SQL = 'select count(distinct p.ID) ' + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join HIVSTAGES hs on hs.REFPERSONS=p.ID and hs.STARTDATE = ' +
    '(select max(STARTDATE) from HIVSTAGES where REFPERSONS=p.ID and ' +
    'STARTDATE between :STARTDATE and :ENDDATE) ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE))';

  SECTION1000_ROWCOUNT = 14;

  SECTION1000_ROWS_SQL: array [1 .. SECTION1000_ROWCOUNT] of PositionedSQL =
    ((Row: 10; SQL: ' and (hs.REFHIVSTAGETYPES=1)'), (Row: 12;
    SQL: ' and (hs.REFHIVSTAGETYPES=2)'), (Row: 13;
    SQL: ' and (hs.REFHIVSTAGETYPES=3)'), (Row: 14;
    SQL: ' and (hs.REFHIVSTAGETYPES=4)'), (Row: 15;
    SQL: ' and (hs.REFHIVSTAGETYPES=4)' + ISMEN_SQL), (Row: 17;
    SQL: ' and (hs.REFHIVSTAGETYPES=5)'), (Row: 19;
    SQL: ' and (hs.REFHIVSTAGETYPES=6)'), (Row: 20;
    SQL: ' and (hs.REFHIVSTAGETYPES=7)'), (Row: 21;
    SQL: ' and (hs.REFHIVSTAGETYPES in (5,6,7))' + ISMEN_SQL), (Row: 23;
    SQL: ' and (hs.REFHIVSTAGETYPES=8)'), (Row: 24;
    SQL: ' and (hs.REFHIVSTAGETYPES=9)'), (Row: 25; SQL: ''), (Row: 26;
    SQL: ISMEN_SQL), (Row: 29; SQL: ISTOWNSPEOPLE_SQL));

  SECTION1000_COLCOUNT = 20;

  SECTION1000_COLUMNS_SQL: array [1 .. SECTION1000_COLCOUNT] of PositionedSQL =
    ((Row: 28; SQL: ''), (Row: 35; SQL: AGE_07_SQL), (Row: 42;
    SQL: AGE_814_SQL), (Row: 49; SQL: AGE_1517_SQL), (Row: 56;
    SQL: REGISTEREDINPERIOD_SQL), (Row: 63;
    SQL: REGISTEREDINPERIOD_SQL + AGE_07_SQL), (Row: 70;
    SQL: REGISTEREDINPERIOD_SQL + AGE_814_SQL), (Row: 77;
    SQL: REGISTEREDINPERIOD_SQL + AGE_1517_SQL), (Row: 84;
    SQL: DEADINPERIOD_SQL), (Row: 91; SQL: DEADINPERIOD_SQL + AGE_07_SQL),
    (Row: 98; SQL: DEADINPERIOD_SQL + AGE_814_SQL), (Row: 105;
    SQL: DEADINPERIOD_SQL + AGE_1517_SQL), (Row: 112;
    SQL: DEADINPERIOD_SQL + ' and (h.REFHIVOUTCOMES=1)'), (Row: 119;
    SQL: DEADINPERIOD_SQL + ' and (h.REFHIVOUTCOMES=1)' + AGE_07_SQL),
    (Row: 126; SQL: DEADINPERIOD_SQL + ' and (h.REFHIVOUTCOMES=1)' +
    AGE_814_SQL), (Row: 133; SQL: DEADINPERIOD_SQL + ' and (h.REFHIVOUTCOMES=1)'
    + AGE_1517_SQL), (Row: 140; SQL: NOTUNREGISTEREDINPERIOD_SQL), (Row: 147;
    SQL: NOTUNREGISTEREDINPERIOD_SQL + AGE_07_SQL), (Row: 154;
    SQL: NOTUNREGISTEREDINPERIOD_SQL + AGE_814_SQL), (Row: 161;
    SQL: NOTUNREGISTEREDINPERIOD_SQL + AGE_1517_SQL));

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_1001 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SECTION1001_SQL = 'select count(distinct p.ID) ' + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE))';

  SECTION1001_COUNT = 49;

  SECTION1001_SQLS: array [1 .. SECTION1001_COUNT, 1 .. 2] of string =
    (('EU1', SECTION1001_SQL), ('AB2', SECTION1001_SQL + AGE_07_SQL),
    ('AZ2', SECTION1001_SQL + AGE_814_SQL),
    ('BZ2', SECTION1001_SQL + AGE_1517_SQL),
    ('CU2', SECTION1001_SQL + ISMEN_SQL),
    ('DT2', SECTION1001_SQL + ISTOWNSPEOPLE_SQL),
    ('CA3', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL),
    ('DI3', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + AGE_07_SQL),
    ('EF3', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + AGE_814_SQL),
    ('FD3', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + AGE_1517_SQL),
    ('V4', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + ISMEN_SQL),
    ('AU4', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + ISTOWNSPEOPLE_SQL),
    ('BM5', SECTION1001_SQL + DEADINPERIOD_SQL),
    ('CQ5', SECTION1001_SQL + DEADINPERIOD_SQL + AGE_07_SQL),
    ('DP5', SECTION1001_SQL + DEADINPERIOD_SQL + AGE_814_SQL),
    ('EP5', SECTION1001_SQL + DEADINPERIOD_SQL + AGE_1517_SQL),
    ('I6', SECTION1001_SQL + DEADINPERIOD_SQL + ISMEN_SQL),
    ('AH6', SECTION1001_SQL + DEADINPERIOD_SQL + ISTOWNSPEOPLE_SQL),
    ('CH6', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + DEADINPERIOD_SQL),
    ('DL6', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + DEADINPERIOD_SQL +
    AGE_07_SQL), ('EK6', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL +
    DEADINPERIOD_SQL + AGE_814_SQL),
    ('I7', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + DEADINPERIOD_SQL +
    AGE_1517_SQL), ('AD7', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL +
    DEADINPERIOD_SQL + ISMEN_SQL),
    ('BC7', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + DEADINPERIOD_SQL +
    ISTOWNSPEOPLE_SQL), ('DP7', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL +
    DEADINPERIOD_SQL + ISPOSTMORTEM_SQL),
    ('ET7', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + DEADINPERIOD_SQL +
    ISPOSTMORTEM_SQL + AGE_07_SQL),
    ('S8', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + DEADINPERIOD_SQL +
    ISPOSTMORTEM_SQL + AGE_814_SQL),
    ('AS8', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + DEADINPERIOD_SQL +
    ISPOSTMORTEM_SQL + AGE_1517_SQL),
    ('BN8', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + DEADINPERIOD_SQL +
    ISPOSTMORTEM_SQL + ISMEN_SQL),
    ('CM8', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + DEADINPERIOD_SQL +
    ISPOSTMORTEM_SQL + ISTOWNSPEOPLE_SQL),
    ('I9', SECTION1001_SQL + ISAUTOPSY_SQL),
    ('BI10', SECTION1001_SQL + ISBOMZH_SQL),
    ('CM10', SECTION1001_SQL + ISOTHERORG_SQL),
    ('DO10', SECTION1001_SQL + ISUFSIN_SQL),
    ('EU10', SECTION1001_SQL + ISUFSIN_SQL + ISMEN_SQL),
    ('R11', SECTION1001_SQL + ISUFSIN_SQL + AGE_07_SQL),
    ('AQ11', SECTION1001_SQL + ISUFSIN_SQL + AGE_814_SQL),
    ('BQ11', SECTION1001_SQL + ISUFSIN_SQL + AGE_1517_SQL),
    ('CY11', SECTION1001_SQL + ISOTHERREGION_SQL),
    ('EF11', SECTION1001_SQL + ISFOREIGN_SQL),
    ('BE12', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + ISBOMZH_SQL),
    ('CI12', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + ISOTHERORG_SQL),
    ('DK12', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + ISUFSIN_SQL),
    ('EQ12', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + ISUFSIN_SQL +
    ISMEN_SQL), ('M13', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + ISUFSIN_SQL
    + AGE_07_SQL), ('AL13', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL +
    ISUFSIN_SQL + AGE_814_SQL),
    ('BL13', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL + ISUFSIN_SQL +
    AGE_1517_SQL), ('CT13', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL +
    ISOTHERREGION_SQL), ('EA13', SECTION1001_SQL + ISFIRSTTIMEINPERIOD_SQL +
    ISFOREIGN_SQL));

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_2000 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SECTION2000_SQL = 'select count(distinct hd.ID) ' + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join HIVDISEASES hd on hd.REFPERSONS=p.ID ' +
    'inner join DISEASES d on hd.REFDISEASES=d.ID ' +
    'where (hd.ISCONCOMITANT=0) and (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(hd.STARTDATE<=:ENDDATE) and ((hd.ENDDATE is null) or (hd.ENDDATE>=:STARTDATE))';

  SECTION2000_ROWCOUNT = 41;

  SECTION2000_ROWS_SQL: array [1 .. SECTION2000_ROWCOUNT] of PositionedSQL =
    ((Row: 22;
    SQL: ' and ((d.CLASSIFIERCODE starting with ''B20'') or (d.CLASSIFIERCODE starting with ''B21'') or (d.CLASSIFIERCODE starting with ''B22''))'),
    (Row: 24; SQL: ' and (d.CLASSIFIERCODE starting with ''B20'')'), (Row: 25;
    SQL: ' and (d.CLASSIFIERCODE=''B20.0'')'), (Row: 26;
    SQL: ' and (d.CLASSIFIERCODE=''B20.0'') and (hd.REFADDITIONALDISEASES=1)'),
    (Row: 27; SQL: ' and (d.CLASSIFIERCODE=''B20.1'')'), (Row: 28;
    SQL: ' and (d.CLASSIFIERCODE=''B20.1'') and (hd.REFADDITIONALDISEASES=2)'),
    (Row: 29; SQL
    : ' and (d.CLASSIFIERCODE=''B20.1'') and (hd.REFADDITIONALDISEASES=3)'),
    (Row: 30; SQL: ' and (d.CLASSIFIERCODE=''B20.2'')'), (Row: 31;
    SQL: ' and (d.CLASSIFIERCODE=''B20.2'') and (hd.REFADDITIONALDISEASES=4)'),
    (Row: 32; SQL
    : ' and (d.CLASSIFIERCODE=''B20.2'') and (hd.REFADDITIONALDISEASES=5)'),
    (Row: 34; SQL: ' and (d.CLASSIFIERCODE=''B20.3'')'), (Row: 35;
    SQL: ' and (d.CLASSIFIERCODE=''B20.3'') and (hd.REFADDITIONALDISEASES=6)'),
    (Row: 36; SQL
    : ' and (d.CLASSIFIERCODE=''B20.3'') and (hd.REFADDITIONALDISEASES=7)'),
    (Row: 37; SQL
    : ' and (d.CLASSIFIERCODE=''B20.3'') and (hd.REFADDITIONALDISEASES=8)'),
    (Row: 38; SQL: ' and (d.CLASSIFIERCODE=''B20.4'')'), (Row: 39;
    SQL: ' and (d.CLASSIFIERCODE=''B20.4'') and (hd.REFADDITIONALDISEASES=9)'),
    (Row: 40; SQL
    : ' and (d.CLASSIFIERCODE=''B20.4'') and (hd.REFADDITIONALDISEASES=10)'),
    (Row: 41; SQL: ' and (d.CLASSIFIERCODE=''B20.5'')'), (Row: 42;
    SQL: ' and (d.CLASSIFIERCODE=''B20.6'')'), (Row: 43;
    SQL: ' and (d.CLASSIFIERCODE=''B20.7'')'), (Row: 44;
    SQL: ' and (d.CLASSIFIERCODE=''B20.8'')'), (Row: 45;
    SQL: ' and (d.CLASSIFIERCODE=''B20.8'') and (hd.REFADDITIONALDISEASES=11)'),
    (Row: 46; SQL
    : ' and (d.CLASSIFIERCODE=''B20.8'') and (hd.REFADDITIONALDISEASES=12)'),
    (Row: 47; SQL
    : ' and (d.CLASSIFIERCODE=''B20.8'') and (hd.REFADDITIONALDISEASES=13)'),
    (Row: 48; SQL
    : ' and (d.CLASSIFIERCODE=''B20.8'') and (hd.REFADDITIONALDISEASES=14)'),
    (Row: 49; SQL
    : ' and (d.CLASSIFIERCODE=''B20.8'') and (hd.REFADDITIONALDISEASES=15)'),
    (Row: 50; SQL: ' and (d.CLASSIFIERCODE=''B20.9'')'), (Row: 58;
    SQL: ' and (d.CLASSIFIERCODE starting with ''B21'')'), (Row: 59;
    SQL: ' and (d.CLASSIFIERCODE=''B21.0'')'), (Row: 60;
    SQL: ' and (d.CLASSIFIERCODE=''B21.1'')'), (Row: 61;
    SQL: ' and (d.CLASSIFIERCODE=''B21.2'')'), (Row: 62;
    SQL: ' and (d.CLASSIFIERCODE=''B21.3'')'), (Row: 63;
    SQL: ' and (d.CLASSIFIERCODE=''B21.7'')'), (Row: 64;
    SQL: ' and (d.CLASSIFIERCODE=''B21.8'')'), (Row: 65;
    SQL: ' and (d.CLASSIFIERCODE=''B21.9'')'), (Row: 66;
    SQL: ' and (d.CLASSIFIERCODE starting with ''B22'')'), (Row: 67;
    SQL: ' and (d.CLASSIFIERCODE=''B22.0'')'), (Row: 68;
    SQL: ' and (d.CLASSIFIERCODE=''B22.1'')'), (Row: 69;
    SQL: ' and (d.CLASSIFIERCODE=''B22.2'')'), (Row: 70;
    SQL: ' and (d.CLASSIFIERCODE starting with ''B23'')'), (Row: 71;
    SQL: ' and (d.CLASSIFIERCODE=''B23.2'')'));

  SECTION2000_COLCOUNT = 8;

  SECTION2000_COLUMNS_SQL: array [1 .. SECTION2000_COLCOUNT] of PositionedSQL =
    ((Row: 90; SQL: ''), (Row: 99; SQL: AGE_07_SQL), (Row: 109;
    SQL: AGE_814_SQL), (Row: 119; SQL: AGE_1517_SQL), (Row: 129;
    SQL: ISCONCOMITANTTREATMENT_SQL + DETECTEDINPERIOD_SQL), (Row: 138;
    SQL: ISCONCOMITANTTREATMENT_SQL + DETECTEDINPERIOD_SQL + AGE_07_SQL),
    (Row: 148; SQL: ISCONCOMITANTTREATMENT_SQL + DETECTEDINPERIOD_SQL +
    AGE_814_SQL), (Row: 158;
    SQL: ISCONCOMITANTTREATMENT_SQL + DETECTEDINPERIOD_SQL + AGE_1517_SQL));

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_2001-2004 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  // ��������� � ���������� ��� 2001-2004
  ISTUBERCULOSIS_SQL = ' and ((d.CLASSIFIERCODE starting with ''A15'') ' +
    'or (d.CLASSIFIERCODE starting with ''A16'') ' +
    'or (d.CLASSIFIERCODE starting with ''A17'') ' +
    'or (d.CLASSIFIERCODE starting with ''A18'') ' +
    'or (d.CLASSIFIERCODE starting with ''A19''))';
  ISHEPATITIS_SQL = ' and ((d.CLASSIFIERCODE starting with ''B16.'') ' +
    'or (d.CLASSIFIERCODE=''B17.0'') ' + 'or (d.CLASSIFIERCODE=''B17.1'') ' +
    'or (d.CLASSIFIERCODE=''B18.0'') ' + 'or (d.CLASSIFIERCODE=''B18.1'') ' +
    'or (d.CLASSIFIERCODE=''B18.2''))';
  ISSEXUALLYTRANSMITTED_SQL = ' and ((d.CLASSIFIERCODE starting with ''A5'') ' +
    'or (d.CLASSIFIERCODE starting with ''A60'') ' +
    'or (d.CLASSIFIERCODE starting with ''A63'') ' +
    'or (d.CLASSIFIERCODE starting with ''A64''))';

  SELECT_PERSONS_SQL = 'select count(distinct p.ID) ';
  SELECT_HIVDISEASES_SQL = 'select count(distinct hd.ID) ';
  FROMWHERE_SQL = 'from HIV h ' + 'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join HIVDISEASES hd on hd.REFPERSONS=p.ID ' +
    'inner join DISEASES d on hd.REFDISEASES=d.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(hd.STARTDATE<=:ENDDATE) and ((hd.ENDDATE is null) or (hd.ENDDATE>=:STARTDATE))';

  SECTION2001_SQL_1 = SELECT_PERSONS_SQL + FROMWHERE_SQL + NOTCONCOMITANT_SQL;
  SECTION2001_SQL_2 = SECTION1000_SQL;
  SECTION2002_SQL_1 = SELECT_HIVDISEASES_SQL + FROMWHERE_SQL +
    ISCONCOMITANT_SQL;
  SECTION2002_SQL_2 = SELECT_PERSONS_SQL + FROMWHERE_SQL + ISCONCOMITANT_SQL;
  SECTION2003_SQL = SECTION2002_SQL_2;
  SECTION2004_SQL = SECTION2002_SQL_1;

  SECTION2001_2004_COUNT = 26;

  SECTION2001_2004_SQLS: array [1 .. SECTION2001_2004_COUNT, 1 .. 2]
    of string = (('DI75', SECTION2001_SQL_1),
    ('AI76', SECTION2001_SQL_2 + ' and (hs.REFHIVSTAGETYPES in (7,8))'),
    ('CM76', SECTION2001_SQL_2 +
    ' and (hs.REFHIVSTAGETYPES in (7,8)) and (hs.STARTDATE between :STARTDATE and :ENDDATE)'),
    ('EC76', SECTION2001_SQL_2 +
    ' and (hs.REFHIVSTAGETYPES in (7,8)) and (p.DATEOFDEATH between :STARTDATE and :ENDDATE)'),
    ('CC78', SECTION2002_SQL_1 + ISTUBERCULOSIS_SQL),
    ('DQ78', SECTION2002_SQL_1 + ' and (d.CLASSIFIERCODE=''B15.0'')'),
    ('EZ78', SECTION2002_SQL_1 + ISUFSIN_SQL +
    ' and (d.CLASSIFIERCODE=''B15.0'')'),
    ('BT79', SECTION2002_SQL_1 + ISNEWDISEASEINPERIOD_SQL +
    ' and (d.CLASSIFIERCODE=''B15.0'')'),
    ('DD79', SECTION2002_SQL_1 + ISNEWDISEASEINPERIOD_SQL + ISUFSIN_SQL +
    ' and (d.CLASSIFIERCODE=''B15.0'')'),
    ('R80', SECTION2002_SQL_1 + DEADINPERIOD_SQL +
    ' and (d.CLASSIFIERCODE=''B15.0'')'),
    ('BA80', SECTION2002_SQL_1 + DEADINPERIOD_SQL + ISUFSIN_SQL +
    ' and (d.CLASSIFIERCODE=''B15.0'')'),
    ('T81', SECTION2002_SQL_2 + DEADINPERIOD_SQL +
    ' and (h.REFHIVOUTCOMES=1) and (d.CLASSIFIERCODE=''B20.0'')'),
    ('AU81', SECTION2002_SQL_2 + DEADINPERIOD_SQL + ISUFSIN_SQL +
    ' and (h.REFHIVOUTCOMES=1) and (d.CLASSIFIERCODE=''B20.0'')'),
    ('CG82', SECTION2003_SQL + ISHEPATITIS_SQL),
    ('DM82', SECTION2003_SQL +
    ' and ((d.CLASSIFIERCODE=''B17.1'') or (d.CLASSIFIERCODE=''B18.2''))'),
    ('FA82', SECTION2003_SQL +
    ' and ((d.CLASSIFIERCODE starting with ''B16.'') or (d.CLASSIFIERCODE=''B17.0'') or (d.CLASSIFIERCODE=''B17.1''))'),
    ('BB83', SECTION2003_SQL + ISHEPATITIS_SQL + ISNEWDISEASEINPERIOD_SQL),
    ('CH83', SECTION2003_SQL + ISHEPATITIS_SQL + DEADINPERIOD_SQL),
    ('DE84', SECTION2004_SQL + ISSEXUALLYTRANSMITTED_SQL),
    ('EM84', SECTION2004_SQL +
    ' and ((d.CLASSIFIERCODE starting with ''A50'') or (d.CLASSIFIERCODE starting with ''A51'') or (d.CLASSIFIERCODE starting with ''A52'') or (d.CLASSIFIERCODE starting with ''A53''))'),
    ('O85', SECTION2004_SQL + ' and (d.CLASSIFIERCODE starting with ''A54'')'),
    ('AM85', SECTION2004_SQL + ' and (d.CLASSIFIERCODE starting with ''A59'')'),
    ('BI85', SECTION2004_SQL +
    ' and ((d.CLASSIFIERCODE starting with ''A55'') or (d.CLASSIFIERCODE starting with ''A56''))'),
    ('CU85', SECTION2004_SQL + ' and (d.CLASSIFIERCODE=''A63.0'')'),
    ('DZ85', SECTION2004_SQL + ' and (d.CLASSIFIERCODE starting with ''A60'')'),
    ('DJ86', SECTION2004_SQL + ISSEXUALLYTRANSMITTED_SQL +
    ISNEWDISEASEINPERIOD_SQL));

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_3000 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SECTION3000_IFAIB_SQL = 'select count(distinct p.ID) ' + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join HIVIFAIB hifa on hifa.REFPERSONS=p.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(hifa.TAKINGDATE between :STARTDATE and :ENDDATE)';
  SECTION3000_TREATMENTS_SQL = 'select count(distinct p.ID) ' + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join TREATMENTHISTORY th on th.REFPERSONS=p.ID ' +
    'inner join PLACESOFTREATMENT pt on th.REFPLACESOFTREATMENT=pt.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(th.STARTDATE<=:ENDDATE) and ' +
    '((th.ENDDATE is null) or (th.ENDDATE>=:STARTDATE)) and ' +
    '(pt.REFTREATMENTPLACETYPES < 3)';
  SECTION3000_MULTI_TREATMENTS_SQL = 'select count(distinct p.ID) ' +
    'from HIV h ' + 'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '((select count(distinct ths.ID) ' + 'from PERSONS ps  ' +
    'inner join TREATMENTHISTORY ths on ths.REFPERSONS=ps.ID ' +
    'inner join PLACESOFTREATMENT pts on ths.REFPLACESOFTREATMENT=pts.ID ' +
    'where (ths.STARTDATE<=:ENDDATE) and ' +
    '((ths.ENDDATE is null) or (ths.ENDDATE>=:STARTDATE)) and ' +
    '(pts.REFTREATMENTPLACETYPES < 3) and (p.ID=ps.ID)) > 1)';

  SECTION3000_HIVARTONLY_SQL = 'select count(distinct p.ID) ' +
    'from HIVART ha ' + 'inner join HIV h on h.REFPERSONS=ha.REFPERSONS ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'left join HIVPASSPORT hp on hp.REFPERSONS=p.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(ha.STARTDATE<=:ENDDATE) and ' +
    '((ha.ENDDATE is null) or (ha.ENDDATE>=:STARTDATE))';
  SECTION3000_DISEASESONLY_SQL = 'select count(distinct p.ID) ' + 'from HIV h  '
    + 'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join HIVDISEASES hd on hd.REFPERSONS=h.REFPERSONS ' +
    'inner join DISEASES d on hd.REFDISEASES=d.ID ' +
    'inner join HIVDISEASETREATMENT hdt on hdt.REFHIVDISEASES=hd.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(hdt.STARTDATE<=:ENDDATE) and ' +
    '((hdt.ENDDATE is null) or (hdt.ENDDATE>=:STARTDATE))';
  SECTION3000_HIVARTDISEASES_SQL = 'select count(distinct p.ID) ' +
    'from HIVART ha ' + 'inner join HIV h on h.REFPERSONS=ha.REFPERSONS ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join HIVDISEASES hd on hd.REFPERSONS=h.REFPERSONS ' +
    'inner join DISEASES d on hd.REFDISEASES=d.ID ' +
    'inner join HIVDISEASETREATMENT hdt on hdt.REFHIVDISEASES=hd.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(ha.STARTDATE<=:ENDDATE) and ' +
    '((ha.ENDDATE is null) or (ha.ENDDATE>=:STARTDATE)) and ' +
    '(hdt.STARTDATE<=:ENDDATE) and ' +
    '((hdt.ENDDATE is null) or (hdt.ENDDATE>=:STARTDATE))';
  SECTION3000_WHERE_TREATMENTRESTART_SQL = ' and exists(select ID ' +
    'from HIVART where (ENDDATE<ha.STARTDATE) and (REFPERSONS=p.ID))';

  SECTION3000_ROWCOUNT = 17;

  SECTION3000_ROWS_SQL: array [1 .. SECTION3000_ROWCOUNT] of PositionedSQL =
    ((Row: 94; SQL: SECTION3000_IFAIB_SQL), (Row: 95;
    SQL: SECTION3000_TREATMENTS_SQL), (Row: 96;
    SQL: SECTION3000_MULTI_TREATMENTS_SQL), (Row: 97;
    SQL: SECTION3000_HIVARTONLY_SQL), (Row: 98;
    SQL: SECTION3000_HIVARTONLY_SQL), (Row: 99;
    SQL: SECTION3000_HIVARTONLY_SQL + ' and (hp.REFADDICTION_INJNARC=1)'),
    (Row: 100; SQL: SECTION3000_HIVARTDISEASES_SQL + ISTUBERCULOSIS_SQL),
    (Row: 101; SQL: SECTION3000_HIVARTDISEASES_SQL + ISHEPATITIS_SQL),
    (Row: 102; SQL: SECTION3000_HIVARTONLY_SQL +
    ' and (ha.REFHIVARTOUTCOMES=6)'), (Row: 103;
    SQL: SECTION3000_HIVARTONLY_SQL + ' and (ha.REFHIVARTOUTCOMES=7)'),
    (Row: 104; SQL: SECTION3000_HIVARTONLY_SQL + DEADINPERIOD_SQL), (Row: 105;
    SQL: SECTION3000_HIVARTONLY_SQL + ' and (ha.REFHIVARTOUTCOMES=3)'),
    (Row: 106; SQL: SECTION3000_HIVARTONLY_SQL +
    SECTION3000_WHERE_TREATMENTRESTART_SQL), (Row: 107;
    SQL: SECTION3000_DISEASESONLY_SQL + ISHEPATITIS_SQL), (Row: 108;
    SQL: SECTION3000_DISEASESONLY_SQL + ISTUBERCULOSIS_SQL), (Row: 109;
    SQL: SECTION3000_DISEASESONLY_SQL + ISTUBERCULOSIS_SQL), (Row: 110;
    SQL: SECTION3000_DISEASESONLY_SQL + ' and (d.CLASSIFIERCODE=''B59'')'));

  SECTION3000_COLCOUNT = 8;

  SECTION3000_COLUMNS_SQL: array [1 .. SECTION3000_COLCOUNT] of PositionedSQL =
    ((Row: 88; SQL: ''), (Row: 98; SQL: AGE_07_SQL), (Row: 108;
    SQL: AGE_814_SQL), (Row: 118; SQL: AGE_1517_SQL), (Row: 128;
    SQL: REGISTEREDINPERIOD_SQL), (Row: 138;
    SQL: REGISTEREDINPERIOD_SQL + AGE_07_SQL), (Row: 148;
    SQL: REGISTEREDINPERIOD_SQL + AGE_814_SQL), (Row: 158;
    SQL: REGISTEREDINPERIOD_SQL + AGE_1517_SQL));

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_3001-3006 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SECTION3001_PREGNANCY_SQL = 'select count(hp.ID) ' + 'from HIVPREGNANCY hp ' +
    'inner join PERSONS p on hp.REFPERSONS=p.ID ' +
    'where (hp.DETECTIONDATE<=:ENDDATE) and ' +
    '((hp.BIRTHDATE is null) or (hp.BIRTHDATE>=:STARTDATE))';
  SECTION3003_CHILDCOUNT_SQL = 'select sum(hp.CHILDRENLIVCOUNT) ' +
    'from HIVPREGNANCY hp ' + 'inner join PERSONS p on hp.REFPERSONS=p.ID ' +
    'where (hp.BIRTHDATE<=:STARTDATE or hp.BIRTHDATE<=:ENDDATE)';
  SECTION3003_CHILDALL_SQL = 'select count(distinct p.ID) ' + 'from PERSONS p '
    + 'inner join HIVCONTACTS hc on hc.REFPERSONS2=p.ID and hc.REFHIVCONTACTTYPES=3 '
    + 'inner join HIVPREGNANCY hp on hc.REFPERSONS1=hp.REFPERSONS ' +
    'left join HIV h on h.REFPERSONS=p.ID ' +
    'where (p.BIRTHDAY<=:STARTDATE or p.BIRTHDAY<=:ENDDATE)';
  SECTION3003_BREASTFEEDINGCOUNT_SQL = 'with CTE as ' +
    '(select ps.id as REFPERSONS, max(hpass.BREASTFEEDINGCOUNT) as CNT ' +
    'from HIVPREGNANCY hp ' + 'inner join PERSONS ps on hp.REFPERSONS=ps.ID ' +
    'inner join HIVPASSPORT hpass on hp.REFPERSONS=hpass.REFPERSONS ' +
    'where (hp.BIRTHDATE between :STARTDATE and :ENDDATE) and ' +
    '(hpass.ONDATE between :STARTDATE and :ENDDATE) ' + 'group by 1) ' +
    'select sum(CTE.CNT) from CTE ' +
    'inner join PERSONS p on CTE.REFPERSONS=p.ID';
  SECTION3005_SQL = 'select count(him.ID) ' + 'from HIVIMMUNOGRAMS him ' +
    'inner join PERSONS p on him.REFPERSONS=p.ID ' +
    'where (him.TAKINGDATE between :STARTDATE and :ENDDATE)';
  SECTION3006_SQL = 'select count(hpcr.ID) ' + 'from HIVPCR hpcr ' +
    'inner join PERSONS p on hpcr.REFPERSONS=p.ID ' +
    'where (hpcr.VIRALLOAD is not null) and (hpcr.TAKINGDATE between :STARTDATE and :ENDDATE)';
  WHERE_NOT_COMMITTED =
    ' and not exists(select ID from HIVIFAIB where REFPERSONS=p.ID and ANSWERDATE<=:ENDDATE and REFHIVANALYSISRESULTS_IB=1)'
    + ' and exists(select ID from HIVIFAIB where REFPERSONS=p.ID and ANSWERDATE<=:ENDDATE and REFHIVANALYSISRESULTS_IFA in (1,3))';

  SECTION3001_3006_COUNT = 21;

  SECTION3001_3006_SQLS: array [1 .. SECTION3001_3006_COUNT, 1 .. 2]
    of string = (('BY112', SECTION3001_PREGNANCY_SQL),
    ('DV112', SECTION3001_PREGNANCY_SQL +
    ' and (hp.BIRTHDATE between :STARTDATE and :ENDDATE)'),
    ('I113', SECTION3001_PREGNANCY_SQL +
    ' and (hp.BIRTHDATE between :STARTDATE and :ENDDATE) and (hp.ISCSECTION=1)'),
    ('DE115', SECTION3001_PREGNANCY_SQL + ' and not(hp.REFDRUGS_PREG is null)'),
    ('DX115', SECTION3001_PREGNANCY_SQL +
    ' and not(hp.REFDRUGS_BIRTHS is null)'),
    ('EZ115', SECTION3001_PREGNANCY_SQL +
    ' and not(hp.REFDRUGS_CHILD is null)'),
    ('AR116', SECTION3001_PREGNANCY_SQL +
    ' and (hp.REFDRUGS_PREG is not null) and (hp.REFDRUGS_BIRTHS is not null) and (hp.REFDRUGS_CHILD is not null)'),
    ('BK117', SECTION3003_CHILDCOUNT_SQL),
    ('CR117', SECTION3003_CHILDCOUNT_SQL +
    ' and (hp.BIRTHDATE between :STARTDATE and :ENDDATE)'),
    ('EY117', SECTION3003_CHILDALL_SQL +
    ' and (h.REGISTRYDATE<=:ENDDATE) and ((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>:ENDDATE))'),
    ('CG118', SECTION3003_CHILDALL_SQL + WHERE_NOT_COMMITTED),
    ('EJ118', SECTION3003_BREASTFEEDINGCOUNT_SQL),
    ('DG119', SECTION3003_CHILDALL_SQL + ' and (h.DETECTIONDATE<=:ENDDATE)'),
    ('EN119', SECTION3003_CHILDALL_SQL +
    ' and (h.DETECTIONDATE between :STARTDATE and :ENDDATE)'),
    ('AK120', SECTION3003_CHILDALL_SQL +
    ' and (h.REGISTRYDATE<=:ENDDATE) and ((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>:ENDDATE))'),
    ('CA120', SECTION3003_CHILDALL_SQL + ' and (h.DETECTIONDATE<=:ENDDATE)' +
    DEADINPERIOD_SQL), ('CC121', SECTION3005_SQL), ('EL121', SECTION3005_SQL),
    ('Y122', SECTION3005_SQL), ('CB123', SECTION3006_SQL),
    ('DY123', SECTION3006_SQL));

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // F16 (�����) +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  AGE_0_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365)<1)';
  AGE_014_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365)<15)';
  AGE_017_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365)<18)';
  AGE_14_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365) between 1 and 4)';
  AGE_514_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365) between 5 and 14)';
  AGE_1824_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365) between 18 and 24)';
  AGE_2534_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365) between 25 and 34)';
  AGE_2544_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365) between 25 and 44)';
  AGE_3544_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365) between 35 and 44)';
  AGE_4554_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365) between 45 and 54)';
  AGE_5559_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365) between 55 and 59)';
  AGE_60_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365) > 59)';

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_1000n +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  B2024_SQL =
    ' and (d.CLASSIFIERCODE BETWEEN ''B20'' AND ''B24Z'')';

  SECTION1000n_SQL =
    'select count(distinct p.ID) ' + LB +
    'from HIV h ' + LB +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' + LB +
    'inner join HIVSTAGES hd on hd.REFPERSONS=p.ID ' + LB +
    'inner join DISEASES d on hd.REFDISEASES=d.ID ' + LB +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' + LB +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' + LB +
    '(hd.STARTDATE<=:ENDDATE) and ((hd.ENDDATE is null) or (hd.ENDDATE>=:STARTDATE))'
    + ISFIRSTTIMEINPERIOD_SQL;

  SECTION1000n_HIVCONTACTS_SQL = 'select count(distinct p.ID) ' +
    'from HIVCONTACTS hc ' + 'inner join PERSONS p on hc.REFPERSONS2=p.ID ' +
    'where (:STARTDATE<''TODAY'') and (p.BIRTHDAY<:ENDDATE) and (p.DATEOFDEATH is null)';

  SECTION1000n_ROWCOUNT = 60;

  SECTION1000n_ROWS_SQL: array [1 .. SECTION1000n_ROWCOUNT] of PositionedSQL =
    (
    (Row: 8; SQL: SECTION1000n_SQL + B2024_SQL + ISMEN_SQL),
    (Row: 9; SQL: SECTION1000n_SQL + B2024_SQL + ISWOMEN_SQL),
    (Row: 10; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE starting with ''B20'')' + ISMEN_SQL),
    (Row: 12; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE starting with ''B20'')' + ISWOMEN_SQL),
    (Row: 13; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B20.0'')' + ISMEN_SQL),
    (Row: 15; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B20.0'')' + ISWOMEN_SQL),
    (Row: 16; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B20.2'')' + ISMEN_SQL),
    (Row: 17; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B20.2'')' + ISWOMEN_SQL),
    (Row: 18; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B20.4'')' + ISMEN_SQL),
    (Row: 19; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B20.4'')' + ISWOMEN_SQL),
    (Row: 20; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B20.6'')' + ISMEN_SQL),
    (Row: 21; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B20.6'')' + ISWOMEN_SQL),
    (Row: 22; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B20.7'')' + ISMEN_SQL),
    (Row: 23; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B20.7'')' + ISWOMEN_SQL),
    (Row: 24; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE starting with ''B21'')' + ISMEN_SQL),
    (Row: 25; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE starting with ''B21'')' + ISWOMEN_SQL),
    (Row: 26; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B21.0'')' + ISMEN_SQL),
    (Row: 27; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B21.0'')' + ISWOMEN_SQL),
    (Row: 28; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE starting with ''B22'')' + ISMEN_SQL),
    (Row: 29; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE starting with ''B22'')' + ISWOMEN_SQL),
    (Row: 30; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B22.0'')' + ISMEN_SQL),
    (Row: 31; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B22.0'')' + ISWOMEN_SQL),
    (Row: 32; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B22.1'')' + ISMEN_SQL),
    (Row: 33; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B22.1'')' + ISWOMEN_SQL),
    (Row: 34; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B22.2'')' + ISMEN_SQL),
    (Row: 35; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B22.2'')' + ISWOMEN_SQL),
    (Row: 36; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B22.7'')' + ISMEN_SQL),
    (Row: 37; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B22.7'')' + ISWOMEN_SQL),
    (Row: 38; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE starting with ''B23'')' + ISMEN_SQL),
    (Row: 39; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE starting with ''B23'')' + ISWOMEN_SQL),
    (Row: 40; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B23.0'')' + ISMEN_SQL),
    (Row: 41; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B23.0'')' + ISWOMEN_SQL),
    (Row: 42; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B23.1'')' + ISMEN_SQL),
    (Row: 43; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B23.1'')' + ISWOMEN_SQL),
    (Row: 44; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B23.2'')' + ISMEN_SQL),
    (Row: 45; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B23.2'')' + ISWOMEN_SQL),
    (Row: 46; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B23.8'')' + ISMEN_SQL),
    (Row: 47; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B23.8'')' + ISWOMEN_SQL),
    (Row: 48; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B24'')' + ISMEN_SQL),
    (Row: 49; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''B24'')' + ISWOMEN_SQL),
    (Row: 50; SQL: SECTION1000n_SQL + B2024_SQL + ISVILLAGEPEOPLE_SQL + ISMEN_SQL),
    (Row: 51; SQL: SECTION1000n_SQL + B2024_SQL + ISVILLAGEPEOPLE_SQL + ISWOMEN_SQL),
    (Row: 52; SQL: SECTION1000n_SQL + B2024_SQL + ISBOMZH_SQL + ISMEN_SQL),
    (Row: 53; SQL: SECTION1000n_SQL + B2024_SQL + ISBOMZH_SQL + ISWOMEN_SQL),
    (Row: 54; SQL: SECTION1000n_SQL + B2024_SQL + ISOTHERORG_SQL + ISMEN_SQL),
    (Row: 55; SQL: SECTION1000n_SQL + B2024_SQL + ISOTHERORG_SQL + ISWOMEN_SQL),
    (Row: 56; SQL: SECTION1000n_SQL + B2024_SQL + ISUFSIN_SQL + ISMEN_SQL),
    (Row: 57; SQL: SECTION1000n_SQL + B2024_SQL + ISUFSIN_SQL + ISWOMEN_SQL),
    (Row: 58; SQL: SECTION1000n_SQL + B2024_SQL + ISPOSTMORTEM_SQL + ISMEN_SQL),
    (Row: 59; SQL: SECTION1000n_SQL + B2024_SQL + ISPOSTMORTEM_SQL + ISWOMEN_SQL),
    (Row: 60; SQL: SECTION1000n_SQL + B2024_SQL + ISFOREIGN_SQL + ISMEN_SQL),
    (Row: 61; SQL: SECTION1000n_SQL + B2024_SQL + ISFOREIGN_SQL + ISWOMEN_SQL),
    (Row: 62; SQL: ''),
    (Row: 63; SQL: ''),
    (Row: 64; SQL: ''),
    (Row: 65; SQL: ''),
    (Row: 66; SQL: SECTION1000n_HIVCONTACTS_SQL + ISMEN_SQL),
    (Row: 68; SQL: SECTION1000n_HIVCONTACTS_SQL + ISWOMEN_SQL),
    (Row: 69; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''Z21'')' + ISMEN_SQL),
    (Row: 70; SQL: SECTION1000n_SQL + ' and (d.CLASSIFIERCODE=''Z21'')' + ISWOMEN_SQL)
    );

  SECTION1000n_COLCOUNT = 11;

  SECTION1000n_COLUMNS_SQL: array [1 .. SECTION1000n_COLCOUNT]
    of PositionedSQL = ((Row: 73; SQL: ''), (Row: 82; SQL: AGE_0_SQL), (Row: 90;
    SQL: AGE_14_SQL), (Row: 98; SQL: AGE_514_SQL), (Row: 106;
    SQL: AGE_1517_SQL), (Row: 114; SQL: AGE_1824_SQL), (Row: 122;
    SQL: AGE_2534_SQL), (Row: 130; SQL: AGE_3544_SQL), (Row: 138;
    SQL: AGE_4554_SQL), (Row: 146; SQL: AGE_5559_SQL), (Row: 154;
    SQL: AGE_60_SQL));

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_2000n +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SECTION2000n1_SQL = 'select count(distinct p.ID) ' + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join HIVSTAGES hd on hd.REFPERSONS=p.ID ' +
    'inner join DISEASES d on hd.REFDISEASES=d.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(hd.STARTDATE<=:ENDDATE) and ((hd.ENDDATE is null) or (hd.ENDDATE>=:STARTDATE))';

  SECTION2000n1_ROWCOUNT = 15;

  SECTION2000n1_ROWS_SQL: array [1 .. SECTION2000n1_ROWCOUNT] of PositionedSQL =
    ((Row: 9; SQL: SECTION2000n1_SQL + B2024_SQL), (Row: 11;
    SQL: SECTION2000n1_SQL + ' and (d.CLASSIFIERCODE starting with ''B20'')'),
    (Row: 15; SQL: SECTION2000n1_SQL + ' and (d.CLASSIFIERCODE=''B20.0'')'),
    (Row: 18; SQL: SECTION2000n1_SQL + ' and (d.CLASSIFIERCODE=''B20.7'')'),
    (Row: 20; SQL: SECTION2000n1_SQL +
    ' and (d.CLASSIFIERCODE starting with ''B21'')'), (Row: 22;
    SQL: SECTION2000n1_SQL + ' and (d.CLASSIFIERCODE starting with ''B22'')'),
    (Row: 24; SQL: SECTION2000n1_SQL + ' and (d.CLASSIFIERCODE=''B22.7'')'),
    (Row: 26; SQL: SECTION2000n1_SQL +
    ' and (d.CLASSIFIERCODE starting with ''B23'')'), (Row: 28;
    SQL: SECTION2000n1_SQL + ' and (d.CLASSIFIERCODE=''B24'')'), (Row: 30;
    SQL: SECTION2000n1_SQL + B2024_SQL + ISFOREIGN_SQL), (Row: 31; SQL: ''),
    (Row: 33; SQL: ''), (Row: 35;
    SQL: SECTION2000n1_SQL + B2024_SQL + ISMEN_SQL), (Row: 36; SQL: ''),
    (Row: 39; SQL: SECTION2000n1_SQL + ' and (d.CLASSIFIERCODE=''Z21'')'));

  SECTION2000n1_COLCOUNT = 12;

  SECTION2000n1_COLUMNS_SQL: array [1 .. SECTION2000n1_COLCOUNT]
    of PositionedSQL = ((Row: 56; SQL: REGISTEREDINPERIOD_SQL), (Row: 65;
    SQL: REGISTEREDINPERIOD_SQL + ISFIRSTTIMEINPERIOD_SQL), (Row: 74;
    SQL: REGISTEREDINPERIOD_SQL + ISFIRSTTIMEINPERIOD_SQL + AGE_017_SQL),
    (Row: 83; SQL: REGISTEREDINPERIOD_SQL + ISOTHERORG_SQL), (Row: 92;
    SQL: REGISTEREDINPERIOD_SQL + ISUFSIN_SQL), (Row: 101;
    SQL: REGISTEREDINPERIOD_SQL + ISOTHERREGION_SQL), (Row: 110;
    SQL: UNREGISTEREDINPERIOD_SQL), (Row: 118;
    SQL: UNREGISTEREDINPERIOD_SQL + ' AND H.REFHIVOUTCOMES IN (5,6)'), (Row: 126;
    SQL: UNREGISTEREDINPERIOD_SQL + ' AND H.REFHIVOUTCOMES=5'), (Row: 134;
    SQL: UNREGISTEREDINPERIOD_SQL + ' AND H.REFHIVOUTCOMES=3'), (Row: 143;
    SQL: UNREGISTEREDINPERIOD_SQL + DEADINPERIOD_SQL), (Row: 152;
    SQL: NOTUNREGISTEREDINPERIOD_SQL));

  SECTION2000n2_SQL = 'select count(distinct p.ID) ' + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join HIVSTAGES hd on hd.REFPERSONS=p.ID ' +
    'inner join DISEASES d on hd.REFDISEASES=d.ID ' +
    'inner join HIVSTAGES hs on hs.REFPERSONS=p.ID and hs.STARTDATE = ' +
    '(select max(STARTDATE) from HIVSTAGES where REFPERSONS=p.ID and ' +
    'STARTDATE<:ENDDATE) ' + 'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(hd.STARTDATE<=:ENDDATE) and ((hd.ENDDATE is null) or (hd.ENDDATE>=:STARTDATE))';

  SECTION2000n2_ROWCOUNT = 13;

  SECTION2000n2_ROWS_SQL: array [1 .. SECTION2000n2_ROWCOUNT] of PositionedSQL =
    ((Row: 45; SQL: SECTION2000n2_SQL + B2024_SQL), (Row: 46;
    SQL: SECTION2000n2_SQL + ' and (d.CLASSIFIERCODE starting with ''B20'')'),
    (Row: 48; SQL: SECTION2000n2_SQL + ' and (d.CLASSIFIERCODE=''B20.0'')'),
    (Row: 50; SQL: SECTION2000n2_SQL + ' and (d.CLASSIFIERCODE=''B20.7'')'),
    (Row: 51; SQL: SECTION2000n2_SQL +
    ' and (d.CLASSIFIERCODE starting with ''B21'')'), (Row: 52;
    SQL: SECTION2000n2_SQL + ' and (d.CLASSIFIERCODE starting with ''B22'')'),
    (Row: 53; SQL: SECTION2000n2_SQL + ' and (d.CLASSIFIERCODE=''B22.7'')'),
    (Row: 54; SQL: SECTION2000n2_SQL +
    ' and (d.CLASSIFIERCODE starting with ''B23'')'), (Row: 55;
    SQL: SECTION2000n2_SQL + ' and (d.CLASSIFIERCODE=''B24'')'), (Row: 56;
    SQL: SECTION2000n2_SQL + B2024_SQL + ISFOREIGN_SQL), (Row: 57; SQL: ''),
    (Row: 59; SQL: ''), (Row: 60;
    SQL: SECTION2000n2_SQL + B2024_SQL + ISMEN_SQL));

  SECTION2000n2_COLCOUNT = 9;

  SECTION2000n2_COLUMNS_SQL: array [1 .. SECTION2000n2_COLCOUNT]
    of PositionedSQL = ((Row: 63; SQL: ' and (hs.REFHIVSTAGETYPES=1)'),
    (Row: 74; SQL: ' and (hs.REFHIVSTAGETYPES=2)'), (Row: 85;
    SQL: ' and (hs.REFHIVSTAGETYPES=3)'), (Row: 96;
    SQL: ' and (hs.REFHIVSTAGETYPES=4)'), (Row: 107;
    SQL: ' and (hs.REFHIVSTAGETYPES=5)'), (Row: 118;
    SQL: ' and (hs.REFHIVSTAGETYPES=6)'), (Row: 129;
    SQL: ' and (hs.REFHIVSTAGETYPES=7)'), (Row: 140;
    SQL: ' and (hs.REFHIVSTAGETYPES=8)'), (Row: 151;
    SQL: ' and (hs.REFHIVSTAGETYPES=9)'));

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_3000n +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  CD4_AND_PCR_SQL = 'select count(distinct p.ID) ' + 'from PERSONS p ' +
    'left join HIVIMMUNOGRAMS him on him.REFPERSONS=p.ID ' +
    'left join HIVPCR hpcr on hpcr.REFPERSONS=p.ID ' +
    'where ((him.ANSWERDATE between :STARTDATE and :ENDDATE) or ' +
    '(hpcr.ANSWERDATE between :STARTDATE and :ENDDATE))';

  CD4_SQL = 'select count(distinct p.ID) ' + 'from HIVIMMUNOGRAMS him ' +
    'inner join PERSONS p on him.REFPERSONS=p.ID ' +
    'where (him.ANSWERDATE between :STARTDATE and :ENDDATE)';

  PCR_SQL = 'select count(distinct p.ID) ' + 'from HIVPCR hpcr ' +
    'inner join PERSONS p on hpcr.REFPERSONS=p.ID ' +
    'where (hpcr.ANSWERDATE between :STARTDATE and :ENDDATE)';

  SECTION3000n_ROWCOUNT = 15;

  SECTION3000n_ROWS_SQL: array [1 .. SECTION3000n_ROWCOUNT] of PositionedSQL =
    ((Row: 9; SQL: CD4_AND_PCR_SQL), (Row: 10; SQL: ''), (Row: 12; SQL: ''),
    (Row: 13; SQL: ''), (Row: 14; SQL: ''), (Row: 15; SQL: ''), (Row: 16;
    SQL: ''), (Row: 17; SQL: ''), (Row: 18; SQL: ''), (Row: 19; SQL: ''),
    (Row: 20; SQL: ''), (Row: 21; SQL: ''), (Row: 22; SQL: CD4_SQL), (Row: 23;
    SQL: PCR_SQL), (Row: 24; SQL: ''));

  SECTION3000n_COLCOUNT = 6;

  SECTION3000n_COLUMNS_SQL: array [1 .. SECTION3000n_COLCOUNT]
    of PositionedSQL = ((Row: 100; SQL: ''), (Row: 111; SQL: AGE_014_SQL),
    (Row: 121; SQL: AGE_1517_SQL), (Row: 131; SQL: ' and p.ID<0'), // ***
    (Row: 142; SQL: AGE_014_SQL + ' and p.ID<0'), // ��� ���-�� �����
    (Row: 152; SQL: AGE_1517_SQL + ' and p.ID<0') // �������������
    );

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_4000n +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  ISTUBERCULOSIS_NEW_SQL =
    ' and ((d.CLASSIFIERCODE=''B20.0'') or (d.CLASSIFIERCODE=''B20.7'') or (d.CLASSIFIERCODE=''B22.7''))';

  AND_ISHEPATITIS_SQL = ' and exists(select hd1.ID ' + 'from HIVDISEASES hd1 ' +
    'inner join DISEASES d1 on hd1.REFDISEASES=d1.ID ' +
    'where (hd1.STARTDATE<=:ENDDATE) and ((hd1.ENDDATE is null) or (hd1.ENDDATE>=:STARTDATE)) and '
    + '((d1.CLASSIFIERCODE starting with ''B15'') or (d1.CLASSIFIERCODE starting with ''B16'') or '
    + '(d1.CLASSIFIERCODE starting with ''B17'') or (d1.CLASSIFIERCODE starting with ''B18'') or '
    + '(d1.CLASSIFIERCODE starting with ''B19'')))';

  AND_ISHEPATITIS_B_SQL = ' and exists(select hd1.ID ' + 'from HIVDISEASES hd1 '
    + 'inner join DISEASES d1 on hd1.REFDISEASES=d1.ID ' +
    'where (hd1.STARTDATE<=:ENDDATE) and ((hd1.ENDDATE is null) or (hd1.ENDDATE>=:STARTDATE)) and '
    + '((d1.CLASSIFIERCODE starting with ''B16'') or (d1.CLASSIFIERCODE=''B18.0'') or '
    + '(d1.CLASSIFIERCODE=''B18.1'')))';

  AND_ISHEPATITIS_C_SQL = ' and exists(select hd1.ID ' + 'from HIVDISEASES hd1 '
    + 'inner join DISEASES d1 on hd1.REFDISEASES=d1.ID ' +
    'where (hd1.STARTDATE<=:ENDDATE) and ((hd1.ENDDATE is null) or (hd1.ENDDATE>=:STARTDATE)) and '
    + '((d1.CLASSIFIERCODE=''B18.0'') or (d1.CLASSIFIERCODE=''B18.1'')))';

  CD4_500_SQL =
    ' and exists(select CD4 from HIVIMMUNOGRAMS where REFPERSONS=p.ID ' +
    'and ANSWERDATE between :STARTDATE and :ENDDATE and CD4 > 500)';
  CD4_351500_SQL =
    ' and exists(select CD4 from HIVIMMUNOGRAMS where REFPERSONS=p.ID ' +
    'and ANSWERDATE between :STARTDATE and :ENDDATE and CD4 between 351 and 500)';
  CD4_350_SQL =
    ' and exists(select CD4 from HIVIMMUNOGRAMS where REFPERSONS=p.ID ' +
    'and ANSWERDATE between :STARTDATE and :ENDDATE and CD4 > 350)';
  CD4_200350_SQL =
    ' and exists(select CD4 from HIVIMMUNOGRAMS where REFPERSONS=p.ID ' +
    'and ANSWERDATE between :STARTDATE and :ENDDATE and CD4 between 200 and 350)';
  CD4_50199_SQL =
    ' and exists(select CD4 from HIVIMMUNOGRAMS where REFPERSONS=p.ID ' +
    'and ANSWERDATE between :STARTDATE and :ENDDATE and CD4 between 50 and 199)';
  CD4_200_SQL =
    ' and exists(select CD4 from HIVIMMUNOGRAMS where REFPERSONS=p.ID ' +
    'and ANSWERDATE between :STARTDATE and :ENDDATE and CD4 < 200)';
  CD4_50_SQL =
    ' and exists(select CD4 from HIVIMMUNOGRAMS where REFPERSONS=p.ID ' +
    'and ANSWERDATE between :STARTDATE and :ENDDATE and CD4 < 50)';

  SECTION4000n_SQL = 'select count(distinct p.ID) ' + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join HIVSTAGES hd on hd.REFPERSONS=p.ID ' +
    'inner join DISEASES d on hd.REFDISEASES=d.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(hd.STARTDATE<=:ENDDATE) and ((hd.ENDDATE is null) or (hd.ENDDATE>=:STARTDATE))';

  SECTION4000n_ROWCOUNT = 18;

  SECTION4000n_ROWS_SQL: array [1 .. SECTION4000n_ROWCOUNT] of PositionedSQL =
    ((Row: 36; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL),
    (Row: 37; SQL: SECTION4000n_SQL + ' and (d.CLASSIFIERCODE=''B20.0'')'),
    (Row: 38; SQL: SECTION4000n_SQL + ' and (d.CLASSIFIERCODE=''B20.7'')'),
    (Row: 39; SQL: SECTION4000n_SQL + ' and (d.CLASSIFIERCODE=''B22.7'')'),
    (Row: 40; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL),
    (Row: 41; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL),
    (Row: 42; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL),
    (Row: 44; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL),
    (Row: 45; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL),
    (Row: 46; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL + CD4_350_SQL),
    (Row: 47; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL + CD4_200350_SQL),
    (Row: 48; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL + CD4_200_SQL),
    (Row: 49; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL + AND_ISHEPATITIS_SQL),
    (Row: 50; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL + AND_ISHEPATITIS_B_SQL),
    (Row: 51; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL + AND_ISHEPATITIS_C_SQL),
    (Row: 52; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL + DEADINPERIOD_SQL),
    (Row: 53; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL + ISMEN_SQL),
    (Row: 54; SQL: SECTION4000n_SQL + ISTUBERCULOSIS_NEW_SQL + ISTOWNSPEOPLE_SQL));

  SECTION4000n_COLCOUNT = 4;

  SECTION4000n_COLUMNS_SQL: array [1 .. SECTION4000n_COLCOUNT]
    of PositionedSQL = ((Row: 94;
    SQL: REGISTEREDINPERIOD_SQL + ISFIRSTTIMEINPERIOD_SQL), (Row: 111;
    SQL: REGISTEREDINPERIOD_SQL + ISFIRSTTIMEINPERIOD_SQL + AGE_2544_SQL),
    (Row: 128; SQL: NOTUNREGISTEREDINPERIOD_SQL), (Row: 145;
    SQL: NOTUNREGISTEREDINPERIOD_SQL + AGE_2544_SQL));

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_5000n +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  PREGNANCY_IN_CURRENT_PERIOD_SQL = ' and (hp.DETECTIONDATE<=:ENDDATE) and ' +
    '((hp.BIRTHDATE is null) or (hp.BIRTHDATE>=:STARTDATE))';

  SECTION5000n_WOMANS_SQL = 'select count(distinct p.ID) ' + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join HIVPREGNANCY hp on hp.REFPERSONS=p.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE))'
    + NOTUNREGISTEREDINPERIOD_SQL;

  SECTION5000n_WOMANSHIVART_SQL = 'select count(distinct p.ID) ' +
    'from HIVART ha ' + 'inner join HIV h on h.REFPERSONS=ha.REFPERSONS ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join HIVPREGNANCY hp on hp.REFPERSONS=p.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and '
    + '(ha.STARTDATE<=:ENDDATE) and ((ha.ENDDATE is null) or (ha.ENDDATE>=:STARTDATE))'
    + NOTUNREGISTEREDINPERIOD_SQL;

  SECTION5000n_CHILDS_SQL = 'select count(distinct p.ID) ' + 'from PERSONS p ' +
    'inner join HIVCONTACTS hc on hc.REFPERSONS2=p.ID and hc.REFHIVCONTACTTYPES=3 '
    + 'inner join HIVPREGNANCY hp on hc.REFPERSONS1=hp.REFPERSONS ' +
    'left join HIV h on h.REFPERSONS=p.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ((h.UNREGISTRYDATE is null) or ' +
    '(h.UNREGISTRYDATE>=:STARTDATE)) and (p.BIRTHDAY<=:STARTDATE)' +
    NOTUNREGISTEREDINPERIOD_SQL;

  SECTION5000n_ROWCOUNT = 19;

  SECTION5000n_ROWS_SQL: array [1 .. SECTION5000n_ROWCOUNT] of PositionedSQL =
    ((Row: 61; SQL: SECTION5000n_WOMANS_SQL + PREGNANCY_IN_CURRENT_PERIOD_SQL),
    (Row: 62; SQL: SECTION5000n_WOMANS_SQL +
    ' and (hp.BIRTHDATE between :STARTDATE and :ENDDATE)'), (Row: 63;
    SQL: SECTION5000n_WOMANS_SQL +
    ' and (hp.REFDRUGS_PREG is not null) or (hp.REFDRUGS_BIRTHS is not null) or (hp.REFDRUGS_CHILD is not null)'),
    (Row: 64; SQL: SECTION5000n_WOMANS_SQL +
    ' and (hp.REFDRUGS_PREG is not null)'), (Row: 65;
    SQL: SECTION5000n_WOMANS_SQL + ' and (hp.REFDRUGS_BIRTHS is not null)'),
    (Row: 66; SQL: SECTION5000n_WOMANS_SQL +
    ' and (hp.REFDRUGS_CHILD is not null)'), (Row: 67;
    SQL: SECTION5000n_WOMANS_SQL +
    ' and (hp.REFDRUGS_PREG is not null) and (hp.REFDRUGS_BIRTHS is not null) and (hp.REFDRUGS_CHILD is not null)'),
    (Row: 68; SQL: SECTION5000n_WOMANSHIVART_SQL), (Row: 69; SQL: ''),
    // ��������
    (Row: 70; SQL: ''), // ��������
    (Row: 71; SQL: ''), (Row: 72; SQL: ''), // ��������
    (Row: 73; SQL: ''), (Row: 74; SQL: ''), (Row: 75;
    SQL: SECTION5000n_CHILDS_SQL), (Row: 76;
    SQL: SECTION5000n_CHILDS_SQL + ' and (h.DETECTIONDATE<=:ENDDATE)'),
    (Row: 77; SQL: ''), (Row: 78; SQL: SECTION3003_BREASTFEEDINGCOUNT_SQL),
    (Row: 79; SQL: SECTION5000n_CHILDS_SQL + WHERE_NOT_COMMITTED));

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // SECTION_6000n +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SECTION6000n_SQL = 'select count(distinct p.ID) ' + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE))'
    + NOTUNREGISTEREDINPERIOD_SQL;

  SECTION6000n_ROWCOUNT = 18;

  SECTION6000n_ROWS_SQL: array [1 .. SECTION6000n_ROWCOUNT] of PositionedSQL =
    ((Row: 7; SQL: SECTION3000_HIVARTONLY_SQL), (Row: 8;
    SQL: SECTION3000_HIVARTONLY_SQL + CD4_500_SQL), (Row: 9;
    SQL: SECTION3000_HIVARTONLY_SQL + CD4_351500_SQL), (Row: 10;
    SQL: SECTION3000_HIVARTONLY_SQL + CD4_200350_SQL), (Row: 11;
    SQL: SECTION3000_HIVARTONLY_SQL + CD4_50199_SQL), (Row: 12;
    SQL: SECTION3000_HIVARTONLY_SQL + CD4_50_SQL), (Row: 13;
    SQL: 'select count(distinct p.id) ' + 'from persons p ' +
    'inner join hivart ha on ha.refpersons=p.id ' +
    ' AND (ha.STARTDATE<=:ENDDATE) and ' +
    '((ha.ENDDATE is null) or (ha.ENDDATE>=:STARTDATE)) ' +
    'left join hiv h on h.refpersons=p.id ' +
    'where exists(select hpc.id from hivpcr hpc ' +
    'where hpc.refpersons=p.id and hpc.takingdate between ' +
    ':StartDate and :EndDate and hpc.isbelowsensivity=1 and ' +
    'not exists (select hpc1.id from hivpcr hpc1 ' +
    'where hpc1.refpersons=hpc.refpersons' +
    ' and hpc1.takingdate>hpc.takingdate' +
    '  and hpc.takingdate between :StartDate and :EndDate))'), (Row: 15;
    SQL: SECTION3000_HIVARTONLY_SQL + ' and (ha.REFHIVARTOUTCOMES=3)'),
    (Row: 16; SQL: SECTION3000_HIVARTONLY_SQL +
    SECTION3000_WHERE_TREATMENTRESTART_SQL), (Row: 17;
    SQL: SECTION3000_DISEASESONLY_SQL + ISTUBERCULOSIS_SQL), (Row: 18;
    SQL: SECTION3000_HIVARTDISEASES_SQL + ISTUBERCULOSIS_SQL), (Row: 19;
    SQL: SECTION3000_DISEASESONLY_SQL + ISTUBERCULOSIS_SQL), (Row: 21; SQL: ''),
    (Row: 22; SQL: SECTION3000_DISEASESONLY_SQL +
    ' and (d.CLASSIFIERCODE=''B59'')'), (Row: 23; SQL: ''), (Row: 24;
    SQL: SECTION3000_DISEASESONLY_SQL + ' and (d.CLASSIFIERCODE=''B18.2'')'),
    (Row: 25; SQL: SECTION3000_TREATMENTS_SQL), (Row: 26;
    SQL: SECTION3000_MULTI_TREATMENTS_SQL));

  SECTION6000n_COLCOUNT = 3;

  SECTION6000n_COLUMNS_SQL: array [1 .. SECTION6000n_COLCOUNT]
    of PositionedSQL = ((Row: 102; SQL: ''), (Row: 124;
    SQL: ISFIRSTTIMEINPERIOD_SQL), (Row: 143;
    SQL: NOTUNREGISTEREDINPERIOD_SQL));

  VISIT_NODISTRICT_STATISTICS_SQL =
    'WITH' + LB +
    '  CTE_VISITS AS' + LB +
    '  (SELECT' + LB +
    '  V.VISITDATE,' + LB +
    '  EXTRACT (DAY FROM V.VISITDATE) VDAY,' + LB +
    '  V.REFPERSONNEL_DOCTOR DOCTOR' + LB +
    ' FROM VISITS V' + LB +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' + LB +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID WHERE' + LB +
    '  V.VISITDATE BETWEEN :STARTDATE AND :ENDDATE' + LB +
    '  AND V.ISVISITHAPPENED=1 ' + LB +
    ' AND V.REFPERSONNEL_DOCTOR=:DOCTOR';

  VISIT_STATISTICS_SQL =
    ' GROUP BY 1,2,3)' + LB +
    ' SELECT' + LB +
    '  CV.VISITDATE,' + LB +
    '  CV.VDAY,' +
  // �����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE' +
    '   AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR AND V.ISVISITHAPPENED=1) OVERAL,' +
  // ����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE  V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND P.ADDRESS_REG NOT LIKE ''% �. %'') RURAL,' +
  // ��������� �� ������������ �����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    ' INNER JOIN HIV ON HIV.REFPERSONS=P.ID AND V.VISITDATE BETWEEN HIV.REGISTRYDATE' +
    '  AND COALESCE(HIV.UNREGISTRYDATE,V.VISITDATE)' +
    '   WHERE  V.VISITDATE=CV.VISITDATE' +
    '  AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR AND V.ISVISITHAPPENED=1) DISP,' +
  // ����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 < 18 ) VIS_CHILD,' +
  // ���� �������� �������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND P.ADDRESS_REG NOT LIKE ''% �. %''' +
    '   AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 < 18 ) VIS_CHILD_RURAL,' +
  // ������ ��������������� ��������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND (P.REFSEX=1 AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 >59.999' +
    '    OR P.REFSEX=2 AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 >54.999)) VIS60,' +
  // ������ ��������������� �������� ��������� �� ������������ �����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    ' INNER JOIN HIV ON HIV.REFPERSONS=P.ID AND V.VISITDATE BETWEEN HIV.REGISTRYDATE' +
    '  AND COALESCE(HIV.UNREGISTRYDATE,V.VISITDATE)' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND (P.REFSEX=1 AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 >59.999' +
    '    OR P.REFSEX=2 AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 >54.999)) VIS60,' +
  // �� ������ �����������
    '  (SELECT COUNT(V.ID) FROM VISITS V ' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND V.REFVISITPURPOSES IN (1)) DIS_OVERAL,' +
  // �� ������ ����������� �������� �������
    '  (SELECT COUNT(V.ID) FROM VISITS V ' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND P.ADDRESS_REG NOT LIKE ''% �. %''' +
    '   AND V.REFVISITPURPOSES IN (1)) DIS_RURAL,' +
  // �� ������ ����������� c�������� �� ������������ �����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    ' INNER JOIN HIV ON HIV.REFPERSONS=P.ID AND V.VISITDATE BETWEEN HIV.REGISTRYDATE' +
    '  AND COALESCE(HIV.UNREGISTRYDATE,V.VISITDATE)' +
    '   WHERE  V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND V.REFVISITPURPOSES IN (1)) DIS_DISP,' +
  // �� ������ ����������� - ����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 < 18' +
    ' AND V.REFVISITPURPOSES IN (1)) DIS_CHILD,' +
  // �� ������ ����������� - ������ ��������������� ��������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '  AND V.REFVISITPURPOSES IN (1)' +
    '   AND (P.REFSEX=1 AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 >59.999' +
    '    OR P.REFSEX=2 AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 >54.999)) DIS60,' +
  // ������������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR ' +
    ' AND V.REFVISITPURPOSES IN (5)) CONSULT,' +
  // ��� 120
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND EXISTS(SELECT R.ID FROM HIVIFAIB R' +
    '  INNER JOIN HIVSCREENINGS HS ON HS.REFHIVIFAIB=R.ID' +
    '  WHERE R.REFPERSONS=P.ID AND HS.REFHIVSCREENINGCODES=120)) CODE_120,' +
  // ���
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND EXISTS(SELECT D.ID FROM DIAGNOSIS D' +
    '  INNER JOIN DISEASES DIS ON D.REFDISEASES=DIS.ID' +
    '  WHERE D.REFTREATMENTCASES=TC.ID AND DIS.CLASSIFIERCODE BETWEEN ''B20'' AND ''B24Z'')) HIV,' +
  // ��� �������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    ' INNER JOIN HIV ON HIV.REFPERSONS=P.ID AND V.VISITDATE BETWEEN HIV.REGISTRYDATE' +
    '  AND COALESCE(HIV.UNREGISTRYDATE,V.VISITDATE)' +
    '  AND EXTRACT(YEAR FROM HIV.REGISTRYDATE)=EXTRACT(YEAR FROM V.VISITDATE)' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND EXISTS(SELECT D.ID FROM DIAGNOSIS D' +
    '  INNER JOIN DISEASES DIS ON D.REFDISEASES=DIS.ID' +
    '  WHERE D.REFTREATMENTCASES=TC.ID AND DIS.CLASSIFIERCODE BETWEEN ''B20'' AND ''B24Z'')) HIV_FIRST,' +
  // ��������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND EXISTS(SELECT D.ID FROM DIAGNOSIS D' +
    '  INNER JOIN DISEASES DIS ON D.REFDISEASES=DIS.ID' +
    '  WHERE D.REFTREATMENTCASES=TC.ID AND DIS.CLASSIFIERCODE BETWEEN ''B15'' AND ''B19Z'')) HEPATIT,' +
  // R75
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND EXISTS(SELECT D.ID FROM DIAGNOSIS D' +
    '  INNER JOIN DISEASES DIS ON D.REFDISEASES=DIS.ID' +
    '  WHERE D.REFTREATMENTCASES=TC.ID AND DIS.CLASSIFIERCODE BETWEEN ''R75'' AND ''R75Z'')) R75,' +
  // ������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND V.REFVISITPURPOSES IN (4)) OTHER,' +
  // ������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.REFPERSONNEL_DOCTOR=CV.DOCTOR ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND TC.REFPAYMENTTYPES=2) BUDGET' +
    '  FROM CTE_VISITS CV';

  ALLVISIT_NODISTRICT_STATISTICS_SQL =
    'WITH' +
    '  CTE_VISITS AS' +
    '  (SELECT' +
    '  V.VISITDATE,' +
    '  EXTRACT (DAY FROM V.VISITDATE) VDAY' +
    ' FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID  WHERE' +
    '  V.VISITDATE BETWEEN :STARTDATE AND :ENDDATE' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' GROUP BY 1,2)' +
    ' SELECT' +
    '  CV.VISITDATE,' +
    '  CV.VDAY,' +
  // �����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE AND V.ISVISITHAPPENED=1) OVERAL,' +
  // ����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE  V.VISITDATE=CV.VISITDATE ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND P.ADDRESS_REG NOT LIKE ''% �. %'') RURAL,' +
  // ��������� �� ������������ �����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    ' INNER JOIN HIV ON HIV.REFPERSONS=P.ID AND V.VISITDATE BETWEEN HIV.REGISTRYDATE' +
    '  AND COALESCE(HIV.UNREGISTRYDATE,V.VISITDATE)' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   WHERE  V.VISITDATE=CV.VISITDATE) DISP,' +
  // ����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 < 18 ) VIS_CHILD,' +
  // ���� �������� �������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND P.ADDRESS_REG NOT LIKE ''% �. %''' +
    '   AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 < 18 ) VIS_CHILD_RURAL,' +
  // ������ ��������������� ��������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND (P.REFSEX=1 AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 >59.999' +
    '    OR P.REFSEX=2 AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 >54.999)) VIS60,' +
  // ������ ��������������� �������� ��������� �� ������������ �����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    ' INNER JOIN HIV ON HIV.REFPERSONS=P.ID AND V.VISITDATE BETWEEN HIV.REGISTRYDATE' +
    '  AND COALESCE(HIV.UNREGISTRYDATE,V.VISITDATE)' +
    '   WHERE V.VISITDATE=CV.VISITDATE' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND (P.REFSEX=1 AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 >59.999' +
    '    OR P.REFSEX=2 AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 >54.999)) VIS60,' +
  // �� ������ �����������
    '  (SELECT COUNT(V.ID) FROM VISITS V ' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND V.REFVISITPURPOSES IN (1)) DIS_OVERAL,' +
  // �� ������ ����������� �������� �������
    '  (SELECT COUNT(V.ID) FROM VISITS V ' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND P.ADDRESS_REG NOT LIKE ''% �. %''' +
    '   AND V.REFVISITPURPOSES IN (1)) DIS_RURAL,' +
  // �� ������ ����������� c�������� �� ������������ �����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    ' INNER JOIN HIV ON HIV.REFPERSONS=P.ID AND V.VISITDATE BETWEEN HIV.REGISTRYDATE' +
    '  AND COALESCE(HIV.UNREGISTRYDATE,V.VISITDATE)' +
    '   WHERE  V.VISITDATE=CV.VISITDATE' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND V.REFVISITPURPOSES IN (1)) DIS_DISP,' +
  // �� ������ ����������� - ����
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '   AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 < 18' +
    ' AND V.REFVISITPURPOSES IN (1)) DIS_CHILD,' +
  // �� ������ ����������� - ������ ��������������� ��������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE' +
    '  AND V.ISVISITHAPPENED=1 ' +
    '  AND V.REFVISITPURPOSES IN (1)' +
    '   AND (P.REFSEX=1 AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 >59.999' +
    '    OR P.REFSEX=2 AND CAST(V.VISITDATE - P.BIRTHDAY AS DOUBLE PRECISION)/365.25 >54.999)) DIS60,' +
  // ������������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND V.REFVISITPURPOSES IN (5)) CONSULT,' +
  // ��� 120
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND EXISTS(SELECT R.ID FROM HIVIFAIB R' +
    '  INNER JOIN HIVSCREENINGS HS ON HS.REFHIVIFAIB=R.ID' +
    '  WHERE R.REFPERSONS=P.ID AND HS.REFHIVSCREENINGCODES=120)) CODE_120,' +
  // ���
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND EXISTS(SELECT D.ID FROM DIAGNOSIS D' +
    '  INNER JOIN DISEASES DIS ON D.REFDISEASES=DIS.ID' +
    '  WHERE D.REFTREATMENTCASES=TC.ID AND DIS.CLASSIFIERCODE BETWEEN ''B20'' AND ''B24Z'')) HIV,' +
  // ��� �������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    ' INNER JOIN HIV ON HIV.REFPERSONS=P.ID AND V.VISITDATE BETWEEN HIV.REGISTRYDATE' +
    '  AND COALESCE(HIV.UNREGISTRYDATE,V.VISITDATE)' +
    '  AND EXTRACT(YEAR FROM HIV.REGISTRYDATE)=EXTRACT(YEAR FROM V.VISITDATE)' +
    '   WHERE V.VISITDATE=CV.VISITDATE ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND EXISTS(SELECT D.ID FROM DIAGNOSIS D' +
    '  INNER JOIN DISEASES DIS ON D.REFDISEASES=DIS.ID' +
    '  WHERE D.REFTREATMENTCASES=TC.ID AND DIS.CLASSIFIERCODE BETWEEN ''B20'' AND ''B24Z'')) HIV_FIRST,' +
  // ��������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND EXISTS(SELECT D.ID FROM DIAGNOSIS D' +
    '  INNER JOIN DISEASES DIS ON D.REFDISEASES=DIS.ID' +
    '  WHERE D.REFTREATMENTCASES=TC.ID AND DIS.CLASSIFIERCODE BETWEEN ''B15'' AND ''B19Z'')) HEPATIT,' +
  // R75
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND EXISTS(SELECT D.ID FROM DIAGNOSIS D' +
    '  INNER JOIN DISEASES DIS ON D.REFDISEASES=DIS.ID' +
    '  WHERE D.REFTREATMENTCASES=TC.ID AND DIS.CLASSIFIERCODE BETWEEN ''R75'' AND ''R75Z'')) R75,' +
  // ������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND V.REFVISITPURPOSES IN (4)) OTHER,' +
  // ������
    '  (SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON V.REFTREATMENTCASES=TC.ID' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID ' +
    '   WHERE V.VISITDATE=CV.VISITDATE ' +
    '  AND V.ISVISITHAPPENED=1 ' +
    ' AND TC.REFPAYMENTTYPES=2) BUDGET' +
    '  FROM CTE_VISITS CV';

  { +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ++++++++++++++++++++++++++++++++ ����� "F4" ++++++++++++++++++++++++++++++++++++
    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ }
procedure PrintHIVStatisticF4(Month, Year: integer; DistrictFilter: String;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
var
  i, j: integer;
  qry: TFIBDataset;
  dd, mm, yy: WORD;
  StartDate, EndDate: TDateTime;
  App, Document, Table, VarData: Variant;
begin
  // ���������� ������ ������ (STARTDATE, ENDDATE) -----------------------------
  if (Month in [1 .. 12]) then
  begin
    yy := Year;
    mm := Month;
    dd := 1;
    StartDate := EncodeDate(yy, mm, dd);
    EndDate := IncMonth(StartDate);
    EndDate := EndDate - 1;
  end
  else
  begin
    yy := Year;
    mm := 1;
    dd := 1;
    StartDate := EncodeDate(yy, mm, dd);
    mm := 12;
    dd := 31;
    EndDate := EncodeDate(yy, mm, dd);
  end;
  // ���������� ������ ---------------------------------------------------------
  App := InitWord(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\F4.dot', '\\reports', '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    Document := App.ActiveDocument;
    SetVariable(Document, 'year', FormatDateTime('yyyy', StartDate));
    if (Month in [1 .. 12]) then
    begin
      SetVariable(Document, 'month', FormatDateTime('mmmm', StartDate));
      SetVariable(Document, 'stat1', '');
      SetVariable(Document, 'stat2', '��������');
      SetVariable(Document, 'stat3', ', �������');
    end
    else
    begin
      SetVariable(Document, 'month', '              ');
      SetVariable(Document, 'stat1', '��������, ');
      SetVariable(Document, 'stat2', '�������');
      SetVariable(Document, 'stat3', '');
    end;
    Table := Document.Tables.Item(7);
    qry := TFIBDataset.Create(nil);
    try
      qry.Transaction := Trans;
      VarData := VarArrayCreate([1, 18, 1, 10], VarVariant);
      for i := 1 to 18 do
        for j := 1 to 10 do
          VarData[i, j] := 0;
      // -----------------------------------------------------------------------
      for i := 1 to F4SECTION1000_ROWCOUNT do
        for j := 1 to F4SECTION1000_COLCOUNT do
        begin
          qry.Active := false;
          qry.SQLs.SelectSQL.Text := F4SECTION1000_COLUMNS_SQL[j].SQL +
            F4SECTION1000_ROWS_SQL[i].SQL + DistrictFilter;
          qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
          qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
          qry.Open;
          VarData[F4SECTION1000_ROWS_SQL[i].Row,
            F4SECTION1000_COLUMNS_SQL[j].Row] := qry.Fields[0].AsInteger;
        end;
      // -----------------------------------------------------------------------
      // �������� ������ ������� �����, ����� WHERE ������� ���������� ����������
      for j := 4 to 10 do
      begin
        VarData[5, j] := VarData[6, j] + VarData[7, j];
        VarData[8, j] := VarData[9, j] + VarData[10, j] + VarData[11, j] +
          VarData[12, j] + VarData[13, j] + VarData[14, j] + VarData[15, j];
        VarData[4, j] := VarData[5, j] + VarData[8, j] + VarData[16, j];
        VarData[18, j] := VarData[4, j] + VarData[17, j];
      end;
      // ������ "�" � �������� ������� -----------------------------------------
      VarData[5, 7] := 'X';
      for i := 6 to 7 do
        for j := 5 to 7 do
          VarData[i, j] := 'X';
      VarData[12, 7] := 'X';
      // ����� ������ �� CONTROLSAMPLES ----------------------------------------
      qry.Active := false;
      qry.SQLs.SelectSQL.Text := 'select sum(coalesce(CONTROLCOUNT, 0)) ' +
        ' + sum(coalesce(DUPLICATESCOUNT, 0)) ' + 'from CONTROLSAMPLES ' +
        'where REFHIVANALYSISTYPES=1 and ' +
        '(ADATE between :STARTDATE and :ENDDATE)';
      qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
      qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
      qry.Open;
      VarData[F4SECTION1000_ROWS_SQL[9].Row, F4SECTION1000_COLUMNS_SQL[5].Row]
        := VarData[F4SECTION1000_ROWS_SQL[9].Row,
        F4SECTION1000_COLUMNS_SQL[5].Row] + qry.Fields[0].AsInteger;
      // ����� � ������� MS WORD -----------------------------------------------
      for i := 4 to 18 do
        for j := 4 to 10 do
          if (VarToStr(VarData[i, j]) <> '0') then
            Table.Cell(i, j).Range.Text := VarToStr(VarData[i, j]);
    finally
      qry.Free;
    end;
    // -------------------------------------------------------------------------
    Document.ActiveWindow.View.ShowFieldCodes := false;
    Document.Fields.Update;
    Document.Application.Selection.HomeKey(6);
    Document.Saved := true;
    Document := UnAssigned;
  finally
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

{ +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++ ����� "F61" +++++++++++++++++++++++++++++++++++
  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ }
procedure PrintHIVStatisticF61(Year: integer; DistrictFilter: String;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
var
  i, j: integer;
  dd, mm, yy: WORD;
  qry: TFIBDataset;
  App, WS: Variant;
  StartDate, EndDate: TDateTime;
  // ---------------------------------------------------------------------------
begin
  yy := Year;
  mm := 1;
  dd := 1;
  StartDate := EncodeDate(yy, mm, dd);
  mm := 12;
  dd := 31;
  EndDate := EncodeDate(yy, mm, dd);
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\F61.xlt', '\\reports', '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Range['BY8'] := FormatDateTime('yy', EndDate);
    qry := TFIBDataset.Create(nil);
    try
      qry.Transaction := Trans;
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_1000 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      WS := App.ActiveWorkbook.Worksheets.Item[2];
      for i := 1 to SECTION1000_ROWCOUNT do
        for j := 1 to SECTION1000_COLCOUNT do
        begin
          qry.Active := false;
          qry.SQLs.SelectSQL.Text := SECTION1000_SQL + SECTION1000_ROWS_SQL[i]
            .SQL + SECTION1000_COLUMNS_SQL[j].SQL + DistrictFilter;
          qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
          qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
          qry.Open;
          if qry.Fields[0].AsInteger > 0 then
            WS.Cells.Item[SECTION1000_ROWS_SQL[i].Row,
              SECTION1000_COLUMNS_SQL[j].Row] := qry.Fields[0].AsInteger;
        end;
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_1001 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      WS := App.ActiveWorkbook.Worksheets.Item[3];
      for i := 1 to SECTION1001_COUNT do
      begin
        qry.Active := false;
        qry.SQLs.SelectSQL.Text := SECTION1001_SQLS[i, 2] + DistrictFilter;
        qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
        qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
        qry.Open;
        if qry.Fields[0].AsInteger > 0 then
          WS.Range[SECTION1001_SQLS[i, 1]] := qry.Fields[0].AsInteger;
      end;
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_2000 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      for i := 1 to SECTION2000_ROWCOUNT do
        for j := 1 to SECTION2000_COLCOUNT do
        begin
          if (i in [6, 7, 9, 10, 13, 14, 16, 17, 20]) and (j in [5, 6, 7, 8])
          then
            WS.Cells.Item[SECTION2000_ROWS_SQL[i].Row,
              SECTION2000_COLUMNS_SQL[j].Row] := 'x'
          else
          begin
            qry.Active := false;
            qry.SQLs.SelectSQL.Text := SECTION2000_SQL + SECTION2000_ROWS_SQL[i]
              .SQL + SECTION2000_COLUMNS_SQL[j].SQL + DistrictFilter;
            qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
            qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
            qry.Open;
            if qry.Fields[0].AsInteger > 0 then
              WS.Cells.Item[SECTION2000_ROWS_SQL[i].Row,
                SECTION2000_COLUMNS_SQL[j].Row] := qry.Fields[0].AsInteger;
          end;
        end;
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_2001_2004 +++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      for i := 1 to SECTION2001_2004_COUNT do
      begin
        qry.Active := false;
        qry.SQLs.SelectSQL.Text := SECTION2001_2004_SQLS[i, 2] + DistrictFilter;
        qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
        qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
        qry.Open;
        if qry.Fields[0].AsInteger > 0 then
          WS.Range[SECTION2001_2004_SQLS[i, 1]] := qry.Fields[0].AsInteger;
      end;
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_3000 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      for i := 1 to SECTION3000_ROWCOUNT do
        for j := 1 to SECTION3000_COLCOUNT do
        begin
          qry.Active := false;
          qry.SQLs.SelectSQL.Text := SECTION3000_ROWS_SQL[i].SQL +
            SECTION3000_COLUMNS_SQL[j].SQL + DistrictFilter;
          qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
          qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
          qry.Open;
          if qry.Fields[0].AsInteger > 0 then
            WS.Cells.Item[SECTION3000_ROWS_SQL[i].Row,
              SECTION3000_COLUMNS_SQL[j].Row] := qry.Fields[0].AsInteger;
        end;
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_3001-3006 +++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      for i := 1 to SECTION3001_3006_COUNT do
      begin
        qry.Active := false;
        qry.SQLs.SelectSQL.Text := SECTION3001_3006_SQLS[i, 2] + DistrictFilter;
        qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
        qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
        qry.Open;
        if qry.Fields[0].AsInteger > 0 then
          WS.Range[SECTION3001_3006_SQLS[i, 1]] := qry.Fields[0].AsInteger;
      end;
    finally
      qry.Free;
    end;
    App.ActiveWorkbook.Saved := true;
  finally
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

{ +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  +++++++++++++++++++++++++++ ����� "F61" (�����) +++++++++++++++++++++++++++++++
  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ }
procedure PrintHIVStatisticF61_new(Year: integer; DistrictFilter: String;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
var
  i, j: integer;
  dd, mm, yy: WORD;
  qry: TFIBDataset;
  App, WS: Variant;
  StartDate, EndDate: TDateTime;
  // ---------------------------------------------------------------------------
begin
  yy := Year;
  mm := 1;
  dd := 1;
  StartDate := EncodeDate(yy, mm, dd);
  mm := 12;
  dd := 31;
  EndDate := EncodeDate(yy, mm, dd);
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\F61_new.xlt', '\\reports', '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Range['CA12'] := FormatDateTime('yy', EndDate);
    qry := TFIBDataset.Create(nil);
    try
      qry.Transaction := Trans;
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_1000n +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      WS := App.ActiveWorkbook.Worksheets.Item[2];
      for i := 1 to SECTION1000n_ROWCOUNT do
        if (SECTION1000n_ROWS_SQL[i].SQL <> '') then
          for j := 1 to SECTION1000n_COLCOUNT do
          begin
            qry.Active := false;
            qry.SQLs.SelectSQL.Text := SECTION1000n_ROWS_SQL[i].SQL +
              SECTION1000n_COLUMNS_SQL[j].SQL + DistrictFilter;
            qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
            qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
            qry.Open;
            if qry.Fields[0].AsInteger > 0 then
            begin
              WS.Cells.Item[SECTION1000n_ROWS_SQL[i].Row,
                SECTION1000n_COLUMNS_SQL[j].Row] := qry.Fields[0].AsInteger;
            end;
          end;
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_2000n1 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      WS := App.ActiveWorkbook.Worksheets.Item[3];
      for i := 1 to SECTION2000n1_ROWCOUNT do
        if (SECTION2000n1_ROWS_SQL[i].SQL <> '') then
          for j := 1 to SECTION2000n1_COLCOUNT do
          begin
            qry.Active := false;
            qry.SQLs.SelectSQL.Text := SECTION2000n1_ROWS_SQL[i].SQL +
              SECTION2000n1_COLUMNS_SQL[j].SQL + DistrictFilter;
            qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
            qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
            qry.Open;
            if qry.Fields[0].AsInteger > 0 then
            begin
              WS.Cells.Item[SECTION2000n1_ROWS_SQL[i].Row,
                SECTION2000n1_COLUMNS_SQL[j].Row] := qry.Fields[0].AsInteger;
            end;
          end;
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_2000n2 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      WS := App.ActiveWorkbook.Worksheets.Item[3];
      for i := 1 to SECTION2000n2_ROWCOUNT do
        if (SECTION2000n2_ROWS_SQL[i].SQL <> '') then
          for j := 1 to SECTION2000n2_COLCOUNT do
          begin
            qry.Active := false;
            qry.SQLs.SelectSQL.Text := SECTION2000n2_ROWS_SQL[i].SQL +
              SECTION2000n2_COLUMNS_SQL[j].SQL + DistrictFilter;
            qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
            qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
            qry.Open;
            if qry.Fields[0].AsInteger > 0 then
            begin
              WS.Cells.Item[SECTION2000n2_ROWS_SQL[i].Row,
                SECTION2000n2_COLUMNS_SQL[j].Row] := qry.Fields[0].AsInteger;
            end;
          end;
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_3000n +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      WS := App.ActiveWorkbook.Worksheets.Item[4];
      for i := 1 to SECTION3000n_ROWCOUNT do
        if (SECTION3000n_ROWS_SQL[i].SQL <> '') then
          for j := 1 to SECTION3000n_COLCOUNT do
          begin
            qry.Active := false;
            qry.SQLs.SelectSQL.Text := SECTION3000n_ROWS_SQL[i].SQL +
              SECTION3000n_COLUMNS_SQL[j].SQL + DistrictFilter;
            qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
            qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
            qry.Open;
            if qry.Fields[0].AsInteger > 0 then
            begin
              WS.Cells.Item[SECTION3000n_ROWS_SQL[i].Row,
                SECTION3000n_COLUMNS_SQL[j].Row] := qry.Fields[0].AsInteger;
            end;
          end;
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_4000n +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      WS := App.ActiveWorkbook.Worksheets.Item[4];
      for i := 1 to SECTION4000n_ROWCOUNT do
        if (SECTION4000n_ROWS_SQL[i].SQL <> '') then
          for j := 1 to SECTION4000n_COLCOUNT do
          begin
            qry.Active := false;
            qry.SQLs.SelectSQL.Text := SECTION4000n_ROWS_SQL[i].SQL +
              SECTION4000n_COLUMNS_SQL[j].SQL + DistrictFilter;
            qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
            qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
            qry.Open;
            if qry.Fields[0].AsInteger > 0 then
            begin
              WS.Cells.Item[SECTION4000n_ROWS_SQL[i].Row,
                SECTION4000n_COLUMNS_SQL[j].Row] := qry.Fields[0].AsInteger;
            end;
          end;
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_5000n +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      WS := App.ActiveWorkbook.Worksheets.Item[4];
      for i := 1 to SECTION5000n_ROWCOUNT do
        if (SECTION5000n_ROWS_SQL[i].SQL <> '') then
        begin
          qry.Active := false;
          qry.SQLs.SelectSQL.Text := SECTION5000n_ROWS_SQL[i].SQL +
            DistrictFilter;
          qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
          qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
          qry.Open;
          if qry.Fields[0].AsInteger > 0 then
          begin
            WS.Cells.Item[SECTION5000n_ROWS_SQL[i].Row, 144] :=
              qry.Fields[0].AsInteger;
          end;
        end;

      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // SECTION_6000n +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      WS := App.ActiveWorkbook.Worksheets.Item[5];
      for i := 1 to SECTION6000n_ROWCOUNT do
        if (SECTION6000n_ROWS_SQL[i].SQL <> '') then
          for j := 1 to SECTION6000n_COLCOUNT do
          begin
            qry.Active := false;
            qry.SQLs.SelectSQL.Text := SECTION6000n_ROWS_SQL[i].SQL +
              SECTION6000n_COLUMNS_SQL[j].SQL + DistrictFilter;
            qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
            qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
            qry.Open;
            if qry.Fields[0].AsInteger > 0 then
            begin
              WS.Cells.Item[SECTION6000n_ROWS_SQL[i].Row,
                SECTION6000n_COLUMNS_SQL[j].Row] := qry.Fields[0].AsInteger;
            end;
          end;
    finally
      qry.Free;
    end;
    App.ActiveWorkbook.Saved := true;
  finally
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure PrintHIVDetectionAndTreatmentRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction);
const
  HIV_NEW_INFECTED_COUNT = 'SELECT' + '  COUNT(*)' + ' FROM' + '  PERSONS P ' +
    '  LEFT JOIN HIV H ON H.REFPERSONS=P.ID' +
    '  LEFT JOIN PLACESHISTORY PH ON PH.ID = (SELECT FIRST 1' +
    ' PH1.ID FROM PLACESHISTORY PH1 WHERE PH1.REFPERSONS=P.ID ' +
    '      AND PH1.STARTDATE<=:ENDDATE AND (PH1.ENDDATE>=:STARTDATE' +
    ' OR PH1.ENDDATE IS NULL) ORDER BY PH1.STARTDATE DESC)' + ' WHERE' +
    '  H.DETECTIONDATE BETWEEN :STARTDATE AND :ENDDATE';

  HIV_ON_TREATMENT_COUNT = 'SELECT' + '  COUNT(DISTINCT P.ID)' + ' FROM' +
    '  PERSONS P ' + '  INNER JOIN HIVART HA ON HA.REFPERSONS=P.ID' +
    '  LEFT JOIN PLACESHISTORY PH ON PH.ID = (SELECT FIRST' +
    ' 1 PH1.ID FROM PLACESHISTORY PH1 WHERE PH1.REFPERSONS=P.ID ' +
    '      AND PH1.STARTDATE<=:ENDDATE' +
    ' AND (PH1.ENDDATE>=:STARTDATE OR PH1.ENDDATE IS NULL)' +
    ' ORDER BY PH1.STARTDATE DESC)' + ' WHERE' +
    '  HA.STARTDATE<=:ENDDATE AND (HA.ENDDATE>=:STARTDATE OR HA.ENDDATE IS NULL)';

  HIV_ON_TREATMENT_LIST = 'SELECT' + '  DISTINCT P.NAME, P.BIRTHDAY, D.NAME' +
    ' FROM' + '  PERSONS P ' + '  INNER JOIN HIVART HA ON HA.REFPERSONS=P.ID' +
    '  LEFT JOIN HIV H ON H.REFPERSONS=P.ID' +
    '  LEFT JOIN PLACESHISTORY PH ON PH.ID = (SELECT FIRST 1 PH1.ID FROM PLACESHISTORY PH1 WHERE PH1.REFPERSONS=P.ID '
    + '      AND PH1.STARTDATE<=:ENDDATE AND (PH1.ENDDATE>=:STARTDATE OR PH1.ENDDATE IS NULL) ORDER BY PH1.STARTDATE DESC)'
    + '  LEFT JOIN DISTRICTS D ON PH.REFDISTRICTS=D.ID' + ' WHERE' +
    '  HA.STARTDATE<=:ENDDATE AND (HA.ENDDATE>=:STARTDATE OR HA.ENDDATE IS NULL) ORDER BY D.NAME';

  HIV_START_TREATMENT_LIST = 'SELECT' +
    '  DISTINCT P.NAME, P.BIRTHDAY, D.NAME, HA.STARTDATE, HA.STARTREASON' +
    ' FROM' +
    '  PERSONS P ' +
    '  INNER JOIN HIVART HA ON HA.REFPERSONS=P.ID' +
    '  LEFT JOIN HIV H ON H.REFPERSONS=P.ID' +
    '  LEFT JOIN PLACESHISTORY PH ON PH.ID = (SELECT FIRST 1 PH1.ID FROM PLACESHISTORY PH1 WHERE PH1.REFPERSONS=P.ID '
    + '      AND PH1.STARTDATE<=:ENDDATE AND (PH1.ENDDATE>=:STARTDATE OR PH1.ENDDATE IS NULL) ORDER BY PH1.STARTDATE DESC)'
    + '  LEFT JOIN DISTRICTS D ON PH.REFDISTRICTS=D.ID' +
    ' WHERE' +
    '  NOT EXISTS(SELECT HA1.ID FROM HIVART HA1 WHERE HA1.REFPERSONS=P.ID' +
    '     AND HA1.ENDDATE BETWEEN (HA.STARTDATE-60) AND HA.STARTDATE)' +
    '  AND HA.STARTDATE BETWEEN :STARTDATE AND :ENDDATE ORDER BY D.NAME';

  HIV_FINISH_TREATMENT_LIST = 'SELECT' +
    '  DISTINCT P.NAME, P.BIRTHDAY, D.NAME, HA.ENDDATE, HA.ENDREASON' + ' FROM'
    + '  PERSONS P ' + '  INNER JOIN HIVART HA ON HA.REFPERSONS=P.ID' +
    '  LEFT JOIN HIV H ON H.REFPERSONS=P.ID' +
    '  LEFT JOIN PLACESHISTORY PH ON PH.ID = (SELECT FIRST 1 PH1.ID FROM PLACESHISTORY PH1 WHERE PH1.REFPERSONS=P.ID '
    + '      AND PH1.STARTDATE<=:ENDDATE AND (PH1.ENDDATE>=:STARTDATE OR PH1.ENDDATE IS NULL) ORDER BY PH1.STARTDATE DESC)'
    + '  LEFT JOIN DISTRICTS D ON PH.REFDISTRICTS=D.ID' +
    ' WHERE' +
    '  COALESCE(HA.REFHIVARTOUTCOMES,0)<>5' +
    '  AND HA.ENDDATE BETWEEN :STARTDATE AND :ENDDATE ORDER BY D.NAME';

  HIV_IFA_COUNT = 'SELECT' + '  COUNT(DISTINCT P.ID)' + ' FROM' +
    '  HIVIFAIB IFA' + '  INNER JOIN PERSONS P ON IFA.REFPERSONS=P.ID' +
    '  LEFT JOIN PLACESHISTORY PH ON PH.REFPERSONS=P.ID ' +
    '      AND :ENDDATE BETWEEN PH.STARTDATE AND COALESCE(PH.ENDDATE,:ENDDATE)'
    + ' WHERE' + '  IFA.MAKINGDATE BETWEEN :STARTDATE AND :ENDDATE';

  REGIONS_SQL: array [1 .. 26] of PositionedSQL = ((Row: 13; SQL: ''), (Row: 14;
    SQL: ' AND PH.REFDISTRICTS=1'), (Row: 15; SQL: ' AND PH.REFDISTRICTS=2'),
    (Row: 16; SQL: ' AND PH.REFDISTRICTS=3'), (Row: 17;
    SQL: ' AND PH.REFDISTRICTS=4'), (Row: 18; SQL: ' AND PH.REFDISTRICTS=5'),
    (Row: 19; SQL: ' AND PH.REFDISTRICTS=6'), (Row: 20;
    SQL: ' AND PH.REFDISTRICTS=7'), (Row: 21; SQL: ' AND PH.REFDISTRICTS=8'),
    (Row: 22; SQL: ' AND PH.REFDISTRICTS=9'), (Row: 23;
    SQL: ' AND PH.REFDISTRICTS=10'), (Row: 24; SQL: ' AND PH.REFDISTRICTS=11'),
    (Row: 25; SQL: ' AND PH.REFDISTRICTS=12'), (Row: 26;
    SQL: ' AND PH.REFDISTRICTS=13'), (Row: 27; SQL: ' AND PH.REFDISTRICTS=14'),
    (Row: 28; SQL: ' AND PH.REFDISTRICTS=15'), (Row: 29;
    SQL: ' AND PH.REFDISTRICTS=16'), (Row: 30; SQL: ' AND PH.REFDISTRICTS=17'),
    (Row: 31; SQL: ' AND PH.REFDISTRICTS=18'), (Row: 32;
    SQL: ' AND PH.REFDISTRICTS=19'), (Row: 33; SQL: ' AND PH.REFDISTRICTS=20'),
    (Row: 34; SQL: ' AND PH.REFDISTRICTS=21'), (Row: 35;
    SQL: ' AND PH.REFDISTRICTS=22'), (Row: 37; SQL: ' AND PH.REFDISTRICTS=23'),
    (Row: 38; SQL: ' AND PH.REFDISTRICTS=24'), (Row: 39;
    SQL: ' AND PH.REFDISTRICTS=25'));
Var
  App, WS: Variant;
  i: integer;
  qry: TFIBQuery;
begin;
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\����� �� ������� ��������.xlt', '\\reports', '\reports', []));
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    for i := 1 to 26 do
    begin;
      WS.Range['C' + IntToStr(REGIONS_SQL[i].Row)] :=
        Trans.DefaultDatabase.QueryValue(HIV_NEW_INFECTED_COUNT + REGIONS_SQL[i]
        .SQL, 0, [EndDate, StartDate]);
      WS.Range['D' + IntToStr(REGIONS_SQL[i].Row)] :=
        Trans.DefaultDatabase.QueryValue(HIV_ON_TREATMENT_COUNT + REGIONS_SQL[i]
        .SQL, 0, [EndDate, StartDate]);
    end;
    WS := App.ActiveWorkbook.Worksheets.Item[2];
    qry := TFIBQuery.Create(nil);
    try
      qry.Transaction := Trans;
      qry.SQL.Text := HIV_ON_TREATMENT_LIST;
      qry.ParamByName('STARTDATE').AsDateTime := StartDate;
      qry.ParamByName('ENDDATE').AsDateTime := EndDate;
      qry.ExecQuery;
      i := 1;
      if not qry.Eof then
        repeat
          inc(i);
          WS.Range['A' + IntToStr(i)] := qry.Fields[0].asString;
          WS.Range['B' + IntToStr(i)] := qry.Fields[1].asString;
          WS.Range['C' + IntToStr(i)] := qry.Fields[2].asString;
          qry.Next;
        until qry.Eof;
      qry.Close;
      WS := App.ActiveWorkbook.Worksheets.Item[3];
      qry.SQL.Text := HIV_START_TREATMENT_LIST;
      qry.ParamByName('STARTDATE').AsDateTime := StartDate;
      qry.ParamByName('ENDDATE').AsDateTime := EndDate;
      qry.ExecQuery;
      i := 1;
      if not qry.Eof then
        repeat
          inc(i);
          WS.Range['A' + IntToStr(i)] := qry.Fields[0].asString;
          WS.Range['B' + IntToStr(i)] := qry.Fields[1].asString;
          WS.Range['C' + IntToStr(i)] := qry.Fields[2].asString;
          WS.Range['D' + IntToStr(i)] := qry.Fields[3].asString;
          WS.Range['E' + IntToStr(i)] := qry.Fields[4].asString;
          qry.Next;
        until qry.Eof;
      qry.Close;
      WS := App.ActiveWorkbook.Worksheets.Item[4];
      qry.SQL.Text := HIV_FINISH_TREATMENT_LIST;
      qry.ParamByName('STARTDATE').AsDateTime := StartDate;
      qry.ParamByName('ENDDATE').AsDateTime := EndDate;
      qry.ExecQuery;
      i := 1;
      if not qry.Eof then
        repeat
          inc(i);
          WS.Range['A' + IntToStr(i)] := qry.Fields[0].asString;
          WS.Range['B' + IntToStr(i)] := qry.Fields[1].asString;
          WS.Range['C' + IntToStr(i)] := qry.Fields[2].asString;
          WS.Range['D' + IntToStr(i)] := qry.Fields[3].asString;
          WS.Range['E' + IntToStr(i)] := qry.Fields[4].asString;
          qry.Next;
        until qry.Eof;
      qry.Close;
    finally
      qry.Free;
    end;

  finally
    App.Visible := true;
    SetForegroundWindow(App.Hwnd);
  end;
end;

procedure PrintIfaIbRpt(StartDate, EndDate: TDateTime; DistrictFilter: String;
  Trans: TFIBTransaction);
const
  HIV_BY_CODE_STAT = 'SELECT' + '  HSC.CODE,COUNT(DISTINCT R.ID)' + ' FROM' +
    '  HIVIFAIB R ' + '  INNER JOIN PERSONS P ON R.REFPERSONS=P.ID' +
    '  INNER JOIN HIVSCREENINGS HS ON HS.REFHIVIFAIB=R.ID' +
    '  INNER JOIN HIVSCREENINGCODES HSC ON HS.REFHIVSCREENINGCODES=HSC.ID' +
    ' WHERE' + '  R.TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE';

  HIV_BY_ORG_STAT = 'SELECT' + '  O.NAME,COUNT(DISTINCT R.ID)' + ' FROM' +
    '  HIVIFAIB R ' + '  INNER JOIN PERSONS P ON R.REFPERSONS=P.ID' +
    '  LEFT JOIN ORGANIZATIONS O ON R.REFORGANIZATIONS=O.ID' + ' WHERE' +
    '  R.TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE';

  HIV_BY_ORG_AND_CODE_STAT = 'SELECT' + '  O.NAME,HSC.CODE,COUNT(DISTINCT R.ID)'
    + ' FROM' + '  HIVIFAIB R ' + '  INNER JOIN PERSONS P ON R.REFPERSONS=P.ID'
    + '  LEFT JOIN HIVSCREENINGS HS ON HS.REFHIVIFAIB=R.ID' +
    '  LEFT JOIN HIVSCREENINGCODES HSC ON HS.REFHIVSCREENINGCODES=HSC.ID' +
    '  LEFT JOIN ORGANIZATIONS O ON R.REFORGANIZATIONS=O.ID' + ' WHERE' +
    '  R.TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE';

  IFA_PATIENT_LIST = 'SELECT ' + '  R.TAKINGDATE,O.NAME,HSC.CODE,P.NAME' +
    ' FROM' + '  HIVIFAIB R ' + '  INNER JOIN PERSONS P ON R.REFPERSONS=P.ID' +
    '  LEFT JOIN HIVSCREENINGS HS ON HS.REFHIVIFAIB=R.ID' +
    '  LEFT JOIN HIVSCREENINGCODES HSC ON HS.REFHIVSCREENINGCODES=HSC.ID' +
    '  LEFT JOIN ORGANIZATIONS O ON R.REFORGANIZATIONS=O.ID' + ' WHERE' +
    '  R.TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE';

Var
  App, WS: Variant;
  i, j: integer;
  qry: TFIBQuery;
begin;
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\����� �� ����������.xlt', '\\reports', '\reports', []));
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    qry := TFIBQuery.Create(nil);
    try
      i := 1;
      qry.Transaction := Trans;
      qry.SQL.Text := HIV_BY_CODE_STAT + DistrictFilter + ' GROUP BY 1';
      qry.ParamByName('STARTDATE').AsDateTime := StartDate;
      qry.ParamByName('ENDDATE').AsDateTime := EndDate;
      qry.ExecQuery;
      if not qry.Eof then
      begin;
        WS.Range['A' + IntToStr(i)] := '���������� ������������ �� �����';
        inc(i);
        WS.Range['A' + IntToStr(i)] := '���';
        WS.Range['B' + IntToStr(i)] := '���-�� ������������';
        j := i;
        repeat
          inc(i);
          WS.Range['A' + IntToStr(i)] := qry.Fields[0].asString;
          WS.Range['B' + IntToStr(i)] := qry.Fields[1].asString;
          qry.Next;
        until qry.Eof;
        WS.Range['A' + IntToStr(j), 'B' + IntToStr(i)].Borders.Linestyle :=
          xlContinuous;
      end;
      qry.Close;

      qry.SQL.Text := HIV_BY_ORG_STAT + DistrictFilter + ' GROUP BY 1';
      qry.ParamByName('STARTDATE').AsDateTime := StartDate;
      qry.ParamByName('ENDDATE').AsDateTime := EndDate;
      qry.ExecQuery;
      if not qry.Eof then
      begin;
        inc(i, 2);
        WS.Range['A' + IntToStr(i)] :=
          '���������� ������������ �� ������������';
        inc(i);
        WS.Range['A' + IntToStr(i)] := '�����������';
        WS.Range['B' + IntToStr(i)] := '���-�� ������������';
        j := i;
        repeat
          inc(i);
          WS.Range['A' + IntToStr(i)] := qry.Fields[0].asString;
          WS.Range['B' + IntToStr(i)] := qry.Fields[1].asString;
          qry.Next;
        until qry.Eof;
        WS.Range['A' + IntToStr(j), 'B' + IntToStr(i)].Borders.Linestyle :=
          xlContinuous;
      end;
      qry.Close;

      qry.SQL.Text := HIV_BY_ORG_AND_CODE_STAT + DistrictFilter +
        ' GROUP BY 1,2';
      qry.ParamByName('STARTDATE').AsDateTime := StartDate;
      qry.ParamByName('ENDDATE').AsDateTime := EndDate;
      qry.ExecQuery;
      if not qry.Eof then
      begin;
        inc(i, 2);
        WS.Range['A' + IntToStr(i)] :=
          '���������� ������������ �� ������������ � �����';
        inc(i);
        WS.Range['A' + IntToStr(i)] := '�����������';
        WS.Range['B' + IntToStr(i)] := '���';
        WS.Range['C' + IntToStr(i)] := '���-�� ������������';
        j := i;
        repeat
          inc(i);
          WS.Range['A' + IntToStr(i)] := qry.Fields[0].asString;
          WS.Range['B' + IntToStr(i)] := qry.Fields[1].asString;
          WS.Range['C' + IntToStr(i)] := qry.Fields[2].asString;
          qry.Next;
        until qry.Eof;
        WS.Range['A' + IntToStr(j), 'C' + IntToStr(i)].Borders.Linestyle :=
          xlContinuous;
      end;
      qry.Close;

      WS := App.ActiveWorkbook.Worksheets.Item[2];
      qry.SQL.Text := IFA_PATIENT_LIST + DistrictFilter + ' ORDER BY 1,2,3';
      qry.ParamByName('STARTDATE').AsDateTime := StartDate;
      qry.ParamByName('ENDDATE').AsDateTime := EndDate;
      qry.ExecQuery;
      if not qry.Eof then
      begin;
        i := 1;
        repeat
          inc(i);
          WS.Range['A' + IntToStr(i)] := qry.Fields[0].asString;
          WS.Range['B' + IntToStr(i)] := qry.Fields[1].asString;
          WS.Range['C' + IntToStr(i)] := qry.Fields[2].asString;
          WS.Range['D' + IntToStr(i)] := qry.Fields[3].asString;
          qry.Next;
        until qry.Eof;
        WS.Range['A1', 'D' + IntToStr(i)].Borders.Linestyle := xlContinuous;
      end;
    finally
      qry.Free;
    end;
  finally
    App.Visible := true;
    SetForegroundWindow(App.Hwnd);
  end;
end;

procedure PrintLaboratoryTestIFARpt(StartDate, EndDate: TDateTime;
  DistrictFilter: String; Trans: TFIBTransaction);
const
  HIV_IFAIB = 'select hifa.TAKINGDATE, count(distinct hifa.ID) ' +
    'from HIVIFAIB hifa ' + 'inner join PERSONS p on hifa.REFPERSONS=p.ID ' +
    'inner join HIVSCREENINGS hscr on hifa.ID=hscr.REFHIVIFAIB ' +
    'inner join HIVSCREENINGCODES hscc on hscr.REFHIVSCREENINGCODES=hscc.ID ' +
    'where (hifa.TAKINGDATE between :STARTDATE and :ENDDATE) and ' +
    'hifa.REFORGANIZATIONS=:REFORGANIZATIONS';
  HSCODES_SQL: array [1 .. 11] of PositionedSQL = ((Row: 1;
    SQL: ' and hscc.CODE=''102'''), (Row: 2; SQL: ' and hscc.CODE=''118'''),
    (Row: 3; SQL: ' and hscc.CODE=''113'''), (Row: 4;
    SQL: ' and hscc.CODE=''109'''), (Row: 5; SQL: ' and hscc.CODE=''104'''),
    (Row: 6; SQL: ' and hscc.CODE=''108'''), (Row: 7;
    SQL: ' and hscc.CODE=''112'''), (Row: 8; SQL: ' and hscc.CODE=''115'''),
    (Row: 9; SQL: ' and hscc.CODE=''103'''), (Row: 10;
    SQL: ' and hscc.CODE=''120'''), (Row: 11; SQL: ' and hscc.CODE=''200'''));
  LIST12: array [1 .. 11] of PositionedSQL = ((Row: 2; SQL: '''102'''),
    // Row - ����� ������� �� ����� 12
    (Row: 10; SQL: '''118'''), // SQL - �������� ����� � ''
    (Row: 8; SQL: '''113'''), (Row: 6; SQL: '''109'''), (Row: 4;
    SQL: '''104'''), (Row: 5; SQL: '''108'''), (Row: 7; SQL: '''112'''),
    (Row: 9; SQL: '''115'''), (Row: 3; SQL: '''103'''), (Row: 11;
    SQL: '''120'''), (Row: 12; SQL: '''200'''));
var
  App, WS, WB, VarData: Variant;
  qry, qry1, qry2: TFIBDataset;
  cDate: TDateTime;
  i, j, sh, cc, rc: integer;
  icol, mcol, mn, iRow: integer;
  SumColumns: array of integer;
  Formula: String;
begin;
  if EndDate < StartDate then
    exit;
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\����� �� �������������.xlt', '\\reports', '\reports', []));
  try
    cc := Round(EndDate - StartDate) + 0;
    WB := App.ActiveWorkbook;
    for sh := 1 to 11 do
    begin
      Formula := '';
      SetLength(SumColumns, 0);
      WS := App.ActiveWorkbook.Worksheets.Item[sh];
      // ���������� ������������ ���������� ��������, ������� ������ -----------
      if cc > 2 then
        WS.Range['C2:C2'].Resize[, cc - 2].EntireColumn.Insert;
      // ���������� �������� (����, �����, ���) --------------------------------
      if cc > 0 then
      begin
        cDate := StartDate;
        mn := MonthOf(cDate);
        icol := 2;
        mcol := 2;
        repeat
          if mn <> MonthOf(cDate) then
          begin
            if mcol <> (icol - 1) then
            begin
              WS.Range[WS.Cells.Item[1, mcol], WS.Cells.Item[1, icol - 1]]
                .Cells.MergeCells := true;
              WS.Cells.Item[1, mcol] := FormatDateTime('mmmm yyyy�.',
                cDate - 1);
            end;
            mn := MonthOf(cDate);
            mcol := icol;
          end;
          WS.Cells.Item[2, icol] := DayOf(cDate);
          cDate := cDate + 1;
          inc(icol);
          Application.ProcessMessages;
        until cDate >= (EndDate);
        if mcol < (icol - 1) then
        begin
          WS.Range[WS.Cells.Item[1, mcol], WS.Cells.Item[1, icol - 1]]
            .Cells.MergeCells := true;
          WS.Cells.Item[1, mcol] := FormatDateTime('mmmm yyyy�.', cDate - 1);
        end;
      end;
      // ����� ������ ����������� � ���������� ������ --------------------------
      qry := TFIBDataset.Create(nil); // ������ ����� �����������
      qry1 := TFIBDataset.Create(nil); // ������ �����������
      qry2 := TFIBDataset.Create(nil); // ���������� ������������
      try
        qry.Transaction := Trans;
        qry1.Transaction := Trans;
        qry2.Transaction := Trans;
        // ���������� � ���������� ������ ���������� ����������� ����� ---------
        // ����������� ������ ��� ������ � ������ ������, �� ����� ��� ���� 12 -
        if sh = 1 then
        begin
          qry.SelectSQL.Text :=
            'select count(ID), count(distinct REFORGANIZATIONTYPES) ' +
            'from ORGANIZATIONS where REFORGANIZATIONTYPES<5';
          qry.Open;
          rc := qry.Fields[0].AsInteger + qry.Fields[1].AsInteger * 2;
          for j := 1 to 12 do
            WB.Worksheets.Item[j].Range['A5:A5'].Resize[rc - 2]
              .EntireRow.Insert;
        end;
        // ������ ����������� �� ����� -----------------------------------------
        qry.Active := false;
        qry.SelectSQL.Text := 'select ID, NAME from ORGANIZATIONTYPES ' +
          'where ID<5 order by ID';
        qry.Open;
        iRow := 3;
        while not qry.Eof do
        begin
          // ���������� ������ � ��������� ������ ����������� ------------------
          WS.Range[WS.Cells.Item[iRow, 1], WS.Cells.Item[iRow, cc + 2]]
            .Cells.MergeCells := true;
          WS.Range[WS.Cells.Item[iRow, 1], WS.Cells.Item[iRow, cc + 2]]
            .Interior.ColorIndex := 3;
          WS.Range[WS.Cells.Item[iRow, 1], WS.Cells.Item[iRow, cc + 2]]
            .Cells.Font.Name := 'Palatino Linotype';
          WS.Range[WS.Cells.Item[iRow, 1], WS.Cells.Item[iRow, cc + 2]]
            .Cells.Font.Size := 12;
          WS.Rows[iRow].RowHeight := 18;
          WS.Cells.Item[iRow, 1] := qry.Fields[1].asString;
          if sh = 1 then
          begin
            WB.Worksheets.Item[12].Range[Format('A%d:M%d', [iRow, iRow])
              ].Cells.MergeCells := true;
            WB.Worksheets.Item[12].Range[Format('A%d:M%d', [iRow, iRow])
              ].Interior.ColorIndex := 3;
            WB.Worksheets.Item[12].Range[Format('A%d:M%d', [iRow, iRow])
              ].Cells.Font.Name := 'Palatino Linotype';
            WB.Worksheets.Item[12].Range[Format('A%d:M%d', [iRow, iRow])
              ].Cells.Font.Size := 12;
            WB.Worksheets.Item[12].Rows[iRow].RowHeight := 18;
            WB.Worksheets.Item[12].Cells.Item[iRow, 1] :=
              qry.Fields[1].asString;
          end;
          // �������� ������ ����������� ---------------------------------------
          inc(iRow);
          qry1.Active := false;
          qry1.SelectSQL.Text := 'select ID, NAME from ORGANIZATIONS ' +
            'where REFORGANIZATIONTYPES=:ID order by NAME';
          qry1.Params.ParamByName('ID').AsInteger := qry.Fields[0].AsInteger;
          qry1.Open;
          qry1.FetchAll;
          rc := qry1.RecordCount;
          qry1.First;
          // ����� ������ ����������� ------------------------------------------
          VarData := VarArrayCreate([1, rc, 1, 1], VarVariant);
          i := 1;
          while not qry1.Eof do
          begin
            VarData[i, 1] := qry1.Fields[1].asString;
            qry1.Next;
            inc(i);
          end;
          WS.Range[Format('A%d:A%d', [iRow, iRow + rc - 1])].Value := VarData;
          if sh = 1 then
            WB.Worksheets.Item[12].Range[Format('A%d:A%d', [iRow, iRow + rc - 1]
              )].Value := VarData;
          // ������� ���������� ������������ -----------------------------------
          qry1.First;
          VarData := VarArrayCreate([1, rc, 1, cc], VarVariant);
          i := 1;
          while not qry1.Eof do
          begin
            qry2.Active := false;
            qry2.SelectSQL.Text := HIV_IFAIB + HSCODES_SQL[sh].SQL +
              DistrictFilter + ' group by 1';
            qry2.Params.ParamByName('REFORGANIZATIONS').AsInteger :=
              qry1.Fields[0].AsInteger;
            qry2.Params.ParamByName('STARTDATE').AsDateTime := StartDate;
            qry2.Params.ParamByName('ENDDATE').AsDateTime := EndDate;
            qry2.Open;
            while not qry2.Eof do
            begin
              icol := Round(qry2.Fields[0].AsDateTime - StartDate) + 1;
              VarData[i, icol] := qry2.Fields[1].asString;
              qry2.Next;
              Application.ProcessMessages;
            end;
            qry1.Next;
            inc(i);
          end;
          WS.Range[WS.Cells.Item[iRow, 2], WS.Cells.Item[iRow + rc - 1, cc + 1]]
            .Value := VarData;
          // ������� (� �.�. ������� �� �������� 12) ---------------------------
          WS.Range[WS.Cells[iRow + rc, 2], WS.Cells[iRow + rc, cc + 1]]
            .FormulaR1C1 := Format('=SUM(R%dC:R%dC)', [iRow, iRow + rc - 1]);
          WS.Range[WS.Cells[iRow, cc + 2], WS.Cells[iRow + rc, cc + 2]]
            .FormulaR1C1 := Format('=SUM(RC%d:RC%d)', [2, cc + 1]);
          WB.Worksheets.Item[12].Range[WB.Worksheets.Item[12].Cells[iRow,
            LIST12[sh].Row], WB.Worksheets.Item[12].Cells[iRow + rc - 1,
            LIST12[sh].Row]].FormulaR1C1 := '=' + LIST12[sh].SQL + '!' +
            Format('RC%d:RC%d', [cc + 2, cc + 2]);
          if sh = 1 then
          begin
            WB.Worksheets.Item[12].Range
              [Format('B%d:L%d', [iRow + rc, iRow + rc])].FormulaR1C1 :=
              Format('=SUM(R%dC:R%dC)', [iRow, iRow + rc - 1]);
            WB.Worksheets.Item[12].Range[Format('M%d:M%d', [iRow, iRow + rc])
              ].FormulaR1C1 := '=SUM(RC2:RC12)';
          end;
          // ���������� �������� ������ �� ������ ��� --------------------------
          inc(iRow, rc);
          WS.Range[WS.Cells.Item[iRow, 1], WS.Cells.Item[iRow, cc + 2]]
            .Interior.ColorIndex := 35;
          WS.Range[WS.Cells.Item[iRow, 1], WS.Cells.Item[iRow, cc + 2]]
            .Cells.Font.ColorIndex := 55;
          WS.Cells.Item[iRow, 1] := '�����:';
          if sh = 1 then
          begin
            WB.Worksheets.Item[12].Range[Format('A%d:M%d', [iRow, iRow])
              ].Interior.ColorIndex := 35;
            WB.Worksheets.Item[12].Range[Format('A%d:M%d', [iRow, iRow])
              ].Cells.Font.Size := 12;
            WB.Worksheets.Item[12].Range[Format('A%d:M%d', [iRow, iRow])
              ].Cells.Font.ColorIndex := 9;
            WB.Worksheets.Item[12].Range[Format('M%d:M%d', [iRow, iRow])
              ].Cells.Font.Color := clblack;
            WB.Worksheets.Item[12].Rows[iRow].RowHeight := 15;
            WB.Worksheets.Item[12].Cells.Item[iRow, 1] := '�����:';
          end;
          // ���������� ����� ������ � �������������� �������
          SetLength(SumColumns, length(SumColumns) + 1);
          SumColumns[length(SumColumns) - 1] := iRow;
          inc(iRow);
          qry.Next;
        end;
      finally
        qry.Free;
        qry1.Free;
        qry2.Free;
      end;
      // ������ ����� ----------------------------------------------------------
      for i := 0 to length(SumColumns) - 1 do
        Formula := AddStrBySplitter(Formula, '+',
          Format('R%dC', [SumColumns[i]]));
      WS.Range[WS.Cells.Item[iRow, 1], WS.Cells.Item[iRow, cc + 2]]
        .Interior.ColorIndex := 8;
      WS.Range[WS.Cells.Item[iRow, 1], WS.Cells.Item[iRow, cc + 2]]
        .Cells.Font.Color := clblack;
      WS.Range[WS.Cells.Item[iRow, 1], WS.Cells.Item[iRow, cc + 2]]
        .Cells.Font.Size := 12;
      WS.Range[Format('A%d:A%d', [iRow, iRow])].Cells.Font.Italic := true;
      WS.Cells.Item[iRow, 1] := '�����:';
      if Formula <> '' then
        WS.Range[WS.Cells[iRow, 2], WS.Cells[iRow, cc + 2]].FormulaR1C1 :=
          '=' + Formula;
    end;
    // ������ ����� �� ��������� ����� -----------------------------------------
    WB.Worksheets.Item[12].Range[Format('A%d:M%d', [iRow, iRow])
      ].Interior.ColorIndex := 8;
    WB.Worksheets.Item[12].Range[Format('A%d:M%d', [iRow, iRow])
      ].Cells.Font.Color := clblack;
    WB.Worksheets.Item[12].Range[Format('A%d:M%d', [iRow, iRow])
      ].Cells.Font.Size := 14;
    WB.Worksheets.Item[12].Range[Format('A%d:A%d', [iRow, iRow])
      ].Cells.Font.Italic := true;
    WB.Worksheets.Item[12].Cells.Item[iRow, 1] := '�����:';
    if Formula <> '' then
      WB.Worksheets.Item[12].Range[Format('B%d:M%d', [iRow, iRow])].FormulaR1C1
        := '=' + Formula;
  finally
    App.Visible := true;
    SetForegroundWindow(App.Hwnd);
  end;
end;

procedure PrintPDNAgreement(PersonID: integer; Trans: TFIBTransaction);
const
  SQL_PERSON_FOR_AGREEMENT =
    'SELECT P.NAME, P.BIRTHDAY, P.ADDRESS_REG, DT.NAME DOCTYPE,' +
    'P.DOCUMENTNO,p.DELIVERYDATE,P.DOCDELIVER, P.ADDRESS_LIV ' +
    'FROM PERSONS P LEFT JOIN DOCUMENTTYPES DT ON P.REFDOCUMENTTYPES=DT.ID WHERE P.ID=:ID';
var
  qry: TFIBQuery;
  App, WS: Variant;
  s: String;
begin
  App := InitExcel(Replace(ExtractFilePath(Application.ExeName) +
    '\reports\�������� �� ��������� ���.xlt', '\\reports\', '\reports\'));
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    qry := TFIBQuery.Create(nil);
    try
      qry.Transaction := Trans;
      qry.SQL.Text := SQL_PERSON_FOR_AGREEMENT;
      qry.ParamByName('ID').AsInteger := PersonID;
      qry.ExecQuery;
      WS.Range('I6') := '�, ' + qry.FieldByName('NAME').asString;
      WS.Range('I7') := '���� ��������: ' + FullRussianDateToStr
        (qry.FieldByName('BIRTHDAY').AsDateTime);
      s := '����� �����������: ' + qry.FieldByName('ADDRESS_REG').asString;
      s := AddStrBySplitter(s, ';'#$A, '����� ������������ ����������: ' +
        qry.FieldByName('ADDRESS_LIV').asString);
      WS.Range('I11') := s;
      if not qry.FieldByName('DOCTYPE').isNull then
        WS.Range('I13') := '��������, �������������� ��������: ' +
          qry.FieldByName('DOCTYPE').asString + ' ����� � �����: ' +
          qry.FieldByName('DOCUMENTNO').asString + ', ����� ' +
          qry.FieldByName('DELIVERYDATE').asString + ' ' +
          qry.FieldByName('DOCDELIVER').asString;
      WS.Range('M44') := DateToStr(Date);
    finally
      qry.Free;
    end;
  finally
    WS.PrintOut;
    App.DisplayAlerts := false;
    App.Quit;
  end;
end;

procedure PrintInfConsent(PersonID: integer; Trans: TFIBTransaction);
const
  PATIENT_SQL =
    'SELECT P.NAME, P.ADDRESS_REG||COALESCE('', ����� ����������:''||P.ADDRESS_LIV,'''')'
    + ' ADDRESS_REG, P.BIRTHDAY FROM PERSONS P WHERE ID=:ID;';
var
  vals: Variant;
  App, doc: Variant;
begin
  vals := Trans.DefaultDatabase.QueryValues(PATIENT_SQL, [PersonID]);
  if not VarIsNull(vals) then
  begin;
    App := InitWord(Replace(ExtractFilePath(Application.ExeName) +
      '\reports\��������.dot', '\\reports', '\reports'));
    try
      doc := App.ActiveDocument;
      doc.Variables.Item('DocDate').Value := FullRussianDateToStr(Date);
      doc.Variables.Item('PatientName').Value := VarToStr(vals[0]);
      if not VarIsNull(vals[1]) then
        doc.Variables.Item('PatientAddress').Value := VarToStr(vals[1])
      else
        doc.Variables.Item('PatientAddress').Value := ' ';
      if not VarIsNull(vals[2]) then
      begin;
        doc.Variables.Item('BirthDay').Value := IntToStr(DayOf(vals[2]));
        doc.Variables.Item('BirthMonth').Value :=
          RussianMonths[MonthOf(vals[2])];
        doc.Variables.Item('BirthYear').Value := IntToStr(YearOf(vals[2]));
      end;
    finally
      doc.Fields.Update;
      doc.PrintOut;
      App.DisplayAlerts := 0;
      doc.Close(0);
      App.Quit;
    end;
  end;
end;

procedure PrintHIVMeasuresInfo(Month, Year: integer; DistrictFilter: String;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
const
  NEW_SQL = ' and (h.REGISTRYDATE>=:STARTDATE)';
  HIVIMM_PERSONS_SQL = 'select count(distinct p.ID) ' +
    'from HIVIMMUNOGRAMS himm ' +
    'inner join PERSONS p on himm.REFPERSONS=p.ID ' +
    'inner join HIV h on h.REFPERSONS=p.ID ' +
    'where (himm.TAKINGDATE between :STARTDATE and :ENDDATE)';
  HIVPCR_PERSONS_SQL = 'select count(distinct p.ID) ' + 'from HIVPCR hpcr ' +
    'inner join PERSONS p on hpcr.REFPERSONS=p.ID ' +
    'inner join HIV h on h.REFPERSONS=p.ID ' +
    'where (hpcr.VIRALLOAD is not null) and ' +
    '(hpcr.TAKINGDATE between :STARTDATE and :ENDDATE)';
  DISPENSARY_PERSONS_SQL = 'select count(distinct p.ID) ' + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join TREATMENTCASES tc on TC.REFPERSONS=p.ID ' +
    'inner join VISITS v on V.REFTREATMENTCASES=tc.ID ' +
    'where v.ISVISITHAPPENED=1 AND v.VISITDATE between :STARTDATE and :ENDDATE';
  DISPENSARY_COUNT_SQL = 'select count(distinct v.ID) ' + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join TREATMENTCASES tc on TC.REFPERSONS=p.ID ' +
    'inner join VISITS v on V.REFTREATMENTCASES=tc.ID ' +
    'where v.ISVISITHAPPENED=1 AND v.VISITDATE between :STARTDATE and :ENDDATE';
  D_PASSEDTBSCREENING = ' ';
  D_PASSEDIMMUNOGRAM = ' and EXISTS(SELECT r.ID from HIVIMMUNOGRAMS r' +
    ' WHERE r.REFPERSONS=p.ID' +
    ' AND r.TAKINGDATE between :STARTDATE and :ENDDATE)';
  D_PASSEDVIRALLOAD = ' and EXISTS(SELECT r.ID from HIVPCR r' +
    ' WHERE r.REFPERSONS=p.ID' +
    ' AND r.TAKINGDATE between :STARTDATE and :ENDDATE)';
  ARTWAITING_SQL = ' and ' +
    '(exists (select ID from HIVART where REFPERSONS=p.ID and ' +
    '(STARTDATE<=:ENDDATE) and ' +
    '((ENDDATE is null) or (ENDDATE>=:STARTDATE))) or ' +
    'exists (select ID from HIVARTWAITINGLIST where REFPERSONS=p.ID and ' +
    '(STARTDATE<=:ENDDATE) and ' +
    '((ENDDATE is null) or (ENDDATE>=:STARTDATE))))';
  CHILDS_ART_SQL = 'select count(distinct p.ID) ' + 'from PERSONS p ' +
    'inner join HIVCONTACTS hc on hc.REFPERSONS2=p.ID and hc.REFHIVCONTACTTYPES=3 '
    + 'inner join HIVPREGNANCY hp on hc.REFPERSONS1=hp.REFPERSONS ' +
    'inner join HIV h on h.REFPERSONS=p.ID ' +
    'inner join HIVART ha on h.REFPERSONS=ha.REFPERSONS ' +
    'where (p.BIRTHDAY<=:ENDDATE) and ' + '(h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(ha.STARTDATE<=:ENDDATE) and ' +
    '((ha.ENDDATE is null) or (ha.ENDDATE>=:STARTDATE))';
  HIVART_IMMPCR_SQL = 'select count(distinct p.ID) ' + 'from HIVART ha ' +
    'inner join HIV h on h.REFPERSONS=ha.REFPERSONS ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'left join HIVIMMUNOGRAMS himm on himm.REFPERSONS=p.ID ' +
    'left join HIVPCR hpcr on hpcr.REFPERSONS=p.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(ha.STARTDATE<=:ENDDATE) and ' +
    '((ha.ENDDATE is null) or (ha.ENDDATE>=:STARTDATE))';
  CHILDS_INPERIOD_SQL = 'select count(distinct p.ID) ' + 'from PERSONS p ' +
    'inner join HIVCONTACTS hc on hc.REFPERSONS2=p.ID and hc.REFHIVCONTACTTYPES=3 '
    + 'inner join HIVPREGNANCY hp on hc.REFPERSONS1=hp.REFPERSONS ' +
    'left join HIV h on h.REFPERSONS=p.ID ' +
    'where (p.BIRTHDAY>=:STARTDATE and p.BIRTHDAY<=:ENDDATE)';
  PREGNANCY_AND_HIVART_SQL = 'select count(distinct p.ID) ' + 'from HIVART ha '
    + 'inner join HIV h on h.REFPERSONS=ha.REFPERSONS ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join HIVPREGNANCY hp on hp.REFPERSONS=p.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ' +
    '((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and ' +
    '(ha.STARTDATE<=:ENDDATE) and ' +
    '((ha.ENDDATE is null) or (ha.ENDDATE>=:STARTDATE)) and ' +
    '(hp.DETECTIONDATE<=:ENDDATE) and ' +
    '((hp.BIRTHDATE is null) or (hp.BIRTHDATE>=:STARTDATE))';

  STATS_SQL_COUNT = 55;
  STATS_SQL: array [1 .. STATS_SQL_COUNT] of PositionedSQL = ((Row: 57;
    SQL: ''), (Row: 58; SQL: F4PERSONS_SQL), (Row: 59;
    SQL: SECTION1001_SQL + NEW_SQL), (Row: 60; SQL: SECTION1001_SQL + NEW_SQL),
    (Row: 61; SQL: SECTION1001_SQL + DEADINPERIOD_SQL), (Row: 62;
    SQL: SECTION1001_SQL + DEADINPERIOD_SQL + ' and (h.REFHIVOUTCOMES=1)'),
    (Row: 63; SQL: SECTION1001_SQL), (Row: 65; SQL: SECTION1001_SQL), (Row: 67;
    SQL: DISPENSARY_PERSONS_SQL), (Row: 68;
    SQL: DISPENSARY_PERSONS_SQL + D_PASSEDIMMUNOGRAM { HIVIMM_PERSONS_SQL } ),
    (Row: 71; SQL: DISPENSARY_PERSONS_SQL +
    D_PASSEDVIRALLOAD { HIVPCR_PERSONS_SQL } ), (Row: 73;
    SQL: DISPENSARY_COUNT_SQL + D_PASSEDIMMUNOGRAM { SECTION3005_SQL } ),
    (Row: 74; SQL: DISPENSARY_COUNT_SQL +
    D_PASSEDVIRALLOAD { SECTION3006_SQL } ), (Row: 75;
    SQL: DISPENSARY_PERSONS_SQL + D_PASSEDTBSCREENING), (Row: 76;
    SQL: SECTION1001_SQL + ARTWAITING_SQL), (Row: 78;
    SQL: CHILDS_INPERIOD_SQL + ARTWAITING_SQL), (Row: 80;
    SQL: SECTION3000_HIVARTONLY_SQL), (Row: 82;
    SQL: SECTION3000_HIVARTONLY_SQL + ' and (ha.REFHIVARTOUTCOMES=3)'),
    (Row: 83; SQL: SECTION3000_HIVARTONLY_SQL + ' and (ha.REFHIVARTOUTCOMES=3)'
    + DEADINPERIOD_SQL), (Row: 84; SQL: SECTION3000_HIVARTONLY_SQL), (Row: 85;
    SQL: SECTION3000_DISEASESONLY_SQL + ISTUBERCULOSIS_SQL), (Row: 86;
    SQL: CHILDS_ART_SQL), (Row: 87;
    SQL: HIVART_IMMPCR_SQL +
    ' and (himm.TAKINGDATE between :STARTDATE and :ENDDATE)'), (Row: 90;
    SQL: HIVART_IMMPCR_SQL +
    ' and (hpcr.VIRALLOAD is not null) and (hpcr.TAKINGDATE between :STARTDATE and :ENDDATE)'),
    (Row: 92; SQL: SECTION1001_SQL + ISUFSIN_SQL), (Row: 93;
    SQL: SECTION1001_SQL + ARTWAITING_SQL + ISUFSIN_SQL), (Row: 95;
    SQL: SECTION3000_HIVARTONLY_SQL + ISUFSIN_SQL), (Row: 97;
    SQL: SECTION3000_HIVARTONLY_SQL + ' and (ha.REFHIVARTOUTCOMES=3)' +
    ISUFSIN_SQL), (Row: 98;
    SQL: SECTION3000_HIVARTONLY_SQL + ' and (ha.REFHIVARTOUTCOMES=3)' +
    DEADINPERIOD_SQL + ISUFSIN_SQL), (Row: 99;
    SQL: SECTION3000_HIVARTONLY_SQL + ISUFSIN_SQL), (Row: 100;
    SQL: HIVIMM_PERSONS_SQL + ISUFSIN_SQL), (Row: 101;
    SQL: SECTION3000_DISEASESONLY_SQL + ISTUBERCULOSIS_SQL + ISUFSIN_SQL),
    (Row: 102; SQL: SECTION3000_DISEASESONLY_SQL +
    ' and (d.CLASSIFIERCODE=''B15.0'')'), (Row: 103;
    SQL: SECTION3001_PREGNANCY_SQL), (Row: 105;
    SQL: SECTION3001_PREGNANCY_SQL +
    ' and (hp.BIRTHDATE between :STARTDATE and :ENDDATE)'), (Row: 107;
    SQL: PREGNANCY_AND_HIVART_SQL +
    ' and ((hp.REFDRUGS_PREG is not null) or (hp.REFDRUGS_BIRTHS is not null))'),
    (Row: 108; SQL: PREGNANCY_AND_HIVART_SQL +
    ' and ((hp.REFDRUGS_PREG is not null) or (hp.REFDRUGS_BIRTHS is not null))'),
    (Row: 109; SQL: SECTION3001_PREGNANCY_SQL +
    ' and (hp.REFDRUGS_PREG is not null)'), (Row: 110;
    SQL: SECTION3001_PREGNANCY_SQL + ' and (hp.REFDRUGS_BIRTHS is not null)'),
    (Row: 111; SQL: SECTION3001_PREGNANCY_SQL +
    ' and ((hp.REFDRUGS_PREG is null) and (hp.REFDRUGS_BIRTHS is not null))'),
    (Row: 112; SQL: SECTION3003_CHILDCOUNT_SQL +
    ' and (hp.REFDRUGS_CHILD is not null)'), (Row: 113;
    SQL: SECTION3003_CHILDCOUNT_SQL +
    ' and ((hp.REFDRUGS_PREG is null) and (hp.REFDRUGS_BIRTHS is not null) and (hp.REFDRUGS_CHILD is not null))'),
    (Row: 114; SQL: SECTION3003_CHILDCOUNT_SQL), (Row: 116;
    SQL: CHILDS_INPERIOD_SQL + WHERE_NOT_COMMITTED), (Row: 117;
    SQL: CHILDS_INPERIOD_SQL +
    ' and (h.DETECTIONDATE between :STARTDATE and :ENDDATE)'), (Row: 118;
    SQL: SECTION3003_CHILDCOUNT_SQL +
    ' and (select h.ISFIRSTTIME from HIV h where h.REFPERSONS=hp.REFPERSONS)=1'),
    (Row: 119;
    SQL: 'select (select count(distinct hb.REFPERSONS) from HBVPCR hb where hb.TAKINGDATE between :STARTDATE and :ENDDATE) + '
    + '(select count(distinct hc.REFPERSONS) from HCVPCR hc where hc.TAKINGDATE between :STARTDATE and :ENDDATE) from rdb$database'),
    (Row: 120; SQL: ''), (Row: 121; SQL: ''), (Row: 122; SQL: ''), (Row: 123;
    SQL: ''), (Row: 124; SQL: ''), (Row: 125; SQL: ''), (Row: 126; SQL: ''),
    (Row: 127; SQL: ''));
var
  i, j: integer;
  dd, mm, yy: WORD;
  qry: TFIBDataset;
  App, WS: Variant;
  iDate: TDateTime;
  ColumnPeriods: Array of TColPeriod;
begin
  // ���������� ������ ������ (STARTDATE, ENDDATE) -----------------------------
  if (Month in [1 .. 12]) then
  begin
    SetLength(ColumnPeriods, 2);
    yy := Year;
    mm := 1;
    dd := 1;
    ColumnPeriods[0].StartDate := EncodeDate(yy, mm, dd);
    ColumnPeriods[0].Column := 8;
    yy := Year;
    mm := Month;
    dd := 1;
    ColumnPeriods[1].StartDate := EncodeDate(yy, mm, dd);
    ColumnPeriods[0].EndDate := IncMonth(ColumnPeriods[1].StartDate) - 1;
    ColumnPeriods[1].EndDate := ColumnPeriods[0].EndDate;
    ColumnPeriods[1].Column := 12;
    iDate := ColumnPeriods[0].EndDate;
  end
  else
  begin
    SetLength(ColumnPeriods, 1);
    yy := Year;
    mm := 1;
    dd := 1;
    ColumnPeriods[0].StartDate := EncodeDate(yy, mm, dd);
    mm := 12;
    dd := 31;
    ColumnPeriods[0].EndDate := EncodeDate(yy, mm, dd);
    ColumnPeriods[0].Column := 8;
    iDate := ColumnPeriods[0].EndDate;
  end;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\�������� � ������������.xlt', '\\reports', '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Range['BY8'] := FormatDateTime('yy', iDate);
    if (Month in [1 .. 12]) then
    begin
      WS.Range['E10'] := FormatDateTime('mmmm', iDate);
      WS.Range['G10'] := FormatDateTime('yyyy', iDate);
      WS.Range['K28'].Cells.Font.Underline := 2;
      WS.Range['H50'] := '� ��� ����� � ' + FormatDateTime('01.01.yyyy', iDate)
        + ' �� �������� ����� (����������� ������) (� ���.������)';
    end
    else
    begin
      WS.Range['G10'] := FormatDateTime('yyyy', iDate);
      WS.Range['K29'].Cells.Font.Underline := 2;
      WS.Range['H50'] := '� ��� ����� � ' + FormatDateTime('01.01.yyyy', iDate)
        + ' �� �������� ����� (����������� ������) (� ���.������)';
    end;
    qry := TFIBDataset.Create(nil);
    try
      qry.Transaction := Trans;
      for i := 1 to STATS_SQL_COUNT do
      begin
        if STATS_SQL[i].SQL <> '' then
        begin
          for j := 0 to length(ColumnPeriods) - 1 do
          begin
            qry.Active := false;
            qry.SQLs.SelectSQL.Text := STATS_SQL[i].SQL + DistrictFilter;
            // �����-�����, ����� ������ ��������, �� �� �������� ������ �����,
            // ��� ��� ����� ������� ��������... ((
            if (i = 47) and (DistrictFilter <> '') then
            begin
              qry.SQLs.SelectSQL.Text := 'select ' +
                '(select count(distinct hb.REFPERSONS) ' + 'from HBVPCR hb ' +
                'inner join PERSONS p on hb.REFPERSONS=p.ID ' +
                'where hb.TAKINGDATE between :STARTDATE and :ENDDATE ' +
                DistrictFilter + ') + ' +
                '(select count(distinct hc.REFPERSONS) ' + 'from HCVPCR hc ' +
                'inner join PERSONS p on hc.REFPERSONS=p.ID ' +
                'where hc.TAKINGDATE between :STARTDATE and :ENDDATE ' +
                DistrictFilter + ') from rdb$database';
            end;
            qry.Params.ParamByName('StartDate').AsDateTime :=
              ColumnPeriods[j].StartDate;
            qry.Params.ParamByName('EndDate').AsDateTime :=
              ColumnPeriods[j].EndDate;
            qry.Open;
            if qry.Fields[0].AsInteger > 0 then
              WS.Cells.Item[STATS_SQL[i].Row, ColumnPeriods[j].Column] :=
                qry.Fields[0].AsInteger;
          end;
        end;
      end;
      WS.Cells.Item[57, 6] := 'x';
      WS.Cells.Item[58, 6] := 'x';
      WS.Cells.Item[67, 6] := 'x';
      WS.Cells.Item[68, 6] := 'x';
      WS.Cells.Item[71, 6] := 'x';
      WS.Cells.Item[73, 6] := 'x';
      WS.Cells.Item[74, 6] := 'x';
      WS.Cells.Item[75, 6] := 'x';
      WS.Cells.Item[87, 6] := 'x';
      WS.Cells.Item[90, 6] := 'x';
      WS.Cells.Item[100, 6] := 'x';
      WS.Cells.Item[116, 13] := 'x';
      WS.Cells.Item[117, 13] := 'x';
      WS.Cells.Item[119, 6] := 'x';
      WS.Cells.Item[120, 6] := 'x';
      WS.Cells.Item[121, 6] := 'x';
      WS.Cells.Item[127, 6] := 'x';
    finally
      qry.Free;
    end;
    App.ActiveWorkbook.Saved := true;
  finally
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

{ +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++ ����� "������ ���������, ���������� ��������" +++++++++++++++++
  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ }
procedure PrintPatientsList(StartDate, EndDate: TDateTime; DrugsFilter: String;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
var
  qry: TFIBDataset;
  i, rc: integer;
  App, WS, VarData: Variant;
const
  LIST_SQL =
    'WITH R AS ' +
    ' (SELECT REFPERSONS,MAX(EXAMINATIONDATE) D FROM XRAYSEXAMINATIONS ' +
    ' WHERE EXAMINATIONDATE BETWEEN :STARTDATE and :ENDDATE ' +
    ' GROUP BY 1)' +
    ' select p.NAME, p.AMBULATORYCARDNO, p.BIRTHDAY, ' +
    ' R.D,' +
    'LIST(distinct dstr.NAME, '', ''), LIST(distinct d.DESCR, '', '') ' +
    'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join PLACESHISTORY ph on ph.REFPERSONS=p.ID ' +
    'inner join DISTRICTS dstr on ph.REFDISTRICTS=dstr.ID ' +
    'inner join HIVART ha on ha.REFPERSONS=p.ID ' +
    'inner join HIVARTDRUGS hadr on hadr.REFHIVART=ha.ID ' +
    'inner join DRUGS d on hadr.REFDRUGS=d.ID ' +
    ' LEFT JOIN R ON R.REFPERSONS=P.ID ' +
    ' where ' +
    '(ha.STARTDATE<=:ENDDATE) and ' +
    '((ha.ENDDATE is null) or (ha.ENDDATE>=:STARTDATE)) and ' +
    '(ph.STARTDATE<=:ENDDATE) and ' +
    '((ph.ENDDATE is null) or (ph.ENDDATE>=:STARTDATE)) %s ' +
    'group by 1, 2, 3, 4 ' +
    'order by 1, 3';

  GROUP_SQL = 'SELECT LST,COUNT(ID) FROM ' +
    '(select DISTINCT p.ID, REPLACE(d.DESCR,DTN.NAMERUS,COALESCE(DIN.NAMERUS,DTN.NAMERUS)) LST '
    + 'from HIV h ' +
    'inner join PERSONS p on h.REFPERSONS=p.ID ' +
    'inner join PLACESHISTORY ph on ph.REFPERSONS=p.ID ' +
    'inner join DISTRICTS dstr on ph.REFDISTRICTS=dstr.ID ' +
    'inner join HIVART ha on ha.REFPERSONS=p.ID ' +
    'inner join HIVARTDRUGS hadr on hadr.REFHIVART=ha.ID ' +
    'inner join DRUGS d on hadr.REFDRUGS=d.ID ' +
    'inner join DRUGTRADENAMES dtn on d.REFDRUGTRADENAMES=dtn.ID ' +
    'left join DRUGINTNAMES din ON DTN.REFDRUGINTNAMES=DIN.ID ' + 'where ' +
    '(ha.STARTDATE<=:ENDDATE) and ' +
    '((ha.ENDDATE is null) or (ha.ENDDATE>=:STARTDATE)) and ' +
    '(ph.STARTDATE<=:ENDDATE) and ' +
    '((ph.ENDDATE is null) or (ph.ENDDATE>=:STARTDATE)) ' +
    ') ' +
    'group by 1';
begin
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\������ ���������.xlt', '\\reports', '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    qry := TFIBDataset.Create(nil);
    try
      qry.Transaction := Trans;
      // "�����" ---------------------------------------------------------------
      WS.Range['A2'] := '������: ' + FormatDateTime('dd.mm.yyyy', StartDate) +
        ' - ' + FormatDateTime('dd.mm.yyyy', EndDate);
      if DrugsFilter <> '' then
      begin
        qry.SelectSQL.Text := 'select 1, LIST(d.DESCR, '', '') ' +
          'from DRUGS d where ' + DrugsFilter + 'group by 1 order by 2';
        qry.Open;
        WS.Range['A3'] := '������ ����������: ' + qry.Fields[1].asString;
        qry.Active := false;
      end;
      // ��������� ����� -------------------------------------------------------
      if DrugsFilter <> '' then
        DrugsFilter := 'and ' + DrugsFilter;
      qry.SelectSQL.Text := Format(LIST_SQL, [DrugsFilter]);
      qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
      qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
      qry.Open;
      qry.FetchAll;
      qry.First;
      rc := qry.RecordCount;
      VarData := VarArrayCreate([1, rc, 1, 7], VarVariant);
      i := 1;
      while not qry.Eof do
      begin
        VarData[i, 1] := i;
        VarData[i, 2] := qry.Fields[0].asString; // �.�.�.
        VarData[i, 3] := qry.Fields[1].asString; // ����.�����
        if not qry.Fields[2].isNull then
          VarData[i, 4] := qry.Fields[2].asString; // ���� ��������
        if not qry.Fields[3].isNull then
          VarData[i, 5] := qry.Fields[3].asString; // ���� ������������
        VarData[i, 6] := qry.Fields[4].asString; // �������� �������
        VarData[i, 7] := qry.Fields[5].asString; // �������� ����������
        if Assigned(Status) then
          Status.Panels[0].Text := '������������ ������ (' +
            IntToStr(Round(i / rc * 100)) + '%)...';
        Application.ProcessMessages;
        qry.Next;
        inc(i);
      end;
      if rc > 0 then
      begin
        WS.Range[Format('A%d:G%d', [6, rc + 5])].Value := VarData;
        WS.Range[Format('A%d:G%d', [6, rc + 5])].Borders.Linestyle :=
          xlContinuous;
      end;
      qry.Close;
      qry.SelectSQL.Text := GROUP_SQL;
      qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
      qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
      qry.Open;
      qry.FetchAll;
      qry.First;
      rc := qry.RecordCount;
      VarData := VarArrayCreate([1, rc, 1, 3], VarVariant);

      i := 1;
      while not qry.Eof do
      begin
        VarData[i, 1] := i;
        VarData[i, 2] := qry.Fields[0].asString; // ���������� ����������
        VarData[i, 3] := qry.Fields[1].AsInteger; // ���-��
        qry.Next;
        inc(i);
      end;
      WS := App.ActiveWorkbook.Worksheets.Item[2];
      if rc > 0 then
      begin
        WS.Range[Format('A%d:C%d', [2, rc + 1])].Value := VarData;
        WS.Range[Format('A%d:C%d', [2, rc + 1])].Borders.Linestyle :=
          xlContinuous;
      end;
    finally
      qry.Free;
    end;
    App.ActiveWorkbook.Saved := true;
  finally
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormHIVDetectionRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
Const
  SQL = 'SELECT COUNT(DISTINCT H.ID) FROM HIV H' +
    ' INNER JOIN PERSONS P ON H.REFPERSONS=P.ID' +
    ' WHERE H.DETECTIONDATE BETWEEN :StartDate AND :EndDate AND P.REFSEX=:S ';

  FILTER_COUNT = 21;

  FILTERS: Array [1 .. FILTER_COUNT] of PositionedSQL = ((Row: 4; SQL: ''),
    (Row: 5; SQL: ' AND (H.DETECTIONDATE-P.BIRTHDAY)/365.25 < 15'), (Row: 6;
    SQL: ' AND (H.DETECTIONDATE-P.BIRTHDAY)/365.25 BETWEEN 15 AND 18.9999'),
    (Row: 7; SQL
    : ' AND (H.DETECTIONDATE-P.BIRTHDAY)/365.25 BETWEEN 19 AND 20.9999'),
    (Row: 8; SQL
    : ' AND (H.DETECTIONDATE-P.BIRTHDAY)/365.25 BETWEEN 21 AND 30.9999'),
    (Row: 9; SQL
    : ' AND (H.DETECTIONDATE-P.BIRTHDAY)/365.25 BETWEEN 31 AND 40.9999'),
    (Row: 10; SQL
    : ' AND (H.DETECTIONDATE-P.BIRTHDAY)/365.25 BETWEEN 41 AND 50.9999'),
    (Row: 11; SQL: ' AND (H.DETECTIONDATE-P.BIRTHDAY)/365.25 > 51'), (Row: 13;
    SQL: ' AND H.REFHIVINFECTIONREASONS=108'), (Row: 14;
    SQL: ' AND H.REFHIVINFECTIONREASONS=104'), (Row: 15;
    SQL: ' AND H.REFHIVINFECTIONREASONS=100'), (Row: 16;
    SQL: ' AND H.REFHIVINFECTIONREASONS IN (111,112,113)'), (Row: 17;
    SQL: ' AND H.REFHIVINFECTIONREASONS IN (109,110)'), (Row: 18;
    SQL: ' AND H.REFHIVINFECTIONREASONS IN (114,115)'), (Row: 20;
    SQL: ' AND P.ISWORKING=1'), (Row: 21;
    SQL: ' AND P.ISWORKING=0 AND P.ISPENSIONER=0 AND P.ISSTUDENT=0'), (Row: 22;
    SQL: ' AND (P.ISBUDGETWORKER=1 OR P.ISSOLDIER=1)'), (Row: 23;
    SQL: ' AND H.ISUFSIN=1'), (Row: 24; SQL: ' AND P.ISSTUDENT=1'), (Row: 25;
    SQL: ' AND P.ISPENSIONER=1'), (Row: 26;
    SQL: ' AND (H.DETECTIONDATE-P.BIRTHDAY)/365.25 < 16'));

var
  App, WS: Variant;
  i, j: integer;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\����� �� ������� ���������� ���.xlt', '\\reports',
    '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Range['A2'] := '�� ������ � ' + DateToStr(StartDate) + ' �� ' +
      DateToStr(EndDate);
    for i := 1 to 2 do
      for j := 1 to FILTER_COUNT do
      begin;
        WS.Cells.Item[FILTERS[j].Row, i + 1] := Trans.DefaultDatabase.QueryValue
          (SQL + FILTERS[j].SQL, 0, [StartDate, EndDate, i]);
        if Assigned(Status) then
          Status.Panels[0].Text := '������������ ������ (' +
            IntToStr(Round(j / FILTER_COUNT * 100)) + '%)...';
      end;
  finally
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormHIVLabDirectionsRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
Const
  ALL_DIRECTIONS_SQL = 'SELECT D.ADATE,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(1,2,6),1,0)) HIV,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(7,44),1,0)) PSA,' +
    ' COUNT(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(26,27,28,29,30,31,32,33),D.REFPERSONS,NULL)) HEPATIT,'
    + ' SUM(IIF(D.REFHIVANALYSISTYPES IN(11),1,0)) SIPHILYS,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(12),1,0)) MRP,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(13),1,0)) RPGA,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(4),1,0)) IMMUNOGRAMS,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(14),1,0)) BLOODCT,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(15),1,0)) BIOCHEM,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(16),1,0)) TOXO,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(17),1,0)) CMV,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(18),1,0)) VPG,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(19),1,0)) CHLAM,' +
    ' COUNT(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(3,5,8,9,10,35,36,37,38,39,40,41),D.REFPERSONS,NULL)) PCR,'
    + ' SUM(IIF(D.REFHIVANALYSISTYPES IN(20),1,0)) VIRALLOAD,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(21),1,0)) INJ,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(22),1,0)) KRASN,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(24),1,0)) CA_125,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(23),1,0)) DUPL,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(25),1,0)) HORM,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(43),1,0)) VEB,' +
    ' COUNT(DISTINCT D.REFPERSONS)' + ' FROM ' +
    ' HIVDIRECTIONS D WHERE D.ADATE BETWEEN :StartDate and :EndDate GROUP BY 1';
  HIV_DIRECTIONS_SQL = 'SELECT D.ADATE,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=102,1,0)) H102,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=103,1,0)) H103,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=104,1,0)) H104,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=108,1,0)) H108,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=109,1,0)) H109,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=113,1,0)) H113,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=115,1,0)) H115,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=118,1,0)) H118,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=119,1,0)) H118B,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=120,1,0)) H120,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=200,1,0)) H200' + ' FROM ' +
    ' HIVDIRECTIONS D WHERE D.ADATE BETWEEN :StartDate and :EndDate' +
    ' AND D.REFHIVANALYSISTYPES IN(1,2,6)' + ' GROUP BY 1';
  HIV_DIRECTIONS_ANON_SQL = 'SELECT D.ADATE,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=102,1,0)) H102,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=103,1,0)) H103,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=104,1,0)) H104,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=108,1,0)) H108,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=109,1,0)) H109,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=113,1,0)) H113,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=115,1,0)) H115,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=118,1,0)) H118,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=119,1,0)) H118B,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=120,1,0)) H120,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=200,1,0)) H200' + ' FROM ' +
    ' HIVDIRECTIONS D INNER JOIN PERSONS P ON D.REFPERSONS=P.ID' +
    ' WHERE D.ADATE BETWEEN :StartDate and :EndDate AND P.ISANONYM=1' +
    ' AND D.REFHIVANALYSISTYPES IN(1,2,6)' + ' GROUP BY 1';
var
  VarData, App, WS: Variant;
  i, j, cnt, rc: integer;
  qry: TFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\����� �� ������������.xlt', '\\reports', '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := ALL_DIRECTIONS_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 24], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := qry.Fields[00].AsDateTime;
      VarData[i, 02] := qry.Fields[22].AsInteger;
      VarData[i, 03] := qry.Fields[01].AsInteger; // ���-��
      VarData[i, 04] := qry.Fields[02].AsInteger; // ���-��
      VarData[i, 05] := qry.Fields[03].AsInteger; // ���-��
      VarData[i, 06] := qry.Fields[04].AsInteger; // ���-��
      VarData[i, 07] := qry.Fields[05].AsInteger; // ���-��
      VarData[i, 08] := qry.Fields[06].AsInteger; // ���-��
      VarData[i, 09] := qry.Fields[07].AsInteger; // ���-��
      VarData[i, 10] := qry.Fields[08].AsInteger; // ���-��
      VarData[i, 11] := qry.Fields[09].AsInteger; // ���-��
      VarData[i, 12] := qry.Fields[10].AsInteger; // ���-��
      VarData[i, 13] := qry.Fields[11].AsInteger; // ���-��
      VarData[i, 14] := qry.Fields[12].AsInteger; // ���-��
      VarData[i, 15] := qry.Fields[13].AsInteger; // ���-��
      VarData[i, 16] := qry.Fields[14].AsInteger; // ���-��
      VarData[i, 17] := qry.Fields[15].AsInteger; // ���-��
      VarData[i, 18] := qry.Fields[16].AsInteger; // ���-��
      VarData[i, 19] := qry.Fields[17].AsInteger; // ���-��
      VarData[i, 20] := qry.Fields[18].AsInteger; // ���-��
      VarData[i, 21] := qry.Fields[19].AsInteger; // ���-��
      VarData[i, 22] := qry.Fields[20].AsInteger; // ���-��
      VarData[i, 23] := qry.Fields[21].AsInteger; // ���-��
      cnt := 0;
      for j := 1 to 21 do
        cnt := cnt + qry.Fields[j].AsInteger;
      VarData[i, 24] := cnt;
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:X%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:X%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:X%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
    WS := App.ActiveWorkbook.Worksheets.Item[2];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry.SelectSQL.Text := HIV_DIRECTIONS_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 12], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := qry.Fields[00].AsDateTime;
      VarData[i, 02] := qry.Fields[01].AsInteger; // ���-��
      VarData[i, 03] := qry.Fields[02].AsInteger; // ���-��
      VarData[i, 04] := qry.Fields[03].AsInteger; // ���-��
      VarData[i, 05] := qry.Fields[04].AsInteger; // ���-��
      VarData[i, 06] := qry.Fields[05].AsInteger; // ���-��
      VarData[i, 07] := qry.Fields[06].AsInteger; // ���-��
      VarData[i, 08] := qry.Fields[07].AsInteger; // ���-��
      VarData[i, 09] := qry.Fields[08].AsInteger; // ���-��
      VarData[i, 10] := qry.Fields[09].AsInteger; // ���-��
      VarData[i, 11] := qry.Fields[10].AsInteger; // ���-��
      VarData[i, 12] := qry.Fields[11].AsInteger; // ���-��
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:L%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:L%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:L%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
    WS := App.ActiveWorkbook.Worksheets.Item[3];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry.SelectSQL.Text := HIV_DIRECTIONS_ANON_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 12], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := qry.Fields[00].AsDateTime;
      VarData[i, 02] := qry.Fields[01].AsInteger; // ���-��
      VarData[i, 03] := qry.Fields[02].AsInteger; // ���-��
      VarData[i, 04] := qry.Fields[03].AsInteger; // ���-��
      VarData[i, 05] := qry.Fields[04].AsInteger; // ���-��
      VarData[i, 06] := qry.Fields[05].AsInteger; // ���-��
      VarData[i, 07] := qry.Fields[06].AsInteger; // ���-��
      VarData[i, 08] := qry.Fields[07].AsInteger; // ���-��
      VarData[i, 09] := qry.Fields[08].AsInteger; // ���-��
      VarData[i, 10] := qry.Fields[09].AsInteger; // ���-��
      VarData[i, 11] := qry.Fields[10].AsInteger; // ���-��
      VarData[i, 12] := qry.Fields[11].AsInteger; // ���-��
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:L%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:L%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:L%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
  finally
    qry.Free;
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormHIVLabDirectionsYearlyRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
Const
  ALL_DIRECTIONS_SQL = 'SELECT EXTRACT(MONTH FROM D.ADATE),' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(1,2,6),1,0)) HIV,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(7,44),1,0)) PSA,' +
    ' COUNT(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(26,27,28,29,30,31,32,33),D.REFPERSONS,NULL)) HEPATIT,'
    + ' SUM(IIF(D.REFHIVANALYSISTYPES IN(11),1,0)) SIPHILYS,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(12),1,0)) MRP,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(13),1,0)) RPGA,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(4),1,0)) IMMUNOGRAMS,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(14),1,0)) BLOODCT,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(15),1,0)) BIOCHEM,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(16),1,0)) TOXO,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(17),1,0)) CMV,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(18),1,0)) VPG,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(19),1,0)) CHLAM,' +
    ' COUNT(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(3,5,8,9,10,35,36,37,38,39,40,41),D.REFPERSONS,NULL)) PCR,'
    + ' SUM(IIF(D.REFHIVANALYSISTYPES IN(20),1,0)) VIRALLOAD,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(21),1,0)) INJ,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(22),1,0)) KRASN,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(24),1,0)) CA_125,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(23),1,0)) DUPL,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(25),1,0)) HORM,' +
    ' SUM(IIF(D.REFHIVANALYSISTYPES IN(43),1,0)) VEB,' +
    ' COUNT(DISTINCT D.REFPERSONS)' + ' FROM ' +
    ' HIVDIRECTIONS D WHERE D.ADATE BETWEEN :StartDate and :EndDate GROUP BY 1';
  HIV_DIRECTIONS_SQL = 'SELECT EXTRACT(MONTH FROM D.ADATE),' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=102,1,0)) H102,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=103,1,0)) H103,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=104,1,0)) H104,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=108,1,0)) H108,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=109,1,0)) H109,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=113,1,0)) H113,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=115,1,0)) H115,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=118,1,0)) H118,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=119,1,0)) H118B,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=120,1,0)) H120,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=200,1,0)) H200' + ' FROM ' +
    ' HIVDIRECTIONS D WHERE D.ADATE BETWEEN :StartDate and :EndDate' +
    ' AND D.REFHIVANALYSISTYPES IN(1,2,6)' + ' GROUP BY 1';
  HIV_DIRECTIONS_ANON_SQL = 'SELECT EXTRACT(MONTH FROM D.ADATE),' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=102,1,0)) H102,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=103,1,0)) H103,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=104,1,0)) H104,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=108,1,0)) H108,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=109,1,0)) H109,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=113,1,0)) H113,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=115,1,0)) H115,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=118,1,0)) H118,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=119,1,0)) H118B,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=120,1,0)) H120,' +
    ' SUM(IIF(D.REFHIVSCREENINGCODES=200,1,0)) H200' + ' FROM ' +
    ' HIVDIRECTIONS D INNER JOIN PERSONS P ON D.REFPERSONS=P.ID' +
    ' WHERE D.ADATE BETWEEN :StartDate and :EndDate AND P.ISANONYM=1' +
    ' AND D.REFHIVANALYSISTYPES IN(1,2,6)' + ' GROUP BY 1';
var
  VarData, App, WS: Variant;
  i, j, cnt, rc: integer;
  qry: TFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\����� �� ������������.xlt', '\\reports', '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := ALL_DIRECTIONS_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 24], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := RussianMonthsInf[qry.Fields[00].AsInteger];
      VarData[i, 02] := qry.Fields[22].AsInteger;
      VarData[i, 03] := qry.Fields[01].AsInteger; // ���-��
      VarData[i, 04] := qry.Fields[02].AsInteger; // ���-��
      VarData[i, 05] := qry.Fields[03].AsInteger; // ���-��
      VarData[i, 06] := qry.Fields[04].AsInteger; // ���-��
      VarData[i, 07] := qry.Fields[05].AsInteger; // ���-��
      VarData[i, 08] := qry.Fields[06].AsInteger; // ���-��
      VarData[i, 09] := qry.Fields[07].AsInteger; // ���-��
      VarData[i, 10] := qry.Fields[08].AsInteger; // ���-��
      VarData[i, 11] := qry.Fields[09].AsInteger; // ���-��
      VarData[i, 12] := qry.Fields[10].AsInteger; // ���-��
      VarData[i, 13] := qry.Fields[11].AsInteger; // ���-��
      VarData[i, 14] := qry.Fields[12].AsInteger; // ���-��
      VarData[i, 15] := qry.Fields[13].AsInteger; // ���-��
      VarData[i, 16] := qry.Fields[14].AsInteger; // ���-��
      VarData[i, 17] := qry.Fields[15].AsInteger; // ���-��
      VarData[i, 18] := qry.Fields[16].AsInteger; // ���-��
      VarData[i, 19] := qry.Fields[17].AsInteger; // ���-��
      VarData[i, 20] := qry.Fields[18].AsInteger; // ���-��
      VarData[i, 21] := qry.Fields[19].AsInteger; // ���-��
      VarData[i, 22] := qry.Fields[20].AsInteger; // ���-��
      VarData[i, 23] := qry.Fields[21].AsInteger; // ���-��
      cnt := 0;
      for j := 1 to 21 do
        cnt := cnt + qry.Fields[j].AsInteger;
      VarData[i, 24] := cnt;
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:X%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:X%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:X%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
    WS := App.ActiveWorkbook.Worksheets.Item[2];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry.SelectSQL.Text := HIV_DIRECTIONS_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 12], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := RussianMonthsInf[qry.Fields[00].AsInteger];
      VarData[i, 02] := qry.Fields[01].AsInteger; // ���-��
      VarData[i, 03] := qry.Fields[02].AsInteger; // ���-��
      VarData[i, 04] := qry.Fields[03].AsInteger; // ���-��
      VarData[i, 05] := qry.Fields[04].AsInteger; // ���-��
      VarData[i, 06] := qry.Fields[05].AsInteger; // ���-��
      VarData[i, 07] := qry.Fields[06].AsInteger; // ���-��
      VarData[i, 08] := qry.Fields[07].AsInteger; // ���-��
      VarData[i, 09] := qry.Fields[08].AsInteger; // ���-��
      VarData[i, 10] := qry.Fields[09].AsInteger; // ���-��
      VarData[i, 11] := qry.Fields[10].AsInteger; // ���-��
      VarData[i, 12] := qry.Fields[11].AsInteger; // ���-��
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:L%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:L%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:L%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
    WS := App.ActiveWorkbook.Worksheets.Item[3];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry.SelectSQL.Text := HIV_DIRECTIONS_ANON_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 12], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := RussianMonthsInf[qry.Fields[00].AsInteger];
      VarData[i, 02] := qry.Fields[01].AsInteger; // ���-��
      VarData[i, 03] := qry.Fields[02].AsInteger; // ���-��
      VarData[i, 04] := qry.Fields[03].AsInteger; // ���-��
      VarData[i, 05] := qry.Fields[04].AsInteger; // ���-��
      VarData[i, 06] := qry.Fields[05].AsInteger; // ���-��
      VarData[i, 07] := qry.Fields[06].AsInteger; // ���-��
      VarData[i, 08] := qry.Fields[07].AsInteger; // ���-��
      VarData[i, 09] := qry.Fields[08].AsInteger; // ���-��
      VarData[i, 10] := qry.Fields[09].AsInteger; // ���-��
      VarData[i, 11] := qry.Fields[10].AsInteger; // ���-��
      VarData[i, 12] := qry.Fields[11].AsInteger; // ���-��
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:L%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:L%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:L%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
  finally
    qry.Free;
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormHIVLabDirectionsDailyRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
Const
  ALL_DIRECTIONS_SQL = 'SELECT D.ADATE, P.NAME, P.BIRTHDAY,' +
    ' CAST(LIST(DISTINCT HSC.CODE) AS VARCHAR(500)) CODES,' +
    ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(1,2,6),D.DIRECTIONNO,NULL)) HIV,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(7,44),D.DIRECTIONNO,NULL)) PSA,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(26,27,28,29,30,31,32,33),' +
    '    D.DIRECTIONNO,NULL)) HEPATIT,' +
    ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(11),D.DIRECTIONNO,NULL)) SIPHILYS,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(12),D.DIRECTIONNO,NULL)) MRP,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(13),D.DIRECTIONNO,NULL)) RPGA,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(4),D.DIRECTIONNO,NULL)) IMMUNOGRAMS,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(14),D.DIRECTIONNO,NULL)) BLOODCT,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(15),D.DIRECTIONNO,NULL)) BIOCHEM,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(16),D.DIRECTIONNO,NULL)) TOXO,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(17),D.DIRECTIONNO,NULL)) CMV,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(18),D.DIRECTIONNO,NULL)) VPG,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(19),D.DIRECTIONNO,NULL)) CHLAM,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(3,5,8,9,10,35,36,37,38,39,40,41),'
    + '     D.DIRECTIONNO,NULL)) PCR,' +
    ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(20),D.DIRECTIONNO,NULL)) VIRALLOAD,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(21),D.DIRECTIONNO,NULL)) INJ,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(22),D.DIRECTIONNO,NULL)) KRASN,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(24),D.DIRECTIONNO,NULL)) CA_125,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(23),D.DIRECTIONNO,NULL)) DUPL,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(25),D.DIRECTIONNO,NULL)) HORM,'
    + ' LIST(DISTINCT IIF(D.REFHIVANALYSISTYPES IN(43),D.DIRECTIONNO,NULL)) VEB,'
    + ' COALESCE(LIST(DISTINCT D.CHEQUENO),LIST(DISTINCT C.NAME),'''') CHEQUES,'
    + ' MAX(D.ID) ORDERNO' + ' FROM ' + ' HIVDIRECTIONS D' +
    ' INNER JOIN PERSONS P ON D.REFPERSONS=P.ID' +
    ' LEFT JOIN CONTRACTORS C ON D.REFCONTRACTORS=C.ID' +
    ' LEFT JOIN HIVSCREENINGCODES HSC ON D.REFHIVSCREENINGCODES=HSC.ID' +
    ' WHERE D.ADATE BETWEEN :StartDate and :EndDate GROUP BY 1,2,3 ORDER BY 27,1';
  HEPATIT_DIRECTIONS_SQL = 'SELECT D.ADATE, P.NAME,' + '  MAX(D.ID) ORDERNO,' +
    '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=26,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) HBsAg,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=27,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) "aHCV-G",'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=29,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) aHBs,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=30,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) "aHBcor-M,G",'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=28,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) "aHCV-M,G",'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=32,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) "aHDV-M,G",'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=33,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) "aHAV-M,G",'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=31,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) "HBeAg. aHBe"'
    + ' FROM' + '  HIVDIRECTIONS D' +
    '  INNER JOIN PERSONS P ON D.REFPERSONS=P.ID' + ' WHERE' +
    '  D.ADATE BETWEEN :StartDate and :EndDate' +
    '  AND D.REFHIVANALYSISTYPES IN (26,27,28,29,30,31,32,33)' +
    ' GROUP BY 1,2 ORDER BY 3';
  PCR_DIRECTIONS_SQL = 'SELECT D.ADATE,P.NAME,' + '  MAX(D.ID) ORDERNO,' +
    '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=36,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) RNAHCV_QL,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=37,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) RNAHCV_QT,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=38,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) RNAHCV_GT,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=05,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) DNAHBV_QL,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=35,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) DNAHBV_QL,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=39,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) RNAHDV_QL,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=03,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) HIV,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=09,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) TOXO,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=08,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) VPCH,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=10,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) CMV,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=40,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) HLAB,'
    + '  CAST(LIST(IIF(D.REFHIVANALYSISTYPES=41,D.DIRECTIONNO,NULL)) AS VARCHAR(500)) RESIST'
    + ' FROM' + '  HIVDIRECTIONS D' +
    '  INNER JOIN PERSONS P ON D.REFPERSONS=P.ID' + ' WHERE' +
    '  D.ADATE BETWEEN :StartDate and :EndDate' +
    '  AND D.REFHIVANALYSISTYPES IN (3,5,8,9,10,35,36,37,38,39,40,41)' +
    ' GROUP BY 1,2 ORDER BY 3';
var
  VarData, App, WS: Variant;
  i, rc: integer;
  qry: TFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\���������� ����� �� ������������.xlt', '\\reports',
    '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    // if Trunc(StartDate) = Trunc(EndDate) then
    // ws.Columns.Item[1].ColumnWidth := 0;
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := ALL_DIRECTIONS_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 27], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := i;
      VarData[i, 02] := qry.Fields[00].AsDateTime;
      VarData[i, 03] := qry.Fields[01].asString;
      VarData[i, 04] := qry.Fields[02].asString; // ���-��
      VarData[i, 05] := qry.Fields[03].asString; // ���-��
      VarData[i, 06] := qry.Fields[04].asString; // ���-��
      VarData[i, 07] := qry.Fields[05].asString; // ���-��
      VarData[i, 08] := qry.Fields[06].asString; // ���-��
      VarData[i, 09] := qry.Fields[07].asString; // ���-��
      VarData[i, 10] := qry.Fields[08].asString; // ���-��
      VarData[i, 11] := qry.Fields[09].asString; // ���-��
      VarData[i, 12] := qry.Fields[10].asString; // ���-��
      VarData[i, 13] := qry.Fields[11].asString; // ���-��
      VarData[i, 14] := qry.Fields[12].asString; // ���-��
      VarData[i, 15] := qry.Fields[13].asString; // ���-��
      VarData[i, 16] := qry.Fields[14].asString; // ���-��
      VarData[i, 17] := qry.Fields[15].asString; // ���-��
      VarData[i, 18] := qry.Fields[16].asString; // ���-��
      VarData[i, 19] := qry.Fields[17].asString; // ���-��
      VarData[i, 20] := qry.Fields[18].asString; // ���-��
      VarData[i, 21] := qry.Fields[19].asString; // ���-��
      VarData[i, 22] := qry.Fields[20].asString; // ���-��
      VarData[i, 23] := qry.Fields[21].asString; // ���-��
      VarData[i, 24] := qry.Fields[22].asString; // ���-��
      VarData[i, 25] := qry.Fields[23].asString;
      VarData[i, 26] := qry.Fields[24].asString;
      VarData[i, 27] := qry.Fields[25].asString;
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      WS.Range[Format('A%d:AA%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:AA%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
      if Trunc(StartDate) = Trunc(EndDate) then
        WS.Columns.Item[2].ColumnWidth := 0;
    end;
    // ����� �� ���������
    WS := App.ActiveWorkbook.Worksheets.Item[2];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    if Trunc(StartDate) = Trunc(EndDate) then
      WS.Columns.Item[1].ColumnWidth := 0;
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := HEPATIT_DIRECTIONS_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 11], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := i;
      VarData[i, 02] := qry.Fields[00].AsDateTime;
      VarData[i, 03] := qry.Fields[01].asString;
      VarData[i, 04] := qry.Fields[03].asString; // ���-��
      VarData[i, 05] := qry.Fields[04].asString; // ���-��
      VarData[i, 06] := qry.Fields[05].asString; // ���-��
      VarData[i, 07] := qry.Fields[06].asString; // ���-��
      VarData[i, 08] := qry.Fields[07].asString; // ���-��
      VarData[i, 09] := qry.Fields[08].asString; // ���-��
      VarData[i, 10] := qry.Fields[09].asString; // ���-��
      VarData[i, 11] := qry.Fields[10].asString; // ���-��
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      WS.Range[Format('A%d:K%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:K%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
      if Trunc(StartDate) = Trunc(EndDate) then
        WS.Columns.Item[2].ColumnWidth := 0;
    end;
    // ����� �� ���������
    WS := App.ActiveWorkbook.Worksheets.Item[3];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    if Trunc(StartDate) = Trunc(EndDate) then
      WS.Columns.Item[1].ColumnWidth := 0;
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := PCR_DIRECTIONS_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 15], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := i;
      VarData[i, 02] := qry.Fields[00].AsDateTime;
      VarData[i, 03] := qry.Fields[01].asString;
      VarData[i, 04] := qry.Fields[03].asString; // ���-��
      VarData[i, 05] := qry.Fields[04].asString; // ���-��
      VarData[i, 06] := qry.Fields[05].asString; // ���-��
      VarData[i, 07] := qry.Fields[06].asString; // ���-��
      VarData[i, 08] := qry.Fields[07].asString; // ���-��
      VarData[i, 09] := qry.Fields[08].asString; // ���-��
      VarData[i, 10] := qry.Fields[09].asString; // ���-��
      VarData[i, 11] := qry.Fields[10].asString; // ���-��
      VarData[i, 12] := qry.Fields[11].asString; // ���-��
      VarData[i, 13] := qry.Fields[12].asString; // ���-��
      VarData[i, 14] := qry.Fields[13].asString; // ���-��
      VarData[i, 15] := qry.Fields[14].asString; // ���-��
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      WS.Range[Format('A%d:O%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:O%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
      if Trunc(StartDate) = Trunc(EndDate) then
        WS.Columns.Item[2].ColumnWidth := 0;
    end;
  finally
    qry.Free;
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormHIVLabAvgCountRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
Const
  IMMUNOGRAMS_SQL = 'SELECT p.AMBULATORYCARDNO,' +
    ' P.NAME, COUNT(R.ID),LIST(COALESCE(R.ANALYSISNO,'''')||'' �� ''||R.TAKINGDATE)'
    + ' FROM ' + ' HIVIMMUNOGRAMS R' +
    ' INNER JOIN PERSONS P ON R.REFPERSONS=P.ID' +
    ' LEFT JOIN HIV H ON H.REFPERSONS=P.ID' +
    '   AND R.TAKINGDATE BETWEEN H.REGISTRYDATE' +
    '     AND COALESCE(H.UNREGISTRYDATE,CAST(''01.01.3000'' as DATE))' +
    ' WHERE R.TAKINGDATE BETWEEN :StartDate and :EndDate GROUP BY 1,2 ORDER BY 3 DESC';
  VIRALLOAD_SQL = 'SELECT p.AMBULATORYCARDNO,' +
    ' P.NAME, COUNT(R.ID),LIST(COALESCE(R.ANALYSISNO,'''')||'' �� ''||R.TAKINGDATE)'
    + ' FROM ' + ' HIVPCR R' + ' INNER JOIN PERSONS P ON R.REFPERSONS=P.ID' +
    ' LEFT JOIN HIV H ON H.REFPERSONS=P.ID' +
    '   AND R.TAKINGDATE BETWEEN H.REGISTRYDATE' +
    '     AND COALESCE(H.UNREGISTRYDATE,CAST(''01.01.3000'' as DATE))' +
    ' WHERE R.TAKINGDATE BETWEEN :StartDate and :EndDate GROUP BY 1,2 ORDER BY 3 DESC';
var
  VarData, App, WS: Variant;
  i, rc: integer;
  qry: TFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\��������� ������������ ������������.xlt', '\\reports',
    '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := IMMUNOGRAMS_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 5], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := i;
      VarData[i, 02] := qry.Fields[00].asString;
      VarData[i, 03] := qry.Fields[01].asString;
      VarData[i, 04] := qry.Fields[02].AsInteger; // ���-��
      VarData[i, 05] := qry.Fields[03].asString; // ������ ������� ����
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:E%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:E%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:E%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
    WS := App.ActiveWorkbook.Worksheets.Item[2];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry.SelectSQL.Text := VIRALLOAD_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 5], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := i;
      VarData[i, 02] := qry.Fields[00].asString;
      VarData[i, 03] := qry.Fields[01].asString;
      VarData[i, 04] := qry.Fields[02].AsInteger; // ���-��
      VarData[i, 05] := qry.Fields[03].asString; // ������ ������� ����
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:E%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:E%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:E%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
  finally
    qry.Free;
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormHIVCD4LessThan350Rpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
Const
  IMMUNOGRAMS_SQL =
    'SELECT' +
    ' p.AMBULATORYCARDNO,' +
    ' P.NAME,' +
    ' R.CD4,' +
    ' R.TAKINGDATE,' +
    ' (SELECT MAX(HA.STARTDATE) FROM HIVART HA WHERE HA.REFPERSONS=R.REFPERSONS' +
    '  AND :ENDDATE BETWEEN HA.STARTDATE AND COALESCE(HA.ENDDATE,:ENDDATE)) ARTDATE' +
    ' FROM ' +
    ' HIVIMMUNOGRAMS R' +
    ' INNER JOIN PERSONS P ON R.REFPERSONS=P.ID' +
    ' LEFT JOIN HIV H ON H.REFPERSONS=P.ID' +
    '   AND R.TAKINGDATE BETWEEN H.REGISTRYDATE' +
    '     AND COALESCE(H.UNREGISTRYDATE,CAST(''01.01.3000'' as DATE))' +
    ' WHERE R.TAKINGDATE BETWEEN :StartDate and :EndDate' + ' AND R.CD4<350 ' +
    ' AND NOT EXISTS(SELECT R1.ID FROM HIVIMMUNOGRAMS R1' +
    ' WHERE R1.TAKINGDATE>R.TAKINGDATE' + '   AND R1.REFPERSONS=P.ID)' +
    ' AND :EndDate BETWEEN H.REGISTRYDATE' +
    '     AND COALESCE(H.UNREGISTRYDATE,CAST(''01.01.3000'' as DATE))' +
    ' ORDER BY 2';
var
  VarData, App, WS: Variant;
  i, rc: integer;
  qry: TFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\������ ��������� CD4 ����� 350.xlt', '\\reports',
    '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := IMMUNOGRAMS_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 6], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := i;
      VarData[i, 02] := qry.Fields[00].asString;
      VarData[i, 03] := qry.Fields[01].asString;
      VarData[i, 04] := qry.Fields[02].AsInteger; // ���-��
      VarData[i, 05] := qry.Fields[03].asString; // ������ ������� ����
      VarData[i, 06] := qry.Fields[04].asString;
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:F%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:F%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:F%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
  finally
    qry.Free;
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure PrintConsentHK(ContractorID: integer; Trans: TFIBTransaction;
  Status: TStatusBar = nil);
var
  App, doc, VarData: Variant;
  StrValue: String;
const
  PERSON_SQL = 'select c.NAME,c.BIRTHDAY,c.ADDRESS,p.NAME,' +
    ' p.ADDRESS_REG, RT.NAME ' +
    ' from PERSONS p INNER JOIN CONTRACTORS C ON C.REFPERSONS=P.ID ' +
    ' INNER JOIN RELATIONTYPES rt ON c.REFRELATIONTYPES=RT.ID ' +
    'where c.ID=:ID';
begin
  App := InitWord(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\�������� HK.dot', '\\reports', '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    doc := App.ActiveDocument;
    // ���������� �� ����������� -----------------------------------------------
    VarData := Trans.DefaultDatabase.QueryValues(PERSON_SQL, [ContractorID]);
    doc.Variables.Item('PatientName').Value := VarToStr(VarData[0]);
    if VarIsNull(VarData[1]) then
      doc.Variables.Item('Birthday').Value := ' '
    else
      doc.Variables.Item('Birthday').Value := FullRussianDateToStr(VarData[1]);
    StrValue := VarToStr(VarData[2]);
    if StrValue = '' then
      StrValue := ' ';
    doc.Variables.Item('AddressReg').Value := StrValue;
    doc.Variables.Item('ChildName').Value := VarToStr(VarData[3]);
    StrValue := VarToStr(VarData[4]);
    if StrValue = '' then
      StrValue := ' ';
    doc.Variables.Item('ChildAddress').Value := StrValue;
    doc.Variables.Item('DocDate').Value := FullRussianDateToStr(Date);
    StrValue := VarToStr(VarData[5]);
    if StrValue = '' then
      StrValue := ' ';
    doc.Variables.Item('Rtype').Value := StrValue;
    // -------------------------------------------------------------------------
    doc.ActiveWindow.View.ShowFieldCodes := false;
    doc.Fields.Update;
    doc.Application.Selection.HomeKey(6);
    doc.Saved := true;
  finally
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.DisplayAlerts := 0;
    App.Options.PrintBackground := false;
    doc.PrintOut;
    doc.Close(0);
    doc := UnAssigned;
    App.Options.PrintBackground := true;
    App.Quit;
  end;
end;

procedure FormContractorDirectionsRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
Const
  SQL = 'SELECT HD.ADATE,' + ' P.NAME, HAT.NAME, HD.DIRECTIONNO, CT.NAME' +
    ' FROM ' + ' HIVDIRECTIONS HD' +
    ' INNER JOIN PERSONS P ON HD.REFPERSONS=P.ID' +
    ' INNER JOIN HIVANALYSISTYPES HAT ON HD.REFHIVANALYSISTYPES=HAT.ID' +
    ' INNER JOIN CONTRACTORS CT ON HD.REFCONTRACTORS=CT.ID' +
    ' WHERE HD.ADATE BETWEEN :StartDate and :EndDate' + ' ORDER BY 1';

  AGGR_SQL = 'SELECT' + '  CT.NAME,' +
    '  COUNT(DISTINCT IIF(HAT.ID NOT IN (3,5,8,9,10,35,36,37,38,39,40,41),' +
    '  P.ID||''-''||HD.ADATE,NULL)),' +
    '  COUNT(DISTINCT IIF(HAT.ID IN (3,5,8,9,10,35,36,37,38,39,40,41),' +
    '  P.ID||''-''||HD.ADATE,NULL))' + ' FROM' + ' HIVDIRECTIONS HD' +
    ' INNER JOIN PERSONS P ON HD.REFPERSONS=P.ID' +
    ' INNER JOIN HIVANALYSISTYPES HAT ON HD.REFHIVANALYSISTYPES=HAT.ID' +
    ' INNER JOIN CONTRACTORS CT ON HD.REFCONTRACTORS=CT.ID' +
    ' WHERE HD.ADATE BETWEEN :StartDate and :EndDate' + ' GROUP BY 1';
var
  VarData, App, WS: Variant;
  i, rc: integer;
  qry: TFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\����� �� ������������ �� ����������.xlt', '\\reports',
    '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    i := 1;
    VarData := VarArrayCreate([1, rc, 1, 5], VarVariant);
    while not qry.Eof do
    begin
      VarData[i, 01] := qry.Fields[00].AsDateTime; // ���� �����������
      VarData[i, 02] := qry.Fields[01].asString; // ��� ��������
      VarData[i, 03] := qry.Fields[02].asString; // ��� �������
      VarData[i, 04] := qry.Fields[03].asString; // ����� �����������
      VarData[i, 05] := qry.Fields[04].asString; // ��������
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:E%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:E%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:E%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
    WS := App.ActiveWorkbook.Worksheets.Item[2];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := AGGR_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 3], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 1] := qry.Fields[0].asString; // ��������, ������������
      VarData[i, 2] := qry.Fields[1].AsInteger; // ���������� �����
      VarData[i, 3] := qry.Fields[2].AsInteger; // ���������� ���
      if Assigned(Status) then
        Status.Panels[0].Text := '������������ ������ (' +
          IntToStr(Round(i / rc * 100)) + '%)...';
      Application.ProcessMessages;
      qry.Next;
      inc(i);
    end;
    if (rc > 2) then
      WS.Range['A5:C5'].Resize[rc - 2].EntireRow.Insert;
    if (rc > 0) then
      WS.Range[Format('A%d:C%d', [4, rc + 3])].Value := VarData;
  finally
    qry.Free;
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormHIVDirectionsListRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
Const
  SQL = 'SELECT R.DIRECTIONNO, R.ADATE,' +
    ' P.NAME, LEFT(S.NAME,1), EXTRACT(YEAR FROM P.BIRTHDAY) AGE, HSC.CODE' +
    ' FROM ' + ' HIVDIRECTIONS R' + ' INNER JOIN PERSONS P ON R.REFPERSONS=P.ID'
    + ' INNER JOIN SEX S ON P.REFSEX=S.ID' +
    ' LEFT JOIN HIVSCREENINGCODES HSC ON R.REFHIVSCREENINGCODES=HSC.ID' +
    ' WHERE R.ADATE BETWEEN :StartDate and :EndDate AND R.REFHIVANALYSISTYPES IN (1,2,6)'
    + ' ORDER BY 2,1';
var
  VarData, App, WS: Variant;
  i, rc: integer;
  qry: TFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\������-����������� � �����������.xlt', '\\reports',
    '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 6], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := qry.Fields[00].asString;
      // VarData[i, 02] := qry.Fields[01].asDateTime;
      VarData[i, 03] := qry.Fields[02].asString;
      VarData[i, 04] := qry.Fields[03].asString;
      VarData[i, 05] := qry.Fields[04].asString;
      VarData[i, 06] := qry.Fields[05].asString;
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:F%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:F%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:F%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
  finally
    qry.Free;
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormHIVDispensaryRegistrationRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
Const
  SQL = 'SELECT' + ' (SELECT COUNT (DISTINCT P.ID)' + ' FROM' + '  HIV H' +
    '  INNER JOIN PERSONS P ON H.REFPERSONS=P.ID' + ' WHERE' +
    '  H.REGISTRYDATE<:STARTDATE AND' +
    '   COALESCE(H.UNREGISTRYDATE,CAST(''3000-01-01'' AS DATE))>=:STARTDATE) ONSTART,'
    + ' (SELECT COUNT (DISTINCT P.ID)' + ' FROM' + '  HIV H' +
    '  INNER JOIN PERSONS P ON H.REFPERSONS=P.ID' +
    ' WHERE H.REGISTRYDATE BETWEEN :STARTDATE AND :ENDDATE) REG,' +
    ' (SELECT COUNT (DISTINCT P.ID)' + ' FROM' + '  HIV H' +
    '  INNER JOIN PERSONS P ON H.REFPERSONS=P.ID' +
    ' WHERE H.UNREGISTRYDATE BETWEEN :STARTDATE AND :ENDDATE) UNREG,' +
    ' (SELECT COUNT (DISTINCT P.ID)' + ' FROM' + '  HIV H' +
    '  INNER JOIN PERSONS P ON H.REFPERSONS=P.ID' +
    ' WHERE H.REGISTRYDATE<=:ENDDATE AND' +
    ' COALESCE(H.UNREGISTRYDATE,CAST(''3000-01-01'' AS DATE))>:ENDDATE) ONEND' +
    ' FROM RDB$DATABASE';
  REG_SQL = 'SELECT' + ' p.AMBULATORYCARDNO, P.NAME, H.REGISTRYDATE ' + ' FROM'
    + '  HIV H' + '  INNER JOIN PERSONS P ON H.REFPERSONS=P.ID' +
    ' WHERE H.REGISTRYDATE BETWEEN :STARTDATE AND :ENDDATE';
  UNREG_SQL = 'SELECT' + ' p.AMBULATORYCARDNO,' + ' P.NAME,' +
    ' H.UNREGISTRYDATE,' + ' HO.NAME' + ' FROM' + '  HIV H' +
    '  INNER JOIN PERSONS P ON H.REFPERSONS=P.ID' +
    '  LEFT JOIN HIVOUTCOMES HO ON H.REFHIVOUTCOMES=HO.ID' +
    ' WHERE H.UNREGISTRYDATE BETWEEN :STARTDATE AND :ENDDATE';
var
  VarData, App, WS: Variant;
  i, rc: integer;
  qry: TFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\����� �� ������������� �����.xlt', '\\reports', '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    // ����� � ��������� ������������� �����
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    VarData := Trans.DefaultDatabase.QueryValues(SQL,
      [StartDate, StartDate, StartDate, EndDate, StartDate, EndDate, EndDate,
      EndDate]);
    WS.Range['A4:D4'].Value := VarData;
    // ������ �� ����
    WS := App.ActiveWorkbook.Worksheets.Item[2];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := REG_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 4], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := i;
      VarData[i, 02] := qry.Fields[0].asString;
      VarData[i, 03] := qry.Fields[1].asString;
      VarData[i, 04] := qry.Fields[2].asString;
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:D%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:D%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:D%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
    // ������ � �����
    WS := App.ActiveWorkbook.Worksheets.Item[3];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := UNREG_SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 5], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := i;
      VarData[i, 02] := qry.Fields[0].asString;
      VarData[i, 03] := qry.Fields[1].asString;
      VarData[i, 04] := qry.Fields[2].asString;
      VarData[i, 05] := qry.Fields[3].asString;
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:E%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:E%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:E%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
  finally
    qry.Free;
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormHIVZeroViralLoadRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
Const
  SQL = 'SELECT p.AMBULATORYCARDNO,' + ' P.NAME,  R.TAKINGDATE' + ' FROM ' +
    ' HIVPCR R' + ' INNER JOIN PERSONS P ON R.REFPERSONS=P.ID' +
    ' LEFT JOIN HIV H ON H.REFPERSONS=P.ID' +
    '   AND R.TAKINGDATE BETWEEN H.REGISTRYDATE' +
    '     AND COALESCE(H.UNREGISTRYDATE,CAST(''01.01.3000'' as DATE))' +
    ' WHERE R.TAKINGDATE BETWEEN :StartDate and :EndDate' +
    ' AND (R.ISBELOWSENSIVITY=1 OR R.VIRALLOAD=0)' +
    ' AND NOT EXISTS(SELECT R1.ID FROM HIVPCR R1' +
    '  WHERE R1.TAKINGDATE>R.TAKINGDATE' + '   AND R1.REFPERSONS=P.ID)' +
    ' ORDER BY 2';

  ISWOMEN_SQL = ' and (p.REFSEX=2)';
  CHILDS_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365)<18)';
  NOT_CHILDS_SQL =
    ' and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365)>=18)';
  PERSONS_COUNT_SQL = 'select ' + 'count(distinct p.id) ' + 'from persons p ' +
    'left join hiv h on h.refpersons=p.id ' +
    'where exists(select hpc.id from hivpcr hpc ' +
    'where hpc.refpersons=p.id and hpc.takingdate between ' +
    ':StartDate and :EndDate and hpc.isbelowsensivity=1 and ' +
    'not exists (select hpc1.id from hivpcr hpc1 ' +
    'where hpc1.refpersons=hpc.refpersons' +
    ' and hpc1.takingdate>hpc.takingdate' +
    '  and hpc.takingdate between :StartDate and :EndDate))';

  SECTION1_COLS_SQL: array [1 .. 8] of PositionedSQL = ((Row: 2; SQL: ''),
    (Row: 3; SQL: ISMEN_SQL + NOT_CHILDS_SQL), (Row: 4;
    SQL: ISWOMEN_SQL + NOT_CHILDS_SQL), (Row: 5; SQL: CHILDS_SQL), (Row: 6;
    SQL: REGISTEREDINPERIOD_SQL), (Row: 7;
    SQL: ISMEN_SQL + NOT_CHILDS_SQL + REGISTEREDINPERIOD_SQL), (Row: 8;
    SQL: ISWOMEN_SQL + NOT_CHILDS_SQL + REGISTEREDINPERIOD_SQL), (Row: 9;
    SQL: CHILDS_SQL + REGISTEREDINPERIOD_SQL));
var
  VarData, App, WS: Variant;
  i, rc: integer;
  qry: TFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\������ ��������� � �������������� ��.xlt', '\\reports',
    '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    WS.Range['A5'] := FormatDateTime('dd.mm.yyyy', EndDate);
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    for i := 1 to 8 do
    begin
      qry.Active := false;
      qry.SQLs.SelectSQL.Text := PERSONS_COUNT_SQL + SECTION1_COLS_SQL[i].SQL;
      qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
      qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
      qry.Open;
      WS.Cells.Item[5, SECTION1_COLS_SQL[i].Row] := qry.Fields[0].AsInteger;
    end;
    WS := App.ActiveWorkbook.Worksheets.Item[2];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 4], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := i;
      VarData[i, 02] := qry.Fields[00].asString; // ������������ �����
      VarData[i, 03] := qry.Fields[01].asString; // ���
      VarData[i, 04] := qry.Fields[02].asString; // ���� ������
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:D%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:D%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:D%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
  finally
    qry.Free;
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

{ +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++ "����� �� ����������" ++++++++++++++++++++++++++++++
  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ }
procedure PrintHIVPregnancy(Year: integer; DistrictFilter: String;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
const
  REGISTRY_UNTIL_BIRTHS =
    ' and ((h.REGISTRYDATE<:hp.BIRTHDATE) or (hp.BIRTHDATE is null))';
  REGISTRY_AFTER_BIRTHS = ' and (h.REGISTRYDATE>=:hp.BIRTHDATE)';
  IS_FIRST_HIVPCR = ' and exists (select hpc.ID from HIVPCR hpc ' +
    'where hpc.REFPERSONS=p.ID and hpc.TAKINGDATE between :StartDate ' +
    'and :EndDate and not exists (select hpc1.ID from HIVPCR hpc1 ' +
    'where hpc1.REFPERSONS=hpc.REFPERSONS and hpc1.TAKINGDATE>hpc.TAKINGDATE))';
  IS_FIRST_HIVPCR_34_36 = ' and exists (select hpc.ID from HIVPCR hpc ' +
    'where hpc.REFPERSONS=p.ID and hpc.TAKINGDATE between :StartDate ' +
    'and :EndDate and not exists (select hpc1.ID from HIVPCR hpc1 ' +
    'where hpc1.REFPERSONS=hpc.REFPERSONS and hpc1.TAKINGDATE>hpc.TAKINGDATE) '
    + 'and ((cast(hpc.TAKINGDATE-hp.DETECTIONDATE as integer)/7+FIRSTVISITWEEK) between 34 and 36))';
  IS_FIRST_HIVPCR_34_36_SENS = ' and exists (select hpc.ID from HIVPCR hpc ' +
    'where hpc.REFPERSONS=p.ID and hpc.TAKINGDATE between :StartDate ' +
    'and :EndDate and not exists (select hpc1.ID from HIVPCR hpc1 ' +
    'where hpc1.REFPERSONS=hpc.REFPERSONS and hpc1.TAKINGDATE>hpc.TAKINGDATE) '
    + 'and ((cast(hpc.TAKINGDATE-hp.DETECTIONDATE as integer)/7+FIRSTVISITWEEK) '
    + 'between 34 and 36) and hpc.ISBELOWSENSIVITY=1)';
  EXISTS_HIVPCR = ' and exists (select hpc.ID from hivpcr hpc ' +
    'where hpc.REFPERSONS=p.ID and hpc.TAKINGDATE between :StartDate and :EndDate)';
  EXISTS_2MONTH_PCR = ' and exists (select hpc.ID from hivpcr hpc ' +
    'where hpc.REFPERSONS=p.ID and hpc.TAKINGDATE between :StartDate and :EndDate '
    + 'and (cast(hpc.TAKINGDATE-p.BIRTHDAY as integer)<60))';
  EXISTS_POSITIVE_PCR = ' and exists (select hpc.ID from hivpcr hpc ' +
    'where hpc.REFPERSONS=p.ID and hpc.TAKINGDATE between :StartDate and :EndDate '
    + 'and hpc.REFHIVANALYSISRESULTS=1)';
  PREG_WOMANS_SQL = 'select count(distinct p.ID) ' + 'from PERSONS p ' +
    'inner join HIV h on h.REFPERSONS=p.ID ' +
    'inner join HIVPREGNANCY hp on hp.REFPERSONS=p.ID ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) and '
    + '(hp.DETECTIONDATE<=:ENDDATE) and ((hp.BIRTHDATE is null) or (hp.BIRTHDATE>=:STARTDATE))';
  PREG_CHILDREN_SQL = 'select count(distinct p.ID) ' + 'from PERSONS p ' +
    'inner join HIVCONTACTS hc on hc.REFPERSONS2=p.ID and hc.REFHIVCONTACTTYPES=3 '
    + 'inner join HIVPREGNANCY hp on hc.REFPERSONS1=hp.REFPERSONS ' +
    'inner join HIV h on h.REFPERSONS=hc.REFPERSONS1 ' +
    'where (h.REGISTRYDATE<=:ENDDATE) and ((h.UNREGISTRYDATE is null) or ' +
    '(h.UNREGISTRYDATE>=:STARTDATE)) and (p.BIRTHDAY between :STARTDATE and :ENDDATE)';
  CHILD_IS_REGISTERED = ' and exists (select hch.ID from HIV hch ' +
    'where hch.REFPERSONS=p.ID and hch.REGISTRYDATE between :STARTDATE and :ENDDATE)';
  HIV_CHILDREN_SQL = 'select count(distinct p.ID) ' + 'from PERSONS p ' +
    'inner join HIV h on h.REFPERSONS=p.ID ' +
    'left join HIVCONTACTS hc on hc.REFPERSONS2=p.ID and hc.REFHIVCONTACTTYPES=3 '
    + 'where (h.REGISTRYDATE<=:ENDDATE) and ((h.UNREGISTRYDATE is null) or (h.UNREGISTRYDATE>=:STARTDATE)) '
    + 'and ((cast(cast(:ENDDATE as DATE)-p.BIRTHDAY AS INTEGER)/365)<=18)';
  M_SERONEGATIVE_SQL = ' and hc.REFHIVCONTACTTYPES=3 ' +
    'and not exists (select h1.ID from HIV h1 where h1.REFPERSONS=hc.REFPERSONS1 '
    + 'and h1.REGISTRYDATE<=p.BIRTHDAY)';
  WITH_HIVART_SQL =
    ' and exists (select ha.ID from HIVART ha where ha.REFPERSONS=p.ID ' +
    'and ha.STARTDATE<=:ENDDATE and ((ha.ENDDATE is null) or (ha.ENDDATE>=:STARTDATE)))';
  WITH_HIVART_24W_SQL =
    ' and exists (select ha.ID from HIVART ha where ha.REFPERSONS=p.ID ' +
    'and ha.STARTDATE<=:ENDDATE and ((ha.ENDDATE is null) or (ha.ENDDATE>=:STARTDATE)) and '
    + 'cast(COALESCE(ha.ENDDATE, :ENDDATE)-ha.STARTDATE as integer)/7>24)';
  WITH_HIVART_1Y_SQL =
    ' and exists (select ha.ID from HIVART ha where ha.REFPERSONS=p.ID ' +
    'and ha.STARTDATE<=:ENDDATE and ((ha.ENDDATE is null) or (ha.ENDDATE>=:STARTDATE)) '
    + 'and (cast(ha.STARTDATE-p.BIRTHDAY as integer)/365<1))';
  WITH_CONTROL_VIRALLOAD_SQL = ' and exists (select hpc.ID from HIVPCR hpc ' +
    'where hpc.REFPERSONS=p.ID and hpc.TAKINGDATE between :STARTDATE and :ENDDATE)';
  WITH_CONTROL_VIRALLOAD_SENS_SQL =
    ' and exists (select hpc.ID from HIVPCR hpc ' +
    'where hpc.REFPERSONS=p.ID and hpc.TAKINGDATE between :STARTDATE and :ENDDATE and hpc.ISBELOWSENSIVITY=1)';
  WITH_CONTROL_CD4_SQL = ' and exists (select him.ID from HIVIMMUNOGRAMS him ' +
    'where him.REFPERSONS=p.ID and him.TAKINGDATE between :STARTDATE and :ENDDATE)';

  PREG_ROWCOUNT = 30;
  PREG_ROWS_SQL: array [1 .. PREG_ROWCOUNT] of PositionedSQL = ((Row: 2;
    SQL: PREG_WOMANS_SQL), (Row: 4;
    SQL: PREG_WOMANS_SQL + REGISTRY_UNTIL_BIRTHS), (Row: 5;
    SQL: PREG_WOMANS_SQL + REGISTRY_UNTIL_BIRTHS +
    ' and not(hp.REFDRUGS_PREG is null)'), (Row: 6;
    SQL: PREG_WOMANS_SQL + REGISTRY_UNTIL_BIRTHS + IS_FIRST_HIVPCR), (Row: 7;
    SQL: PREG_WOMANS_SQL + REGISTRY_UNTIL_BIRTHS + IS_FIRST_HIVPCR_34_36),
    (Row: 8; SQL: PREG_WOMANS_SQL + REGISTRY_UNTIL_BIRTHS +
    IS_FIRST_HIVPCR_34_36_SENS), (Row: 9;
    SQL: PREG_WOMANS_SQL + REGISTRY_UNTIL_BIRTHS + ' and (hp.ISCSECTION=1)'),
    (Row: 10; SQL: PREG_WOMANS_SQL + REGISTRY_UNTIL_BIRTHS +
    ' and not(hp.REFDRUGS_BIRTHS is null)'), (Row: 11;
    SQL: PREG_WOMANS_SQL + REGISTRY_UNTIL_BIRTHS +
    ' and not(hp.REFDRUGS_PREG is null) and ' +
    'not (hp.REFDRUGS_BIRTHS is null) and not(hp.REFDRUGS_CHILD is null)'),
    (Row: 12; SQL: PREG_CHILDREN_SQL + REGISTRY_UNTIL_BIRTHS), (Row: 13;
    SQL: PREG_CHILDREN_SQL + REGISTRY_UNTIL_BIRTHS +
    ' and not(hp.REFDRUGS_CHILD is null)'), (Row: 14;
    SQL: PREG_CHILDREN_SQL + REGISTRY_UNTIL_BIRTHS +
    ' and (hp.IS3DRUGS_CHILD=1)'), (Row: 15;
    SQL: PREG_CHILDREN_SQL + REGISTRY_UNTIL_BIRTHS + EXISTS_HIVPCR), (Row: 16;
    SQL: PREG_CHILDREN_SQL + REGISTRY_UNTIL_BIRTHS + EXISTS_2MONTH_PCR),
    (Row: 17; SQL: PREG_CHILDREN_SQL + REGISTRY_UNTIL_BIRTHS +
    EXISTS_POSITIVE_PCR), (Row: 18;
    SQL: PREG_CHILDREN_SQL + REGISTRY_UNTIL_BIRTHS + EXISTS_HIVPCR +
    CHILD_IS_REGISTERED), (Row: 21;
    SQL: PREG_WOMANS_SQL + REGISTRY_AFTER_BIRTHS), (Row: 22;
    SQL: PREG_WOMANS_SQL + REGISTRY_AFTER_BIRTHS +
    ' and not(hp.REFHIVANALYSISRESULTS is null)'), (Row: 23;
    SQL: PREG_WOMANS_SQL + REGISTRY_AFTER_BIRTHS +
    ' and (hp.REFHIVANALYSISRESULTS=1)'), (Row: 24;
    SQL: PREG_WOMANS_SQL + REGISTRY_AFTER_BIRTHS +
    ' and not(hp.REFDRUGS_BIRTHS is null)'), (Row: 25; SQL: ''),
    // ���-�� ������, � ������� ���������� � ������� ���-�������� �����
    // �������� ����� 3 ��� ����� ����� � �����
    (Row: 26; SQL: ''), // ���-�� ��� ����� �������������� ��������-�����
    (Row: 27; SQL: PREG_WOMANS_SQL + REGISTRY_AFTER_BIRTHS +
    ' and (hp.ISCSECTION=1)'), (Row: 28;
    SQL: PREG_CHILDREN_SQL + REGISTRY_AFTER_BIRTHS), (Row: 29;
    SQL: PREG_CHILDREN_SQL + REGISTRY_AFTER_BIRTHS +
    ' and not(hp.REFDRUGS_CHILD is null)'), (Row: 30;
    SQL: PREG_CHILDREN_SQL + REGISTRY_AFTER_BIRTHS +
    ' and (hp.IS3DRUGS_CHILD=1)'), (Row: 31;
    SQL: PREG_CHILDREN_SQL + REGISTRY_AFTER_BIRTHS + EXISTS_HIVPCR), (Row: 32;
    SQL: PREG_CHILDREN_SQL + REGISTRY_AFTER_BIRTHS + EXISTS_2MONTH_PCR),
    (Row: 33; SQL: PREG_CHILDREN_SQL + REGISTRY_AFTER_BIRTHS +
    EXISTS_POSITIVE_PCR), (Row: 34;
    SQL: PREG_CHILDREN_SQL + REGISTRY_AFTER_BIRTHS + EXISTS_HIVPCR +
    CHILD_IS_REGISTERED));
  CHILDREN_ROWCOUNT = 8;
  CHILDREN_ROWS_SQL: array [1 .. CHILDREN_ROWCOUNT] of PositionedSQL =
    ((Row: 37; SQL: HIV_CHILDREN_SQL), (Row: 38;
    SQL: HIV_CHILDREN_SQL + M_SERONEGATIVE_SQL), (Row: 39;
    SQL: HIV_CHILDREN_SQL + WITH_HIVART_SQL), (Row: 40;
    SQL: HIV_CHILDREN_SQL + WITH_HIVART_1Y_SQL), (Row: 41;
    SQL: HIV_CHILDREN_SQL + WITH_CONTROL_VIRALLOAD_SQL), (Row: 42;
    SQL: HIV_CHILDREN_SQL + WITH_CONTROL_VIRALLOAD_SENS_SQL), (Row: 43;
    SQL: HIV_CHILDREN_SQL + WITH_HIVART_24W_SQL), (Row: 44;
    SQL: HIV_CHILDREN_SQL + WITH_CONTROL_CD4_SQL));
var
  i, j: integer;
  dd, mm, yy: WORD;
  qry: TFIBDataset;
  App, WS: Variant;
  StartDate, EndDate, StartQDate: TDateTime;
begin
  yy := Year;
  mm := 1;
  dd := 1;
  StartDate := EncodeDate(yy, mm, dd);
  mm := 10;
  dd := 1;
  StartQDate := EncodeDate(yy, mm, dd);
  mm := 12;
  dd := 31;
  EndDate := EncodeDate(yy, mm, dd);
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\����� �� ����������.xlt', '\\reports', '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Range['D1'] := FormatDateTime('�� yyyy ���', EndDate);
    WS.Range['D20'] := FormatDateTime('�� yyyy ���', EndDate);
    qry := TFIBDataset.Create(nil);
    try
      qry.Transaction := Trans;
      WS := App.ActiveWorkbook.Worksheets.Item[1];
      // -----------------------------------------------------------------------
      for i := 1 to PREG_ROWCOUNT do
        if (PREG_ROWS_SQL[i].SQL <> '') then
          for j := 1 to 2 do
          begin
            qry.Active := false;
            qry.SQLs.SelectSQL.Text := PREG_ROWS_SQL[i].SQL + DistrictFilter;
            if j = 1 then
            begin
              qry.Params.ParamByName('StartDate').AsDateTime := StartQDate;
              qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
            end;
            if j = 2 then
            begin
              qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
              qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
            end;
            qry.Open;
            if qry.Fields[0].AsInteger > 0 then
              WS.Cells.Item[PREG_ROWS_SQL[i].Row, j + 2] :=
                qry.Fields[0].AsInteger;
          end;
      // -----------------------------------------------------------------------
      for i := 1 to CHILDREN_ROWCOUNT do
        if (CHILDREN_ROWS_SQL[i].SQL <> '') then
          for j := 1 to 2 do
          begin
            qry.Active := false;
            qry.SQLs.SelectSQL.Text := CHILDREN_ROWS_SQL[i].SQL +
              DistrictFilter;
            if j = 1 then
            begin
              qry.Params.ParamByName('StartDate').AsDateTime := StartQDate;
              qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
            end;
            if j = 2 then
            begin
              qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
              qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
            end;
            qry.Open;
            if qry.Fields[0].AsInteger > 0 then
              WS.Cells.Item[CHILDREN_ROWS_SQL[i].Row, j + 2] :=
                qry.Fields[0].AsInteger;
          end;
      // -----------------------------------------------------------------------
    finally
      qry.Free;
    end;
    App.ActiveWorkbook.Saved := true;
  finally
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormVisitsByTerritoriesRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
const
  ROWS_COUNT = 27;

  SQL_ROWS: Array [1 .. ROWS_COUNT] of PositionedSQL = (
    (Row: 01; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., �. �����������%'' OR PH.REFDISTRICTS=1)'),
    (Row: 02; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., �. �������%'' OR PH.REFDISTRICTS=2)'),
    (Row: 03; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., �. ������%'' OR PH.REFDISTRICTS=3)'),
    (Row: 04; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., �. ����������%'' OR PH.REFDISTRICTS=4)'),
    (Row: 05; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., �. ������������%'' OR PH.REFDISTRICTS=5)'),
    (Row: 06; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., �. ������%'' OR PH.REFDISTRICTS=6)'),
    (Row: 07; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ��������  �-�%'' OR PH.REFDISTRICTS=7)'),
    (Row: 08; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., �������������� �-�%'' OR PH.REFDISTRICTS=8)'),
    (Row: 09; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ����������� �-�%'' OR PH.REFDISTRICTS=9)'),
    (Row: 10; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., �������������� �-�%'' OR PH.REFDISTRICTS=10)'),
    (Row: 11; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ������������� �-�%'' OR PH.REFDISTRICTS=11)'),
    (Row: 12; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ��������� �-�%'' OR PH.REFDISTRICTS=12)'),
    (Row: 13; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ���������� �-�%'' OR PH.REFDISTRICTS=13)'),
    (Row: 14; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ������������� �-�%'' OR PH.REFDISTRICTS=14)'),
    (Row: 15; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., �������  �-�%'' OR PH.REFDISTRICTS=15)'),
    (Row: 16; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ����������� �-�%'' OR PH.REFDISTRICTS=16)'),
    (Row: 17; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ��������� �-�%'' OR PH.REFDISTRICTS=17)'),
    (Row: 18; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ���������� �-�%'' OR PH.REFDISTRICTS=18)'),
    (Row: 19; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., �������� �-�%'' OR PH.REFDISTRICTS=19)'),
    (Row: 20; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ��������� �-�%'' OR PH.REFDISTRICTS=20)'),
    (Row: 21; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ��������� �-�%'' OR PH.REFDISTRICTS=21)'),
    (Row: 22; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ���������� �-�%'' OR PH.REFDISTRICTS=22)'),
    (Row: 23; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ���������� �-�%'' OR PH.REFDISTRICTS=23)'),
    (Row: 24; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ������������ �-�%'' OR PH.REFDISTRICTS=24)'),
    (Row: 24; SQL
    : ' AND (PH.ID IS NULL AND P.AMBULATORYCARDNO IS NULL AND P.ADDRESS_LIV LIKE ''������������� ���., ���������� �-�%'' OR PH.REFDISTRICTS=25)'),
    (Row: 26; SQL: ' AND NOT EXISTS(SELECT * FROM PLACESHISTORY PH1 WHERE PH1.REFPERSONS=P.ID'
    + '  AND V.VISITDATE BETWEEN PH1.STARTDATE' +
    '   AND COALESCE(PH1.ENDDATE,CAST(''3000-01-01'' AS DATE)))' +
    ' AND P.ADDRESS_LIV LIKE ''�������� ���.���.%'''),
    (Row: 27; SQL: ' AND NOT EXISTS(SELECT * FROM PLACESHISTORY PH1 WHERE PH1.REFPERSONS=P.ID'
    + '  AND V.VISITDATE BETWEEN PH1.STARTDATE' +
    '   AND COALESCE(PH1.ENDDATE,CAST(''3000-01-01'' AS DATE)))' +
    ' AND (P.ADDRESS_LIV LIKE ''���. ������%'' OR P.ADDRESS_LIV IS NULL)'));

  MONTHS_SQL = 'SELECT DISTINCT EXTRACT(MONTH FROM VISITDATE)' +
    ' FROM VISITS WHERE ISVISITHAPPENED=1' +
    ' AND VISITDATE BETWEEN :STARTDATE AND :ENDDATE';

  MONTHS_COUNT_SQL = 'SELECT COUNT(DISTINCT EXTRACT(MONTH FROM VISITDATE))' +
    ' FROM VISITS WHERE ISVISITHAPPENED=1' +
    ' AND VISITDATE BETWEEN :STARTDATE AND :ENDDATE';

  SELECT_SQL = 'SELECT COUNT(V.ID) FROM VISITS V' +
    ' INNER JOIN TREATMENTCASES TC ON TC.ID=V.REFTREATMENTCASES' +
    ' INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID' +
    ' LEFT JOIN PLACESHISTORY PH ON PH.REFPERSONS=P.ID' +
    '  AND V.VISITDATE BETWEEN PH.STARTDATE' +
    '   AND COALESCE(PH.ENDDATE, CAST(''3000-01-01'' AS DATE))' +
    ' WHERE V.ISVISITHAPPENED=1 AND EXTRACT(MONTH FROM V.VISITDATE)=:MONTH' +
    ' AND V.VISITDATE BETWEEN :STARTDATE AND :ENDDATE';
var
  App, WS, VarData: Variant;
  qry1, qry2: TFIBQuery;
  cnt, i, j: integer;
  Cell: String;
begin;
  if YearOf(StartDate) <> YearOf(EndDate) then
    raise Exception.Create('����� ����������� ������ � �������� ������ ����!');
  try
    qry1 := TFIBQuery.Create(nil);
    qry1.Transaction := Trans;
    qry2 := TFIBQuery.Create(nil);
    qry2.Transaction := Trans;
    qry1.SQL.Text := MONTHS_SQL;
    qry2.SQL.Text := SELECT_SQL;
    qry1.ParamByName('STARTDATE').AsDate := StartDate;
    qry1.ParamByName('ENDDATE').AsDate := EndDate;
    qry1.ExecQuery;
    if qry1.Eof then
      raise Exception.Create('�� ��������� ������ ��� �� ������ ���������!');
    App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
      '\reports\����� �� ����������.xlt', '\\reports', '\reports', []));
    cnt := Trans.DefaultDatabase.QueryValue(MONTHS_COUNT_SQL, 0,
      [StartDate, EndDate]);
    VarData := VarArrayCreate([1, ROWS_COUNT + 1, 1, cnt], VarVariant);
    i := 1;
    while not qry1.Eof do
    begin
      VarData[01, i] := qry1.Fields[0].AsInteger;
      for j := 1 to ROWS_COUNT do
      begin;
        qry2.SQL.Text := SELECT_SQL + SQL_ROWS[j].SQL;
        qry2.ParamByName('MONTH').AsInteger := qry1.Fields[0].AsInteger;
        qry2.ParamByName('STARTDATE').AsDate := StartDate;
        qry2.ParamByName('ENDDATE').AsDate := EndDate;
        qry2.ExecQuery;
        VarData[j + 1, i] := qry2.Fields[0].AsInteger;
        qry2.Close;
      end;
      qry1.Next;
      inc(i);
    end;
    qry1.Close;
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    Cell := WS.Cells.Item[ROWS_COUNT + 4, cnt + 2].Address;
    WS.Range['C4:' + Cell].Value := VarData;
    WS.Range['A3:' + Cell].Borders.Linestyle := xlContinuous;
    Cell := WS.Cells.Item[1, cnt + 2].Address;
    WS.Range['A1:' + Cell].Merge;
    Cell := WS.Cells.Item[2, cnt + 2].Address;
    WS.Range['A2:' + Cell].Merge;
    Cell := WS.Cells.Item[3, cnt + 2].Address;
    WS.Range['C3:' + Cell].Merge;
    WS.Range['A2'] := '�� ������ � ' + DateToStr(StartDate) + ' �� ' +
      DateToStr(EndDate);
  finally
    App.Visible := true;
    qry1.Free;
    qry2.Free;
  end;
end;

procedure FormPrescriptionsReport(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction);
Const
  SCHEMES_SQL =
    'SELECT' +
    '  PR.PRESCRIPTIONDATE,' +
    '  P.NAME,' +
    '  P.AMBULATORYCARDNO,' +
    '  TRIM(LIST('''#$A'''||D.DESCR))' +
    ' FROM' +
    '  PERSONS P' +
    '  INNER JOIN PRESCRIPTIONS PR ON PR.REFPERSONS=P.ID' +
    '  INNER JOIN DRUGS D ON D.ID=PR.REFDRUGS' +
    ' WHERE PR.PRESCRIPTIONDATE BETWEEN :STARTDATE AND :ENDDATE' +
    '  GROUP BY 1,2,3';

  STAT_SQL =
    'SELECT' +
    '  D.DESCR,' +
    '  COUNT(DISTINCT PR.REFPERSONS) PATCOUNT,' +
    '  COUNT(PR.ID) PRESCRCOUNT,' +
    '  SUM(PR.QUANTITY) QUANTITY' +
    ' FROM' +
    '  PRESCRIPTIONS PR' +
    '  INNER JOIN DRUGS D ON D.ID=PR.REFDRUGS' +
    ' WHERE PR.PRESCRIPTIONDATE BETWEEN :STARTDATE AND :ENDDATE' +
    ' GROUP BY 1';

  STAT_MNN_SQL =
    'SELECT' +
    '  REPLACE(D.DESCR,DTN.NAMERUS,DIN.NAMERUS),' +
    '  COUNT(DISTINCT PR.REFPERSONS) PATCOUNT,' +
    '  COUNT(PR.ID) PRESCRCOUNT,' +
    '  SUM(PR.QUANTITY) QUANTITY' +
    ' FROM' +
    '  PRESCRIPTIONS PR' +
    '  INNER JOIN DRUGS D ON D.ID=PR.REFDRUGS' +
    '  inner JOIN DRUGTRADENAMES DTN ON DTN.ID=D.REFDRUGTRADENAMES' +
    '  LEFT JOIN DRUGINTNAMES DIN ON DTN.REFDRUGINTNAMES=DIN.ID' +
    ' WHERE PR.PRESCRIPTIONDATE BETWEEN :STARTDATE AND :ENDDATE' +
    ' GROUP BY 1';
Var
  App, WS, VarData: Variant;
  qry: TpFIBDataset;
  rc, i: integer;
begin;
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\����� �� ������� ��������.xlt', '\\reports', '\reports', []));
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    qry := TpFIBDataset.Create(nil);
    try
      qry.SelectSQL.Text := STAT_SQL;
      qry.Transaction := Trans;
      qry.ParamByName('STARTDATE').AsDateTime := StartDate;
      qry.ParamByName('ENDDATE').AsDateTime := EndDate;
      qry.Open;
      qry.FetchAll;
      qry.First;
      rc := qry.RecordCount;
      WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
      VarData := VarArrayCreate([1, rc, 1, 5], VarVariant);
      i := 1;
      while not qry.Eof do
      begin
        VarData[i, 01] := i; // � �.�.
        VarData[i, 02] := qry.Fields[00].asString; // ��������
        VarData[i, 03] := qry.Fields[01].AsInteger; // ���-�� ���������
        VarData[i, 04] := qry.Fields[02].AsInteger; // ���-�� ��������
        VarData[i, 05] := qry.Fields[03].AsInteger;
        // ���-�� ���������� ����������
        qry.Next;
        inc(i);
      end;
      qry.Close;
      if rc > 0 then
      begin
        if (rc > 2) then
          WS.Range[Format('A%d:E%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
        WS.Range[Format('A%d:E%d', [4, rc + 3])].Value := VarData;
        WS.Range[Format('A%d:E%d', [4, rc + 3])].Borders.Linestyle :=
          xlContinuous;
      end;

      WS := App.ActiveWorkbook.Worksheets.Item[2];
      qry.SelectSQL.Text := STAT_MNN_SQL;
      qry.Transaction := Trans;
      qry.ParamByName('STARTDATE').AsDateTime := StartDate;
      qry.ParamByName('ENDDATE').AsDateTime := EndDate;
      qry.Open;
      qry.FetchAll;
      qry.First;
      rc := qry.RecordCount;
      WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
      VarData := VarArrayCreate([1, rc, 1, 5], VarVariant);
      i := 1;
      while not qry.Eof do
      begin
        VarData[i, 01] := i; // � �.�.
        VarData[i, 02] := qry.Fields[00].asString; // ��������
        VarData[i, 03] := qry.Fields[01].AsInteger; // ���-�� ���������
        VarData[i, 04] := qry.Fields[02].AsInteger; // ���-�� ��������
        VarData[i, 05] := qry.Fields[03].AsInteger;
        // ���-�� ���������� ����������
        qry.Next;
        inc(i);
      end;
      qry.Close;
      if rc > 0 then
      begin
        if (rc > 2) then
          WS.Range[Format('A%d:E%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
        WS.Range[Format('A%d:E%d', [4, rc + 3])].Value := VarData;
        WS.Range[Format('A%d:E%d', [4, rc + 3])].Borders.Linestyle :=
          xlContinuous;
      end;

      WS := App.ActiveWorkbook.Worksheets.Item[3];
      qry.SelectSQL.Text := SCHEMES_SQL;
      qry.Transaction := Trans;
      qry.ParamByName('STARTDATE').AsDateTime := StartDate;
      qry.ParamByName('ENDDATE').AsDateTime := EndDate;
      qry.Open;
      qry.FetchAll;
      qry.First;
      rc := qry.RecordCount;
      WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
      VarData := VarArrayCreate([1, rc, 1, 4], VarVariant);
      i := 1;
      while not qry.Eof do
      begin
        VarData[i, 01] := qry.Fields[00].AsDateTime; // ����
        VarData[i, 02] := qry.Fields[01].asString; // �������
        VarData[i, 03] := qry.Fields[02].asString; // ����� ���. �����
        VarData[i, 04] := qry.Fields[03].asString; // ����� �������
        qry.Next;
        inc(i);
      end;
      qry.Close;
      if rc > 0 then
      begin
        if (rc > 2) then
          WS.Range[Format('A%d:D%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
        WS.Range[Format('A%d:D%d', [4, rc + 3])].Value := VarData;
        WS.Range[Format('A%d:D%d', [4, rc + 3])].Borders.Linestyle :=
          xlContinuous;
      end;
    finally
      qry.Free;
    end;
  finally
    App.Visible := true;
  end;
end;

procedure PrintHIVVisitsList(const Filter: String; Trans: TFIBTransaction);
Const
  SQL = 'SELECT ' + '  S.VISITDATE,' + '  S.VISITTIME,' +
    '  LEFT(PS.NAME,POSITION('' '',PS.NAME)+1)||''.''' +
    '    ||LEFT(RIGHT(RIGHT(PS.NAME,CHAR_LENGTH(PS.NAME)' +
    '      -POSITION('' '',PS.NAME)-1),' +
    '    CHAR_LENGTH(RIGHT(PS.NAME,CHAR_LENGTH(PS.NAME)' +
    '      -POSITION('' '',PS.NAME)-1))-' +
    '    POSITION('' '',RIGHT(PS.NAME,CHAR_LENGTH(PS.NAME)' +
    '     -POSITION('' '',PS.NAME)-1))),1)||''.''' +
    '   ||'', ''||POS.NAME DOCTOR, ' + '  P.AMBULATORYCARDNO,' + '  P.NAME,' +
    '  S.COMMENTS ' + ' FROM' + '  VISITS S' +
    '  INNER JOIN PERSONNEL PL ON S.REFPERSONNEL_DOCTOR=PL.ID' +
    '  INNER JOIN PERSONS PS ON PL.REFPERSONS=PS.ID' +
    '  INNER JOIN POSITIONS POS ON PL.REFPOSITIONS=POS.ID' +
    '  INNER JOIN TREATMENTCASES TC ON S.REFTREATMENTCASES=TC.ID ' +
    '  INNER JOIN PERSONS P ON TC.REFPERSONS=P.ID' +
    ' WHERE S.VISITDATE IS NOT NULL';
var
  VarData, App, WS: Variant;
  i, rc: integer;
  qry: TFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\������ ��������� �����.xlt', '\\reports', '\reports', []));
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := SQL + Filter + ' ORDER BY S.VISITDATE,S.VISITTIME';
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 7], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := i;
      VarData[i, 02] := qry.Fields[00].AsDateTime;
      VarData[i, 03] := qry.Fields[01].AsDateTime;
      VarData[i, 04] := qry.Fields[02].asString;
      VarData[i, 05] := qry.Fields[03].asString;
      VarData[i, 06] := qry.Fields[04].asString;
      VarData[i, 07] := qry.Fields[05].asString;
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:G%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:G%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:G%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
  finally
    qry.Free;
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormDoctorsVisitsByDate(WS: Variant; StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; DoctorID, DistrictID: Variant);
var
  qry: TFIBQuery;
  i, j: integer;
begin;
  i := 17;
  qry := TFIBQuery.Create(nil);
  try
    qry.Transaction := Trans;
    qry.SQL.Text := VISIT_NODISTRICT_STATISTICS_SQL + VISIT_STATISTICS_SQL;
    if VarIsNull(DoctorID) then
      qry.SQL.Text := Replace(qry.SQL.Text,
        'AND V.REFPERSONNEL_DOCTOR=:DOCTOR', '')
    else
      qry.ParamByName('DOCTOR').asVariant := DoctorID;
    qry.ParamByName('STARTDATE').asVariant := StartDate;
    qry.ParamByName('ENDDATE').asVariant := EndDate;
    if not VarIsNull(DistrictID) then
      qry.ParamByName('DISTRICTID').asVariant := DistrictID;
    qry.ExecQuery;
    repeat
      inc(i);
      WS.Cells.Item[i, 1].FormulaLocal := qry.FieldByName('VISITDATE').AsDate;
      for j := 2 to qry.FieldCount - 1 do
        WS.Cells.Item[i, j].FormulaLocal := qry.Fields[j].AsInteger;
      qry.Next;
    until qry.Eof;
    WS.Range['A17', 'U' + IntToStr(i + 1)].Borders.Linestyle := xlContinuous;
    for j := 2 to qry.FieldCount - 1 do
      WS.Cells.Item[i + 1, j].FormulaLocal := '=����(R18C' + IntToStr(j) + ':R'
        + IntToStr(i) + 'C' + IntToStr(j) + ')';
    WS.Cells.Item[i + 1, 1].FormulaLocal := '�����';
    WS.Range['A' + IntToStr(i + 1), 'AF' + IntToStr(i + 1)].Font.Bold := true;
  finally
    qry.Free;
  end;
end;

procedure FormAllVisitsByDate(WS: Variant; StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction);
var
  qry: TFIBQuery;
  i, j: integer;
begin;
  i := 17;
  qry := TFIBQuery.Create(nil);
  try
    qry.Transaction := Trans;
    qry.SQL.Text := ALLVISIT_NODISTRICT_STATISTICS_SQL;
    qry.ParamByName('STARTDATE').asVariant := StartDate;
    qry.ParamByName('ENDDATE').asVariant := EndDate;
    qry.ExecQuery;
    repeat
      inc(i);
      WS.Cells.Item[i, 1].FormulaLocal := qry.FieldByName('VISITDATE').AsDate;
      for j := 2 to qry.FieldCount - 1 do
        WS.Cells.Item[i, j].FormulaLocal := qry.Fields[j].AsInteger;
      qry.Next;
    until qry.Eof;
    WS.Range['A17', 'U' + IntToStr(i + 1)].Borders.Linestyle := xlContinuous;
    for j := 2 to qry.FieldCount - 1 do
      WS.Cells.Item[i + 1, j].FormulaLocal := '=����(R18C' + IntToStr(j) + ':R'
        + IntToStr(i) + 'C' + IntToStr(j) + ')';
    WS.Cells.Item[i + 1, 1].FormulaLocal := '�����';
    WS.Range['A' + IntToStr(i + 1), 'AF' + IntToStr(i + 1)].Font.Bold := true;
  finally
    qry.Free;
  end;
end;

procedure FormDoctorsVisitsByDateRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; DoctorID: Variant);
Const
  DoctorSQL = 'SELECT PS.NAME ||'', ''||P.NAME NAME FROM ' + ' PERSONS PS' +
    '  INNER JOIN PERSONNEL PL ON PL.REFPERSONS=PS.ID' +
    '  INNER JOIN POSITIONS P ON PL.REFPOSITIONS=P.ID ' + 'WHERE' +
    ' PL.ID=:ID';
Var
  App, WS: Variant;
  doct: String;
  qry: TFIBQuery;
begin;
  App := InitExcel(Replace(ExtractFilePath(Application.ExeName) +
    '\reports\��������� ����� ���������.xlt', '\\reports', '\reports'));
  try
    doct := Trans.DefaultDatabase.QueryValueAsStr(DoctorSQL, 0, [DoctorID]);
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Range('E9') := doct;
    WS.Range('E11') := '�� ' + GetPeriod(StartDate, EndDate);
    FormDoctorsVisitsByDate(WS, StartDate, EndDate, Trans, DoctorID, null);
  finally
    App.Visible := true;
  end;
end;

procedure FormAllVisitsByDateRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; DoctorID: Variant);
Const
  DoctorSQL = 'SELECT PS.NAME ||'', ''||P.NAME NAME FROM ' + ' PERSONS PS' +
    '  INNER JOIN PERSONNEL PL ON PL.REFPERSONS=PS.ID' +
    '  INNER JOIN POSITIONS P ON PL.REFPOSITIONS=P.ID ' + 'WHERE' +
    ' PL.ID=:ID';
Var
  App, WS: Variant;
  doct: String;
begin;
  App := InitExcel(Replace(ExtractFilePath(Application.ExeName) +
    '\reports\��������� ����� ���������.xlt', '\\reports', '\reports'));
  try
    doct := '';
    if not VarIsNull(DoctorID) then
      doct := Trans.DefaultDatabase.QueryValueAsStr(DoctorSQL, 0, [DoctorID]);
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Range('E9') := doct;
    WS.Range('E11') := '�� ' + GetPeriod(StartDate, EndDate);
    if VarIsNull(DoctorID) then
      FormAllVisitsByDate(WS, StartDate, EndDate, Trans)
    else
      FormDoctorsVisitsByDate(WS, StartDate, EndDate, Trans, DoctorID, null);
  finally
    App.Visible := true;
  end;
end;

procedure FormHIVDispensaryReport(const StartDate, EndDate: TDateTime; Trans: TFIBTransaction);
Const
  SQL =
    'SELECT' +
    '  P.NAME,' +
    '  P.BIRTHDAY,' +
    '  P.AMBULATORYCARDNO,' +
    '  P.ADDRESS_REG,' +
    '  P.ADDRESS_LIV,' +
    '  D.NAME DISTRICT,' +
    '  H.REGISTRYDATE,' +
    '   (SELECT FIRST 1 HST.NAME||'' ''|| COALESCE(DIS.CLASSIFIERCODE,'''')' +
    '    FROM HIVSTAGES HS' +
    '    INNER JOIN HIVSTAGETYPES HST ON HS.REFHIVSTAGETYPES=HST.ID' +
    '    LEFT JOIN DISEASES DIS ON HS.REFDISEASES=DIS.ID' +
    '   WHERE HS.REFPERSONS=P.ID AND HS.STARTDATE<CAST(:ENDDATE AS DATE)' +
    '   ORDER BY HS.STARTDATE DESC' +
    '   ) STAGES,' +
    '  H.UNREGISTRYDATE,' +
    '  HO.NAME OUTCOME,' +
    '  (SELECT FIRST 1 LIST(D.DESCR) FROM HIVART HA' +
    '   INNER JOIN HIVARTDRUGS HAD ON HAD.REFHIVART=HA.ID' +
    '  INNER JOIN DRUGS D ON HAD.REFDRUGS=D.ID' +
    '  WHERE HA.REFPERSONS=P.ID ' +
    '  AND CAST(:ENDDATE AS DATE) BETWEEN HA.STARTDATE and COALESCE(HA.ENDDATE,CAST(:ENDDATE AS DATE))' +
    '  GROUP BY HA.ID,HA.STARTDATE' +
    '  ORDER BY HA.STARTDATE DESC) ART,' +
    '  (SELECT LIST(DISTINCT PR.PRESCRIPTIONDATE) FROM PRESCRIPTIONS PR' +
    '  WHERE PR.REFPERSONS=P.ID AND PR.PRESCRIPTIONDATE BETWEEN' +
    '  CAST(:STARTDATE AS DATE) AND CAST(:ENDDATE AS DATE)) PRESC' +
    ' FROM' +
    '  PERSONS P' +
    '  INNER JOIN HIV H ON H.REFPERSONS=P.ID' +
    '  LEFT JOIN PLACESHISTORY PH ON PH.REFPERSONS=P.ID' +
    '  AND CAST(:ENDDATE AS DATE) BETWEEN PH.STARTDATE and COALESCE(PH.ENDDATE,CAST(:ENDDATE AS DATE))' +
    '  LEFT JOIN DISTRICTS D ON PH.REFDISTRICTS=D.ID' +
    '  LEFT JOIN HIVOUTCOMES HO ON H.REFHIVOUTCOMES=HO.ID' +
    ' WHERE  H.REGISTRYDATE<=CAST(:ENDDATE AS DATE)' +
    ' AND COALESCE(H.UNREGISTRYDATE,CAST(:ENDDATE AS DATE))>=CAST(:STARTDATE AS DATE)';
var
  VarData, App, WS: Variant;
  i, rc: integer;
  qry: TpFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\������ ������������� �����.xlt', '\\reports', '\reports', []));
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    qry := TpFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := SQL;
    qry.ParamByName('STARTDATE').asVariant := StartDate;
    qry.ParamByName('ENDDATE').asVariant := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 12], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := qry.Fields[00].asString;
      VarData[i, 02] := qry.Fields[01].asString;
      VarData[i, 03] := qry.Fields[02].asString;
      VarData[i, 04] := qry.Fields[03].asString;
      VarData[i, 05] := qry.Fields[04].asString;
      VarData[i, 06] := qry.Fields[05].asString;
      VarData[i, 07] := qry.Fields[06].AsDateTime;
      VarData[i, 08] := qry.Fields[07].asString;
      VarData[i, 09] := qry.Fields[08].asString;
      VarData[i, 10] := qry.Fields[09].asString;
      VarData[i, 11] := qry.Fields[10].asString;
      VarData[i, 12] := qry.Fields[11].asString;
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:L%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:L%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:L%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
  finally
    qry.Free;
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormHIVandHepatitReport(const StartDate, EndDate: TDateTime; Trans: TFIBTransaction);
const
  StartRow = 4;

  RowCount = 42;

  SQL: array [1 .. RowCount] of PositionedSQL =
    (
    (Row: 02; SQL:
    'SELECT COUNT(DISTINCT REFPERSONS) FROM HIVIFAIB' +
    ' WHERE TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 03; SQL:
    'SELECT COUNT(DISTINCT REFPERSONS) FROM HIV' +
    ' WHERE REGISTRYDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 04; SQL:
    'SELECT COUNT(DISTINCT REFPERSONS) FROM HIV' +
    ' WHERE REGISTRYDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 05; SQL:
    'SELECT COUNT(DISTINCT REFPERSONS) FROM HIV' +
    ' WHERE REFHIVOUTCOMES IN (1,2)' +
    ' AND UNREGISTRYDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 06; SQL:
    'SELECT COUNT(DISTINCT REFPERSONS) FROM HIV' +
    ' WHERE REFHIVOUTCOMES IN (1)' +
    ' AND UNREGISTRYDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 07; SQL:
    'SELECT COUNT(DISTINCT H.REFPERSONS) FROM HIV H ' +
    ' INNER JOIN PERSONS P ON H.REFPERSONS=P.ID' +
    ' WHERE P.AMBULATORYCARDNO IS NOT NULL' +
    ' AND (H.UNREGISTRYDATE IS NULL OR H.UNREGISTRYDATE>=:STARTDATE)' +
    ' AND H.REGISTRYDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 08; SQL:
    'SELECT COUNT(DISTINCT H.REFPERSONS) FROM HIV H ' +
    ' INNER JOIN PERSONS P ON H.REFPERSONS=P.ID' +
    ' WHERE P.AMBULATORYCARDNO IS NOT NULL' +
    ' AND (H.UNREGISTRYDATE IS NULL OR H.UNREGISTRYDATE>=:STARTDATE)' +
    ' AND H.REGISTRYDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 09; SQL:
    'SELECT COUNT(DISTINCT R.REFPERSONS) FROM' +
    ' (SELECT REFPERSONS,TAKINGDATE FROM HIVPCR' +
    ' UNION SELECT REFPERSONS,TAKINGDATE FROM HIVIMMUNOGRAMS) R' +
    ' WHERE R.TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 10; SQL:
    'SELECT COUNT(DISTINCT R.REFPERSONS) FROM' +
    '  HIVIMMUNOGRAMS R' +
    ' WHERE R.TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 11; SQL:
    'SELECT COUNT(DISTINCT R.REFPERSONS) FROM' +
    ' HIVPCR R' +
    ' WHERE R.TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 12; SQL:
    'SELECT COUNT(DISTINCT R.REFPERSONS) FROM' +
    ' HIVIMMUNOGRAMS R' +
    ' WHERE R.CD4<350 AND R.TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE' +
    ' AND NOT EXISTS(SELECT R1.ID FROM HIVIMMUNOGRAMS R1' +
    '    WHERE R1.REFPERSONS=R.REFPERSONS' +
    '    AND R1.TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE)'),
    (Row: 13; SQL:
    'SELECT COUNT(DISTINCT R.REFPERSONS) FROM' +
    ' HIVPCR R' +
    ' WHERE (R.ISBELOWSENSIVITY=1 OR R.VIRALLOAD<500)' +
    ' AND R.TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE' +
    ' AND NOT EXISTS(SELECT R1.ID FROM HIVPCR R1' +
    '    WHERE R1.REFPERSONS=R.REFPERSONS' +
    '    AND R1.TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE)'),
    (Row: 14; SQL:
    'SELECT COUNT(DISTINCT R.REFPERSONS) FROM' +
    ' HIVEVENTS R' +
    ' WHERE R.REFHIVEVENTTYPES=1 AND R.ENDDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 15; SQL:
    'SELECT COUNT(DISTINCT R.REFPERSONS) FROM' +
    ' (SELECT REFPERSONS,TAKINGDATE FROM HIVIMMUNOGRAMS WHERE CD4<350' +
    ' AND TAKINGDATE BETWEEN :STARTDATE AND :ENDDATE' +
    ' UNION SELECT REFPERSONS,STARTDATE FROM HIVDISEASES' +
    ' WHERE STARTDATE<=:ENDDATE AND (ENDDATE IS NULL OR ENDDATE>=:STARTDATE)) R'),
    (Row: 16; SQL:
    'SELECT COUNT(DISTINCT H.REFPERSONS) FROM HIV H' +
    ' INNER JOIN PERSONS P ON H.REFPERSONS=P.ID' +
    ' WHERE P.AMBULATORYCARDNO IS NOT NULL AND' +
    ' H.REGISTRYDATE<=:ENDDATE AND' +
    ' (H.REGISTRYDATE<:STARTDATE AND ((CAST(:STARTDATE AS DATE)-P.BIRTHDAY)/365.25)<18' +
    '  or H.REGISTRYDATE>:STARTDATE AND ((H.REGISTRYDATE-P.BIRTHDAY)/365.25)<18)' +
    '  AND (H.UNREGISTRYDATE IS NULL OR H.UNREGISTRYDATE>:STARTDATE)'),
    (Row: 17; SQL:
    'SELECT COUNT(DISTINCT REFPERSONS) FROM HIVART' +
    ' WHERE STARTDATE<=:ENDDATE AND (ENDDATE IS NULL OR ENDDATE>=:STARTDATE)'),
    (Row: 18; SQL:
    'SELECT COUNT(DISTINCT REFPERSONS) FROM HIVART' +
    ' WHERE ENDDATE BETWEEN :STARTDATE AND :ENDDATE AND REFHIVARTOUTCOMES IN (3,6)'),
    (Row: 19; SQL:
    'SELECT COUNT(DISTINCT REFPERSONS) FROM HIVART' +
    ' WHERE ENDDATE BETWEEN :STARTDATE AND :ENDDATE AND REFHIVARTOUTCOMES IN (6)'),
    (Row: 20; SQL:
    'SELECT COUNT(DISTINCT HD.REFPERSONS) FROM HIVDISEASES HD' +
    ' INNER JOIN DISEASES DIS ON HD.REFDISEASES=DIS.ID WHERE ' +
    ' DIS.CLASSIFIERCODE BETWEEN ''A15'' AND ''A19Z'' ' +
    ' AND HD.STARTDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 21; SQL:
    'SELECT COUNT(DISTINCT HD.REFPERSONS) FROM HIVDISEASES HD' +
    ' INNER JOIN DISEASES DIS ON HD.REFDISEASES=DIS.ID ' +
    ' INNER JOIN HIVDISEASETREATMENT HDT ON HDT.REFHIVDISEASES=HD.ID' +
    ' WHERE' +
    ' DIS.CLASSIFIERCODE BETWEEN ''A15'' AND ''A19Z'' ' +
    ' AND HD.STARTDATE<=:ENDDATE ' +
    ' AND (HD.ENDDATE IS NULL OR HD.ENDDATE>=:STARTDATE)' +
    ' AND HDT.STARTDATE<=:ENDDATE ' +
    ' AND (HDT.ENDDATE IS NULL OR HDT.ENDDATE>=:STARTDATE) '),
    (Row: 22; SQL:
    'SELECT COUNT(DISTINCT HA.REFPERSONS) FROM HIVART HA' +
    ' INNER JOIN PERSONS P ON HA.REFPERSONS=P.ID' +
    ' WHERE ' +
    ' HA.STARTDATE<=:ENDDATE AND' +
    ' (HA.STARTDATE<:STARTDATE AND ((CAST(:STARTDATE AS DATE)-P.BIRTHDAY)/365.25)<18' +
    '  or HA.STARTDATE>:STARTDATE AND ((HA.STARTDATE-P.BIRTHDAY)/365.25)<18)' +
    '  AND (HA.ENDDATE IS NULL OR HA.ENDDATE>:STARTDATE)'),
    (Row: 23; SQL: 'SELECT COUNT(DISTINCT H.REFPERSONS) FROM HIV H ' +
    ' INNER JOIN PERSONS P ON H.REFPERSONS=P.ID' +
    ' WHERE P.AMBULATORYCARDNO IS NOT NULL' +
    ' AND (H.UNREGISTRYDATE IS NULL OR H.UNREGISTRYDATE>=:STARTDATE)' +
    ' AND H.REGISTRYDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 24; SQL: 'SELECT COUNT(DISTINCT HA.REFPERSONS) FROM HIVART HA ' +
    ' INNER JOIN PERSONS P ON HA.REFPERSONS=P.ID' +
    ' WHERE (HA.ENDDATE IS NULL OR HA.ENDDATE>=:STARTDATE)' +
    ' AND HA.STARTDATE BETWEEN :STARTDATE AND :ENDDATE' +
    ' AND NOT EXISTS(SELECT HA1.ID FROM HIVART HA1 WHERE' +
    ' HA1.REFPERSONS=HA.REFPERSONS AND HA1.STARTDATE<HA.STARTDATE)'),
    // ���������� ������ �� ����� - ������ � 25 �� 32
    (Row: 33; SQL:
    'SELECT COUNT(DISTINCT HD.REFPERSONS) FROM HIVDISEASES HD' +
    ' INNER JOIN HIV H ON H.REFPERSONS=HD.REFPERSONS' +
    ' INNER JOIN DISEASES DIS ON HD.REFDISEASES=DIS.ID' +
    ' WHERE DIS.CLASSIFIERCODE BETWEEN ''A15'' AND ''A19Z'' AND' +
    ' HD.STARTDATE<=:ENDDATE AND (HD.ENDDATE IS NULL OR HD.ENDDATE>=:STARTDATE)'),
    (Row: 34; SQL:
    'SELECT COUNT(*) FROM HIVPREGNANCY' +
    ' WHERE ' +
    ' BIRTHDATE BETWEEN :STARTDATE AND :ENDDATE;'),
    (Row: 35; SQL:
    'SELECT COUNT(*) FROM HIVPREGNANCY' +
    ' WHERE REFPREGNANCYOUTCOMES=1' +
    ' AND BIRTHDATE BETWEEN :STARTDATE AND :ENDDATE;'),
    (Row: 36; SQL:
    'SELECT COUNT(*) FROM HIVPREGNANCY HP' +
    ' WHERE HP.REFPREGNANCYOUTCOMES=1' +
    ' AND (HP.REFDRUGS_PREG IS NOT NULL OR HP.REFDRUGS_BIRTHS IS NOT NULL' +
    ' OR HP.REFDRUGS_CHILD IS NOT NULL' +
    ' OR EXISTS(SELECT HA.ID FROM HIVART HA WHERE HA.REFPERSONS=HP.REFPERSONS' +
    ' AND HA.STARTDATE<HP.BIRTHDATE ' +
    ' AND( HA.ENDDATE IS NULL OR HA.ENDDATE>HP.BIRTHDATE)))' +
    ' AND HP.BIRTHDATE BETWEEN :STARTDATE AND :ENDDATE;'),
    (Row: 37; SQL:
    'SELECT COUNT(*) FROM HIVPREGNANCY HP' +
    ' WHERE HP.REFPREGNANCYOUTCOMES=1' +
    ' AND (HP.REFDRUGS_PREG IS NOT NULL OR HP.REFDRUGS_BIRTHS IS NOT NULL' +
    ' OR HP.REFDRUGS_CHILD IS NOT NULL' +
    ' OR EXISTS(SELECT HA.ID FROM HIVART HA WHERE HA.REFPERSONS=HP.REFPERSONS' +
    ' AND HA.STARTDATE<HP.BIRTHDATE ' +
    ' AND( HA.ENDDATE IS NULL OR HA.ENDDATE>HP.BIRTHDATE)))' +
    ' AND HP.BIRTHDATE BETWEEN :STARTDATE AND :ENDDATE;'),
    (Row: 38; SQL:
    'SELECT COUNT(*) FROM HIVPREGNANCY HP' +
    ' WHERE HP.REFPREGNANCYOUTCOMES=1' +
    ' AND (HP.REFDRUGS_PREG IS NOT NULL ' +
    ' OR EXISTS(SELECT HA.ID FROM HIVART HA WHERE HA.REFPERSONS=HP.REFPERSONS' +
    ' AND HA.STARTDATE<HP.BIRTHDATE ' +
    ' AND( HA.ENDDATE IS NULL OR HA.ENDDATE>HP.BIRTHDATE)))' +
    ' AND HP.BIRTHDATE BETWEEN :STARTDATE AND :ENDDATE;'),
    (Row: 39; SQL:
    'SELECT COUNT(*) FROM HIVPREGNANCY HP' +
    ' WHERE HP.REFPREGNANCYOUTCOMES=1' +
    ' AND (HP.REFDRUGS_BIRTHS IS NOT NULL' +
    ' OR EXISTS(SELECT HA.ID FROM HIVART HA WHERE HA.REFPERSONS=HP.REFPERSONS' +
    ' AND HA.STARTDATE<HP.BIRTHDATE ' +
    ' AND( HA.ENDDATE IS NULL OR HA.ENDDATE>HP.BIRTHDATE)))' +
    ' AND HP.BIRTHDATE BETWEEN :STARTDATE AND :ENDDATE;'),
    (Row: 40; SQL:
    'SELECT COUNT(*) FROM HIVPREGNANCY HP' +
    ' WHERE HP.REFPREGNANCYOUTCOMES=1' +
    ' AND HP.REFDRUGS_PREG IS NULL AND HP.REFDRUGS_BIRTHS IS NOT NULL' +
    ' AND HP.REFDRUGS_CHILD IS NULL' +
    ' AND HP.BIRTHDATE BETWEEN :STARTDATE AND :ENDDATE;'),
    (Row: 41; SQL:
    'SELECT SUM(CHILDRENLIVCOUNT) FROM HIVPREGNANCY HP' +
    ' WHERE HP.REFPREGNANCYOUTCOMES=1' +
    ' AND (HP.REFDRUGS_PREG IS NOT NULL OR HP.REFDRUGS_BIRTHS IS NOT NULL' +
    ' OR HP.REFDRUGS_CHILD IS NOT NULL' +
    ' OR EXISTS(SELECT HA.ID FROM HIVART HA WHERE HA.REFPERSONS=HP.REFPERSONS' +
    ' AND HA.STARTDATE<HP.BIRTHDATE ' +
    ' AND( HA.ENDDATE IS NULL OR HA.ENDDATE>HP.BIRTHDATE)))' +
    ' AND HP.BIRTHDATE BETWEEN :STARTDATE AND :ENDDATE;'),
    (Row: 42; SQL:
    'SELECT SUM(CHILDRENLIVCOUNT) FROM HIVPREGNANCY HP' +
    ' WHERE HP.REFPREGNANCYOUTCOMES=1' +
    ' AND HP.REFDRUGS_CHILD IS NOT NULL' +
    ' AND (HP.REFDRUGS_PREG IS NOT NULL AND HP.REFDRUGS_BIRTHS IS NOT NULL' +
    ' OR EXISTS(SELECT HA.ID FROM HIVART HA WHERE HA.REFPERSONS=HP.REFPERSONS' +
    ' AND HA.STARTDATE<HP.BIRTHDATE ' +
    ' AND(HA.ENDDATE IS NULL OR HA.ENDDATE>HP.BIRTHDATE)))' +
    ' AND HP.BIRTHDATE BETWEEN :STARTDATE AND :ENDDATE;'),
    (Row: 43; SQL:
    'SELECT SUM(CHILDRENLIVCOUNT) FROM HIVPREGNANCY' +
    ' WHERE REFPREGNANCYOUTCOMES=1' +
    ' AND BIRTHDATE BETWEEN :STARTDATE AND :ENDDATE;'),
    // (Row: 44; SQL: ''),
    // (Row: 45; SQL: ''),
    // (Row: 46; SQL: ''),
    // (Row: 47; SQL: ''),
    (Row: 48; SQL:
    'SELECT COUNT(DISTINCT HD.REFPERSONS) FROM HIVDISEASES HD' +
    ' INNER JOIN DISEASES DIS ON HD.REFDISEASES=DIS.ID' +
    ' WHERE (DIS.CLASSIFIERCODE BETWEEN ''B16'' AND ''B16.9''' +
    ' OR DIS.CLASSIFIERCODE IN (''B17.0'',''B18.0'',''B18.1''))' +
    ' AND HD.STARTDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 49; SQL:
    'SELECT COUNT(DISTINCT HD.REFPERSONS) FROM HIVDISEASES HD' +
    ' INNER JOIN DISEASES DIS ON HD.REFDISEASES=DIS.ID' +
    ' WHERE DIS.CLASSIFIERCODE IN (''B17.1'',''B18.2'')' +
    ' AND HD.STARTDATE BETWEEN :STARTDATE AND :ENDDATE'),
    (Row: 50; SQL: 'SELECT COUNT(DISTINCT HD.REFPERSONS) FROM HIVDISEASES HD' +
    ' INNER JOIN DISEASES DIS ON HD.REFDISEASES=DIS.ID' +
    ' INNER JOIN HIV H ON H.REFPERSONS=HD.REFPERSONS' +
    ' WHERE (DIS.CLASSIFIERCODE BETWEEN ''B16'' AND ''B16.9''' +
    ' OR DIS.CLASSIFIERCODE IN (''B17.0'',''B17.1'',''B18.2'',' +
    ' ''B18.0'',''B18.1''))' +
    ' AND HD.STARTDATE<=:ENDDATE' +
    ' AND H.REGISTRYDATE<=:ENDDATE AND (H.UNREGISTRYDATE>:STARTDATE OR H.UNREGISTRYDATE IS NULL)'),
    (Row: 51; SQL:
    'SELECT COUNT(DISTINCT HD.REFPERSONS) FROM HIVDISEASES HD' +
    ' INNER JOIN DISEASES DIS ON HD.REFDISEASES=DIS.ID' +
    ' INNER JOIN HIVDISEASETREATMENT HDT ON HDT.REFHIVDISEASES=HD.ID' +
    ' WHERE (DIS.CLASSIFIERCODE BETWEEN ''B16'' AND ''B16.9''' +
    ' OR DIS.CLASSIFIERCODE IN (''B17.0'',''B17.1'',''B18.2'',' +
    ' ''B18.0'',''B18.1''))' +
    ' AND HDT.STARTDATE<=:ENDDATE ' +
    ' AND (HDT.ENDDATE IS NULL OR HDT.ENDDATE>=:STARTDATE)'),
    (Row: 52; SQL: 'SELECT COUNT(DISTINCT HD.REFPERSONS) FROM HIVDISEASES HD' +
    ' INNER JOIN DISEASES DIS ON HD.REFDISEASES=DIS.ID' +
    ' INNER JOIN HIV H ON H.REFPERSONS=HD.REFPERSONS' +
    ' WHERE (DIS.CLASSIFIERCODE BETWEEN ''B16'' AND ''B16.9''' +
    ' OR DIS.CLASSIFIERCODE IN (''B17.0'',''B18.0'',''B18.1''))' +
    ' AND HD.STARTDATE<=:ENDDATE' +
    ' AND H.REGISTRYDATE<=:ENDDATE AND (H.UNREGISTRYDATE>:STARTDATE OR H.UNREGISTRYDATE IS NULL)'),
    (Row: 53; SQL: 'SELECT COUNT(DISTINCT HD.REFPERSONS) FROM HIVDISEASES HD' +
    ' INNER JOIN DISEASES DIS ON HD.REFDISEASES=DIS.ID' +
    ' INNER JOIN HIV H ON H.REFPERSONS=HD.REFPERSONS' +
    ' WHERE DIS.CLASSIFIERCODE IN (''B17.1'',''B18.2'')' +
    ' AND HD.STARTDATE<=:ENDDATE' +
    ' AND H.REGISTRYDATE<=:ENDDATE AND (H.UNREGISTRYDATE>:STARTDATE OR H.UNREGISTRYDATE IS NULL)'),
    (Row: 54; SQL:
    'SELECT COUNT(DISTINCT HD.REFPERSONS) FROM HIVDISEASES HD' +
    ' INNER JOIN DISEASES DIS ON HD.REFDISEASES=DIS.ID' +
    ' INNER JOIN HIVDISEASETREATMENT HDT ON HDT.REFHIVDISEASES=HD.ID' +
    ' WHERE (DIS.CLASSIFIERCODE BETWEEN ''B16'' AND ''B16.9''' +
    ' OR DIS.CLASSIFIERCODE IN (''B17.0'',''B18.0'',''B18.1''))' +
    ' AND HDT.STARTDATE<=:ENDDATE ' +
    ' AND (HDT.ENDDATE IS NULL OR HDT.ENDDATE>=:STARTDATE) '),
    (Row: 55; SQL:
    'SELECT COUNT(DISTINCT HD.REFPERSONS) FROM HIVDISEASES HD' +
    ' INNER JOIN DISEASES DIS ON HD.REFDISEASES=DIS.ID' +
    ' INNER JOIN HIVDISEASETREATMENT HDT ON HDT.REFHIVDISEASES=HD.ID' +
    ' WHERE DIS.CLASSIFIERCODE IN (''B17.1'',''B18.2'')' +
    ' AND HDT.STARTDATE<=:ENDDATE ' +
    ' AND (HDT.ENDDATE IS NULL OR HDT.ENDDATE>=:STARTDATE) ')
    );

var
  App, WS: Variant;
  i, rc: integer;
  qry: TFIBQuery;
  OldStartDate, YearStartDate: TDateTime;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\��� �� 2018.xlt', '\\reports', '\reports', []));
  try
    WS := App.ActiveWorkbook.Worksheets.Item[2];
    qry := TFIBQuery.Create(nil);
    qry.Transaction := Trans;
    OldStartDate := EncodeDate(1980, 01, 01);
    YearStartDate := EncodeDate(YearOf(StartDate), 01, 01);
    // �
    for i := 1 to RowCount do
    begin;
      qry.SQL.Text := SQL[i].SQL;
      if VarToStr(WS.Cells.Item[SQL[i].Row + StartRow, 5].FormulaLocal) <> '�' then
      begin;
        qry.ParamByName('STARTDATE').AsDate := OldStartDate;
        qry.ParamByName('ENDDATE').AsDate := EndDate;
        qry.ExecQuery;
        WS.Cells.Item[SQL[i].Row + StartRow, 5].FormulaLocal := qry.Fields[0].AsInteger;
        qry.Close;
      end;
      if VarToStr(WS.Cells.Item[SQL[i].Row + StartRow, 6].FormulaLocal) <> '�' then
      begin;
        qry.ParamByName('STARTDATE').AsDate := YearStartDate;
        qry.ParamByName('ENDDATE').AsDate := EndDate;
        qry.ExecQuery;
        WS.Cells.Item[SQL[i].Row + StartRow, 6].FormulaLocal := qry.Fields[0].AsInteger;
        qry.Close;
      end;
      if VarToStr(WS.Cells.Item[SQL[i].Row + StartRow, 7].FormulaLocal) <> '�' then
      begin;
        qry.ParamByName('STARTDATE').AsDate := StartDate;
        qry.ParamByName('ENDDATE').AsDate := EndDate;
        qry.ExecQuery;
        WS.Cells.Item[SQL[i].Row + StartRow, 7].FormulaLocal := qry.Fields[0].AsInteger;
        qry.Close;
      end;
    end;
  finally
    qry.Free;
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure OperativeMonitoringRpt(CalcDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);

const
  SQL = 'select count(distinct p.ID) ' +
    'from PERSONS p ' +
    'inner join HIV h on h.REFPERSONS=p.ID ' +
    'inner join PLACESHISTORY ph on ph.REFPERSONS=p.ID ' +
    'left join DISTRICTS d on ph.REFDISTRICTS=d.ID';

  HIMM_SQL = 'select count(distinct himm.ID) ' +
    'from PERSONS p ' +
    'inner join HIV h on h.REFPERSONS=p.ID ' +
    'inner join HIVIMMUNOGRAMS himm on himm.REFPERSONS=p.ID and himm.ANSWERDATE between :STARTDATE and :CALCDATE ' +
    'inner join PLACESHISTORY ph on ph.REFPERSONS=p.ID ' +
    'left join DISTRICTS d on ph.REFDISTRICTS=d.ID';

  // ����� ���������� ������������������
  REGISTERED_SQL =
    ' where (:CALCDATE between h.REGISTRYDATE and COALESCE(h.UNREGISTRYDATE, cast(''01.01.3000'' as DATE)))';
  // � �.�. ������� �� ������������ �����
  DISPENSARY_ACCOUNT_SQL = REGISTERED_SQL + ' and not(p.AMBULATORYCARDNO IS NULL)';
  // �������� ������������
  NEEDING_EXAMINED_SQL =
    ' and not exists(select ID from HIVART where REFPERSONS=p.ID and (:CALCDATE between STARTDATE and COALESCE(ENDDATE, cast(''01.01.3000'' as DATE))))';
  // ����������� �� �������� �������� (���-�� ������������)
  EXAMINED_IN_CURRENT_PERIOD_CNT_SQL = HIMM_SQL + DISPENSARY_ACCOUNT_SQL + ' and ' +
    'not exists(select ID from HIVART where REFPERSONS=p.ID and STARTDATE<=:CALCDATE and (ENDDATE is null or ENDDATE>=:STARTDATE))';
  // ����������� �� �������� ��������
  EXAMINED_IN_CURRENT_PERIOD_SQL =
    ' and exists(select ID from HIVIMMUNOGRAMS where REFPERSONS=p.ID and ANSWERDATE between :STARTDATE and :CALCDATE) and ' +
    'not exists(select ID from HIVART where REFPERSONS=p.ID and STARTDATE<=:CALCDATE and (ENDDATE is null or ENDDATE>=:STARTDATE))';
  // ��������� � ������� (��������� CD4 � ��������� �� ���� ������, �������������� CalcDate)
  // ������ � exists �������� ���������, ������� ������� ��� �� ����� �����������
  NEEDING_TREATMENT_SQL = 'with CTE as (select REFPERSONS, max(ANSWERDATE) ANSWERDATE ' +
    'from HIVIMMUNOGRAMS where ANSWERDATE<=:CALCDATE group by 1) ' +
    'select count(distinct p.ID) ' +
    'from PERSONS p ' +
    'inner join HIV h on h.REFPERSONS=p.ID ' +
    'inner join CTE on CTE.REFPERSONS=p.ID ' +
    'inner join PLACESHISTORY ph on ph.REFPERSONS=p.ID ' +
    'left join DISTRICTS d on ph.REFDISTRICTS=d.ID ' +
    'where ' +
    '(:CALCDATE between h.REGISTRYDATE and COALESCE(h.UNREGISTRYDATE, cast(''01.01.3000'' as DATE))) and ' +
    '(exists(select ID from HIVIMMUNOGRAMS where ANSWERDATE=CTE.ANSWERDATE and REFPERSONS=p.ID and CD4<350) or ' +
    'exists(select ID from HIVART where REFPERSONS=p.ID and (:CALCDATE between STARTDATE and COALESCE(ENDDATE, cast(''01.01.3000'' as DATE)) or '
    +
    '(REFHIVARTOUTCOMES=3 and ENDDATE between CTE.ANSWERDATE and :CALCDATE))))';
  // �������� �������
  RECEIVING_TREATMENT_SQL =
    ' and exists(select ID from HIVART where REFPERSONS=p.ID and (:CALCDATE between STARTDATE and COALESCE(ENDDATE, cast(''01.01.3000'' as DATE))))';
  // ������
  DIED_SQL = SQL + ' where h.REFHIVOUTCOMES in (1,2) and ' +
    'h.REGISTRYDATE<=:CALCDATE and h.UNREGISTRYDATE between :STARTDATE and :CALCDATE and ' +
    '(exists (select ID from HIVART where REFPERSONS=p.ID and STARTDATE<=:CALCDATE) or ' +
  // ��������������, ���������� ���
    'exists (select ID from HIVIMMUNOGRAMS where REFPERSONS=p.ID and CD4<350 and ANSWERDATE<=:CALCDATE))';
  // ��������������� �� �������
  // ������������
  NEEDING_FLUOROGRAPHIC_SQL =
    ' and not exists(select ID from XRAYSEXAMINATIONS where REFXRAYSEXAMTYPES=1 and REFPERSONS=p.ID and EXAMINATIONDATE between (:CALCDATE-365.25/2) and :CALCDATE)';
  FLUOROGRAPHIC_EXAMINED_SQL =
    ' and exists(select ID from XRAYSEXAMINATIONS where REFXRAYSEXAMTYPES=1 and REFPERSONS=p.ID and EXAMINATIONDATE between :STARTDATE and :CALCDATE)';

  DISTRICTS_SQL = 'select distinct d.ID, d.NAME ' +
    'from DISTRICTS d ' +
    'where (exists(select ID from PLACESHISTORY where REFDISTRICTS=d.ID) or ' +
    'exists(select ph.ID from PLACESHISTORY ph inner join DISTRICTS d1 on ph.REFDISTRICTS=d1.ID where d1.REFDISTRICTS=d.ID)) '
    +
    'and d.REFDISTRICTS is null order by 2';

  ROWS_COUNT = 3;
  ROWS_SQL: Array [1 .. ROWS_COUNT] of PositionedSQL =
    ((Row: 11; SQL: ' and h.ISUFSIN=1'),
    (Row: 13; SQL: ' and p.ISBOMZH=1'),
    (Row: 14; SQL: ' and h.ISOTHERREGION=1'));

  COLS_COUNT = 12;
  COLS_SQL: Array [1 .. COLS_COUNT] of PositionedSQL =
    ((Row: 3; SQL: SQL + REGISTERED_SQL),
    (Row: 4; SQL: SQL + DISPENSARY_ACCOUNT_SQL),
    (Row: 6; SQL: SQL + DISPENSARY_ACCOUNT_SQL + NEEDING_EXAMINED_SQL),
    (Row: 7; SQL: EXAMINED_IN_CURRENT_PERIOD_CNT_SQL),
    (Row: 9; SQL: EXAMINED_IN_CURRENT_PERIOD_CNT_SQL),
    (Row: 10; SQL: NEEDING_TREATMENT_SQL),
    (Row: 11; SQL: SQL + DISPENSARY_ACCOUNT_SQL + RECEIVING_TREATMENT_SQL),
    (Row: 12; SQL: SQL + DISPENSARY_ACCOUNT_SQL + EXAMINED_IN_CURRENT_PERIOD_SQL + RECEIVING_TREATMENT_SQL),
    (Row: 13; SQL: SQL + ' where h.REGISTRYDATE=:CALCDATE and p.ID=-1'),
    (Row: 14; SQL: DIED_SQL),
    (Row: 15; SQL: SQL + DISPENSARY_ACCOUNT_SQL + NEEDING_FLUOROGRAPHIC_SQL),
    (Row: 16; SQL: SQL + DISPENSARY_ACCOUNT_SQL + FLUOROGRAPHIC_EXAMINED_SQL));

var
  App, WS: Variant;
  i, j, rc: integer;
  qry, districts: TFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\����������.xlt', '\\reports', '\reports', []));
  WS := App.ActiveWorkbook.Worksheets.Item[1];
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  qry := TFIBDataset.Create(nil);
  districts := TFIBDataset.Create(nil);
  try
    qry.Transaction := Trans;
    WS.Range['G3'] := DateToStr(CalcDate);
    districts.Transaction := Trans;
    // �����, ����, ����������� ------------------------------------------------
    for i := 1 to ROWS_COUNT do
      for j := 1 to COLS_COUNT do
      begin
        qry.Active := false;
        qry.SelectSQL.Text := COLS_SQL[j].SQL + ROWS_SQL[i].SQL;
        qry.Params.ParamByName('CALCDATE').AsDateTime := CalcDate;
        if j = 5 then
          qry.Params.ParamByName('STARTDATE').AsDateTime := StartOfTheWeek(CalcDate);
        if j in [4, 8, 10, 12] then
          qry.Params.ParamByName('STARTDATE').AsDateTime := StartOfTheYear(CalcDate);
        qry.Open;
        WS.Cells.Item[ROWS_SQL[i].Row, COLS_SQL[j].Row] := qry.Fields[0].AsInteger;
      end;
    // �� ������� --------------------------------------------------------------
    districts.SelectSQL.Text := DISTRICTS_SQL;
    districts.Open;
    districts.Last;
    rc := districts.RecordCount;
    districts.First;
    if rc > 2 then
    begin
      WS.Range['A9:A9'].Resize[rc - 2].EntireRow.Insert;
      WS.Range['E9:E' + IntToStr(rc + 6)].FormulaR1C1 := WS.Range['E8:E8'].FormulaR1C1;
      WS.Range['H9:H' + IntToStr(rc + 6)].FormulaR1C1 := WS.Range['H8:H8'].FormulaR1C1;
      WS.Range['Q9:Q' + IntToStr(rc + 6)].FormulaR1C1 := WS.Range['Q8:Q8'].FormulaR1C1;
    end;
    i := 8;
    while not districts.Eof do
    begin
      WS.Cells.Item[i, 2] := districts.Fields[1].asString;
      for j := 1 to COLS_COUNT do
      begin
        qry.Active := false;
        qry.SelectSQL.Text := COLS_SQL[j].SQL
          + ' and (ph.REFDISTRICTS=' + districts.Fields[0].Text
          + ' or d.REFDISTRICTS=' + districts.Fields[0].Text + ')';
        qry.Params.ParamByName('CALCDATE').AsDateTime := CalcDate;
        if j = 5 then
          qry.Params.ParamByName('STARTDATE').AsDateTime := StartOfTheWeek(CalcDate);
        if j in [4, 8, 10, 12] then
          qry.Params.ParamByName('STARTDATE').AsDateTime := StartOfTheYear(CalcDate);
        qry.Open;
        WS.Cells.Item[i, COLS_SQL[j].Row] := qry.Fields[0].AsInteger;
      end;
      if Assigned(Status) then
        Status.Panels[0].Text := '������������ ������...';
      districts.Next;
      inc(i);
    end;
  finally
    qry.Free;
    districts.Free;
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormXRayListReport(const StartDate, EndDate: TDateTime; Trans: TFIBTransaction);
Const
  SQL =
    'SELECT' +
    '  P.NAME,' +
    '  P.BIRTHDAY,' +
    '  P.AMBULATORYCARDNO,' +
    '  P.ADDRESS_REG,' +
    '  D.NAME,' +
    '  X.EXAMINATIONDATE' +
    ' FROM' +
    '  PERSONS P' +
    '  INNER JOIN XRAYSEXAMINATIONS X ON X.REFPERSONS=P.ID' +
    '  AND X.REFXRAYSEXAMTYPES=1' +
    '  INNER JOIN PLACESHISTORY PH ON PH.REFPERSONS=P.ID' +
    '   AND X.EXAMINATIONDATE BETWEEN PH.STARTDATE' +
    '    AND COALESCE(PH.ENDDATE,X.EXAMINATIONDATE)' +
    '  INNER JOIN DISTRICTS D ON PH.REFDISTRICTS=D.ID' +
    ' WHERE X.EXAMINATIONDATE BETWEEN :STARTDATE AND :ENDDATE' +
    ' ORDER BY D.NAME,X.EXAMINATIONDATE';
var
  VarData, App, WS: Variant;
  i, rc: integer;
  qry: TpFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\������ ��������� ���.xlt', '\\reports', '\reports', []));
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    qry := TpFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := SQL;
    qry.ParamByName('STARTDATE').asVariant := StartDate;
    qry.ParamByName('ENDDATE').asVariant := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 6], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := qry.Fields[00].asString;
      VarData[i, 02] := qry.Fields[01].asString;
      VarData[i, 03] := qry.Fields[02].asString;
      VarData[i, 04] := qry.Fields[03].asString;
      VarData[i, 05] := qry.Fields[04].asString;
      VarData[i, 06] := qry.Fields[05].asString;
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:F%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:F%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:F%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
  finally
    qry.Free;
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

procedure FormHIVDeathRpt(StartDate, EndDate: TDateTime;
  Trans: TFIBTransaction; Status: TStatusBar = nil);
Const
  SQL =
    'SELECT ' +
    '  P.NAME,' +
    '  P.AMBULATORYCARDNO,' +
    '  COALESCE(P.DATEOFDEATH,H.UNREGISTRYDATE) DEATH_DATE,' +
    '  HO.NAME REASON,' +
    '  (SELECT FIRST 1 LIST(D.DESCR) FROM HIVART HA' +
    '   INNER JOIN HIVARTDRUGS HAD ON HAD.REFHIVART=HA.ID' +
    '   INNER JOIN DRUGS D ON HAD.REFDRUGS=D.ID' +
    '   WHERE HA.REFPERSONS=P.ID' +
    '   AND COALESCE(P.DATEOFDEATH,H.UNREGISTRYDATE) ' +
    '    BETWEEN HA.STARTDATE and' +
    '    COALESCE(HA.ENDDATE,P.DATEOFDEATH,H.UNREGISTRYDATE)' +
    '   GROUP BY HA.ID,HA.STARTDATE' +
    '   ORDER BY HA.STARTDATE DESC) ART' +
    ' FROM' +
    '  PERSONS P' +
    '  INNER JOIN HIV H ON H.REFPERSONS=P.ID' +
    '  INNER JOIN HIVOUTCOMES HO ON H.REFHIVOUTCOMES=HO.ID' +
    ' WHERE' +
    ' COALESCE(P.DATEOFDEATH,H.UNREGISTRYDATE) BETWEEN :STARTDATE AND :ENDDATE' +
    '  AND H.REFHIVOUTCOMES IN (1,2)';
var
  VarData, App, WS: Variant;
  i, rc: integer;
  qry: TFIBDataset;
begin;
  // ���������� ������ ---------------------------------------------------------
  App := InitExcel(StringReplace(ExtractFilePath(Application.ExeName) +
    '\reports\������ �������.xlt', '\\reports',
    '\reports', []));
  if Assigned(Status) then
  begin
    Status.Panels[0].Text := '������ ������...';
    Application.ProcessMessages;
  end;
  try
    WS := App.ActiveWorkbook.Worksheets.Item[1];
    WS.Cells.Item[2, 1].FormulaLocal := '�� ' + GetPeriod(StartDate, EndDate);
    qry := TFIBDataset.Create(nil);
    qry.Transaction := Trans;
    qry.SelectSQL.Text := SQL;
    qry.Params.ParamByName('StartDate').AsDateTime := StartDate;
    qry.Params.ParamByName('EndDate').AsDateTime := EndDate;
    qry.Open;
    qry.FetchAll;
    qry.First;
    rc := qry.RecordCount;
    VarData := VarArrayCreate([1, rc, 1, 6], VarVariant);
    i := 1;
    while not qry.Eof do
    begin
      VarData[i, 01] := i;
      VarData[i, 02] := qry.Fields[00].asString; // ���
      VarData[i, 03] := qry.Fields[01].asString; // ����� �����
      VarData[i, 04] := qry.Fields[02].asString; // ���� ������
      VarData[i, 05] := qry.Fields[03].asString; // ������� ������
      VarData[i, 06] := qry.Fields[04].asString; // ����� �������
      qry.Next;
      inc(i);
    end;
    qry.Close;
    if rc > 0 then
    begin
      if (rc > 2) then
        WS.Range[Format('A%d:F%d', [5, 5])].Resize[rc - 2].EntireRow.Insert;
      WS.Range[Format('A%d:F%d', [4, rc + 3])].Value := VarData;
      WS.Range[Format('A%d:F%d', [4, rc + 3])].Borders.Linestyle :=
        xlContinuous;
    end;
  finally
    qry.Free;
    if Assigned(Status) then
      Status.Panels[0].Text := '';
    App.Visible := true;
    try
    except
      SetForegroundWindow(App.Hwnd);
    end;
  end;
end;

end.
